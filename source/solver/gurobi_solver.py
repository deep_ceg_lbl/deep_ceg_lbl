#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Short description of this Python module.
Longer description of this module.
This program is for: .
"""

__authors__ = ["Qin Tian<qtian.whu@gmail.com>", "Ming Jin<jinming@berkeley.edu>", "Wei Feng<weifeng@lbl.gov>"]
__contact__ = "Wei Feng"
__copyright__ = "Copyright 2018, CHINA ENERGY GROUP@LBNL"
__date__ = "18-2-8"
__deprecated__ = False
__email__ = "weifeng@lbl.gov"
__license__ = "GPLv3"
__maintainer__ = "Qin Tian, Ming Jin"
__status__ = "Production"
__version__ = "2.0.0"

import gurobipy as grb
import scipy.sparse as sps
import numpy as np


def solve(f=None, vlb=None, vub=None, Aineq=None, bineq=None, Aeq=None, beq=None, xint=None,goal=None):

    m = grb.Model()

    if Aeq is None:
        raise Exception("Aeq can not be None.")
    if Aineq is None:
        raise Exception("Aeq can not be None.")
    if beq is None:
        raise Exception("Aeq can not be None.")
    if bineq is None:
        raise Exception("Aeq can not be None.")
    if vlb is None:
        raise Exception("Aeq can not be None.")
    if vub is None:
        raise Exception("Aeq can not be None.")
    if xint is None:
        raise Exception("Aeq can not be None.")
    if f is None:
        raise Exception("Aeq can not be None.")

    if goal is None:
        raise Exception("Aeq can not be None.")

    # Build prob: The model is to minimize the objective
    m.ModelSense = +1

    # ---------------------------add variables -----------------------------------
    # convert integer index from xint to string

    str1 = grb.GRB.CONTINUOUS * len(f)  # 'C'
    list1 = list(str1)
    for i in range(len(xint)):
        list1[xint[i]] = grb.GRB.BINARY  # 'I',integer

    modelVar = []
    for i in range(len(f)):
        modelVar.append(m.addVar(vtype=list1[i], obj=f[i], lb=vlb[i], ub=vub[i]))

    m.update()

    # lbs is a list of all the lower bounds
    # lbs = prob.variables.get_lower_bounds()

    # ub1 is just the first lower bound
    # ub1 = prob.variables.get_upper_bounds()

    # thus, cols is the entire constraint matrix as a list of column vectors
    # only work if variables.add(columns)
    # get_cols(): Returns a set of columns of the linear constraint matrix.
    cols = m.getVars()  # "x1", "x3"
    numcols = len(cols)

    # --------------------------add constraints----------------------------------------------
    # aeq, aineq: sparse matrix

    # this method keeps the original equality and less-than constraints
    if sps.issparse(Aineq) and sps.issparse(Aeq):
        # lin_expr is a list of SparsePair instances

        # find out all non-zero elements of each row in csc sparse matrix Aineq
        # http://stackoverflow.com/questions/24790649/a-fast-way-to-find-nonzero-entries-by-row-in-a-sparse-matrix-in-python
        # Aineq: coo sparse matrix
        # tempAineq: csr sparse matrix
        # ----------------------------add inequality constraints--------------------------------------
        tempAineq = Aineq.tocsr()

        # nzRow = Aineq.row
        # nzCol = Aineq.col
        # nzRow2 = Aineq.tocoo().row
        # nzCol2 = Aineq.tocoo().col

        knzIn = tempAineq.nonzero()

        # nonzero() will return the a sequence of index arrays of non zero elements
        # flatnonzero() returns the non - zero elements of the flattened version of the array.
        # flatknz = np.flatnonzero(tempAineq)
        # flatVal = np.ravel(tempAineq)[np.flatnonzero(tempAineq)]

        # temp1 is the same as knzIn[1]
        temp1 = tempAineq.indices
        temp2 = tempAineq.indptr[1:-1]
        tempInC = np.split(temp1, temp2)

        # nzCol1 is the same as tempInC, but takes longer time in the loop
        # nzCol1=[r.nonzero()[1] for r in tempAineq]
        # nzRow1 = [r.nonzero()[0] for r in tempAineq]

        # be careful: 0 in val, keep only non-zero value
        valIn = tempAineq.data
        val2 = np.split(valIn, temp2)
        # val1 is the same as val2, but takes longer time in the loop
        # val1 = [r.data for r in tempAineq]
        Aineq_indptr = tempAineq.indptr.copy()
        for i in range(Aineq.shape[0]):
            if len(tempInC[i]):
                start = Aineq_indptr[i]
                end = Aineq_indptr[i + 1]
                variables = [modelVar[j] for j in temp1[start:end]]
                coeff = valIn[start:end]
                expr = grb.LinExpr(coeff, variables)
                m.addConstr(lhs=expr, sense=grb.GRB.LESS_EQUAL, rhs=bineq[i])

        m.update()
        # ----------------------add equality constraints---------------------
        tempAeq = Aeq.tocsr()
        knz = tempAeq.nonzero()
        # temp01 is the same as knz[1]
        temp01 = tempAeq.indices
        temp02 = tempAeq.indptr[1:-1]
        tempC = np.split(temp01, temp02)
        val = tempAeq.data
        val02 = np.split(val, temp02)

        Aeq_indptr = tempAeq.indptr.copy()
        for i in range(Aeq.shape[0]):
            if len(tempC[i]):
                start = Aeq_indptr[i]
                end = Aeq_indptr[i + 1]
                variables = [modelVar[j] for j in temp01[start:end]]
                coeff = val[start:end]
                expr = grb.LinExpr(coeff, variables)
                m.addConstr(lhs=expr, sense=grb.GRB.EQUAL, rhs=beq[i])
        m.update()

    else:

        for i in range(Aineq.shape[0]):
            knzIn = Aineq[i, ].nonzero()
            if len(knzIn):
                variables = [modelVar[j] for j in knzIn]
                coeff = Aineq[i, knzIn]
                expr = grb.LinExpr(coeff, variables)
                m.addConstr(lhs=expr, sense=grb.GRB.LESS_EQUAL, rhs=bineq[i])

        m.update()
        # ----------------------add equality constraints---------------------
        for i in range(Aeq.shape[0]):
            knz = Aeq[i,].nonzero()
            if len(knz):
                variables = [modelVar[j] for j in knz]
                coeff = Aeq[i, knz]
                expr = grb.LinExpr(coeff, variables)
                m.addConstr(lhs=expr, sense=grb.GRB.EQUAL, rhs=beq[i])
        m.update()

    # TODO: why??
    m.params.method = 2

    # parameters/algorithm/gurCntr.csv
    # timeLimit,MIPGap,num_threads,memoryLimit
    # 600,0.01,1,0.5
    # TODO: why????

    gurCntr = dict(timeLimit=600,
                   MIPGap=0.01,
                   num_threads=1,
                   memoryLimit=0.5
                   )

    if gurCntr['num_threads']:
        m.setParam(grb.GRB.Param.Threads, gurCntr['num_threads'])  # 12
    if gurCntr['MIPGap']:
        m.setParam(grb.GRB.Param.MIPGap, gurCntr['MIPGap'])  # 1e-4
    if gurCntr['timeLimit']:
        m.params.timeLimit = gurCntr['timeLimit']  # 10*60 # 10minutes time limit
    # m.setParam(grb.GRB.Param.LogFile, "")

    try:
        m.optimize()
    except grb.GurobiError:
        print('Encountered an error')

    # solution.get_status() returns an integer code

    status = m.status
    print("Solution status = ", status)
    # the following line prints the status as a string
    # print(m.status[status])
    if status == grb.GRB.Status.UNBOUNDED:
        print("Model is unbounded")
        return
    elif status == grb.GRB.Status.INFEASIBLE:
        print("Model is infeasible")
        return
    elif status == grb.GRB.Status.INF_OR_UNBD:
        print("Model is infeasible or unbounded")
        return

    optVal = m.objVal
    print("Objective value = ", optVal)

    x = m.getAttr('x', m.getVars())
    return x, optVal