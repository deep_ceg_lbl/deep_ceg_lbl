# -*- coding: utf-8 -*-
#!/usr/bin/env python

""" Short description of this Python module.
Longer description of this module.
This program is for: .
"""

__authors__ = ["Qin Tian<qtian.whu@gmail.com>", "Ming Jin<jinming@berkeley.edu>", "Wei Feng<weifeng@lbl.gov>"]
__contact__ = "Wei Feng"
__copyright__ = "Copyright 2018, CHINA ENERGY GROUP@LBNL"
__date__ = "18-1-22"
__deprecated__ = False
__email__ = "weifeng@lbl.gov"
__license__ = "GPLv3"
__maintainer__ = "Qin Tian, Ming Jing"
__status__ = "Production"
__version__ = "2.0.0"

import pandas as pd

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import scipy as sp
import numpy.matlib as npmtlb
from mpl_toolkits.mplot3d import Axes3D
import pickle
import scipy.io as sio
import os.path as ospath
import inspect
import os


def draw(pipeNetwFlag=None, currentFilePath=None, candidateTech=None, parTime=None, parCHP=None, parHP=None,
         parEleGrid=None,
         valBinInst=None, pipeHeatLoss=None, pipeCoolLoss=None, nonzeroIndEle=None, nonzeroIndHeat=None,
         nonzeroIndCool=None,
         oriNonzeroIndEle=None, oriNonzeroIndHeat=None, oriNonzeroIndCool=None, eleLoad=None, coolLoad=None,
         heatLoad=None,
         valBinInstTypeCHP=None, valBinInstCHP=None, mainPipeHeatLoss=None, mainPipeCoolLoss=None, bldgHeat=None,
         bldgCool=None, parThemPipe=None):
    if pipeNetwFlag is None:
        pipeNetwFlag = 0

    if currentFilePath is None:
        currentFilePath = ospath.dirname(ospath.abspath(inspect.getfile(inspect.currentframe())))

    pathName = ospath.join(currentFilePath, 'Outputs/Operation/')
    if pipeNetwFlag == 1:
        if mainPipeHeatLoss is None:
            mainPipeHeatLoss = np.loadtxt(pathName + 'mainPipeHeatLoss.out', delimiter=',')
            if len(mainPipeHeatLoss.shape) > 1:
                mainPipeHeatLoss = mainPipeHeatLoss.reshape((mainPipeHeatLoss.shape[0] * mainPipeHeatLoss.shape[1],))

        if mainPipeCoolLoss is None:
            mainPipeCoolLoss = np.loadtxt(pathName + 'mainPipeCoolLoss.out', delimiter=',')
            if len(mainPipeCoolLoss.shape) > 1:
                mainPipeCoolLoss = mainPipeCoolLoss.reshape((mainPipeCoolLoss.shape[0] * mainPipeCoolLoss.shape[1],))

        if bldgCool is None:
            bldgCool = np.loadtxt(pathName + 'bldgCool.out', delimiter=',')

        if bldgHeat is None:
            bldgHeat = np.loadtxt(pathName + 'bldgHeat.out', delimiter=',')

    # drawFlag: used to control which figure is going to be drawn
    drawFlag = 0

    tempFile = ospath.join(currentFilePath, 'Fig')
    if not ospath.exists(tempFile):
        os.makedirs(tempFile)

    if candidateTech is None:
        pkl_file = open('candidateTech.pkl', 'rb')
        candidateTech = pickle.load(pkl_file)
        pkl_file.close()

    if parTime is None:
        pkl_file = open('parTime.pkl', 'rb')
        parTime = pickle.load(pkl_file)
        pkl_file.close()

    if valBinInst is None:
        pkl_file = open('valBinInst.pkl', 'rb')
        valBinInst = pickle.load(pkl_file)
        pkl_file.close()

    if parHP is None and candidateTech['HP'] == 1:
        pkl_file = open('parHP.pkl', 'rb')
        parHP = pickle.load(pkl_file)
        pkl_file.close()

    if parCHP is None and candidateTech['CHP'] == 1 and not all(v == 0 for v in valBinInstTypeCHP):
        pkl_file = open('parCHP.pkl', 'rb')
        parCHP = pickle.load(pkl_file)
        pkl_file.close()

    if parThemPipe is None:
        pkl_file = open('parThemPipe.pkl', 'rb')
        parThemPipe = pickle.load(pkl_file)
        pkl_file.close()

    if parEleGrid is None and candidateTech['eleGrid'] == 1:
        pkl_file = open('parEleGrid.pkl', 'rb')
        parEleGrid = pickle.load(pkl_file)
        pkl_file.close()

    if valBinInstTypeCHP is None and candidateTech['CHP'] == 1:
        valBinInstTypeCHP = np.loadtxt(pathName + 'valBinInstTypeCHP.out', delimiter=',')

    if valBinInstCHP is None and candidateTech['CHP'] == 1:
        valBinInstCHP = np.loadtxt(pathName + 'valBinInstCHP.out', delimiter=',')

    if pipeHeatLoss is None:
        pipeHeatLoss = np.loadtxt(pathName + 'pipeHeatLoss.out', delimiter=',')
    if pipeCoolLoss is None:
        pipeCoolLoss = np.loadtxt(pathName + 'pipeCoolLoss.out', delimiter=',')

    if nonzeroIndCool is None:
        nonzeroIndCool = np.loadtxt(pathName + 'nonzeroIndCool.out', delimiter=',')
    if nonzeroIndHeat is None:
        nonzeroIndHeat = np.loadtxt(pathName + 'nonzeroIndHeat.out', delimiter=',')
    if nonzeroIndEle is None:
        nonzeroIndEle = np.loadtxt(pathName + 'nonzeroIndEle.out', delimiter=',')

    if oriNonzeroIndCool is None:
        oriNonzeroIndCool = np.loadtxt(pathName + 'oriNonzeroIndCool.out', delimiter=',')
    if oriNonzeroIndHeat is None:
        oriNonzeroIndHeat = np.loadtxt(pathName + 'oriNonzeroIndHeat.out', delimiter=',')
    if oriNonzeroIndEle is None:
        oriNonzeroIndEle = np.loadtxt(pathName + 'oriNonzeroIndEle.out', delimiter=',')

    # np.savetxt('coolload.out', coolLoad, delimiter=',')
    # coolLoad * 1.15 = coolHP + coolEleChill
    if heatLoad is None:
        heatLoad = np.loadtxt(pathName + 'heatLoad.out', delimiter=',')
        if len(heatLoad.shape) == 0:
            heatLoad = np.array([0])
        # heatLoad = np.asarray(heatLoad)
        # heatLoad = heatLoad.reshape((len(heatLoad),))
        # heatLoad = heatLoad1[103:103+24*7]
    # 1.1 * heatLoad + heatABSChill+ heatSal= np.sum(heatCHP, axis=1)  +heatEleBoil+heatHP

    if eleLoad is None:
        eleLoad = np.loadtxt(pathName + 'eleLoad.out', delimiter=',')
        if len(eleLoad.shape) == 0:
            eleLoad = np.array([0])
        # eleLoad = eleLoad.reshape((len(eleLoad),))
        # eleLoad = eleLoad1[0:24*7]

    if coolLoad is None:
        # coolLoad = np.loadtxt('coolLoad.out', delimiter=',')
        coolLoad = np.loadtxt(pathName + 'coolload.out', delimiter=',')
        if len(coolLoad.shape) == 0:
            coolLoad = np.array([0])
        # coolLoad =coolLoad.reshape((len(coolLoad),))
        # coolLoad = coolLoad1[2773:2773+24*7]
    # 1.15 * coolLoad  + stoCoolTank = fromCoolTank + coolEleChill+coolHP+coolABSChill

    temptime = np.max((len(heatLoad), len(eleLoad), len(coolLoad)))
    timespan = temptime
    x = np.arange(1, temptime + 1)

    if candidateTech['HP'] == 1 and valBinInst['HP'] == 1:
        eleHP = np.loadtxt(pathName + 'eleHP.out', delimiter=',')

        if parHP['heatSta'] == 1 and len(nonzeroIndHeat):
            heatHP = np.loadtxt(pathName + 'heatHP.out', delimiter=',')
        if parHP['coolSta'] == 1 and len(nonzeroIndCool):
            coolHP = np.loadtxt(pathName + 'coolHP.out', delimiter=',')

    if candidateTech['CHP'] == 1 and not all(v == 0 for v in valBinInstTypeCHP):
        heatCHP = np.loadtxt(pathName + 'heatCHP.out', delimiter=',')
        gasCHP = np.loadtxt(pathName + 'gasCHP.out', delimiter=',')
        eleCHP = np.loadtxt(pathName + 'eleCHP.out', delimiter=',')

    if candidateTech['ABSChill'] == 1 and valBinInst['ABSChill'] == 1:
        heatABSChill = np.loadtxt(pathName + 'heatABSChill.out', delimiter=',')
        coolABSChill = np.loadtxt(pathName + 'coolABSChill.out', delimiter=',')

    if candidateTech['heatInds'] == 1:
        heatInds = np.loadtxt(pathName + 'heatInds.out', delimiter=',')

    if candidateTech['heatSal'] == 1:
        heatSal = np.loadtxt(pathName + 'heatSal.out', delimiter=',')

    # electric boiler
    if candidateTech['eleBoil'] == 1 and np.allclose(valBinInst['eleBoil'], 1):
        heatEleBoil = np.loadtxt(pathName + 'heatEleBoil.out', delimiter=',')
        eleEleBoil = np.loadtxt(pathName + 'eleEleBoil.out', delimiter=',')

    if candidateTech['eleChill'] == 1 and valBinInst['eleChill'] == 1:
        coolEleChill = np.loadtxt(pathName + 'coolEleChill.out', delimiter=',')
        eleEleChill = np.loadtxt(pathName + 'eleEleChill.out', delimiter=',')

    if candidateTech['PV'] == 1 and valBinInst['PV'] == 1:
        elePV = np.loadtxt(pathName + 'elePV.out', delimiter=',')

    if candidateTech['solThem'] == 1 and valBinInst['solThem'] == 1:
        heatSolThem = np.loadtxt(pathName + 'heatSolThem.out', delimiter=',')

    if candidateTech['NGBoil'] == 1 and valBinInst['NGBoil'] == 1:
        gasNGBoil = np.loadtxt(pathName + 'gasNGBoil.out', delimiter=',')
        heatNGBoil = np.loadtxt(pathName + 'heatNGBoil.out', delimiter=',')

    if candidateTech['NGChill'] == 1 and valBinInst['NGChill'] == 1:
        gasNGChill = np.loadtxt(pathName + 'gasNGChill.out', delimiter=',')
        coolNGChill = np.loadtxt(pathName + 'coolNGChill.out', delimiter=',')

    if candidateTech['eleBat'] == 1 and valBinInst['eleBat'] == 1:
        totEleBat = np.loadtxt(pathName + 'totEleBat.out', delimiter=',')
        inEleBat = np.loadtxt(pathName + 'inEleBat.out', delimiter=',')

        chgEleBat = np.loadtxt(pathName + 'chgEleBat.out',
                               delimiter=',')  # .reshape((parTime['numMonth'], parTime['numDay'], parTime['numHour']))
        disEleBat = np.loadtxt(pathName + 'disEleBat.out',
                               delimiter=',')  # .reshape((parTime['numMonth'], parTime['numDay'], parTime['numHour']))

        outEleBat = np.loadtxt(pathName + 'outEleBat.out', delimiter=',')
        socEleBat = np.loadtxt(pathName + 'socEleBat.out', delimiter=',')
        fromEleBat = np.loadtxt(pathName + 'fromEleBat.out', delimiter=',')
        stoEleBat = np.loadtxt(pathName + 'stoEleBat.out', delimiter=',')

    if candidateTech['heatTank'] == 1 and valBinInst['heatTank'] == 1:
        totHeatTank = np.loadtxt(pathName + 'totHeatTank.out', delimiter=',')
        inHeatTank = np.loadtxt(pathName + 'inHeatTank.out', delimiter=',')

        chgHeatTank = np.loadtxt(pathName + 'chgHeatTank.out',
                                 delimiter=',')  # .reshape((parTime['numMonth'], parTime['numDay'], parTime['numHour']))
        disHeatTank = np.loadtxt(pathName + 'disHeatTank.out',
                                 delimiter=',')  # .reshape((parTime['numMonth'], parTime['numDay'], parTime['numHour']))

        outHeatTank = np.loadtxt(pathName + 'outHeatTank.out', delimiter=',')
        socHeatTank = np.loadtxt(pathName + 'socHeatTank.out', delimiter=',')
        fromHeatTank = np.loadtxt(pathName + 'fromHeatTank.out', delimiter=',')
        stoHeatTank = np.loadtxt(pathName + 'stoHeatTank.out', delimiter=',')

    if candidateTech['coolTank'] == 1 and valBinInst['coolTank'] == 1:
        totCoolTank = np.loadtxt(pathName + 'totCoolTank.out', delimiter=',')
        inCoolTank = np.loadtxt(pathName + 'inCoolTank.out', delimiter=',')

        chgCoolTank = np.loadtxt(pathName + 'chgCoolTank.out',
                                 delimiter=',')  # .reshape((parTime['numMonth'], parTime['numDay'], parTime['numHour']))
        disCoolTank = np.loadtxt(pathName + 'disCoolTank.out',
                                 delimiter=',')  # .reshape((parTime['numMonth'], parTime['numDay'], parTime['numHour']))

        outCoolTank = np.loadtxt(pathName + 'outCoolTank.out', delimiter=',')
        socCoolTank = np.loadtxt(pathName + 'socCoolTank.out', delimiter=',')
        fromCoolTank = np.loadtxt(pathName + 'fromCoolTank.out', delimiter=',')
        stoCoolTank = np.loadtxt(pathName + 'stoCoolTank.out', delimiter=',')

    # purEleGrid +np.sum(eleCHP, axis=1)= eleEleChill + eleHP + eleLoad + eleEleBoil  +salEleGrid

    # eleCHP = np.loadtxt('eleCHP.out', delimiter=',')
    # eleEleChill = np.loadtxt('eleEleChill.out')
    # eleHP = np.loadtxt('eleHP.out')
    # eleEleBoil  = np.loadtxt('eleEleBoil.out', delimiter=',')
    # eleEleChill = np.loadtxt('eleEleChill.out')
    if candidateTech['eleGrid'] == 1 and len(nonzeroIndEle):
        salEleGrid = np.loadtxt(pathName + 'salEleGrid.out', delimiter=',')
        purEleGrid = np.loadtxt(pathName + 'purEleGrid.out', delimiter=',')

        if parEleGrid is None:
            dayPurPriceEleGrid = np.asarray(
                [0.0634, 0.0634, 0.0634, 0.0634, 0.0634, 0.0634, 0.0634, 0.1338, 0.2075, 0.2075, 0.2075, 0.1338, 0.1338,
                 0.1338,
                 0.1338, 0.1338, 0.1338, 0.1338, 0.2075, 0.2075, 0.2075, 0.2075, 0.2075, 0.0634]) * 1

            purPriceEleGrid = npmtlb.repmat(dayPurPriceEleGrid[0: parTime['numHour']], 1,
                                            parTime['numMonth'] * parTime['numDay'])

            if len(np.shape(purPriceEleGrid)) > 1:
                purPriceEleGrid = purPriceEleGrid.reshape((parTime['totHrStep'],))
            # purPriceEleGrid = np.squeeze(purPriceEleGrid)
        else:
            dayPurPriceEleGrid = np.loadtxt(pathName + 'dayPurPriceEleGrid.out', delimiter=',')
            purPriceEleGrid = parEleGrid['purPriceEleGrid']

    # ---------------------energy demand--------------------------
    if len(nonzeroIndHeat):
        line_up, = plt.plot(np.arange(1, len(heatLoad) + 1), np.negative(heatLoad / 1000.0), 'r-', linewidth=2.0,
                            label="heating demand")  # [0:len(x)]
    if len(nonzeroIndEle):
        line_down, = plt.plot(np.arange(1, len(eleLoad) + 1), eleLoad / 1000.0, 'g--', linewidth=2.0,
                              label="electricity demand")
    if len(nonzeroIndCool):
        line_mid, = plt.plot(np.arange(1, len(coolLoad) + 1), coolLoad / 1000.0, 'b-o', label="cooling demand")

    # plt.legend(handles=[line_up, line_down])
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
               ncol=2, mode="expand", borderaxespad=0.)
    # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

    plt.grid(True)
    plt.xlabel("hour")
    plt.ylabel("energy(MW)")
    # plt.title('energy demand')

    # plt.savefig("energy demand.png")
    figFile = ospath.join(currentFilePath, 'Fig/energy demand.png')
    plt.savefig(figFile, bbox_inches='tight')
    plt.show()
    plt.close()
    # =============================electric demand===============================
    if len(nonzeroIndEle):
        line_up, = plt.plot(np.arange(1, len(eleLoad) + 1), eleLoad / 1000.0, 'violet', linewidth=2.0,
                            label="electricity demand(MW)")
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        plt.grid(True)
        plt.xlabel("hour")
        plt.ylabel("energy(MW)")

        # plt.savefig("electric demand.png")
        figFile = ospath.join(currentFilePath, 'Fig/electric demand.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()

    # ===========================heat demand ===================
    if len(nonzeroIndHeat):
        line_up, = plt.plot(np.arange(1, len(heatLoad) + 1), heatLoad[:len(x)] / 1000.0, 'violet', linewidth=2.0,
                            label="heating demand(MW)")
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)

        # plt.savefig("heat demand.png")
        figFile = ospath.join(currentFilePath, 'Fig/heat demand.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
    # ========================electricity tariff=============================
    if candidateTech['eleGrid'] == 1 and len(nonzeroIndEle):
        x1 = np.arange(1, len(dayPurPriceEleGrid) + 1)
        line_up, = plt.step(x1, dayPurPriceEleGrid, 'r-o', linewidth=2.0, label='electricity tariff($/kWh)')
        plt.xlabel("hour")
        plt.ylabel('electricity tariff($/kWh)')
        plt.grid(True)
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)

        figFile = ospath.join(currentFilePath, 'Fig/electricity tariff.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()

    # --------------------------------original load index----------------------------------------------
    yyeleload = np.zeros(len(x))
    yyeleload[map(int, oriNonzeroIndEle)] = 1
    yyheatload = np.zeros(len(x))
    yyheatload[map(int, oriNonzeroIndHeat)] = 1
    yycoolload = np.zeros(len(x))
    yycoolload[map(int, oriNonzeroIndCool)] = 1

    plt.figure(1)
    plt.subplot(311)
    # plt.plot(x, eleLoad, 'r--', x, coolLoad, 'b-', x, coolLoad, 'g-')
    # plt.plot(x, socCoolTank, 'r--')
    line_up, = plt.step(np.arange(1, len(yyeleload) + 1), yyeleload, 'r-', linewidth=2.0,
                        label='original non-zero electricity load')
    plt.axis([0, len(x), 0, 1.1])
    plt.xlabel('hour')
    plt.ylabel('electricity period')
    plt.grid(True)

    plt.subplot(312)
    line_bottom, = plt.step(x, yyheatload, 'b--', linewidth=2.0, label='original non-zero heating load')
    plt.axis([0, len(x), 0, 1.1])
    # plt.setp(lines,label = 'soc')
    # plt.setp(lines,label = {'soc', 'max soc','min soc'})
    # plt.setp(lines, color='r', linewidth=2.0)
    plt.xlabel('hour')
    plt.ylabel('heating period')
    plt.grid(True)

    plt.subplot(313)
    line_middle, = plt.step(x, yycoolload, 'g-.', linewidth=2.0, label='original non-zero cooling load')
    plt.axis([0, len(x), 0.0, 1.1])
    # plt.setp(lines,label = 'soc')
    # plt.setp(lines,label = {'soc', 'max soc','min soc'})
    # plt.setp(lines, color='r', linewidth=2.0)
    plt.xlabel('hour')
    plt.ylabel('cooling period')
    plt.grid(True)

    figFile = ospath.join(currentFilePath, 'Fig/pre non zero index.png')
    plt.savefig(figFile, bbox_inches='tight')
    plt.show()
    plt.close()
    # --------------------------------load index----------------------------------------------
    yeleload = np.zeros(len(x))
    yeleload[map(int, nonzeroIndEle)] = 1
    yheatload = np.zeros(len(x))
    yheatload[map(int, nonzeroIndHeat)] = 1
    ycoolload = np.zeros(len(x))
    ycoolload[map(int, nonzeroIndCool)] = 1

    plt.figure(1)
    plt.subplot(311)
    # plt.plot(x, eleLoad, 'r--', x, coolLoad, 'b-', x, coolLoad, 'g-')
    # plt.plot(x, socCoolTank, 'r--')
    line_up, = plt.step(x, yeleload, 'r-', linewidth=2.0, label='processed')
    plt.axis([0, len(x), 0, 1.1])
    plt.xlabel('hour')
    plt.ylabel('electricity period')
    plt.grid(True)

    plt.subplot(312)
    line_bottom, = plt.step(x, yheatload, 'b--', linewidth=2.0, label='processed')  # non-zero heating load
    plt.axis([0, len(x), 0.0, 1.1])
    # plt.setp(lines,label = 'soc')
    # plt.setp(lines,label = {'soc', 'max soc','min soc'})
    # plt.setp(lines, color='r', linewidth=2.0)
    plt.xlabel('hour')
    plt.ylabel('heating period')
    plt.grid(True)

    plt.subplot(313)
    line_middle, = plt.step(x, ycoolload, 'g-.', linewidth=2.0, label='processed')  # non-zero cooling load
    plt.axis([0, len(x), 0.0, 1.1])
    # plt.setp(lines,label = 'soc')
    # plt.setp(lines,label = {'soc', 'max soc','min soc'})
    # plt.setp(lines, color='r', linewidth=2.0)
    plt.xlabel('hour')
    plt.ylabel('cooling period')
    plt.grid(True)

    figFile = ospath.join(currentFilePath, 'Fig/processed non zero index.png')
    plt.savefig(figFile, bbox_inches='tight')
    plt.show()
    plt.close()
    # -------------------------load index comparison--------------------------------------
    plt.figure(1)
    plt.subplot(311)
    # plt.plot(x, eleLoad, 'r--', x, coolLoad, 'b-', x, coolLoad, 'g-')
    # plt.plot(x, socCoolTank, 'r--')
    line_up, = plt.step(x, yeleload, 'r-', linewidth=2.0, label='processed')
    line_up1, = plt.step(x, np.negative(yyeleload), 'b--', linewidth=2.0, label='pre')
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
               ncol=2, mode="expand", borderaxespad=0.)
    plt.axis([0, len(x), -1.1, 1.1])
    plt.xlabel('hour')
    plt.ylabel('electricity period')
    plt.grid(True)

    plt.subplot(312)
    line_bottom, = plt.step(x, yheatload, 'y-', linewidth=2.0, label='processed')
    line_bottom1, = plt.step(x, np.negative(yyheatload), 'b--', linewidth=2.0, label='pre')
    plt.axis([0, len(x), -1.1, 1.1])
    # plt.setp(lines,label = 'soc')
    # plt.setp(lines,label = {'soc', 'max soc','min soc'})
    # plt.setp(lines, color='r', linewidth=2.0)
    plt.xlabel('hour')
    plt.ylabel('heating period')
    plt.grid(True)

    plt.subplot(313)
    line_middle, = plt.step(x, ycoolload, 'r-', linewidth=2.0, label='processed')
    line_middle1, = plt.step(x, np.negative(yycoolload), 'g--', linewidth=2.0, label='pre')
    plt.axis([0, len(x), -1.1, 1.1])
    # plt.setp(lines,label = 'soc')
    # plt.setp(lines,label = {'soc', 'max soc','min soc'})
    # plt.setp(lines, color='r', linewidth=2.0)
    plt.xlabel('hour')
    plt.ylabel('cooling period')
    plt.grid(True)

    figFile = ospath.join(currentFilePath, 'Fig/pre and processed nonzero index.png')
    plt.savefig(figFile, bbox_inches='tight')
    plt.show()
    plt.close()

    # ========================soc, cooling tank=============================
    if candidateTech['coolTank'] == 1 and valBinInst['coolTank'] == 1:
        plt.figure(1)
        plt.subplot(211)
        # plt.plot(x, eleLoad, 'r--', x, coolLoad, 'b-', x, coolLoad, 'g-')
        # plt.plot(x, socCoolTank, 'r--')
        line_up, = plt.step(x, socCoolTank, 'r-', linewidth=2.0, label='soc')
        plt.xlabel('hour')
        plt.ylabel('soc')
        plt.grid(True)

        plt.subplot(212)
        line_up, = plt.step(x, purPriceEleGrid, 'b--', linewidth=2.0, label='electricity tariff($/kWh)')
        # plt.axis([0, 180, 0.05, 0.21])
        # plt.setp(lines,label = 'soc')
        # plt.setp(lines,label = {'soc', 'max soc','min soc'})
        # plt.setp(lines, color='r', linewidth=2.0)
        plt.xlabel('hour')
        plt.ylabel('electricity tariff($/kWh)')
        plt.grid(True)

        figFile = ospath.join(currentFilePath, 'Fig/cool tank_SOC,electricity tariff.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # --------------------soc_ limit-----------------------
        # x=np.arange(1,len(coolLoad)+1)

        lines = plt.plot(x, socCoolTank, 'r--',
                         x, np.ones(temptime), 'b-',
                         x, np.ones(temptime) * 0.1, 'g-.')
        # plt.plot(x, eleLoad, 'r--', x, coolLoad, 'b-', x, coolLoad, 'g-')
        # plt.plot(x, socCoolTank, 'r--')
        # plt.axis([0, 180, 0.4, 1.1])
        # plt.setp(lines,legend = 'soc')
        # plt.setp(lines,label = {'soc', 'max soc','min soc'})
        # plt.setp(lines, color='r', linewidth=2.0)
        plt.xlabel('hour')
        plt.ylabel('soc')
        plt.grid(True)

        # plt.savefig("cool tank_SOC.png")
        figFile = ospath.join(currentFilePath, 'Fig/cool tank_SOC.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ==================charge/discharge status 0/1============================
        plt.figure(1)
        plt.subplot(311)
        line_up, = plt.step(x, chgCoolTank, 'b-', linewidth=2.0, label='charging')
        # plt.axis([-0.1, 180, 0, 1.1])
        # plt.legend(handles=[line_up], loc=4)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
        #          ncol=2, mode="expand", borderaxespad=0.)
        plt.grid(True)

        plt.subplot(312)
        line_up, = plt.step(x, disCoolTank, 'r--', linewidth=2.0, label='discharging')
        # plt.axis([-0.1, 180, 0, 1.1])
        # plt.legend(handles=[line_up], loc=4)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
        #          ncol=2, mode="expand", borderaxespad=0.)
        plt.grid(True)

        plt.subplot(313)
        line_up, = plt.plot(x, socCoolTank, 'g-.', linewidth=2.0, label='soc')
        # plt.legend(handles=[line_up], loc=4)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
        #          ncol=2, mode="expand", borderaxespad=0.)
        plt.grid(True)

        # plt.savefig("cool tank_charge,discharge,SOC.png")
        figFile = ospath.join(currentFilePath, 'Fig/cool tank_charge,discharge,SOC.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ==================charge/discharge rate============================
        plt.figure(1)
        plt.subplot(211)
        line_up, = plt.plot(x, inCoolTank - outCoolTank, 'b-', linewidth=2.0, label='charge rate')
        # plt.legend(handles=[line_up], loc=4)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        # plt.legend([line_up, line_down], ['Line Up', 'Line Down'])
        plt.grid(True)

        plt.subplot(212)
        line_up, = plt.plot(x, socCoolTank, 'g--', linewidth=2.0, label='soc')
        # plt.legend(handles=[line_up], loc=4)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        # plt.savefig("cool tank_energy increase, SOC.png")
        figFile = ospath.join(currentFilePath, 'Fig/cool tank_energy increase, SOC.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ==================total stored energy for cooling storage============================
        plt.figure(1)
        plt.subplot(211)
        line_up, = plt.plot(x, totCoolTank, 'b-', linewidth=2.0, label='total energy')
        # plt.legend(handles=[line_up], loc=4)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        plt.subplot(212)
        line_up, = plt.plot(x, inCoolTank - outCoolTank, 'k--', linewidth=2.0, label='charging rate')
        # plt.legend(handles=[line_up], loc=4)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        # plt.savefig("cool tank_total and increased energy.png")
        figFile = ospath.join(currentFilePath, 'Fig/cool tank_total and increased energy.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ==================cooling storage============================
        plt.figure(1)
        plt.subplot(411)
        line_up, = plt.plot(x, totCoolTank, 'b-', linewidth=2.0, label='total energy')
        # plt.legend(handles=[line_up], loc=4)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        plt.subplot(412)
        line_up, = plt.plot(x, inCoolTank, 'k--', linewidth=2.0, label='charging rate')
        # plt.legend(handles=[line_up], loc=1)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        plt.subplot(413)
        line_up, = plt.plot(x, outCoolTank, 'r-.', linewidth=2.0, label='discharging rate')
        # plt.legend(handles=[line_up], loc=1)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        plt.subplot(414)
        line_up, = plt.plot(x, socCoolTank, 'g:', linewidth=2.0, label='soc')
        # plt.legend(handles=[line_up], loc=4)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        # plt.savefig("cool tank energy_total,In, Out, SOC.png")
        figFile = ospath.join(currentFilePath, 'Fig/cool tank energy_total,In, Out, SOC.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
    # ========================soc, heat tank=============================
    if candidateTech['heatTank'] == 1 and valBinInst['heatTank'] == 1:
        plt.figure(1)
        plt.subplot(211)
        # plt.plot(x, eleLoad, 'r--', x, coolLoad, 'b-', x, coolLoad, 'g-')
        # plt.plot(x, socCoolTank, 'r--')
        line_up, = plt.step(x, socHeatTank, 'r-', linewidth=2.0, label='soc')
        plt.xlabel('hour')
        plt.ylabel('soc')
        plt.grid(True)

        plt.subplot(212)
        line_up, = plt.step(x, purPriceEleGrid, 'b--', linewidth=2.0, label='electricity tariff($/kWh)')
        # plt.axis([0, 180, 0.05, 0.21])
        # plt.setp(lines,label = 'soc')
        # plt.setp(lines,label = {'soc', 'max soc','min soc'})
        # plt.setp(lines, color='r', linewidth=2.0)
        plt.xlabel('hour')
        plt.ylabel('electricity tariff($/kWh)')
        plt.grid(True)

        # plt.savefig("heat tank_SOC,electricity tariff.png")
        figFile = ospath.join(currentFilePath, 'Fig/heat tank_SOC,electricity tariff.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # --------------------------------------------------
        lines = plt.plot(x, socHeatTank, 'r--',
                         x, np.ones(temptime), 'b-',
                         )
        # plt.plot(x, eleLoad, 'r--', x, coolLoad, 'b-', x, coolLoad, 'g-')
        # plt.plot(x, socCoolTank, 'r--')
        # plt.axis([0, 180, 0.0, 1.01])
        # plt.setp(lines,label = 'soc')
        # plt.setp(lines,label = {'soc', 'max soc','min soc'})
        # plt.setp(lines, color='r', linewidth=2.0)
        plt.xlabel('hour')
        plt.ylabel('soc_Heat Tank')
        plt.grid(True)

        # plt.savefig("heat tank_SOC.png")
        figFile = ospath.join(currentFilePath, 'Fig/heat tank_SOC.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # --------------------soc_ limit-----------------------
        # x=np.arange(1,len(coolLoad)+1)

        lines = plt.plot(x, socHeatTank, 'r--',
                         x, np.ones(temptime), 'b-',
                         x, np.ones(temptime) * 0.1, 'g-.')
        # plt.plot(x, eleLoad, 'r--', x, coolLoad, 'b-', x, coolLoad, 'g-')
        # plt.plot(x, socCoolTank, 'r--')
        # plt.axis([0, 180, np.min(socHeatTank), 1.1])
        # plt.setp(lines,legend = 'soc')
        # plt.setp(lines,label = {'soc', 'max soc','min soc'})
        # plt.setp(lines, color='r', linewidth=2.0)
        plt.xlabel('hour')
        plt.ylabel('soc_Heat Tank')
        plt.grid(True)

        # plt.savefig("heat tank_SOC limit.png")
        figFile = ospath.join(currentFilePath, 'Fig/heat tank_SOC limit.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ==================charge/discharge status 0/1============================
        plt.figure(1)
        plt.subplot(311)
        line_up, = plt.step(x, chgHeatTank, 'b-', linewidth=2.0)  # ,label='charging'
        plt.axis([-0.1, len(x), 0, 1.1])
        plt.xlabel('hour')
        plt.ylabel('charge status')
        # plt.legend(handles=[line_up], loc=4)
        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        plt.grid(True)

        plt.subplot(312)
        line_up, = plt.step(x, disHeatTank, 'r--', linewidth=2.0)  # ,label='discharging'
        plt.axis([-0.1, len(x), 0, 1.1])
        plt.xlabel('hour')
        plt.ylabel('discharge status')
        # plt.legend(handles=[line_up], loc=4)
        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        plt.grid(True)

        plt.subplot(313)
        line_up, = plt.plot(x, socHeatTank, 'g-', linewidth=2.0)  # ,label='soc_Heat Tank'
        plt.axis([-0.1, len(x), 0, 1.1])
        plt.xlabel('hour')
        plt.ylabel('soc')
        # plt.legend(handles=[line_up], loc=4)
        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        plt.grid(True)

        # plt.savefig("heat tank_charge, discharge,SOC.png")
        figFile = ospath.join(currentFilePath, 'Fig/heat tank_charge, discharge,SOC.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ==================charge/discharge rate============================
        plt.figure(1)
        plt.subplot(211)
        line_up, = plt.plot(x, (inHeatTank - outHeatTank) / 1000.0, 'b-', linewidth=2.0)  # ,label='charge rate'
        # plt.legend(handles=[line_up], loc=4)
        plt.xlabel('hour')
        plt.ylabel('net charge(MW)')
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        # plt.legend([line_up, line_down], ['Line Up', 'Line Down'])
        plt.grid(True)

        plt.subplot(212)
        line_up, = plt.plot(x, socHeatTank, 'g-', linewidth=2.0)  # ,label='soc_Heat Tank'
        plt.xlabel('hour')
        plt.ylabel('soc')
        # plt.legend(handles=[line_up], loc=4)
        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        plt.grid(True)

        # plt.savefig("heat tank_energy increase, SOC.png")
        figFile = ospath.join(currentFilePath, 'Fig/heat tank_energy increase, SOC.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ==================total stored energy for heat storage============================
        plt.figure(1)
        plt.subplot(211)
        line_up, = plt.plot(x, totHeatTank / 1000.0, 'b-', linewidth=2.0)  # ,label='total heating energy'
        # plt.legend(handles=[line_up], loc=4)
        plt.xlabel('hour')
        plt.ylabel('total energy(MWh)')
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        plt.subplot(212)
        line_up, = plt.plot(x, (inHeatTank - outHeatTank) / 1000.0, 'k-', linewidth=2.0)  # ,label='charging rate'
        # plt.legend(handles=[line_up], loc=4)
        plt.xlabel('hour')
        plt.ylabel('net charge(MW)')
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        # plt.savefig("heat tank energy_total, increase.png")
        figFile = ospath.join(currentFilePath, 'Fig/heat tank energy_total, increase.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ==================heat storage============================
        plt.figure(1)
        plt.subplot(411)
        line_up, = plt.plot(x, totHeatTank / 1000.0, 'b-', linewidth=2.0)  # ,label='total heating energy'
        # plt.legend(handles=[line_up], loc=4)
        plt.xlabel('hour')
        plt.ylabel('total(MWh)')
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        plt.subplot(412)
        line_up, = plt.plot(x, inHeatTank / 1000.0, 'k-', linewidth=2.0)  # ,label='charging rate'
        # plt.legend(handles=[line_up], loc=1)
        plt.xlabel('hour')
        plt.ylabel('charge(MW))')
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        plt.subplot(413)
        line_up, = plt.plot(x, outHeatTank / 1000.0, 'r-', linewidth=2.0)  # ,label='discharging rate'
        # plt.legend(handles=[line_up], loc=1)
        plt.xlabel('hour')
        plt.ylabel('discharge(MW)')
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        plt.subplot(414)
        line_up, = plt.plot(x, socHeatTank, 'g-', linewidth=2.0)  # ,label='soc_Heat Tank'
        # plt.legend(handles=[line_up], loc=4)
        plt.xlabel('hour')
        plt.ylabel('soc')
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        # plt.savefig("heat tank energy_total, In, Out, SOC.png")
        figFile = ospath.join(currentFilePath, 'Fig/heat tank energy_total, In, Out, SOC.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()

    # ========================soc, electric battery=============================
    if candidateTech['eleBat'] == 1 and valBinInst['eleBat'] == 1:
        plt.figure(1)
        plt.subplot(211)
        # plt.plot(x, eleLoad, 'r--', x, coolLoad, 'b-', x, coolLoad, 'g-')
        # plt.plot(x, socCoolTank, 'r--')
        line_up, = plt.step(x, socEleBat, 'r-', linewidth=2.0, label='soc')
        plt.xlabel('hour')
        plt.ylabel('soc')
        plt.grid(True)

        plt.subplot(212)
        if candidateTech['eleGrid'] == 1:
            line_up, = plt.step(x, purPriceEleGrid, 'b--', linewidth=2.0, label='electricity tariff($/kWh)')
        # plt.axis([0, len(x), 0.05, 0.21])
        # plt.setp(lines,label = 'soc')
        # plt.setp(lines,label = {'soc', 'max soc','min soc'})
        # plt.setp(lines, color='r', linewidth=2.0)
        plt.xlabel('hour')
        plt.ylabel('electricity tariff($/kWh)')
        plt.grid(True)

        # plt.savefig("electric battery_SOC,electricity tariff.png")
        figFile = ospath.join(currentFilePath, 'Fig/electric battery_SOC,electricity tariff.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # --------------------------------------------------
        lines = plt.plot(x, socEleBat, 'r--',
                         x, np.ones(temptime), 'b-',
                         )
        # plt.plot(x, eleLoad, 'r--', x, coolLoad, 'b-', x, coolLoad, 'g-')
        # plt.plot(x, socCoolTank, 'r--')
        # plt.axis([0, 180, 0.18, 0.8])
        # plt.setp(lines,label = 'soc')
        # plt.setp(lines,label = {'soc', 'max soc','min soc'})
        # plt.setp(lines, color='r', linewidth=2.0)
        plt.xlabel('hour')
        plt.ylabel('soc')
        plt.grid(True)

        # plt.savefig("electric battery_SOC.png")
        figFile = ospath.join(currentFilePath, 'Fig/electric battery_SOC.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # --------------------soc_ limit-----------------------
        # x=np.arange(1,len(coolLoad)+1)

        lines = plt.plot(x, socEleBat, 'r--',
                         x, np.ones(temptime), 'b-',
                         x, np.ones(temptime) * 0.1, 'g-.')
        # plt.plot(x, eleLoad, 'r--', x, coolLoad, 'b-', x, coolLoad, 'g-')
        # plt.plot(x, socCoolTank, 'r--')
        # plt.axis([0, len(x), np.min(socEleBat), 1.0])
        # plt.setp(lines,legend = 'soc')
        # plt.setp(lines,label = {'soc', 'max soc','min soc'})
        # plt.setp(lines, color='r', linewidth=2.0)
        plt.xlabel('hour')
        plt.ylabel('soc')
        plt.grid(True)

        # plt.savefig("electric battery_SOC limit.png")
        figFile = ospath.join(currentFilePath, 'Fig/electric battery_SOC limit.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ==================charge/discharge status 0/1============================
        plt.figure(1)
        plt.subplot(311)
        line_up, = plt.step(x, chgEleBat, 'b-', linewidth=2.0, label='charge')
        plt.axis([-0.1, len(x), 0, 1.1])
        # plt.legend(handles=[line_up], loc=4)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
        #          ncol=2, mode="expand", borderaxespad=0.)
        plt.grid(True)

        plt.subplot(312)
        line_up, = plt.step(x, disEleBat, 'r--', linewidth=2.0, label='discharge')
        plt.axis([-0.1, len(x), 0, 1.1])
        # plt.legend(handles=[line_up], loc=4)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
        #          ncol=2, mode="expand", borderaxespad=0.)
        plt.grid(True)

        plt.subplot(313)
        line_up, = plt.plot(x, socEleBat, 'g-.', linewidth=2.0, label='soc')
        # plt.legend(handles=[line_up], loc=4)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
        #          ncol=2, mode="expand", borderaxespad=0.)
        plt.grid(True)

        # plt.savefig("electric battery_charge, discharge,SOC.png")
        figFile = ospath.join(currentFilePath, 'Fig/electric battery_charge, discharge,SOC.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ==================charge/discharge rate============================
        plt.figure(1)
        plt.subplot(211)
        line_up, = plt.plot(x, inEleBat - outEleBat, 'b-', linewidth=2.0, label='charge rate')
        # plt.legend(handles=[line_up], loc=4)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        # plt.legend([line_up, line_down], ['Line Up', 'Line Down'])
        plt.grid(True)

        plt.subplot(212)
        line_up, = plt.plot(x, socEleBat, 'g--', linewidth=2.0, label='soc')
        # plt.legend(handles=[line_up], loc=4)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        # plt.savefig("electric battery_energy increase,SOC.png")
        figFile = ospath.join(currentFilePath, 'Fig/electric battery_energy increase,SOC.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ==================total stored energy for cooling storage============================
        plt.figure(1)
        plt.subplot(211)
        line_up, = plt.plot(x, totEleBat, 'b-', linewidth=2.0, label='total energy')
        # plt.legend(handles=[line_up], loc=4)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        plt.subplot(212)
        line_up, = plt.plot(x, inEleBat - outEleBat, 'k--', linewidth=2.0, label='charging rate')
        # plt.legend(handles=[line_up], loc=4)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        # plt.savefig("electric battery_total and increased energy.png")
        figFile = ospath.join(currentFilePath, 'Fig/electric battery_total and increased energy.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ==================electric storage============================
        plt.figure(1)
        plt.subplot(411)
        line_up, = plt.plot(x, totEleBat, 'b-', linewidth=2.0, label='total energy')
        # plt.legend(handles=[line_up], loc=4)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        plt.subplot(412)
        line_up, = plt.plot(x, inEleBat, 'k--', linewidth=2.0, label='charging rate')
        # plt.legend(handles=[line_up], loc=1)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        plt.subplot(413)
        line_up, = plt.plot(x, outEleBat, 'r-.', linewidth=2.0, label='discharging rate')
        # plt.legend(handles=[line_up], loc=1)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        plt.subplot(414)
        line_up, = plt.plot(x, socEleBat, 'g:', linewidth=2.0, label='soc')
        # plt.legend(handles=[line_up], loc=4)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        # plt.savefig("electric battery energy_total,In,Out,SOC.png")
        figFile = ospath.join(currentFilePath, 'Fig/electric battery energy_total,In,Out,SOC.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()

        # -----------------------electric balance_storage---------------------------
        line_mid1, = plt.plot(x, fromEleBat, 'g-', linewidth=2.0, label="from storage")
        line_bot, = plt.plot(x, np.negative(stoEleBat), 'b--', linewidth=2.0, label="for storage")
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        plt.xlabel("hour")
        plt.ylabel("electric energy(kW)")
        # plt.title('electricity in WuXi Eco-city')

        # plt.savefig("electric battery_net from, gross to.png")
        figFile = ospath.join(currentFilePath, 'Fig/electric battery_net from, gross to.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()

    # =====================================================
    if candidateTech['CHP'] == 1 and not all(v == 0 for v in valBinInstTypeCHP):
        # ----------------------ele_CHP(stack)---------------------
        # stacked area
        # http://matplotlib.org/examples/pylab_examples/stackplot_demo.html
        fig, ax = plt.subplots()
        rects = ax.stackplot(np.arange(1, len(eleCHP) + 1), (eleCHP.T) / 1000.0)
        # ax.legend((rects[0], rects[1], rects[2], rects[3]),('CHP_28MW', 'CHP_32MW', 'CHP_39MW', 'CHP_25MW'))  #
        ax.set_xlabel('hours')
        ax.set_ylabel('electric energy(MW)')
        ax.set_title('electricity from CHP')

        # plt.savefig("CHP_electricity stack_A.png")
        figFile = ospath.join(currentFilePath, 'Fig/CHP_electricity stack_A.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ----------------------heat_CHP(stack)---------------------
        # stacked area
        # http://matplotlib.org/examples/pylab_examples/stackplot_demo.html
        fig, ax = plt.subplots()
        rects = ax.stackplot(np.arange(1, len(heatCHP) + 1), (heatCHP.T) / 1000.0)
        # ax.legend((rects[0], rects[1], rects[2], rects[3]),('CHP_28MW', 'CHP_32MW', 'CHP_39MW', 'CHP_25MW'))  #
        ax.set_xlabel('hours')
        ax.set_ylabel('heating energy(MW)')
        ax.set_title('heating generation from CHP')

        # plt.savefig("CHP_electricity stack_A.png")
        figFile = ospath.join(currentFilePath, 'Fig/CHP_heat stack_A.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()

        # --------------------------------------ele_CHP-------------------------
        stepInd = 0
        for i in range(parCHP['numType']):
            if valBinInstTypeCHP[i] > 0:
                for j in range(parCHP['numInst'][i]):
                    if valBinInstCHP[j + stepInd] == 1:
                        line_up1, = plt.plot(x, np.ones(temptime) * parCHP['capGen'][i] * 1, 'b--', linewidth=2.0,
                                             label="max(E_CHP)")

                        line_up2, = plt.plot(x, np.ones(temptime) * parCHP['capGen'][i] * parCHP['parLoad'][i], 'b--',
                                             linewidth=2.0, label="min(E_CHP)")
                        line_up, = plt.plot(np.arange(1, len(eleCHP) + 1), eleCHP[:, j + stepInd], 'g-', linewidth=2.0,
                                            label="CHP")
                        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                                   ncol=2, mode="expand", borderaxespad=0.)
                        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

                        plt.xlabel("hour")
                        plt.ylabel("electric energy(kW)")
                        plt.title('electricity from CHP')
                        # plt.axis([0, 180, 0.0, 25500])
                        plt.grid(True)

                        # plt.savefig("CHP_electricity_25MW_C.png")
                        figFile = ospath.join(currentFilePath, 'Fig/CHP_electricity_C.png')
                        plt.savefig(figFile, bbox_inches='tight')
                        plt.show()
                        plt.close()

            stepInd += parCHP['numInst'][i]

        # ---------------------------heat_CHP-------------
        stepInd = 0
        for i in range(parCHP['numType']):
            # endInd=parCHP['numInst'][i]
            if valBinInstTypeCHP[i] > 0:
                for j in range(parCHP['numInst'][i]):
                    if valBinInstCHP[j + stepInd] == 1:
                        line_up1, = plt.plot(x, np.ones(temptime) * parCHP['capGen'][i] * parCHP['heatEff'][i], 'b--',
                                             linewidth=2.0, label="max(H_CHP)")

                        line_up2, = plt.plot(x, np.ones(temptime) * parCHP['capGen'][i] * parCHP['parLoad'][i] *
                                             parCHP['heatEff'][i], 'b--', linewidth=2.0, label="min(H_CHP)")
                        line_up, = plt.plot(np.arange(1, len(heatCHP) + 1), heatCHP[:, j + stepInd], 'g-',
                                            linewidth=2.0, label="CHP")

                        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                                   ncol=2, mode="expand", borderaxespad=0.)
                        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

                        plt.xlabel("hour")
                        plt.ylabel("heating energy(kW)")
                        plt.title('heating from CHP')
                        plt.grid(True)

                        # plt.savefig("CHP_heat_39MW.png")
                        figFile = ospath.join(currentFilePath, 'Fig/CHP_heat_39MW.png')
                        plt.savefig(figFile, bbox_inches='tight')
                        plt.show()
                        plt.close()

            stepInd += parCHP['numInst'][i]
        # ===========================CHP ele & heat==================
        line_up, = plt.plot(np.arange(1, len(eleCHP) + 1), np.sum(eleCHP, axis=1), 'violet', linewidth=2.0,
                            label="total electricity_CHP (kW)")
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        plt.grid(True)

        figFile = ospath.join(currentFilePath, 'Fig/CHP_electricity.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()

        line_up, = plt.plot(np.arange(1, len(heatCHP) + 1), np.sum(heatCHP, axis=1), 'r', linewidth=2.0,
                            label="total heating_CHP (kW)")
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        plt.grid(True)

        # plt.savefig("CHP_electricity,heat.png")
        figFile = ospath.join(currentFilePath, 'Fig/CHP_heat.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ----------------------------------------------
        plt.figure()
        plt.subplot(211)
        line_up, = plt.step(np.arange(1, len(heatCHP) + 1), np.sum(heatCHP / 1000.0, axis=1), 'r--', linewidth=2.0,
                            label="CHP")
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        plt.xlabel("hour")
        plt.ylabel("heating energy(MW)")

        plt.subplot(212)
        line_down, = plt.step(np.arange(1, len(purPriceEleGrid) + 1), purPriceEleGrid, 'b-',
                              linewidth=2.0)  # ,label='electricity tariff($/kWh)'
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        plt.xlabel("hour")
        plt.ylabel("electricity tariff($/kWh)")

        # plt.savefig("CHP_electricity,heat.png")
        figFile = ospath.join(currentFilePath, 'Fig/CHP_heat_ele tariff.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()

    if candidateTech['CHP'] == 1 and drawFlag == 1 and not all(v == 0 for v in valBinInstTypeCHP):
        # -----------------------ele_CHP---------------------
        if valBinInstTypeCHP[2] > 0:
            line_up, = plt.plot(x, eleCHP[:, 2], 'b-', linewidth=2.0, label="CHP_39MW")
        if valBinInstTypeCHP[3] > 0:
            line_down, = plt.plot(x, eleCHP[:, 3], 'r--', linewidth=2.0, label="CHP_25MW")
        if valBinInstTypeCHP[0] > 0:
            line_mid, = plt.plot(x, eleCHP[:, 0], 'g-.', linewidth=2.0, label="CHP_28MW")
        if valBinInstTypeCHP[1] > 0:
            line_bot, = plt.plot(x, eleCHP[:, 1], 'c:', linewidth=2.0, label="CHP_32MW")

        # put the legend on the top right inside the figure
        # plt.legend(handles=[line_up, line_down,line_mid])

        # put the legend on the top outside the figure
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)

        # put the legend on the top right outside the figure
        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

        plt.xlabel("hour")
        plt.ylabel("electric energy(kW)")
        plt.title('electricity from CHP')

        # plt.savefig("CHP_electricity.png")
        figFile = ospath.join(currentFilePath, 'Fig/CHP_electricity.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ----------------------ele_CHP(3D bar)---------------------
        # http://people.duke.edu/~ccc14/pcfb/numpympl/MatplotlibBarPlots.html
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        i = 0
        for c, z in zip(['r', 'g', 'b', 'y'], [0, 10, 20, 30]):  # [30, 20, 10, 0]

            xs = x  # np.arange(20)
            ys = eleCHP[:, i] / 1000  # np.random.rand(20)

            # You can provide either a single color or an array. To demonstrate this,
            # the first bar of each set will be colored cyan.
            cs = [c] * len(xs)
            cs[0] = 'c'
            ax.bar(xs, ys, zs=z, zdir='y', color=cs, alpha=0.8)
            i += 1

        ax.set_xlabel('hour')
        ax.set_ylabel('CHP:[28, 32, 39, 25]MW')
        ax.set_zlabel('electric energy(MW)')

        # plt.savefig("CHP_electricity_3D bar_A.png")
        figFile = ospath.join(currentFilePath, 'Fig/CHP_electricity_3D bar_A.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ----------------------ele_CHP(3D bar)---------------------
        # http://people.duke.edu/~ccc14/pcfb/numpympl/MatplotlibBarPlots.html
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        i = 3
        for c, z in zip(['r', 'g', 'b', 'y'], [0, 10, 20, 30]):  # [30, 20, 10, 0]

            xs = x  # np.arange(20)
            ys = eleCHP[:, i] / 1000  # np.random.rand(20)

            # You can provide either a single color or an array. To demonstrate this,
            # the first bar of each set will be colored cyan.
            cs = [c] * len(xs)
            cs[0] = 'c'
            ax.bar(xs, ys, zs=z, zdir='y', color=cs, alpha=0.8)
            i -= 1

        ax.set_xlabel('hour')
        ax.set_ylabel('CHP:[ 25, 39, 32,28]MW')
        ax.set_zlabel('electric energy(MW)')

        # plt.savefig("CHP_electricity_3D bar_B.png")
        figFile = ospath.join(currentFilePath, 'Fig/CHP_electricity_3D bar_B.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ----------------------ele_CHP(bar-2D)---------------------
        # https://plot.ly/matplotlib/bar-charts/
        # http://stackoverflow.com/questions/14270391/python-matplotlib-multiple-bars
        # multiple_bars = plt.figure()
        ax = plt.subplot(111)
        endtime = np.min([24, parTime['totHrStep']])  # timespan
        if valBinInstTypeCHP[2] > 0:
            rects1 = ax.bar(x[0:endtime] - 0.2, eleCHP[0:endtime, 2], width=0.2, color='b', align='center')
        if valBinInstTypeCHP[3] > 0:
            rects2 = ax.bar(x[0:endtime], eleCHP[0:endtime, 3], width=0.2, color='g', align='center')
        if valBinInstTypeCHP[0] > 0:
            rects3 = ax.bar(x[0:endtime] + 0.2, eleCHP[0:endtime, 0], width=0.2, color='r', align='center')
        if valBinInstTypeCHP[1] > 0:
            rects4 = ax.bar(x[0:endtime] + 0.4, eleCHP[0:endtime, 1], width=0.2, color='c', align='center')
        # ax.xaxis_date()
        ax.autoscale(tight=True)
        ax.legend((rects1[0], rects2[0], rects3[0], rects4[0]), ('CHP_39MW', 'CHP_25MW', 'CHP_28MW', 'CHP_32MW'))

        # plt.savefig("CHP_electricity_2D bar.png")
        figFile = ospath.join(currentFilePath, 'Fig/CHP_electricity_2D bar.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ----------------------ele_CHP(stacked bar)---------------------
        # http://matplotlib.org/examples/pylab_examples/bar_stacked.html

        endtime = timespan
        if valBinInstTypeCHP[2] > 0:
            rects1 = plt.bar(x[0:endtime], eleCHP[0:endtime, 2], width=0.2, color='b')
        if valBinInstTypeCHP[3] > 0:
            rects2 = plt.bar(x[0:endtime], eleCHP[0:endtime, 3], width=0.2, color='g', bottom=eleCHP[0:endtime, 2], )
        if valBinInstTypeCHP[0] > 0:
            rects3 = plt.bar(x[0:endtime], eleCHP[0:endtime, 0], width=0.2, color='r', bottom=eleCHP[0:endtime, 3])
        if valBinInstTypeCHP[1] > 0:
            rects4 = plt.bar(x[0:endtime], eleCHP[0:endtime, 1], width=0.2, color='violet', bottom=eleCHP[0:endtime, 0])
        # ax.xaxis_date()
        plt.autoscale(tight=True)
        # plt.tight_layout()
        plt.legend((rects1[0], rects2[0], rects3[0], rects4[0]), ('CHP_39MW', 'CHP_25MW', 'CHP_28MW', 'CHP_32MW'))

        # plt.savefig("CHP_electricity_stacked bar.png")
        figFile = ospath.join(currentFilePath, 'Fig/CHP_electricity_stacked bar.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()

        # ----------------------ele_CHP---------------------
        # http://glowingpython.blogspot.com/2015/04/stacked-area-plots-with-matplotlib.html
        fig, ax = plt.subplots()
        # ax.stackplot(x, eleCHP[:,2], eleCHP[:,0], eleCHP[:,3])
        # make the stack plot
        stack_coll = ax.stackplot(np.arange(1, len(eleCHP) + 1),
                                  [eleCHP[:, 2], eleCHP[:, 0], eleCHP[:, 3], eleCHP[:, 1]],
                                  colors=['b', 'r', 'g', 'violet'])  # '#377EB8','#55BA87','#7E1137'

        plt.legend([mpatches.Patch(color='b'),  # '#377EB8'
                    mpatches.Patch(color='r'),  # '#55BA87'
                    mpatches.Patch(color='g'),
                    mpatches.Patch(color='c')],  # '#7E1137'
                   ['CHP_39MW', 'CHP_28MW', 'CHP_25MW', 'CHP_32MW'],
                   loc=1, )
        plt.xlabel("hour")
        plt.ylabel("electric energy(kW)")

        # plt.savefig("CHP_electricity stack_B.png")
        figFile = ospath.join(currentFilePath, 'Fig/CHP_electricity stack_B.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ---------------------------CHP_39MW-------------
        # http://matplotlib.org/examples/api/barchart_demo.html
        if valBinInstTypeCHP[2] > 0:
            width = 0.35  # the width of the bars
            fig, ax = plt.subplots()
            rects1 = ax.bar(x, eleCHP[:, 2], width, color='r')
            rects1[0].set_color('r')
            # add some text for labels, title and axes ticks
            ax.set_xlabel('hours')
            ax.set_ylabel('electric energy(kW)')
            ax.set_title('electricity from CHP_39MW')
            ax.set_xticks(x + width)

            # plt.savefig("CHP_electricity_39MW_A.png")
            figFile = ospath.join(currentFilePath, 'Fig/CHP_electricity_39MW_A.png')
            plt.savefig(figFile, bbox_inches='tight')
            plt.close()
            # ax.set_xticklabels(('G1', 'G2', 'G3', 'G4', 'G5'))

            # ax.legend((rects1[0]), ('CHP_32000kW'))
            # ---------------------------CHP_39MW-------------
            # http://matplotlib.org/examples/pylab_examples/barchart_demo.html
            fig = plt.figure()
            bar_width = 0.35
            opacity = 0.4
            error_config = {'ecolor': '0.3', 'elinewidth': 2, }
            rects1 = plt.bar(x, eleCHP[:, 2], bar_width,
                             alpha=opacity,
                             color='b',  # yerr=std_men,
                             error_kw=error_config,
                             label='CHP_39MW')
            plt.xlabel('hours')
            plt.ylabel('electric energy(kW)')
            plt.title('electricity from CHP_39MW')
            # plt.xticks(x + bar_width, ('A', 'B', 'C', 'D', 'E'))
            plt.legend()

            plt.tight_layout()

            # plt.savefig("CHP_electricity_39MW_B.png")
            figFile = ospath.join(currentFilePath, 'Fig/CHP_electricity_39MW_B.png')
            plt.savefig(figFile, bbox_inches='tight')
            plt.show()
            plt.close()
            # ---------------------------CHP_39MW-------------
            # http://matplotlib.org/examples/pylab_examples/barchart_demo.html
            fig = plt.figure()
            bar_width = 0.35
            rects1 = plt.bar(x, eleCHP[:, 2], bar_width, color="blue")
            plt.xlabel('hours')
            plt.ylabel('electric energy(kW)')
            plt.title('electricity from CHP_39MW')
            # plt.xticks(x + bar_width, ('A', 'B', 'C', 'D', 'E'))
            plt.legend()

            plt.tight_layout()

            # plt.savefig("CHP_electricity_39MW_C.png")
            figFile = ospath.join(currentFilePath, 'Fig/CHP_electricity_39MW_C.png')
            plt.savefig(figFile, bbox_inches='tight')
            plt.show()
            plt.close()
            # ---------------------------ele_CHP_39MW-------------
            line_up1, = plt.plot(x, np.ones(temptime) * parCHP['capGen'][2] * 1, 'b--', linewidth=2.0,
                                 label="max(E_CHP_39MW)")

            line_up2, = plt.plot(x, np.ones(temptime) * parCHP['capGen'][2] * parCHP['parLoad'][2], 'b--',
                                 linewidth=2.0, label="min(E_CHP_39MW)")
            line_up, = plt.plot(x, eleCHP[:, 2], 'r-', linewidth=2.0, label="CHP_39MW")
            plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                       ncol=2, mode="expand", borderaxespad=0.)
            # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

            plt.xlabel("hour")
            plt.ylabel("electric energy(kW)")
            plt.title('electricity from CHP_39MW')
            plt.grid(True)

            # plt.savefig("CHP_electricity_39MW_limit.png")
            figFile = ospath.join(currentFilePath, 'Fig/CHP_electricity_39MW_limit.png')
            plt.savefig(figFile, bbox_inches='tight')
            plt.show()
            plt.close()

        # --------------------------------------ele_CHP_25MW-------------------------
        if valBinInstTypeCHP[3] > 0:
            line_up1, = plt.plot(x, np.ones(temptime) * parCHP['capGen'][3] * 1, 'b--', linewidth=2.0,
                                 label="max(E_CHP_25MW)")

            line_up2, = plt.plot(x, np.ones(temptime) * parCHP['capGen'][3] * parCHP['parLoad'][3], 'b--',
                                 linewidth=2.0, label="min(E_CHP_25MW)")
            line_up, = plt.plot(x, eleCHP[:, 3], 'r-', linewidth=2.0, label="CHP_25MW")
            plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                       ncol=2, mode="expand", borderaxespad=0.)
            # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

            plt.xlabel("hour")
            plt.ylabel("electric energy(kW)")
            plt.title('electricity from CHP_25MW')
            # plt.axis([0, 180, 0.0, 25500])
            plt.grid(True)

            # plt.savefig("CHP_electricity_25MW_C.png")
            figFile = ospath.join(currentFilePath, 'Fig/CHP_electricity_25MW_C.png')
            plt.savefig(figFile, bbox_inches='tight')
            plt.show()
            plt.close()
        # --------------------------------------ele_CHP_28MW-------------------------
        if valBinInstTypeCHP[0] > 0:
            line_up1, = plt.plot(x, np.ones(temptime) * parCHP['capGen'][0] * 1, 'b--', linewidth=2.0,
                                 label="max(E_CHP_28MW)")

            line_up2, = plt.plot(x, np.ones(temptime) * parCHP['capGen'][0] * parCHP['parLoad'][0], 'b--',
                                 linewidth=2.0, label="min(E_CHP_28MW)")
            line_up, = plt.plot(x, eleCHP[:, 0], 'r-', linewidth=2.0, label="CHP_28000kW")

            plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                       ncol=2, mode="expand", borderaxespad=0.)
            # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

            plt.xlabel("hour")
            plt.ylabel("electric energy(kW)")
            plt.title('electricity from CHP_28MW')
            plt.grid(True)

            # plt.savefig("CHP_electricity_28MW_C.png")
            figFile = ospath.join(currentFilePath, 'Fig/CHP_electricity_28MW_C.png')
            plt.savefig(figFile, bbox_inches='tight')
            plt.show()
            plt.close()
        # -----------------------heat_CHP----------------------------
        if valBinInstTypeCHP[2] > 0:
            line_up, = plt.plot(x, heatCHP[:, 2], 'b:', label="CHP_39MW")
        if valBinInstTypeCHP[0] > 0:
            line_down, = plt.plot(x, heatCHP[:, 0], 'g--', label="CHP_28MW")
        if valBinInstTypeCHP[3] > 0:
            line_mid, = plt.plot(x, heatCHP[:, 3], 'r-.', label="CHP_25MW")
        if valBinInstTypeCHP[1] > 0:
            line_mid0, = plt.plot(x, heatCHP[:, 1], 'violet', label="CHP_32MW")

        # plt.legend(handles=[line_up, line_down])
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

        plt.xlabel("hour")
        plt.ylabel("heating energy(kW)")
        plt.title('heat generation from CHP')

        # plt.savefig("CHP_heat.png")
        figFile = ospath.join(currentFilePath, 'Fig/CHP_heat.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ----------------------heat_CHP---------------------
        # http://glowingpython.blogspot.com/2015/04/stacked-area-plots-with-matplotlib.html
        fig, ax = plt.subplots()
        # ax.stackplot(x, eleCHP[:,2], eleCHP[:,0], eleCHP[:,3])
        # make the stack plot
        stack_coll = ax.stackplot(x,
                                  [heatCHP[:, 2], heatCHP[:, 0], heatCHP[:, 3], heatCHP[:, 1]],
                                  colors=['b', 'r', 'g', 'c'])  # '#377EB8','#55BA87','#7E1137'
        plt.legend([mpatches.Patch(color='b'),  # '#377EB8'
                    mpatches.Patch(color='r'),  # '#55BA87'
                    mpatches.Patch(color='g'),
                    mpatches.Patch(color='c')],  # '#7E1137'
                   ['CHP_39MW', 'CHP_28MW', 'CHP_25MW', 'CHP_32MW'],
                   loc=2, )
        # ax.set_ylim([0,30])
        plt.xlabel("hour")
        plt.ylabel("heating energy(kW)")

        # plt.savefig("CHP_heat stack.png")
        figFile = ospath.join(currentFilePath, 'Fig/CHP_heat stack.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()

        # ---------------------------heat_CHP_39MW-------------
        if valBinInstTypeCHP[2] > 0:
            line_up1, = plt.plot(x, np.ones(temptime) * parCHP['capGen'][2] * parCHP['heatEff'][2], 'b--',
                                 linewidth=2.0, label="max(H_CHP_39MW)")

            line_up2, = plt.plot(x,
                                 np.ones(temptime) * parCHP['capGen'][2] * parCHP['parLoad'][2] * parCHP['heatEff'][2],
                                 'b--', linewidth=2.0, label="min(H_CHP_39MW)")
            line_up, = plt.plot(x, heatCHP[:, 2], 'r-', linewidth=2.0, label="CHP_39MW")

            plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                       ncol=2, mode="expand", borderaxespad=0.)
            # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

            plt.xlabel("hour")
            plt.ylabel("heating energy(kW)")
            plt.title('heating from CHP_39MW')
            plt.grid(True)

            # plt.savefig("CHP_heat_39MW.png")
            figFile = ospath.join(currentFilePath, 'Fig/CHP_heat_39MW.png')
            plt.savefig(figFile, bbox_inches='tight')
            plt.show()
            plt.close()
        # --------------------------------------heat_CHP_25MW-------------------------
        if valBinInstTypeCHP[3] > 0:
            line_up1, = plt.plot(x, np.ones(temptime) * parCHP['capGen'][3] * parCHP['heatEff'][3], 'b--',
                                 linewidth=2.0, label="max(H_CHP_25MW)")

            line_up2, = plt.plot(x,
                                 np.ones(temptime) * parCHP['capGen'][3] * parCHP['parLoad'][3] * parCHP['heatEff'][3],
                                 'b--', linewidth=2.0, label="min(H_CHP_25MW)")
            line_up, = plt.plot(x, heatCHP[:, 3], 'r-', linewidth=2.0, label="CHP_25MW")

            plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                       ncol=2, mode="expand", borderaxespad=0.)
            # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

            plt.xlabel("hour")
            plt.ylabel("heating energy(kW)")
            plt.title('heating from CHP_25MW')
            # plt.axis([0, 180, 0.0, 25500])
            plt.grid(True)

            # plt.savefig("CHP_heat_25MW.png")
            figFile = ospath.join(currentFilePath, 'Fig/CHP_heat_25MW.png')
            plt.savefig(figFile, bbox_inches='tight')
            plt.show()
            plt.close()
        # --------------------------------------heat_CHP_28MW-------------------------
        if valBinInstTypeCHP[0] > 0:
            line_up1, = plt.plot(x, np.ones(temptime) * parCHP['capGen'][0] * parCHP['heatEff'][0], 'b--',
                                 linewidth=2.0, label="max(H_CHP_28MW)")

            line_up2, = plt.plot(x,
                                 np.ones(temptime) * parCHP['capGen'][0] * parCHP['parLoad'][0] * parCHP['heatEff'][0],
                                 'b--', linewidth=2.0, label="min(H_CHP_28MW)")
            line_up, = plt.plot(x, heatCHP[:, 0], 'r-', linewidth=2.0, label="CHP_28MW")
            plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                       ncol=2, mode="expand", borderaxespad=0.)
            # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

            plt.xlabel("hour")
            plt.ylabel("heating energy(kW)")
            plt.title('heating from CHP_28MW')
            plt.grid(True)

            # plt.savefig("CHP_heat_28MW.png")
            figFile = ospath.join(currentFilePath, 'Fig/CHP_heat_28MW.png')
            plt.savefig(figFile, bbox_inches='tight')
            plt.show()
            plt.close()
        # --------------------------------------heat_CHP_32MW-------------------------
        if valBinInstTypeCHP[1] > 0:
            line_up1, = plt.plot(x, np.ones(temptime) * parCHP['capGen'][1] * parCHP['heatEff'][1], 'b--',
                                 linewidth=2.0, label="max(H_CHP_32MW)")

            line_up2, = plt.plot(x,
                                 np.ones(temptime) * parCHP['capGen'][1] * parCHP['parLoad'][1] * parCHP['heatEff'][1],
                                 'b--', linewidth=2.0, label="min(H_CHP_32MW)")
            line_up, = plt.plot(x, heatCHP[:, 1], 'r-', linewidth=2.0, label="CHP_32MW")
            plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                       ncol=2, mode="expand", borderaxespad=0.)
            # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

            plt.xlabel("hour")
            plt.ylabel("heating energy(kW)")
            plt.title('heating from CHP_32MW')
            plt.grid(True)

            # plt.savefig("CHP_heat_28MW.png")
            figFile = ospath.join(currentFilePath, 'Fig/CHP_heat_28MW.png')
            plt.savefig(figFile, bbox_inches='tight')
            plt.show()
            plt.close()

    if candidateTech['eleGrid'] == 1 and len(nonzeroIndEle):
        # ==========================electricity sale==========================
        line_up, = plt.plot(np.arange(1, len(salEleGrid) + 1), np.negative(salEleGrid), 'violet', linewidth=2.0,
                            label="electricity sold (kW)")
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        plt.grid(True)

        # plt.savefig("electricity sale.png")
        figFile = ospath.join(currentFilePath, 'Fig/electricity sale.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()

        # ======================electricity purchase==================================
        f, ax = plt.subplots()  # we manually make a figure and axis
        ax.legend()  # create a legend for the axis
        ax.set_title("electricity")  # we set the title on the axis
        # ax.plot(x, coolHP)
        ax.plot(np.arange(1, len(purEleGrid) + 1), purEleGrid, 'violet')
        # ax.set_title("My heart be still, this one's even better")
        # We now have access to the "axis" object that we created,
        # which lets us do lots of other cool things with it
        ax.set_xlabel("time")
        ax.set_ylabel("purchased electricity(kW)")

        # plt.savefig("electricity purchase.png")
        figFile = ospath.join(currentFilePath, 'Fig/electricity purchase.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.close()
        # ==========================electricity exchange=========================
        line_up, = plt.plot(np.arange(1, len(salEleGrid) + 1), np.negative(salEleGrid), 'violet', linewidth=2.0,
                            label="electricity sold (kW)")
        line_bot, = plt.plot(np.arange(1, len(purEleGrid) + 1), purEleGrid, 'r--', linewidth=2.0,
                             label="electricity purchase (kW)")
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        plt.grid(True)

        # plt.savefig("electricity exchange.png")
        figFile = ospath.join(currentFilePath, 'Fig/electricity exchange.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()

    # ----------------------electricity balance-------------------------
    # np.sum(eleCHP, axis=1)+fromEleBat= eleLoad  +salEleGrid+stoEleBat
    if len(nonzeroIndEle):
        # --------------------------------------------------
        line_down, = plt.plot(np.arange(1, len(eleLoad) + 1), np.negative(eleLoad / 1000.0), 'g--',
                              label="electricity load")

        if candidateTech['PV'] == 1 and valBinInst['PV'] == 1:
            line_mid0, = plt.plot(np.arange(1, len(elePV) + 1), elePV / 1000.0, 'r-', label="PV")

        if candidateTech['CHP'] == 1 and not all(v == 0 for v in valBinInstTypeCHP):
            line_mid2, = plt.plot(np.arange(1, len(eleCHP) + 1), np.sum(eleCHP / 1000.0, axis=1), 'y-o', label="CHP")

        if candidateTech['eleChill'] == 1 and valBinInst['eleChill'] == 1:
            line_mid, = plt.plot(np.arange(1, len(eleEleChill) + 1), np.negative(eleEleChill / 1000.0), 'k:',
                                 label="electric chiller")

        if candidateTech['HP'] == 1 and valBinInst['HP'] == 1:
            line_mid1, = plt.plot(np.arange(1, len(eleHP) + 1), np.negative(eleHP / 1000.0), 'c-<', label="heat pump")

        if candidateTech['eleGrid'] == 1:
            line_bot1, = plt.plot(np.arange(1, len(salEleGrid) + 1), np.negative(salEleGrid / 1000.0), 'b-^',
                                  label="electricity sale")
            line_bot, = plt.plot(np.arange(1, len(purEleGrid) + 1), purEleGrid / 1000.0, 'm-',
                                 label="electricity purchase")

        # electric boiler
        if candidateTech['eleBoil'] == 1 and np.allclose(valBinInst['eleBoil'], 1):
            line_bot2, = plt.plot(np.arange(1, len(eleEleBoil) + 1), np.negative(eleEleBoil / 1000.0), 'k->',
                                  label="electric boiler")

        # if candidateTech['eleBat'] == 1  and valBinInst['eleBat']==1:
        #    line_mid3, = plt.plot(x, fromEleBat,'g-', linewidth=2.0,label="from storage")
        #    line_bot3, = plt.plot(x, np.negative(stoEleBat),'g--', linewidth=2.0,label="for storage")

        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)

        plt.xlabel("hour")
        plt.ylabel("electric energy(MW)")
        # plt.show()
        plt.grid(True)
        # plt.title('electric balance')

        # plt.savefig("electricity balance.png")
        figFile = ospath.join(currentFilePath, 'Fig/electric balance.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # -----------------------------------------------------------------------
        plt.figure(1)
        plt.subplot(211)
        line_down, = plt.plot(np.arange(1, len(eleLoad) + 1), np.negative(eleLoad), 'm-', label="electricity demand")

        if candidateTech['PV'] == 1 and valBinInst['PV'] == 1:
            line_mid0, = plt.plot(np.arange(1, len(elePV) + 1), elePV, 'r--', label="electricity_PV")

        if candidateTech['CHP'] == 1 and not all(v == 0 for v in valBinInstTypeCHP):
            line_mid2, = plt.plot(np.arange(1, len(eleCHP) + 1), np.sum(eleCHP, axis=1), 'r-.', label="CHP")

        if candidateTech['eleChill'] == 1 and valBinInst['eleChill'] == 1:
            line_mid, = plt.plot(np.arange(1, len(eleEleChill) + 1), np.negative(eleEleChill), 'k:',
                                 label="electric chiller")

        if candidateTech['HP'] == 1 and valBinInst['HP'] == 1:
            line_mid1, = plt.plot(np.arange(1, len(eleHP) + 1), np.negative(eleHP), 'c->', label="heat pump")

        if candidateTech['eleGrid'] == 1:
            line_bot1, = plt.plot(np.arange(1, len(salEleGrid) + 1), np.negative(salEleGrid), 'b-',
                                  label="electricity sale")
            line_bot, = plt.plot(np.arange(1, len(purEleGrid) + 1), purEleGrid, 'b-<', label="electricity purchase")

        # electric boiler
        if candidateTech['eleBoil'] == 1 and np.allclose(valBinInst['eleBoil'], 1):
            line_bot2, = plt.plot(np.arange(1, len(eleEleBoil) + 1), np.negative(eleEleBoil), 'c-^',
                                  label="electric boiler")

        # if candidateTech['eleBat'] == 1  and valBinInst['eleBat']==1:
        #    line_mid3, = plt.plot(x, fromEleBat,'g-', linewidth=2.0,label="from storage")
        #    line_bot3, = plt.plot(x, np.negative(stoEleBat),'g--', linewidth=2.0,label="for storage")

        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)

        # plt.xlabel("hour")
        plt.ylabel("electric energy(kW)")
        # plt.show()
        plt.grid(True)

        plt.subplot(212)
        if candidateTech['eleBat'] == 1 and valBinInst['eleBat'] == 1:
            line_mid1, = plt.plot(x, fromEleBat, 'g', linewidth=2.0, label="from storage")
            line_bot, = plt.plot(x, np.negative(stoEleBat), 'b', linewidth=2.0, label="for storage")

            plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                       ncol=2, mode="expand", borderaxespad=0.)
        plt.xlabel("hour")
        plt.ylabel("battery electricity(kW)")
        plt.grid(True)
        # plt.title('electric battery')

        # plt.savefig("electricity balance.png")
        figFile = ospath.join(currentFilePath, 'Fig/electricity balance.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
    # =======================heat balance======================
    #  heatLoad+ heatSal= np.sum(heatCHP, axis=1)
    if len(nonzeroIndHeat):
        # --------------------------------------------------------------
        line_down, = plt.plot(np.arange(1, len(heatLoad) + 1), np.negative(heatLoad / 1000.0), 'r-x',
                              label="heating Load")

        if pipeNetwFlag == 1:
            line_down1, = plt.plot(np.arange(1, len(heatLoad) + 1), np.negative(pipeHeatLoss / 1000.0), 'g--',
                                   label="heating Loss")

        else:
            line_down1, = plt.plot(np.arange(1, len(heatLoad) + 1),
                                   np.negative((1 - parThemPipe['heatDistEff']) * heatLoad / 1000.0), 'g--',
                                   label="heating Loss")

        if candidateTech['heatInds'] == 1:
            line_bot5, = plt.plot(np.arange(1, len(heatLoad) + 1), heatInds / 1000.0, 'violet',
                                  label="industry waste heat")

        if candidateTech['heatSal'] == 1:
            line_bot1, = plt.plot(np.arange(1, len(heatSal) + 1), np.negative(heatSal / 1000.0), 'y-o',
                                  label="heating sale")

        if candidateTech['ABSChill'] == 1 and valBinInst['ABSChill'] == 1:
            line_mid, = plt.plot(np.arange(1, len(heatABSChill) + 1), np.negative(heatABSChill / 1000.0), 'b->',
                                 label="absorption chiller")

        if candidateTech['CHP'] == 1 and not all(v == 0 for v in valBinInstTypeCHP):
            line_up, = plt.plot(np.arange(1, len(heatCHP) + 1), np.sum(heatCHP / 1000.0, axis=1), 'm-<', label="CHP")

        if candidateTech['HP'] == 1 and valBinInst['HP'] == 1 and parHP['heatSta'] == 1:
            line_bot0, = plt.plot(np.arange(1, len(heatHP) + 1), heatHP / 1000.0, 'k-^', label="heat pump")

        if candidateTech['eleBoil'] == 1 and np.allclose(valBinInst['eleBoil'], 1):
            line_bot2, = plt.plot(np.arange(1, len(heatEleBoil) + 1), heatEleBoil / 1000.0, 'c-v',
                                  label="electric boiler")

        if candidateTech['solThem'] == 1 and valBinInst['solThem'] == 1:
            line_bot3, = plt.plot(np.arange(1, len(heatSolThem) + 1), heatSolThem / 1000.0, 'c-s',
                                  label="solar thermal")

        if candidateTech['NGBoil'] == 1 and valBinInst['NGBoil'] == 1:
            line_bot4, = plt.plot(np.arange(1, len(heatNGBoil) + 1), heatNGBoil / 1000.0, 'k-p',
                                  label="natural gas boiler")

        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        plt.xlabel("hour")
        plt.ylabel("heat energy(MW)")
        # plt.show()
        plt.grid(True)

        # plt.savefig("heat balance.png")
        figFile = ospath.join(currentFilePath, 'Fig/heat_balance.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ------------------------------------------------------------------------
        plt.subplot(211)
        # line_bot, = plt.plot(x, np.negative(heatSal),'violet',label="heat sold")
        line_down, = plt.plot(np.arange(1, len(heatLoad) + 1), np.negative(heatLoad / 1000.0), 'g-.',
                              label="heating Load")

        if pipeNetwFlag == 1:
            line_down1, = plt.plot(np.arange(1, len(pipeHeatLoss) + 1), np.negative(pipeHeatLoss / 1000.0), 'g--',
                                   label="heating Loss")

        else:
            line_down1, = plt.plot(np.arange(1, len(heatLoad) + 1),
                                   np.negative((1 - parThemPipe['heatDistEff']) * heatLoad / 1000.0), 'g--',
                                   label="heating Loss")

        if candidateTech['heatInds'] == 1:
            line_bot5, = plt.plot(np.arange(1, len(heatInds) + 1), heatInds / 1000.0, 'violet',
                                  label="industry waste heat")

        if candidateTech['heatSal'] == 1:
            line_bot1, = plt.plot(np.arange(1, len(heatSal) + 1), np.negative(heatSal / 1000.0), 'y-o',
                                  label="heating sale")

        if candidateTech['ABSChill'] == 1 and valBinInst['ABSChill'] == 1:
            line_mid, = plt.plot(np.arange(1, len(heatABSChill) + 1), np.negative(heatABSChill / 1000.0), 'b-v',
                                 label="absorption chiller")

        if candidateTech['CHP'] == 1 and not all(v == 0 for v in valBinInstTypeCHP):
            line_up, = plt.plot(np.arange(1, len(heatCHP) + 1), np.sum(heatCHP / 1000.0, axis=1), 'r-^', label="CHP")

        if candidateTech['HP'] == 1 and valBinInst['HP'] == 1 and parHP['heatSta'] == 1:
            line_bot0, = plt.plot(np.arange(1, len(heatHP) + 1), heatHP / 1000.0, 'k-<', label="heat pump")

        if candidateTech['eleBoil'] == 1 and np.allclose(valBinInst['eleBoil'], 1):
            line_bot2, = plt.plot(np.arange(1, len(heatEleBoil) + 1), heatEleBoil / 1000.0, 'c->',
                                  label="electric boiler")

        if candidateTech['solThem'] == 1 and valBinInst['solThem'] == 1:
            line_bot3, = plt.plot(np.arange(1, len(heatSolThem) + 1), heatSolThem / 1000.0, 'c-s',
                                  label="solar thermal")

        if candidateTech['NGBoil'] == 1 and valBinInst['NGBoil'] == 1:
            line_bot4, = plt.plot(np.arange(1, len(heatNGBoil) + 1), heatNGBoil / 1000.0, 'g-p',
                                  label="natural gas boiler")

        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        # plt.xlabel("hour")
        plt.ylabel("heat energy(MW)")
        # plt.show()
        plt.grid(True)

        plt.subplot(212)
        if candidateTech['heatTank'] == 1 and valBinInst['heatTank'] == 1:
            line_mid0, = plt.plot(np.arange(1, len(fromHeatTank) + 1), fromHeatTank / 1000.0, 'm--', linewidth=2.0,
                                  label="from storage")
            line_bot, = plt.plot(np.arange(1, len(stoHeatTank) + 1), -stoHeatTank / 1000.0, 'g-', linewidth=2.0,
                                 label="for storage")

            # plt.legend(handles=[line_up, line_down])
            plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                       ncol=2, mode="expand", borderaxespad=0.)

        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.xlabel("hour")
        plt.ylabel("heating energy(MW)")
        # plt.title('heat tank')

        # plt.savefig("heat balance.png")
        figFile = ospath.join(currentFilePath, 'Fig/heat balance.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()

    # -----------------------cooling balance-------------------
    # coolLoad  + pipeCoolLoss + stoCoolTank = fromCoolTank + coolEleChill+coolHP+coolABSChill+coolNGChill
    if len(nonzeroIndCool):
        # ------------------------------------------------------------------------------
        line_down, = plt.plot(np.arange(1, len(coolLoad) + 1), - coolLoad / 1000.0, 'b-o', linewidth=2.0,
                              label="Cooling Load")

        if pipeNetwFlag == 1:
            line_down1, = plt.plot(np.arange(1, len(pipeCoolLoss) + 1), -pipeCoolLoss / 1000.0, 'violet', linewidth=2.0,
                                   label="Cooling Loss")

        else:
            line_down1, = plt.plot(np.arange(1, len(coolLoad) + 1),
                                   np.negative((1 - parThemPipe['coolDistEff']) * coolLoad / 1000.0), 'violet',
                                   linewidth=2.0,
                                   label="Cooling Loss")

        if candidateTech['ABSChill'] == 1 and valBinInst['ABSChill'] == 1:
            line_up1, = plt.plot(np.arange(1, len(coolABSChill) + 1), coolABSChill / 1000.0, 'y->',
                                 label="absorption chiller")

        if candidateTech['eleChill'] == 1 and valBinInst['eleChill'] == 1:
            line_top, = plt.plot(np.arange(1, len(coolEleChill) + 1), coolEleChill / 1000.0, 'r-x', linewidth=2.0,
                                 label="electric chiller")

        if candidateTech['HP'] == 1 and valBinInst['HP'] == 1 and parHP['coolSta'] == 1:
            line_up, = plt.plot(np.arange(1, len(coolHP) + 1), coolHP / 1000.0, 'c-v', linewidth=2.0, label="heat pump")

        # if candidateTech['coolTank'] == 1  and valBinInst['coolTank']==1:
        #    line_mid, = plt.plot(x, fromCoolTank,'k', linewidth=2.0,label="from storage")
        #    line_bot, = plt.plot(x, -stoCoolTank,'g', linewidth=2.0,label="for storage")

        if candidateTech['NGChill'] == 1 and valBinInst['NGChill'] == 1:
            line_bot1, = plt.plot(np.arange(1, len(coolNGChill) + 1), coolNGChill / 1000.0, 'g-^', linewidth=2.0,
                                  label="natural gas chiller")

        # plt.legend(handles=[line_up, line_down])
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)

        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)
        plt.xlabel("hour")
        plt.ylabel("cooling energy (MW)")
        # plt.title('heat generation from CHP')
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        # plt.xlabel("hour")
        # plt.ylabel("cool energy(kW)")
        # plt.show()
        # plt.grid(True)

        # plt.savefig("cooling balance.png")
        figFile = ospath.join(currentFilePath, 'Fig/cool balance.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ---------------------------------------------------------------------------------
        plt.subplot(211)
        line_down, = plt.plot(np.arange(1, len(coolLoad) + 1), - coolLoad, 'r--', linewidth=2.0, label="Cooling Load")

        if pipeNetwFlag == 1:
            line_down1, = plt.plot(np.arange(1, len(pipeCoolLoss) + 1), -pipeCoolLoss, 'violet', linewidth=2.0,
                                   label="Cooling Loss")

        else:
            line_down1, = plt.plot(np.arange(1, len(coolLoad) + 1),
                                   np.negative((1 - parThemPipe['coolDistEff']) * coolLoad), 'violet', linewidth=2.0,
                                   label="Cooling Loss")

        if candidateTech['ABSChill'] == 1 and valBinInst['ABSChill'] == 1:
            line_up1, = plt.plot(np.arange(1, len(coolABSChill) + 1), coolABSChill, 'b:', label="absorption chiller")

        if candidateTech['eleChill'] == 1 and valBinInst['eleChill'] == 1:
            line_top, = plt.plot(np.arange(1, len(coolEleChill) + 1), coolEleChill, 'y-', linewidth=2.0,
                                 label="electric chiller")

        if candidateTech['HP'] == 1 and valBinInst['HP'] == 1 and parHP['coolSta'] == 1:
            line_up, = plt.plot(np.arange(1, len(coolHP) + 1), coolHP, 'c-o', linewidth=2.0, label="heat pump")

        # if candidateTech['coolTank'] == 1  and valBinInst['coolTank']==1:
        #    line_mid, = plt.plot(x, fromCoolTank,'k', linewidth=2.0,label="from storage")
        #    line_bot, = plt.plot(x, -stoCoolTank,'g', linewidth=2.0,label="for storage")

        if candidateTech['NGChill'] == 1 and valBinInst['NGChill'] == 1:
            line_bot1, = plt.plot(np.arange(1, len(coolNGChill) + 1), coolNGChill, 'g-v', linewidth=2.0,
                                  label="from natural gas chiller")

        # plt.legend(handles=[line_up, line_down])
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)

        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)
        plt.xlabel("hour")
        plt.ylabel("cooling energy (kW)")
        # plt.title('heat generation from CHP')
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        plt.xlabel("hour")
        plt.ylabel("cool energy(kW)")
        # plt.show()
        plt.grid(True)

        plt.subplot(212)
        if candidateTech['coolTank'] == 1 and valBinInst['coolTank'] == 1:
            line_mid0, = plt.plot(np.arange(1, len(fromCoolTank) + 1), fromCoolTank, 'm--', linewidth=2.0,
                                  label="from storage")
            line_bot, = plt.plot(np.arange(1, len(stoCoolTank) + 1), -stoCoolTank, 'g-', linewidth=2.0,
                                 label="for storage")

            # plt.legend(handles=[line_up, line_down])
            plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                       ncol=2, mode="expand", borderaxespad=0.)

        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.xlabel("hour")
        plt.ylabel("cool energy(kW)")
        # plt.title('cool tank')

        # plt.savefig("cooling balance.png")
        figFile = ospath.join(currentFilePath, 'Fig/cooling balance.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
    # -------------------------------thermal loss------------------------------
    # if len(nonzeroIndCool) or len(nonzeroIndHeat):
    if pipeNetwFlag == 1:
        plt.figure()
        plt.subplot(211)
        line_up, = plt.plot(np.arange(1, len(pipeHeatLoss) + 1), pipeHeatLoss, 'b-', linewidth=2.0,
                            label='heating loss')
        # plt.legend(handles=[line_up], loc=4)
        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.legend(handles=[line_up], loc=1)
        plt.grid(True)
        plt.ylabel('heating loss (kW)')

        plt.subplot(212)
        line_up, = plt.plot(np.arange(1, len(pipeCoolLoss) + 1), pipeCoolLoss, 'r--', linewidth=2.0,
                            label='cooling loss')
        # plt.legend(handles=[line_up], loc=1)
        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.legend(handles=[line_up], loc=1)
        plt.grid(True)
        plt.xlabel('hours')
        plt.ylabel('cooling loss (kW)')

        figFile = ospath.join(currentFilePath, 'Fig/thermal loss.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ================================ mainPipeHeatLoss===========================================
        # if len(nonzeroIndHeat):
        fig, ax = plt.subplots()
        x = np.arange(1, len(heatLoad) + 1)
        rects = ax.stackplot(x, mainPipeHeatLoss[0:len(x)])
        # ax.legend((rects[0],rects[1],rects[2],rects[3],rects[4],rects[5]),( 'bldg #1', 'bldg #2', 'bldg #3', 'bldg #4','bldg #5','bldg #6') )#
        ax.set_xlabel('hours')
        ax.set_ylabel('heating loss(kW)')
        ax.set_title('heating loss for main pipeline')
        # ax.set_xticks(x + width)
        # ax.savefig('CHP_electricity_stacked.png')

        plt.grid(True)
        figFile = ospath.join(currentFilePath, 'Fig/mainHeatLoss_building cluster.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
        # ----------------------------------main pipe cooling loss-------------------------------------------
        if len(nonzeroIndCool):
            fig, ax = plt.subplots()
            x = np.arange(1, temptime + 1)
            rects = ax.stackplot(x, mainPipeCoolLoss)
            # ax.legend((rects[0],rects[1],rects[2],rects[3],rects[4],rects[5]),( 'bldg #1', 'bldg #2', 'bldg #3', 'bldg #4','bldg #5','bldg #6') )#
            ax.set_xlabel('hours')
            ax.set_ylabel('cooling loss(kW)')
            ax.set_title('cooling loss for main pipeline')
            # ax.set_xticks(x + width)
            # ax.savefig('CHP_electricity_stacked.png')

            plt.grid(True)
            figFile = ospath.join(currentFilePath, 'Fig/mainCoolLoss_building cluste.png')
            plt.savefig(figFile, bbox_inches='tight')
            plt.show()
            plt.close()

        # ----------------------------------------bldgHeat---------------------------------------------
        if len(nonzeroIndHeat):
            # plot building thermal demands individually
            fig, ax = plt.subplots()
            x = np.arange(1, temptime + 1)
            rects = ax.stackplot(x, bldgHeat.T)
            # ax.legend((rects[0],rects[1],rects[2],rects[3],rects[4],rects[5]),( 'bldg #1', 'bldg #2', 'bldg #3', 'bldg #4','bldg #5','bldg #6') )#
            ax.set_xlabel('hours')
            ax.set_ylabel('heating energy(kW)')
            ax.set_title('heating demand for building cluster')
            # ax.set_xticks(x + width)
            # ax.savefig('CHP_electricity_stacked.png')

            plt.grid(True)
            figFile = ospath.join(currentFilePath, 'Fig/heating_building cluster.png')
            plt.savefig(figFile, bbox_inches='tight')
            plt.show()
            plt.close()
        # ---------------------------------------------bldgCool------------------------------------------------
        if len(nonzeroIndCool):
            fig, ax = plt.subplots()
            x = np.arange(1, temptime + 1)
            rects = ax.stackplot(x, bldgCool.T)
            # ax.legend((rects[0],rects[1],rects[2],rects[3],rects[4],rects[5]),( 'bldg #1', 'bldg #2', 'bldg #3', 'bldg #4','bldg #5','bldg #6') )#
            ax.set_xlabel('hours')
            ax.set_ylabel('cooling energy(kW)')
            ax.set_title('cooling demand for building cluster')
            # ax.set_xticks(x + width)
            # ax.savefig('CHP_electricity_stacked.png')

            plt.grid(True)
            figFile = ospath.join(currentFilePath, 'Fig/cooling_building cluster.png')
            plt.savefig(figFile, bbox_inches='tight')
            plt.show()
            plt.close()

    # ---------------------------------solar PV-----------------------------------------
    if candidateTech['PV'] == 1 and valBinInst['PV'] == 1:
        line_mid0, = plt.plot(x, elePV, 'r-', label="electricity_PV")
        # plt.legend(handles=[line_up, line_down])
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)

        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)
        plt.xlabel("hour")
        plt.ylabel("electric energy (kW)")
        # plt.title('heat generation from CHP')

        # plt.savefig("solar PV.png")
        figFile = ospath.join(currentFilePath, 'Fig/solar PV.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
    # --------------------------------------solar thermal-------------------------
    if candidateTech['solThem'] == 1 and valBinInst['solThem'] == 1:
        line_bot3, = plt.plot(np.arange(1, len(heatSolThem) + 1), heatSolThem, 'r-', label="heating_solar thermal")
        # plt.legend(handles=[line_up, line_down])
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)

        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)
        plt.xlabel("hour")
        plt.ylabel("heating energy (kW)")
        # plt.title('heat generation from CHP')

        # plt.savefig("solar thermal.png")
        figFile = ospath.join(currentFilePath, 'Fig/solar thermal.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
    # -------------------------------------natural gas boiler------------------------------------
    if candidateTech['NGBoil'] == 1 and valBinInst['NGBoil'] == 1:
        line_bot4, = plt.plot(np.arange(1, len(heatNGBoil) + 1), heatNGBoil / 1000.0, 'g-', label="natural gas boiler")
        # plt.legend(handles=[line_up, line_down])
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)

        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)
        plt.xlabel("hour")
        plt.ylabel("heating energy (MW)")
        # plt.title('heat generation from CHP')

        # plt.savefig("natural gas boiler.png")
        figFile = ospath.join(currentFilePath, 'Fig/natural gas boiler.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
    # ---------------------------------natural gas chiller-------------------------
    if candidateTech['NGChill'] == 1 and valBinInst['NGChill'] == 1:
        line_bot1, = plt.plot(np.arange(1, len(coolNGChill) + 1), coolNGChill / 1000.0, 'violet', linewidth=2.0,
                              label="natural gas chiller")

        # plt.legend(handles=[line_up, line_down])
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)

        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)
        plt.xlabel("hour")
        plt.ylabel("cooling energy (MW)")
        # plt.title('heat generation from CHP')

        # plt.savefig("natural gas chiller.png")
        figFile = ospath.join(currentFilePath, 'Fig/natural gas chiller.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
    # ---------------------heat pump------------------------------------
    if candidateTech['HP'] == 1 and valBinInst['HP'] == 1:

        line_up, = plt.plot(np.arange(1, len(eleHP) + 1), -eleHP, 'b-', linewidth=2.0, label="ele_heat pump")
        if parHP['coolSta'] == 1 and len(nonzeroIndCool):
            line_mid, = plt.plot(np.arange(1, len(coolHP) + 1), coolHP, 'g--', linewidth=2.0, label="cooling_heat pump")
        if parHP['heatSta'] == 1 and len(nonzeroIndHeat):
            line_down, = plt.plot(np.arange(1, len(heatHP) + 1), heatHP, 'r-.', linewidth=2.0,
                                  label="heating_heat pump")
        # line_mid, = plt.plot(x, eleLoad,'g',label="electric load")

        # plt.legend(handles=[line_up, line_down])
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)
        plt.xlabel("hour")
        plt.ylabel("energy for heat pump(kW)")
        # plt.title('heat generation from CHP')

        # plt.savefig("heat pump.png")
        figFile = ospath.join(currentFilePath, 'Fig/heat pump.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()

    # ---------------------electric chiller-----------------------------------
    if candidateTech['eleChill'] == 1 and valBinInst['eleChill'] == 1:
        f, ax = plt.subplots()
        ax.set_title("electric chiller")
        ax.plot(np.arange(1, len(eleEleChill) + 1), eleEleChill / 1000.0, linewidth=2.0)
        ax.set_xlabel("time")
        ax.set_ylabel("electricity consumption (MW)")

        # plt.savefig("electric Chiller.png")
        figFile = ospath.join(currentFilePath, 'Fig/electric Chiller.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.close()

    # -------------------------------electric boiler----------------------------
    if candidateTech['eleBoil'] == 1 and np.allclose(valBinInst['eleBoil'], 1):
        line_bot2, = plt.plot(np.arange(1, len(eleEleBoil) + 1), np.negative(eleEleBoil / 1000.0), 'c--',
                              label="electricity")
        line_bot1, = plt.plot(np.arange(1, len(heatEleBoil) + 1), heatEleBoil / 1000.0, 'r-', label="heat")
        plt.grid(True)
        plt.xlabel("hour")
        plt.ylabel("input/output(MW)")
        # plt.legend(handles=[line_up, line_down])
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        # plt.title('heat generation from CHP')

        # plt.savefig("electric boiler.png")
        figFile = ospath.join(currentFilePath, 'Fig/electric boiler.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
    # =======================heat exchange===========================
    if len(nonzeroIndHeat):
        if candidateTech['heatInds'] == 1:
            line_bot5, = plt.plot(np.arange(1, len(heatInds) + 1), heatInds / 1000.0, 'violet',
                                  label="heating_industry")

        if candidateTech['heatSal'] == 1:
            line_bot1, = plt.plot(np.arange(1, len(heatSal) + 1), np.negative(heatSal / 1000.0), 'y-o',
                                  label="heating_sale")

        plt.grid(True)
        plt.xlabel("hour")
        plt.ylabel("heat(MW)")
        # plt.legend(handles=[line_up, line_down])
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        # plt.title('heat generation from CHP')

        # plt.savefig("heat exchange.png")
        figFile = ospath.join(currentFilePath, 'Fig/heat exchange.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()
    # ==========================absorption chiller===================
    if candidateTech['ABSChill'] == 1 and valBinInst['ABSChill'] == 1 and len(nonzeroIndCool):
        line_up, = plt.plot(np.arange(1, len(coolABSChill) + 1), coolABSChill / 1000.0, 'b--', label="cooling")
        line_down, = plt.plot(np.arange(1, len(heatABSChill) + 1), -heatABSChill / 1000.0, 'r-.', label="heating")
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        plt.xlabel("hour")
        plt.ylabel("input/output(MW)")
        plt.grid(True)
        # plt.legend(handles=[line_up], loc=1)

        # plt.savefig("absorption chiller_cool, heat.png")
        figFile = ospath.join(currentFilePath, 'Fig/absorption chiller.png')
        plt.savefig(figFile, bbox_inches='tight')
        plt.show()
        plt.close()


if __name__ == "__main__":
    draw()
