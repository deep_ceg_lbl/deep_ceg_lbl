# -*- coding: utf-8 -*-
# !/usr/bin/env python

""" Model building process for DEEP.
Longer description of this module.
This program is for: .
"""
'''
the parameters for time, technology, can be adjusted in the module'
the solver information can be changed in the module 'solveOpeDES'
'''

__authors__ = ["Qin Tian<qtian.whu@gmail.com>", "Ming Jin<jinming@berkeley.edu>", "Wei Feng<weifeng@lbl.gov>"]
__contact__ = "Wei Feng"
__copyright__ = "Copyright 2018, CHINA ENERGY GROUP@LBNL"
__date__ = "18-1-22"
__deprecated__ = False
__email__ = "weifeng@lbl.gov"
__license__ = "GPLv3"
__maintainer__ = "Qin Tian, Ming Jing"
__status__ = "Production"
__version__ = "2.0.0"


import math
import copy
import logging
import os

import numpy as np
import numpy.matlib as npmtlb
import scipy.sparse as sps

from solver import gurobi_solver


logging.basicConfig(level=logging.DEBUG)


# heatLoad:
# coolLoad:
# totTimeStep:
# lossCalMode: 'loadFollow, lengthFollow'
# numbldgCluster: number of buildings connected to the pipeline network
# bldgHeatPercent: ditribution of heat load in buildings, size is 1 * numbldgCluster
# bldgCoolPercent: ditribution of cool load in buildings, size is 1 * numbldgCluster
# mainPipeLink:

def calc_radius(heatLoad, coolLoad, mainPipeLink, totHr):
    """
    Calculate the pipe radius according to the lossCalMode.

    :param heatLoad:
    :param coolLoad:
    :param mainPipeLink:connection of buildings with primary pipeline section,
           size is parMainPipe['numMainSec']* numbldgCluster
    :param totHr:
    :return: heatPipeRad, coolPipeRad, heatPipe, coolPipe
    """
    numPipes, numBld = mainPipeLink.shape
    heatPipe, coolPipe = np.zeros((totHr, numPipes)), np.zeros((totHr, numPipes))

    for i in range(numPipes):
        for j in range(numBld):
            if mainPipeLink[i, j] == 1: #energy flows through pipe i to building j
                heatPipe[:, i] += heatLoad[j]
                coolPipe[:, i] += coolLoad[j]
        heatPipe[:, i] = np.sqrt(heatPipe[:, i])
        coolPipe[:, i] = np.sqrt(coolPipe[:, i])

    heatPipeRad = np.sqrt(np.mean(heatPipe, axis=0))
    coolPipeRad = np.sqrt(np.mean(coolPipe, axis=0))

    return heatPipeRad, coolPipeRad, heatPipe, coolPipe

# this code assumes that we have no hourly data for individual building
# so we need to figure out it with the hourly data for building clusters

def pipeLoss_buildnet(heatLoad_cluster, coolLoad_cluster, totTimeStep, lossCalMode, numbldgCluster,
                      mainPipeLink=None, parMainPipe=None):
    """
    This function is defined to calculate the pipeline network loss for heating and cooling energy delivery considering
    the physical structure.

    :param heatLoad_cluster:
    :param coolLoad_cluster:
    :param totTimeStep:
    :param lossCalMode:
    :param numbldgCluster:
    :param mainPipeLink:
    :param parMainPipe:
    :return:
    """

    numbldgCluster = int(numbldgCluster)
    # bldgHeat: heating load for building cluster, dimension is number of time period by number of building cluster
    # bldgCool: cooling load for building cluster,
    bldgHeat = np.zeros((totTimeStep, numbldgCluster), dtype=np.float32)
    bldgCool = np.zeros((totTimeStep, numbldgCluster), dtype=np.float32)

    for i in range(int(numbldgCluster)):
        bldgHeat[:, i] = heatLoad_cluster[i]
        bldgCool[:, i] = coolLoad_cluster[i]

    # number of primary pipe sections
    parMainPipe['numMainSec'] = len(parMainPipe['lenMainSec'])

    # calculate pipeline loss based on energy delivered
    mainPipeHeatLoad = np.zeros((parMainPipe['numMainSec'], totTimeStep), dtype=np.float32)
    mainPipeHeatLoss = np.zeros((parMainPipe['numMainSec'], totTimeStep), dtype=np.float32)

    mainPipeCoolLoad = np.zeros((parMainPipe['numMainSec'], totTimeStep), dtype=np.float32)
    mainPipeCoolLoss = np.zeros((parMainPipe['numMainSec'], totTimeStep), dtype=np.float32)

    if lossCalMode == 'loadFollow':  # lossCalMode['load']==1 and lossCalMode['length']==0:
        # calculate the pipeline loss based on the delivered thermal energy
        for i in range(parMainPipe['numMainSec']):
            # total energy delivered in primary pipeline section
            mainPipeHeatLoad[i, :] = np.sum(bldgHeat * npmtlb.repmat(np.array(mainPipeLink)[i, :], totTimeStep, 1), 1).T
            mainPipeCoolLoad[i, :] = np.sum(bldgCool * npmtlb.repmat(np.array(mainPipeLink)[i, :], totTimeStep, 1), 1).T

            mainPipeHeatLoss[i, :] = np.array(mainPipeHeatLoad)[i, :] * parMainPipe['heatLossCoef'][i]
            mainPipeCoolLoss[i, :] = np.array(mainPipeCoolLoad)[i, :] * parMainPipe['coolLossCoef'][i]

    elif lossCalMode == 'lengthFollow':  # lossCalMode['load']==0 and lossCalMode['length']==1:
        # calculate the pipeline loss based on the length of primary and secondary pipeline sections
        # for i in range(parMainPipe['numMainSec']):
        # the method for heating and cooling loss calculation is similar since it is independent of
        # delivered thermal energy
        mainPipeHeatLoss = npmtlb.repmat(np.asarray(parMainPipe['lenMainSec']) * np.asarray(parMainPipe['lenLossCoef']),
                                         totTimeStep, 1).T
        mainPipeCoolLoss = mainPipeHeatLoss

    pipeHeatLoss = np.sum(mainPipeHeatLoss, 0)  # + np.sum(scndPipeHeatLoss,0)
    pipeCoolLoss = np.sum(mainPipeCoolLoss, 0)  # + np.sum(scndPipeCoolLoss,0)

    return pipeHeatLoss, pipeCoolLoss, mainPipeHeatLoss, mainPipeCoolLoss, bldgHeat, bldgCool


# TODO: the method below can be deleted after candidante1 and candidate2 merged.
def merge_two_dicts(x, y):
    '''Given two dicts, merge them into a new dict as a shallow copy.'''
    z = x.copy()
    z.update(y)
    return z

def nonzero_row_chp(CHParray, parTime, parCHP, nonzeroIndEle, nonzeroIndHeat, nonzeroIndCool):
    """
    Remove zero rows from CHP model based on the operation status of CHP.
    Find out the nonzero columns.
    :param CHParray:
    :param parTime:
    :param parCHP:
    :param nonzeroIndEle:
    :param nonzeroIndHeat:
    :param nonzeroIndCool:
    :return:
    """
    tempIdx = []
    if len(nonzeroIndEle) and parCHP['eleSta']==1:
        # there is non-zero load during the time period
        if len(nonzeroIndEle)<parTime['totHrStep']:
            # there are zero load during the time period
            tempIdx = nonzeroIndEle
        else:
            # there is no zero load during the time period
            return CHParray
    elif len(nonzeroIndHeat) and parCHP['heatSta']==1:
        if len(nonzeroIndHeat)<parTime['totHrStep']:
            tempIdx = nonzeroIndHeat
        else:
            return CHParray
    elif len(nonzeroIndCool) and parCHP['coolSta']==1:
        if len(nonzeroIndCool)<parTime['totHrStep']:
            tempIdx = nonzeroIndCool
        else:
            return CHParray
    else:
        # there are no non-zero load during the time period
        CHParray = []
        return CHParray

    if isinstance(CHParray, dict):
        for name, contents in CHParray.items():
            temp = CHParray[name]
            temp = temp.reshape((parTime['totHrStep'], np.sum(parCHP['numInst'])))
            temp = temp[tempIdx,:]

            CHParray[name] = temp.reshape(len(temp) * np.sum(parCHP['numInst']), 1)
            CHParray[name] = np.squeeze(np.asarray(CHParray[name]))
            del temp

    elif isinstance(CHParray, np.ndarray):
        temp = CHParray
        temp = temp.reshape((parTime['totHrStep'], np.sum(parCHP['numInst'])))
        temp = temp[tempIdx,:]

        CHParray = temp.reshape(len(temp) * np.sum(parCHP['numInst']), 1)
        CHParray = np.squeeze(np.asarray(CHParray))
        del temp

    return CHParray


def end_chg(idxCons, idxCap, paratime, Aeq, beq, idxVar, bCon0):
    """
    Define the final status of battery as the equivalent of initial status.

    Actually, the coefficient of Aeq[idxEqCons["endEleBat"],idxContOpeVar["totEleBat"]] are 0 for hours other than the
    last hour on the date. This will facilitate the structure of idxEqCons["endEleBat"] since it is hour irrelevant.

    The size of idxEqCons["endEleBat"] is numMonth * numDay.

    :param idxCons:
    :param idxCap:
    :param paratime:
    :param Aeq:
    :param beq:
    :param idxVar:
    :param bCon0:
    :return:
    """
    # bCon0: initial soc
    # idxVar: index of total energy
    #idxEleBat = idxCons.copy()
    idxCons = idxCons.reshape(paratime['numMonth'], paratime['numDay'])
    idxVar = idxVar.reshape(paratime['numMonth'], paratime['numDay'], paratime['numHour'])

    # beq[idxCons[:,:]] = bCon0
    Aeq[idxCons[:, :], idxCap] = -bCon0

    for i in range(paratime['numMonth']):
        for j in range(paratime['numDay']):
            Aeq[idxCons[i, j], idxVar[i, j, paratime['numHour'] - 1]] = paratime['stepHr']

    return Aeq, beq


def dym_chg(idxCons, idxCap, partime, Aeq, beq, bCon0, preCon, idxPreTotSto, mode=None):
    """
    In order to take initial SOC into consideration.

    :param idxCons:
    :param idxCap:
    :param partime:
    :param Aeq:
    :param beq:
    :param bCon0:
    :param preCon:
    :param idxPreTotSto:
    :param mode:
    :return:
    """
    # idxCap: index of installation capacity of energy storage system
    # bCon0: constant for dynamic charging constraint when h = 0
    # preCon: coefficient  for dynamic charging constraint when h > 0
    # idxEleBat = idxCons.copy()
    idxCons = idxCons.reshape(partime['numMonth'], partime['numDay'], partime['numHour'])

    # this is specifying the first hour of the first day
    Aeq[idxCons[0, 0, 0], idxCap] += bCon0

    for i in range(partime['numMonth']):
        for j in range(partime['numDay']):
            # specify the initial value of SoC at h = 0
            # beq[idxCons[i,j,0]] = bCon0
            '''
            if mode is 'cycleCharge':
                # this is because the end of the previous day is the same as the initial soc at the first day under cycle charge mode
                # this has been realized through endchg module
                # capacity variable only appear at the beginning of the first day
                Aeq[idxCons[i,j,0], idxCap] = bCon0
            '''
            # starting from h = 1
            for k in range(partime['numHour']):
                if i == 0 and j == 0:
                    # the first day of the first month
                    if k > 0:
                        Aeq[idxCons[i, j, k], idxPreTotSto[i, j, k]] = preCon
                else:
                    Aeq[idxCons[i, j, k], idxPreTotSto[i, j, k]] = preCon

    return Aeq, beq


def cal_idx_cons(numinstcons, numhrlyeq, numeqchpcons, paratime, idxeqcons, idxeqconschp, numeqconspertypechp, parchp=None):
    # for each hour, the number of equal constraints, including those of CHP plants
    totNumHrEqCons = numhrlyeq + numeqchpcons
    # total number of equality constraints during the whole time horizon
    totCons = totNumHrEqCons * paratime['totHrStep'] + numinstcons

    # index of equality constraint
    numEqCons = np.asarray([numinstcons + totNumHrEqCons * k + totNumHrEqCons * paratime['numHour'] * j +
                            totNumHrEqCons * paratime['numHour'] * paratime['numDay'] * i
                            for i in range(paratime['numMonth'])
                            for j in range(paratime['numDay'])
                            for k in range(paratime['numHour'])])

    logging.debug("numEqCons in cal_idx_cons:{}".format(numEqCons))
    # numEqCons = numEqCons.reshape((numMonth, numDay, numHour))

    idxCons = {}
    for name, contents in idxeqcons.items():
        idxCons[name] = contents + numEqCons

    idxConsCHP = []
    if parchp is not None:
        numEqConsMat = numEqCons.reshape((paratime['numMonth'], paratime['numDay'], paratime['numHour']))
        idxEqPerTypeCHP = np.asarray([numeqconspertypechp * m + numhrlyeq + numEqConsMat[i, j, k]
                                      for i in range(paratime['numMonth'])
                                      for j in range(paratime['numDay'])
                                      for k in range(paratime['numHour'])
                                      for m in range(np.sum(parchp['numInst']))])

        idxConsCHP = {}
        for name, contents in idxeqconschp.items():
            idxConsCHP[name] = contents + idxEqPerTypeCHP

    return totCons, idxCons, idxConsCHP


def calc_idx_sto(totSto, parTime, rnumVar):
    """
    Calculates index of dynamic behavior of storage system.

    :param totSto:
    :param parTime:
    :param rnumVar:
    :return:
    """
    # is initializing necessary ???
    idxPreTotSto1 = np.zeros((parTime['numMonth'], parTime['numDay'], parTime['numHour']), int)
    idxPreTotSto1[:, :, :] = totSto + rnumVar[:, :, 0:parTime['numHour']]
    idxPreTotSto1 = idxPreTotSto1.reshape((1,parTime['totHrStep']))
    temp = np.zeros((1,parTime['totHrStep']+1), int)
    temp[0][1:] = idxPreTotSto1
    idxPreTotSto = temp[0][0:parTime['totHrStep']].reshape((parTime['numMonth'], parTime['numDay'], parTime['numHour']))
    return idxPreTotSto


def build(pipeNetwFlag, parThemPipe=None, ctrStr=None, eleLoad=None,
          coolLoad=None, heatLoad=None, bigM=None, smallM=None,
          itemParCHP=None, itemParTime=None, itemLenLifeYr=None, itemCandidateTech1=None, itemCandidateTech2=None,
          itemForceSelect=None,
          itemParPV=None, itemParSolThem=None, itemParEleBoil=None, itemParNGBoil=None, itemParHeatTank=None,
          itemParCoolTank=None, itemParEleBat=None, itemParHP=None, itemParNGChill=None, itemParABSChill=None,
          itemParEleChill=None,
          itemParEleGrid=None, itemParHeat=None, rateMonthDemandCharge=None, emiFac=None, taxCO2=None, lossCalMode=None,
          numbldgCluster=None, mainPipeLink=None, parMainPipe=None):

    if ctrStr is None:
        raise Exception("ctrStr can not be None")

    if ctrStr['carbOnly']:
        ctrStr['taxCO2Flag'] = 1

    sampleTimeFlag = ctrStr['sampleTimeFlag']
    sparseProbFlag = ctrStr['sparseProbFlag']
    probReduceFlag = ctrStr['probReduceFlag']

    taxCO2Flag = ctrStr['taxCO2Flag']
    carbOnly = ctrStr['carbOnly']



    if itemParTime is None:
        raise Exception("itemParTime can be None.")
    # TODO: to be deleted.
    parTime = itemParTime

    # TODO: what is the difference between itemIdxLifeYr and itemLenLifeYr?
    # There already has life info in every technology parameter set.
    # if itemIdxLifeYr is None:

    idxLifeYr = {'PV': 0,
                 'solThem': 1,
                 'HP': 2,
                 'eleBoil': 3,
                 'NGBoil': 4,
                 'NGChill': 5,
                 'ABSChill': 6,
                 'eleChill': 7,
                 'heatTank': 8,
                 'eleBat': 9,
                 'coolTank': 10
                 }

    # lifetime definition for each technology
    # [CHP,  solar PV,  solar thermal,     heat pump, electric boiler, natural gas boiler,
    #     natural gas fired absorption chiller, heat fired chiller, electric chiller, heat storage, electric storage]
    #
    if itemLenLifeYr is None:
        raise Exception("itemLenLifeYr can be None")

    # TODO lifeYear should be deleted.
    lifeYear = itemLenLifeYr

    if itemCandidateTech1 is None:
        raise Exception("itemCandidateTech1 can be not None.")

    candidateTech1 = itemCandidateTech1

    if itemCandidateTech2 is None:
        raise Exception("itemCandidateTech2 can not be None")

    candidateTech2 = itemCandidateTech2

    candidateTech = merge_two_dicts(candidateTech1, candidateTech2)

    if bigM is None:
        raise Exception("bigM can not be None")

    if smallM is None:
        raise Exception("smallM can not be None")

    # ----------------------------------------------------------------------------------------------------------------
    # ---------------------------------1. Solar PV -------------------------------------------------------------------

    # varIns = 100  # capital cost of solar PV panels, $/kW
    # unitOutPV = 10  # rated electricity generation per unit area of PV panel system, kW/m^2
    # perfRatio = 0.75 # performance ratio / irradiation efficiency
    # solYield=0.15 # solar panel yield
    # E_PV <= A_PV * perfRatio * solYield * unitOut

    parPV = {}
    if candidateTech['PV'] == 1:
        if itemParPV is None:
            raise Exception("PV has been selected as a candidate tech, paramter itemParaPV can not be None.")

        parPV = itemParPV

        if len(np.shape(parPV['mtnPricePV'])) != 1:
            parPV['mtnPricePV'] = parPV['mtnPricePV'].reshape((parTime['totHrStep'],))

        #if parPV['solIrradiation'] == None:
        #    pass
            # TODO: load solar irradiation data from the database.

        if len(np.shape(parPV['solIrradiation'])) != 1:
            parPV['solIrradiation'] = parPV['solIrradiation'].reshape((parTime['totHrStep'],))

    # -----------------------------------------------------------------------------------------------
    # ------------------------------2. Solar Thermal ------------------------------------------------
    #
    # varIns = 100  # capital cost of solar thermal, $/kW
    # unitOutSolThem = 10  # rated electricity generation per unit area of PV panel system, kW/m^2
    # H_SolThem <= A_SolThem * irrEff * unitOut
    parSolThem = {}
    if candidateTech['solThem'] == 1:
        if itemParSolThem is None:
            raise Exception("itemParSolThem can not be None.")

        parSolThem = itemParSolThem

        if len(np.shape(parSolThem['mtnPriceSolThem'])) != 1:
            parSolThem['mtnPriceSolThem'] = parSolThem['mtnPriceSolThem'].reshape((parTime['totHrStep'],))

        if len(np.shape(parSolThem['solIrradiation'])) != 1:
            parSolThem['solIrradiation'] = parSolThem['solIrradiation'].reshape((parTime['totHrStep'],))

    # ------------------------------------------------------------------------------------------------------
    # ---------------------------------2. CHP --------------------------------------------------------------

    # define the number & capacity of discrete CHP technologies
    # typeCHP = 5  # number of types of CHP plant
    # capCostCHP = [100, 200, 300, 400, 500]  # capital cost of CHP plant, $/kW
    # capGenCHP = [10, 20, 30, 40, 50]  # rated capacity of CHP, kW
    # eleEffCHP = [0.4, 0.5, 0.6, 0.7, 0.9]  # electrical efficiency of CHP
    # heatEffCHP = [0.1, 0.2, 0.3, 0.4, 0.5]  # factor relating electricity generation to heat supply from CHP
    # dict structure is a little different from {} since in {}, each element for parCHP is a list
    # in dict parPV, each element is a int or float

    # 'heatSta', 'coolSta', added 09/20/2015, used to detece when CHP should be considered
    # when CHP is considered for heating purpose, heatSta =1
    # when CHP is considered for electricity purpose, eleSta =1
    # when CHP is used for cooling purpose, providing heating for ABSChiller, coolSta = 1
    # CHP efficiency data see http://www.epa.gov/chp/basic/methods.html
    # 'coolSta'=1: this condition means no heating or electricity load,
    #              but CHP is used to provide heating energy for cooling purpose
    # 'heatSta' =1: this parameter is always be 1, especially
    #              with the condition -- no electricity demand, but CHP is used for heating purpose
    # 'eleSta' = 1: CHP is used either for electricity, heating or cooling purpose,
    #              this parameter should always be 1
    # if candidataTech['CHP']=1, 'heatSta' =1, 'eleSta' = 1
    # actuallly we can keep 'coolSta' while deleting the 'heatSta' and 'eleSta'

    parCHP = {}
    if candidateTech['CHP'] == 1:
        if itemParCHP is None:
            raise Exception(" itemParCHP can not be None.")

        parCHP = itemParCHP

        parCHP['numType'] = len(parCHP['capGen'])
        # TODO:
        parCHP['minHrEle'] = [bigM, bigM, bigM, bigM]  # This is wrong!
    else:
        parCHP['numType'] = 0

    # ------------------------------------------------------------------------------------------------------
    # -----------------------------------3. Electric Boiler ------------------------------------------------

    # fixInsEleBoil = 100  # fixed installation cost, $
    # varInsEleBoil = 200  # variable installation cost, $/kW
    # themEffEleBoil = 0.9  # thermal efficiency of electric boiler converting from electricity to heat
    parEleBoil = {}
    if candidateTech['eleBoil'] == 1:
        if itemParEleBoil is None:
            raise Exception("itemParEleBoil can be None.")

        parEleBoil = itemParEleBoil

    # ---------------------------------------------------------------------------------------------------------
    # -----------------------------------4. natural gas boiler ------------------------------------------------
    #
    # fixInsNGBoil = 100  # fixed installation cost, $
    # varInsNGBoil = 200  # variable installation cost, $/kW
    # themEffNGBoil = 0.9  # thermal efficiency of natural gas boiler converting natural gas to heat
    parNGBoil = {}
    if candidateTech['NGBoil'] == 1:
        if itemParNGBoil is None:
            raise Exception("itemParNGBoil can not be None.")

        parNGBoil = itemParNGBoil

        if len(np.shape(parNGBoil['gasPriceNGBoil'])) > 1:
            parNGBoil['gasPriceNGBoil'] = parNGBoil['gasPriceNGBoil'].reshape((parTime['totHrStep'],))

        if len(np.shape(parNGBoil['mtnPriceNGBoil'])) > 1:
            parNGBoil['mtnPriceNGBoil'] = parNGBoil['mtnPriceNGBoil'].reshape((parTime['totHrStep'],))

    # ----------------------------------------------------------------------------------------------------------
    # -----------------------------------------5. thermal storage ----------------------------------------------
    #
    # fixInsHeatTank = 0  # fixed installation cost, $
    # varInsHeatTank = 0  # variable installation cost, $/kW
    # chgEffHeatTank = 0.95  # fraction of stored heat into water tank that is not lost in energy transfer
    # disEffHeatTank = 0.87  # fraction of released heat into water tank that is not lost in energy transfer
    # decayFacHeatTank = 0.12  # fraction of heat in water tank that is lost in one time period
    # iniTotHeatTank = 0  # initial heat storage, kW
    parHeatTank = {}
    if candidateTech['heatTank'] == 1:
        if itemParHeatTank is None:
            raise Exception("itemParHeatTank can not be None.")

        parHeatTank = copy.deepcopy(itemParHeatTank)

        # modified on 10/30/15, introduced to linearize heating storage model
        parHeatTank['maxChg'] = parHeatTank['maxCap'] * parHeatTank['maxFracChg'] * parHeatTank['unitOut']
        parHeatTank['minChg'] = parHeatTank['maxCap'] * parHeatTank['minFracChg'] * parHeatTank['unitOut']
        parHeatTank['maxDis'] = parHeatTank['maxCap'] * parHeatTank['maxFracDis'] * parHeatTank['unitOut']
        parHeatTank['minDis'] = parHeatTank['maxCap'] * parHeatTank['minFracDis'] * parHeatTank['unitOut']

        # 10/20/15, introduced to limit P(CST_IN), P(CST_OUT)
        parHeatTank['maxIn'] = parHeatTank['maxCap']
        parHeatTank['minIn'] = smallM  # parHeatTank['minCap']
        parHeatTank['maxOut'] = parHeatTank['maxCap']
        parHeatTank['minOut'] = smallM  # parHeatTank['minCap']

        if len(np.shape(parHeatTank['mtnPriceHeatTank'])) > 1:
            parHeatTank['mtnPriceHeatTank'] = parHeatTank['mtnPriceHeatTank'].reshape((parTime['totHrStep'],))

    # --------------------------------------------------------------------------------------------------------
    # -----------------------------------------. Cooling Storage (cool tank) --------------------------------

    parCoolTank = {}
    if candidateTech['coolTank'] == 1:
        if itemParCoolTank is None:
            raise Exception("itemParCoolTank can not be None.")

        parCoolTank = itemParCoolTank

        # modified on 10/16/15, introduced to linearize cooling storage model
        parCoolTank['maxChg'] = parCoolTank['maxCap'] * parCoolTank['maxFracChg'] * parCoolTank['unitOut']
        parCoolTank['minChg'] = parCoolTank['maxCap'] * parCoolTank['minFracChg'] * parCoolTank['unitOut']
        parCoolTank['maxDis'] = parCoolTank['maxCap'] * parCoolTank['maxFracDis'] * parCoolTank['unitOut']
        parCoolTank['minDis'] = parCoolTank['maxCap'] * parCoolTank['minFracDis'] * parCoolTank['unitOut']

        # 10/20/15, introduced to limit P(CST_IN), P(CST_OUT)
        parCoolTank['maxIn'] = parCoolTank['maxCap']
        parCoolTank['minIn'] = smallM  # parCoolTank['minCap']
        parCoolTank['maxOut'] = parCoolTank['maxCap']
        parCoolTank['minOut'] = smallM  # parCoolTank['minCap']

        if len(np.shape(parCoolTank['mtnPriceCoolTank'])) > 1:
            parCoolTank['mtnPriceCoolTank'] = parCoolTank['mtnPriceCoolTank'].reshape((parTime['totHrStep'],))

    # ---------------------------------------------------------------------------------------------------------
    # ----------------------------------------6. Electric battery ---------------------------------------------

    # fixInsEleBat = 0  # fixed installation cost
    # varInsEleBat = 0  # variable installation cost
    # chgEffEleBat = 0.95  # fraction of charged electricity into battery that is not lost in energy transfer
    # disEffEleBat = 0.87  # fraction of discharged electricity from battery that is not lost in energy transfer
    # decayFacEleBat = 0.12  # fraction of electricity in battery that is lost in one time period
    # iniTotEleBat = 0  # initial electricity storage, kW
    #
    # creating a dictionary for input parameters of electric battery

    # 11/18/15, mode feature added: {'priceFollow', 'cycleCharge'}
    # 'price Follow': which means no final soc requirements imposed at the end of each day,
    #                 the charging strategy of battery tank will follow the electricity tariff.
    # 'cycle charging': no matter the electricity tariff, battery tank will charge to
    #                 make sure the final soc will reach the same level as the initial status
    parEleBat = {}
    if candidateTech['eleBat'] == 1:
        if itemParEleBat is None:
            raise Exception("itemParEleBat can not be None.")

        parEleBat = itemParEleBat

        # modified on 10/30/15, introduced to linearize cooling storage model
        parEleBat['maxChg'] = parEleBat['maxCap'] * parEleBat['maxFracChg'] * parEleBat['unitOut']
        parEleBat['minChg'] = parEleBat['maxCap'] * parEleBat['minFracChg'] * parEleBat['unitOut']
        parEleBat['maxDis'] = parEleBat['maxCap'] * parEleBat['maxFracDis'] * parEleBat['unitOut']
        parEleBat['minDis'] = parEleBat['maxCap'] * parEleBat['minFracDis'] * parEleBat['unitOut']

        # 10/20/15, introduced to limit P(CST_IN), P(CST_OUT)
        parEleBat['maxIn'] = parEleBat['maxCap']
        parEleBat['minIn'] = smallM  # parEleBat['minCap']
        parEleBat['maxOut'] = parEleBat['maxCap']
        parEleBat['minOut'] = smallM  # parEleBat['minCap']

        if len(np.shape(parEleBat['mtnPriceEleBat'])) > 1:
            parEleBat['mtnPriceEleBat'] = parEleBat['mtnPriceEleBat'].reshape((parTime['totHrStep'],))

    # ------------------------------------------------------------------------------------------------------
    # -------------------------------------7. Heat Pump ----------------------------------------------------
    #
    # fixInsHP = 0  # fixed installation cost
    # varInsHP = 0  # variable installation cost
    # coolSta: determine the constraints related to cooling purpose
    # heatSta: determine the constraints related to heating purpose
    parHP = {}
    if candidateTech['HP'] == 1:
        if itemParHP is None:
            raise Exception("itemParHP can not be None.")

        parHP = itemParHP

        if len(np.shape(parHP['mtnPriceHP'])) > 1:
            parHP['mtnPriceHP'] = parHP['mtnPriceHP'].reshape((parTime['totHrStep'],))

    # ----------------------------------------------------------------------------------------------
    # -----------------------------8. Natural gas chiller ------------------------------------------
    #
    # fixInsNGChill = 0  # fixed installation cost
    # varInsNGChill = 0  # variable installation cost
    # themEffNGChill = 0.52  # coefficient of converting cooling to heat
    # copNGChill = 1.2  # coefficient of performance
    parNGChill = {}
    if candidateTech['NGChill'] == 1:
        if itemParNGChill is None:
            raise Exception("itemParNGChill can not be None.")

        parNGChill = itemParNGChill

        if len(np.shape(parNGChill['gasPriceNGChill'])) > 1:
            parNGChill['gasPriceNGChill'] = parNGChill['gasPriceNGChill'].reshape((parTime['totHrStep'],))


        if len(np.shape(parNGChill['mtnPriceNGChill'])) > 1:
            parNGChill['mtnPriceNGChill'] = parNGChill['mtnPriceNGChill'].reshape((parTime['totHrStep'],))

    # ------------------------------------------------------------------------------------------------------
    # ----------------------------9. heat fired absorption chiller --------------------------------------------
    #
    # fixInsABSChill = 0  # fixed installation cost
    # varInsABSChill = 0  # variable installation cost
    # copABSChill = 1.2  # coefficient of performance
    parABSChill = {}
    if candidateTech['ABSChill'] == 1:
        if itemParABSChill is None:
            raise Exception("itemParABSChill can not be None.")

        parABSChill = itemParABSChill

    # ---------------------------------------------------------------------------------------------------------
    # ---------------------------------10. Electric chiller ---------------------------------------------------
    #
    # fixInsEleChill = 0  # fixed installation cost
    # varInsEleChill = 0  # variable installation cost
    # copEleChill = 1.3  # coefficient of performance

    parEleChill = {}
    if candidateTech['eleChill'] == 1:
        if itemParEleChill is None:
            raise Exception("itemParEleChill can not be None.")

        parEleChill = itemParEleChill

    # TODO no pipeline data?
    # ----------------------------------------------------------------------------------------------------------
    # ----------------------------11. pipeline -----------------------------------------------------------------
    #
    # coolDistEff = 0.85  # distribution efficiency of pipeline for cooling energy
    # heatDistEff = 0.9  # distribution efficiency of pipeline for heat energy
    # 10/30/15, the pipeline loss model should be extended based on networkX

    # -----------------------------------------------------------------------------------------------------------
    # -----------------------------12 Electric Grid -------------------------------------------------------------
    # 12, electric grid

    # outType defines whether purchase and sell electricity are allowed at the same time
    # outType=1 means only one of action is permitted
    # outType=2 means both actions are permitted

    parEleGrid = {}
    if candidateTech['eleGrid'] == 1:
        if itemParEleGrid is None:
            raise Exception("itemParEleGrid can not be None.")

        parEleGrid = itemParEleGrid

        # if (parTime['numMonth']>1 or parTime['numDay']>1 or parTime['numHour']>1):
        if len(np.shape(parEleGrid['salPriceEleGrid'])) > 1:
            parEleGrid['salPriceEleGrid'] = parEleGrid['salPriceEleGrid'].reshape((parTime['totHrStep'],))

        if len(np.shape(parEleGrid['purPriceEleGrid'])) > 1:
            parEleGrid['purPriceEleGrid'] = parEleGrid['purPriceEleGrid'].reshape((parTime['totHrStep'],))

    # purchase waste heat from industry or sell extra heat out
    # TODO: parHeat can be deleted.
    parHeat = itemParHeat
    if candidateTech['heatSal'] == 1 or candidateTech['heatInds'] == 1:
        if itemParHeat is None:
            raise Exception(" itemParHeat can not be None.")

        if candidateTech['heatInds'] == 1 and len(np.shape(parHeat['purPriceInds'])) > 1:
            parHeat['purPriceInds'] = parHeat['purPriceInds'].reshape((parTime['totHrStep'],))

        if candidateTech['heatSal'] == 1 and len(np.shape(parHeat['salPriceHeat'])) > 1:
            parHeat['salPriceHeat'] = parHeat['salPriceHeat'].reshape((parTime['totHrStep'],))

    # Initialize emiFac and taxCO2
    if emiFac is None:
        raise Exception(" emiFac can not be None")

    if taxCO2 is None:
        raise Exception("taxCO2 can not be None")

    # 08/06/2015, add demand charge, $/kW
    if candidateTech['demandCharge'] == 1 and rateMonthDemandCharge is None:
        raise Exception("parameter rateMonthDemandCharge can not be None while demangCharge is one of the candidates")

    # index of nonzero elements / index of zero elements
    # 09/20/2015, the energy demand should not only be based on the energy load
    # it should take consideration of energy demand for energy conversion technologies
    # for example, if there is no heating demand, but the heating from CHP could be provided as input for ABSchill
    # then we should have heating balance constraint which is used to balance heating supply from CHP and heating
    # demand for ABSchill

    if np.max(eleLoad) == 0 and np.max(coolLoad) == 0 and np.max(heatLoad) == 0:
        raise Exception("no energy demand exists! the optimization problem cannot be built.")

    # FIXME: detailed running loads here.

    # ---------------------------------pipeline data here -----------------------------------------
    # del heatLoad1
    # 11, pipeline
    # 10/30/15, the pipeline loss model is extended based on network
    pipeHeatLoss = []
    pipeCoolLoss = []
    mainPipeHeatLoss = []
    mainPipeCoolLoss = []
    bldgHeat = []
    bldgCool = []

    if pipeNetwFlag == 1:
        # number of building cluster
        if numbldgCluster is None:
            raise Exception("numbldgCluster can not be None.")

        [pipeHeatLoss, pipeCoolLoss, mainPipeHeatLoss, mainPipeCoolLoss, bldgHeat, bldgCool] = pipeLoss_buildnet(
            heatLoad_cluster=heatLoad, coolLoad_cluster=coolLoad, totTimeStep=parTime['totHrStep'], lossCalMode=lossCalMode,
            numbldgCluster=numbldgCluster, mainPipeLink=mainPipeLink, parMainPipe=parMainPipe)
    else:
        if parThemPipe is None:
            raise Exception("parThemPipe can not be None.")

    # TODO: What does it mean here?
    # because of the complicated relationship between CHP and absorption chiller
    # it is hard to distinguish the nonzeroEle and nonzeroHeat, nonzeroCool
    # so we decide to set them as the same, with the value of the whole time horizon

    # eleLoad
    nonzeroIndEle = np.arange(0, len(eleLoad))
    zeroIndEle = []
    if candidateTech['eleBat'] == 0:
        assert (len(np.shape(eleLoad)) == 1), 'EleLoad should be a one dimensional array'
        nonzeroIndEle = np.nonzero(eleLoad)[0]
        zeroIndEle = np.where(eleLoad == 0)[0]


    # heatLoad
    nonzeroIndHeat = np.arange(0, len(heatLoad))
    zeroIndHeat = []
    if candidateTech['heatTank'] == 0:
        assert (len(np.shape(heatLoad)) == 1), 'heatLoad should be a one dimensional array'
        nonzeroIndHeat = np.nonzero(heatLoad)[0]
        zeroIndHeat = np.where(heatLoad == 0)[0]


    # coolLoad
    nonzeroIndCool = np.arange(0, len(coolLoad))
    zeroIndCool = []
    if candidateTech['coolTank'] == 0:
        assert (len(np.shape(coolLoad)) == 1), 'coolLoad should be a one dimensional array.'
        nonzeroIndCool = np.nonzero(coolLoad)[0]
        zeroIndCool = np.where(coolLoad == 0)[0]


    oriNonzeroIndCool = nonzeroIndCool.copy()
    oriNonzeroIndHeat = nonzeroIndHeat.copy()
    oriNonzeroIndEle = nonzeroIndEle.copy()

    # 09/18/2015, a special case, when CHP is used to provide heating for chiller in the summer,
    # even though there is no heating load in the summer season.
    #
    # we will still have the heating balance to make sure the heating supply from CHP is equivalent to the heating
    #  demand for chiller
    #
    # electricity balance may also need even there is no electricity demand since some techs need electricity
    # as energy input such as CHP

    # CHP
    # case1: this condition means no heating load, but CHP is used to provide heating energy for cooling purpose
    temp1 = np.arange(0, parTime['totHrStep'])
    # case 1
    # ABSChill for cooling, put nonzerocool into nonzeroheat
    # or CHP for cooling purpose, not providing heating or electricity at nonzerocool period

    if len(nonzeroIndCool) and len(nonzeroIndHeat) < parTime['totHrStep'] and not np.array_equal(nonzeroIndHeat,
                                                                                                 nonzeroIndCool):
        if candidateTech['ABSChill'] == 1 or (candidateTech['CHP'] == 1 and parCHP['coolSta'] == 1):
            nonzeroIndHeat = np.unique(np.hstack((nonzeroIndHeat, nonzeroIndCool)))
            zeroIndHeat = np.array(sorted(list(set(temp1) - set(nonzeroIndHeat))))

    # electric chiller, put nonzeroIdxCool into nonzeroIdxEle
    # should we combine nonzeroIdxEle and nonzeroIdxCool when parCHP['coolSta']==1 and parCHP['eleSta']==1???

    if len(nonzeroIndEle) < parTime['totHrStep'] and len(nonzeroIndCool) and not np.array_equal(nonzeroIndEle,
                                                                                                nonzeroIndCool):
        if candidateTech['eleChill'] == 1 or (candidateTech['CHP'] == 1 and parCHP['coolSta'] == 1) \
                or (candidateTech['HP'] == 1 and parHP['coolSta'] == 1):
            nonzeroIndEle = np.unique(np.hstack((nonzeroIndEle, nonzeroIndCool)))
            zeroIndEle = np.array(sorted(list(set(temp1) - set(nonzeroIndEle))))

    # heat pump for heating, put nonzeroheat into nonzeroele
    if len(nonzeroIndHeat) and len(nonzeroIndEle) < parTime['totHrStep'] \
            and not np.array_equal(nonzeroIndEle, nonzeroIndHeat):
        if (candidateTech['HP'] == 1 and parHP['heatSta'] == 1) or candidateTech['eleBoil'] == 1 \
                or (candidateTech['CHP'] == 1 and parCHP['heatSta'] == 1 and parCHP['eleSta'] == 0):
            nonzeroIndEle = np.unique(np.hstack((nonzeroIndEle, nonzeroIndHeat)))
            zeroIndEle = np.array(sorted(list(set(temp1) - set(nonzeroIndEle))))

    # case 2
    # CHP for heating purpose,  put nonzeroheat into nonzeroele
    # it means CHP is not providing electricity at all nonzeroele period
    '''
    if candidateTech['CHP'] == 1 and parCHP['heatSta'] == 1 and parCHP['eleSta'] == 0 and len(nonzeroIndHeat) \
            and len(nonzeroIndEle) < parTime['totHrStep'] and not np.array_equal(nonzeroIndEle,nonzeroIndHeat):
        nonzeroIndEle = np.unique(np.hstack((nonzeroIndHeat, nonzeroIndEle)))         
        zeroIndEle = np.array(sorted(list(set(temp1)-set(nonzeroIndEle))))
    '''
    # case 3
    # CHP for electric purpose,  put nonzeroEle into nonzeroHeat
    # it means CHP is not providing heating energy at all nonzeroHeat period
    # if candidateTech['CHP'] == 1 and parCHP['eleSta'] == 1 and parCHP['heatSta'] == 0 and len(nonzeroIndEle) \
    #        and len(nonzeroIndHeat) < parTime['totHrStep'] and not np.array_equal(nonzeroIndEle,nonzeroIndHeat):
    if len(nonzeroIndEle) and len(nonzeroIndHeat) < parTime['totHrStep'] and not np.array_equal(nonzeroIndEle,
                                                                                                nonzeroIndHeat):
        if candidateTech['CHP'] == 1 and parCHP['eleSta'] == 1:  # and parCHP['heatSta'] == 0
            nonzeroIndHeat = np.unique(np.hstack((nonzeroIndHeat, nonzeroIndEle)))
            zeroIndHeat = np.array(sorted(list(set(temp1) - set(nonzeroIndHeat))))

    # case 4
    # CHP for both electricity and heating purpose
    # this way put all nonzeroele and nonzeroheat together
    # if candidateTech['CHP'] == 1 and parCHP['eleSta'] == 1 and parCHP['heatSta'] == 1 and len(nonzeroIndHeat) \
    #        and len(nonzeroIndEle) and not np.array_equal(nonzeroIndEle,nonzeroIndHeat):
    if len(nonzeroIndHeat) and len(nonzeroIndEle) and not np.array_equal(nonzeroIndEle, nonzeroIndHeat):
        if candidateTech['CHP'] == 1 and parCHP['eleSta'] == 1 and parCHP['heatSta'] == 1:
            if len(nonzeroIndEle) == parTime['totHrStep'] and len(nonzeroIndHeat) < parTime['totHrStep']:
                # in case 4, this situation means case 1 is redundant by putting nonzeroIndCool into nonzeroIndHeat
                nonzeroIndHeat = nonzeroIndEle
                zeroIndHeat = zeroIndEle
            elif len(nonzeroIndEle) < parTime['totHrStep'] and len(nonzeroIndHeat) == parTime['totHrStep']:
                nonzeroIndEle = nonzeroIndHeat
                zeroIndEle = zeroIndHeat
            elif len(nonzeroIndEle) < parTime['totHrStep'] and len(nonzeroIndHeat) < parTime['totHrStep']:
                temp = np.unique(np.hstack((nonzeroIndHeat, nonzeroIndEle)))
                nonzeroIndHeat = temp
                nonzeroIndEle = temp
                temp2 = np.array(sorted(list(set(temp1) - set(temp))))
                zeroIndHeat = temp2
                zeroIndEle = temp2

    # =======================================================================================================================
    # ' variable definition for each technology in the central plant'
    #
    # '''
    # areaPV = 1  # installation area of solar PV panel
    # areaSolTherm = 2  # installation area of solar thermal, m^2

    # capNGBoil = 3  # capacity of natural gas boiler
    # capNGChill = 4  # capacity of natural gas chiller
    # capHeatTank = 7  # heat capacity of thermal storage
    # capEleBat = 8  # electricity capacity of electric battery
    # capEleChill = 9  # cooling capacity of electric chiller
    # capHP = 10  # capacity of heat pump

    # capEleBoil = 13  # heat capacity of electric boiler
    # capABSChill = 14  # cooling capacity of heat fired absorption chiller
    # coolTank = 11, # cooling capacity of thermal storage, kWh
    # '''

    # should we start the numbering of index from 0???
    # the value in selectTech is binary, either '0' or '1'

    # the dictionary 'idxContInstVar' and 'idxBinInstVar' are easy to deal with since there is a unique element
    # for each tech
    # but 'idxContOpeVar' and 'idxBinOpeVar' are more difficult since there are more than one var for a single tech

    # Index of continuous installation variables.
    idxContInstVar = {}

    # Index of binary installation variables.
    idxBinInstVar = {}

    # Index of continuous operation variables
    idxContOpeVar = {}

    # Index of binary operation variables
    idxBinOpeVar = {}

    # Continuous installation variable
    instCountTech = int(0)

    # Continuous operation variable
    opeCountTech = 0

    # Binary operation variable
    binOpeCount = 0

    for name, contents in candidateTech.items():

        if candidateTech[name] == 1 and name not in candidateTech2:
            instCountTech += 1
            # continuous installation variable
            idxContInstVar[name] = instCountTech

            # binary installation variable
            idxBinInstVar[name] = instCountTech

            if name == 'NGBoil' and len(nonzeroIndHeat):
                # continuous operation variable
                opeCountTech += 1
                idxContOpeVar['gasNGBoil'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['heatNGBoil'] = opeCountTech

            elif name == 'NGChill' and len(nonzeroIndCool):
                # continuous operation variable
                opeCountTech += 1
                idxContOpeVar['gasNGChill'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['coolNGChill'] = opeCountTech

            elif name == 'PV' and len(nonzeroIndEle):
                # continuous operation variable
                opeCountTech += 1
                idxContOpeVar['elePV'] = opeCountTech

            elif name == 'solThem' and len(nonzeroIndHeat):
                # continuous operation variable
                opeCountTech += 1
                idxContOpeVar['heatSolThem'] = opeCountTech

            elif name == 'eleBat' and len(nonzeroIndEle):
                # continuous operation variable
                opeCountTech += 1
                idxContOpeVar['stoEleBat'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['fromEleBat'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['totEleBat'] = opeCountTech

                # added on 10/30/15, linearize energy storage model
                # we need to do this similarly for electricity storage and heating storage
                opeCountTech += 1
                idxContOpeVar['upInYEleBat'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['minInYEleBat'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['upOutYEleBat'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['minOutYEleBat'] = opeCountTech

                # binary operation variable
                binOpeCount += 1
                idxBinOpeVar['chgEleBat'] = binOpeCount
                binOpeCount += 1
                idxBinOpeVar['disEleBat'] = binOpeCount

            elif name == 'heatTank' and len(nonzeroIndHeat):
                # continuous operation variable
                opeCountTech += 1
                idxContOpeVar['stoHeatTank'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['fromHeatTank'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['totHeatTank'] = opeCountTech

                # added on 10/30/15, linearize energy storage model
                # we need to do this similarly for electricity storage and heating storage
                opeCountTech += 1
                idxContOpeVar['upInYHeatTank'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['minInYHeatTank'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['upOutYHeatTank'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['minOutYHeatTank'] = opeCountTech

                # binary operation variable
                binOpeCount += 1
                idxBinOpeVar['chgHeatTank'] = binOpeCount
                binOpeCount += 1
                idxBinOpeVar['disHeatTank'] = binOpeCount

            elif name == 'coolTank' and len(nonzeroIndCool):
                # continuous operation variable
                opeCountTech += 1
                idxContOpeVar['stoCoolTank'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['fromCoolTank'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['totCoolTank'] = opeCountTech

                # added on 10/16/15, linearize energy storage model
                # we need to do this similarly for electricity storage and heating storage
                opeCountTech += 1
                idxContOpeVar['upInYCoolTank'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['minInYCoolTank'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['upOutYCoolTank'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['minOutYCoolTank'] = opeCountTech

                # binary operation variable
                binOpeCount += 1
                idxBinOpeVar['chgCoolTank'] = binOpeCount
                binOpeCount += 1
                idxBinOpeVar['disCoolTank'] = binOpeCount

            elif name == 'eleChill' and len(nonzeroIndCool):
                # continuous operation variable
                opeCountTech += 1
                idxContOpeVar['eleEleChill'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['coolEleChill'] = opeCountTech

            elif name == 'HP':
                # continuous operation variable
                opeCountTech += 1
                idxContOpeVar['eleHP'] = opeCountTech

                # operation variable
                opeCountTech += 1
                idxContOpeVar['heatHP'] = opeCountTech

                opeCountTech += 1
                idxContOpeVar['coolHP'] = opeCountTech

                # heating status
                if parHP['heatSta'] == 1 and len(nonzeroIndHeat):
                    # modified on 09/10/2015, introduced to linearize the heat pump model
                    opeCountTech += 1
                    idxContOpeVar['heatZHP'] = opeCountTech

                    # binary variable
                    binOpeCount += 1
                    idxBinOpeVar['heatHP'] = binOpeCount

                # cooling status
                if parHP['coolSta'] == 1 and len(nonzeroIndCool):
                    # operation variable
                    # modified on 09/10/2015, introduced to linearize the heat pump model
                    opeCountTech += 1
                    idxContOpeVar['coolZHP'] = opeCountTech

                    # binary operation variable
                    binOpeCount += 1
                    idxBinOpeVar['coolHP'] = binOpeCount

            elif name == 'eleBoil' and len(nonzeroIndHeat):
                # continuous operation variable
                opeCountTech += 1
                idxContOpeVar['eleEleBoil'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['heatEleBoil'] = opeCountTech

            elif name == 'ABSChill' and len(nonzeroIndCool):
                # continuous operation variable
                opeCountTech += 1
                idxContOpeVar['heatABSChill'] = opeCountTech
                opeCountTech += 1
                idxContOpeVar['coolABSChill'] = opeCountTech

        elif candidateTech[name] == 1 and name == 'eleGrid':
            # continuous operation variable
            opeCountTech += 1
            idxContOpeVar['purEleGrid'] = opeCountTech
            opeCountTech += 1
            idxContOpeVar['salEleGrid'] = opeCountTech

            # binary operation variable
            binOpeCount += 1
            idxBinOpeVar['purEleGrid'] = binOpeCount
            binOpeCount += 1
            idxBinOpeVar['salEleGrid'] = binOpeCount

        elif candidateTech[name] == 1 and name == 'heatSal' and len(nonzeroIndHeat):
            # continuous operation variable
            opeCountTech += 1
            idxContOpeVar['heatSal'] = opeCountTech

            # binary operation variable
            binOpeCount += 1
            idxBinOpeVar['heatSal'] = binOpeCount

        elif candidateTech[name] == 1 and name == 'heatInds' and len(nonzeroIndHeat):
            # continuous operation variable
            opeCountTech += 1
            idxContOpeVar['heatInds'] = opeCountTech

            # binary operation variable
            binOpeCount += 1
            idxBinOpeVar['heatInds'] = binOpeCount

    del binOpeCount, opeCountTech, instCountTech

    # total number of continuous installation variables
    numContInstVar = len(idxContInstVar)

    # TODO: What happened here??
    for name, contents in idxContInstVar.items():
        idxContInstVar[name] = contents - 1

    logging.debug("total number of continuous installation variables {}".format(idxContInstVar))

    # 'Index of binary installation variables'
    # '''
    # binInstNGBoil = 1  # installation decision of natural gas boiler
    # binInstNGChill = 2  # installation decision of natural gas chiller
    # binInstPV = 3  # installation decision of solar PV panel
    # binInstSolThem = 4  # installation decision of solar thermal
    # binInstABSChill = 5  # installation decision of heat fired absorption chiller
    # binInstEleBoil = 6  # installation decision of electric boiler
    # binInstHP = 7  # installation decision of heat pump
    # binInstEleChill = 8  # installation decision of electric chiller
    # binInstHeatTank = 9  # installation decision of heat storage
    # binInstEleBat = 10  # installation decision of electric storage
    # '''

    # should we start the numbering of index from 0???

    # idxBinInstVar = dict(NGBoil=1, NGChill=2, PV=3, solThem=4, ABSChill=5, eleBoil=6, HP=7, eleChill=8, heatTank=9,
    #                      eleBat=10)

    # total number of binary installation variable, except CHP
    # install or not install
    numBinInstVar = len(idxBinInstVar)

    # idxBinInst indicates the absolute (actual) index of binary installation variables
    idxBinInst = {}
    for name, contents in idxBinInstVar.items():
        idxBinInstVar[name] = contents - 1
        idxBinInst[name] = int(idxBinInstVar[name] + numContInstVar)

    logging.debug("binary installation variable except CHP. idxBinInstVar: {}".format(idxBinInstVar))
    logging.debug("absolute index of binary installation variables: idxBinInst:{}".format(idxBinInst))

    # CHP is considered as a discrete technology with k types
    idxInstCHP = 0
    if candidateTech['CHP'] == 1:
        idxInstCHP = numBinInstVar + numContInstVar  # index of installation decision of CHP

    totNumBinInstVar = numBinInstVar  # total number of integer installation variables
    if candidateTech['CHP'] == 1:
        totNumBinInstVar += np.sum(parCHP['numInst'])

    # 'index of continuous operational variables, related to hour/day/week/month/season in a year'
    # 1, natural gas boiler
    # gasNGBoil = 1  # natural gas consumption of natural gas fired boiler
    # heatNGBoil = 2  # heat generation of natural gas boiler

    # 2, natural gas chiller
    # gasNGChill = 3  # natural gas consumption of natural gas chiller
    # coolNGChill = 5  # cooling energy generation from natural gas chiller

    # 3, solar PV panel
    # elePV = 6  # electricity generation from solar PV panels

    # 4, solar thermal
    # heatSolThem = 7  # heat generation from solar thermal

    # 5, electricity storage
    # stoEleBat = 8  # gross electricity sent to batteries for storage during hour h, type of day d, in month m, kW
    # fromEleBat = 9  # total net electricity supply from batteries, kW
    # totEleBat = 10  # total electricity stored in batteries, kW

    # 6, thermal storage
    # stoHeatTank = 11  # gross heat sent to water tank for storage
    # fromHeatTank = 12  # total net heat supply from water tank, kW
    # totHeatTank = 13  # total heat stored in water tank, kW

    # 7, grid
    # purEleGrid = 14  # total electricity purchased from the power grid
    # salEleGrid = 15  # total electricity sold back to the power grid

    # 8, electric chiller
    # eleEleChill = 16  # electricity consumption of electric chiller
    # coolEleChill = 17  # cooling energy generation of electric chiller

    # 9,  heat pump
    # eleHP = 18  # electricity consumption of heat pump
    # heatHP = 19  # heat generation from heat pump
    # coolHP = 20  # cooling energy generation from heat pump

    # 10, electric boiler
    # eleEleBoil = 21  # electricity consumption of electric boiler
    # heatEleBoil = 22  # heat supply from electric boiler

    # 11, heat fired absorption chiller
    # heatABSChill = 23  # heat consumption of heat fired absorption chiller
    # coolABSChill = 24  # cooling energy supply from heat fired absorption chiller

    # 12, industry waste heat
    # heatInds = 25  # waste heat from industry

    # 13, selling heat outside the district energy network
    # heatSal = 26
    # should we start the numbering of index from 0???

    numContOpeVar = len(idxContOpeVar)  # number of continuous operational variables, except CHP
    for name, contents in idxContOpeVar.items():
        idxContOpeVar[name] = contents - 1

    # 13, CHP
    # gasCHP = 1  # natural gas consumption of CHP plant
    # heatCHP = 2  # heat generation from CHP
    # eleCHP = 3  # electricity generation from CHP
    idxContOpeVarCHP = {}
    if candidateTech['CHP'] == 1:
        idxContOpeVarCHP = dict(gasCHP=0,
                                heatCHP=1,
                                eleCHP=2)

    # total number of continuous variable for each type of CHP plant
    numContOpeVarCHP = len(idxContOpeVarCHP)

    # 09/28/2015, binary operational status of CHP
    # add binary operation variables in order to control the minimum partial load requirement
    idxBinOpeVarCHP = {}
    if candidateTech['CHP'] == 1:
        idxBinOpeVarCHP = dict(CHP=0)
    numBinOpeVarCHP = len(idxBinOpeVarCHP)

    # total number of continuous operational variables, including CHP
    totNumContOpeVar = numContOpeVar
    if candidateTech['CHP'] == 1:
        totNumContOpeVar = numContOpeVar + numContOpeVarCHP * np.sum(parCHP['numInst'])

    # 'index of binary operational variables'

    numBinOpeVar = len(idxBinOpeVar)  # total number of binary operation variables

    for name, contents in idxBinOpeVar.items():
        idxBinOpeVar[name] = contents - 1

    logging.debug("index of binary operational variables:{}".format(idxBinOpeVar))
    logging.debug("index of binary CHP operational variables:{}".format(idxBinOpeVarCHP))
    logging.debug("index of continuous operational variables. idxContOpeVar:{}".format(idxContOpeVar))
    # total number of installation variables
    totNumInstVar = numContInstVar + totNumBinInstVar

    logging.debug("total number of installation variables:{}".format(totNumInstVar))

    # total number of binary operational variables
    totNumBinOpeVar = numBinOpeVar
    if candidateTech['CHP'] == 1:
        totNumBinOpeVar = numBinOpeVar + numBinOpeVarCHP * np.sum(parCHP['numInst'])

    # total number of hourly operational variables
    numHrOpeVar = totNumContOpeVar + totNumBinOpeVar

    logging.debug("total number of hourly operational varaibles: {}. continuous:{}. binary:{}".format(numHrOpeVar,
                                                                                totNumContOpeVar, totNumBinOpeVar))

    # 08/06/2015, the introduced variable in order to calculate the monthly maximum demand
    numMaxMonthDemand = 0
    if candidateTech['demandCharge'] == 1:
        numMaxMonthDemand = parTime['numMonth']
        idxMaxDemand = totNumInstVar + numHrOpeVar * parTime['totHrStep']

    # total number of planning and operational variables
    totVars = totNumInstVar + numHrOpeVar * parTime['totHrStep'] + numMaxMonthDemand

    logging.debug("Total variables:{}. totNumInstVar:{}. operational variables:{} * totHr{}. "
                  "numMaxMonthDemand:{}".format(totVars, totNumInstVar, numHrOpeVar, parTime['totHrStep'],
                                                numMaxMonthDemand))

    # create an array numVar = np.zeros((numMonth,numDay, numHour)) ??? is initialization necessary
    numVar = np.asarray([totNumInstVar + k * numHrOpeVar + j * numHrOpeVar * parTime['numHour'] +
                         i * numHrOpeVar * parTime['numHour'] * parTime['numDay']
                         for i in range(parTime['numMonth'])
                         for j in range(parTime['numDay'])
                         for k in range(parTime['numHour'])])

    logging.debug("numVar shape:{}. numVar:{}".format(numVar.shape, numVar))
    # numVar = numVar.reshape((numMonth, numDay, numHour))# is reassigning value to the same variable possible

    # Keep the original index of continuous variables for further use in calculation of previous
    # index of total energy storage
    idxContOpe = idxContOpeVar.copy()

    logging.debug("index of binary continuous operational variabls: idxContOpeVar({}). {}".format(len(idxContOpeVar),
                                                                                                  idxContOpeVar))

    for name, contents in idxContOpeVar.items():
        idxContOpeVar[name] = contents + numVar

    logging.debug("index of binary continuous operational variabls: idxContOpeVar({}). {}".format(len(idxContOpeVar),
                                                                                                  idxContOpeVar))

    # the user can control the lower and upper bound of binary installation variables through idxBinOpeVar to determine
    #      whether the energy conversion technology is selected or not
    #  idxBinOpe  will be used to calculate idxIntOpe in the 'lb'& 'ub' section
    idxBinOpe = idxBinOpeVar.copy()
    for name, contents in idxBinOpeVar.items():
        idxBinOpeVar[name] = contents + numVar + totNumContOpeVar

    numVarMat = numVar.reshape((parTime['numMonth'], parTime['numDay'], parTime['numHour']))

    # is reassigning value to the same variable possible
    # CHP
    oriIdxContOpeVarCHP = []
    oriIdxBinOpeVarCHP = []
    if candidateTech['CHP'] == 1:
        # continuous operational variables of CHP
        idxVarCHP = np.asarray([numContOpeVarCHP * m + numContOpeVar + numVarMat[i, j, k]
                                for i in range(parTime['numMonth'])
                                for j in range(parTime['numDay'])
                                for k in range(parTime['numHour'])
                                for m in range(np.sum(parCHP['numInst']))])

        for name, contents in idxContOpeVarCHP.items():
            idxContOpeVarCHP[name] = contents + idxVarCHP
        del idxVarCHP

        # binary operational variables of CHP
        idxVarCHP = np.asarray([numBinOpeVarCHP * m + (totNumContOpeVar + numBinOpeVar) + numVarMat[i, j, k]
                                for i in range(parTime['numMonth'])
                                for j in range(parTime['numDay'])
                                for k in range(parTime['numHour'])
                                for m in range(np.sum(parCHP['numInst']))])

        for name, contents in idxBinOpeVarCHP.items():
            idxBinOpeVarCHP[name] = contents + idxVarCHP
        del idxVarCHP

        # keep track of index of continuous operational variables for CHP
        # in order to process the restructure of CHP solutions after solving the problem
        oriIdxContOpeVarCHP = idxContOpeVarCHP.copy()
        oriIdxBinOpeVarCHP = idxBinOpeVarCHP.copy()

        # if len(nonzeroIndEle)>0 and len(nonzeroIndEle)<parTime['totHrStep']:
        idxContOpeVarCHP = nonzero_row_chp(idxContOpeVarCHP, parTime, parCHP, nonzeroIndEle, nonzeroIndHeat,
                                           nonzeroIndCool)
        idxBinOpeVarCHP = nonzero_row_chp(idxBinOpeVarCHP, parTime, parCHP, nonzeroIndEle, nonzeroIndHeat,
                                          nonzeroIndCool)

    # ===================================================================================================================
    # index of equality constraints
    # All the equality constraints are related to hour
    numInstEqCons = 0

    # 08/05/2015, modified
    idxOpeEqCons = {}
    countOpeEqCons = 0

    # electricity balance
    if len(nonzeroIndEle):
        countOpeEqCons += 1
        idxOpeEqCons['eleBal'] = countOpeEqCons
    # heating balance
    if len(nonzeroIndHeat):
        countOpeEqCons += 1
        idxOpeEqCons['heatBal'] = countOpeEqCons
    # cooling balance
    if len(nonzeroIndCool):
        countOpeEqCons += 1
        idxOpeEqCons['coolBal'] = countOpeEqCons

    # electric battery
    if candidateTech['eleBat'] == 1 and len(nonzeroIndEle):
        countOpeEqCons += 1
        idxOpeEqCons['dynEleBat'] = countOpeEqCons

    # heat tank
    if candidateTech['heatTank'] == 1 and len(nonzeroIndHeat):
        countOpeEqCons += 1
        idxOpeEqCons['dynHeatTank'] = countOpeEqCons

    # cool tank
    if candidateTech['coolTank'] == 1 and len(nonzeroIndCool):
        countOpeEqCons += 1
        idxOpeEqCons['dynCoolTank'] = countOpeEqCons

    # natural gas boiler
    if candidateTech['NGBoil'] == 1 and len(nonzeroIndHeat):
        countOpeEqCons += 1
        idxOpeEqCons['NGBoil'] = countOpeEqCons

    # electric boiler
    if candidateTech['eleBoil'] == 1 and len(nonzeroIndHeat):
        countOpeEqCons += 1
        idxOpeEqCons['eleBoil'] = countOpeEqCons

    # natural gas chiller
    if candidateTech['NGChill'] == 1 and len(nonzeroIndCool):
        countOpeEqCons += 1
        idxOpeEqCons['coolNGChill'] = countOpeEqCons

    # electric chiller
    if candidateTech['eleChill'] == 1 and len(nonzeroIndCool):
        countOpeEqCons += 1
        idxOpeEqCons['eleChill'] = countOpeEqCons

    # heat fired absorption chiller
    if candidateTech['ABSChill'] == 1 and len(nonzeroIndCool):
        countOpeEqCons += 1
        idxOpeEqCons['ABSChill'] = countOpeEqCons

    # heat pump
    if candidateTech['HP'] == 1:
        # cooling status
        if len(nonzeroIndCool) and parHP['coolSta'] == 1:
            countOpeEqCons += 1
            idxOpeEqCons['coolHP'] = countOpeEqCons

        # heating status
        if len(nonzeroIndHeat) and parHP['heatSta'] == 1:
            countOpeEqCons += 1
            idxOpeEqCons['heatHP'] = countOpeEqCons

    del countOpeEqCons

    numHrEq = len(idxOpeEqCons)  # for each hour, the number of equal constraints except those of CHP plants

    for name, contents in idxOpeEqCons.items():
        idxOpeEqCons[name] = contents - 1

    # endChgSto determines the state of charge at the end of a day
    endChgSto = {}
    countOpeEqCons = -1

    # electric battery
    if candidateTech['eleBat'] == 1 and len(nonzeroIndEle) and parEleBat['mode'] == 'cycleCharge':
        countOpeEqCons += 1
        endChgSto['endEleBat'] = countOpeEqCons

    # heat tank
    if candidateTech['heatTank'] == 1 and len(nonzeroIndHeat) and parHeatTank['mode'] == 'cycleCharge':
        countOpeEqCons += 1
        endChgSto['endHeatTank'] = countOpeEqCons

    # cool tank
    if candidateTech['coolTank'] == 1 and len(nonzeroIndCool) and parCoolTank['mode'] == 'cycleCharge':
        countOpeEqCons += 1
        endChgSto['endCoolTank'] = countOpeEqCons

    numEndChgStoCons = len(endChgSto)
    # CHP
    idxEqConsCHP = {}
    if candidateTech['CHP'] == 1:
        idxEqConsCHP = dict(eleCHP=0,
                            heatCHP=1)

    numEqConsPerTypeCHP = len(idxEqConsCHP)
    numEqConsCHP = 0
    if candidateTech['CHP'] == 1:
        numEqConsCHP = numEqConsPerTypeCHP * np.sum(parCHP['numInst'])

    # -----------------------------------------------------------------------------------------------------------------------
    # index of inequality constraints
    # '''
    # purEleGrid = 1, hourly electricity purchased from the power grid within the upper limit
    # salEleGrid = 2, hourly electricity sold back to the grid from the district network within an upper limit
    # upEleExchg = 3, hourly electricity exchange status, upper limit '1'
    # minEleExchg = 4, hourly electricity exchange status, lower limit '0'
    # heatInds = 5, maximum waste heat from the industry
    # upHeatSal = 6, maximum heat sold from the district network

    # upInEleBat = 7, maximum net electricity sent to the electric battery
    # minInEleBat = 8, minimum net electricity sent to the electric battery
    # upOutEleBat = 9, maximum gross electricity released from the electric battery
    # minOutEleBat = 10, minimum gross electricity released from the electric battery
    # upChgEleBat = 11, maximum charging of electric battery
    # minChgEleBat = 12, minimum charging of electric battery
    # upDisEleBat = 13, maximum discharging of electric battery
    # minDisEleBat = 14, minimum discharging of electric battery
    # upTotEleBat = 15, maximum total electricity stored in the electric battery
    # minTotEleBat = 16, minimum total electricity stored in the electric battery
    # upCapEleBat = 17, maximum rated capacity of electric battery
    # minCapEleBat = 18, minimum rated capacity of electric battery
    # upStaEleBat = 19,hourly charging/discharging status of electric battery, upper limit '1'

    # upInHeatTank = 21, maximum net heat sent to the heat tank
    # downInHeatTank = 22, minimum net heat sent to the heat tank
    # upOutHeatTank = 23, maximum gross heat released from the heat tank
    # minOutHeatTank = 24, minimum gross heat released from the heat tank
    # upChgHeatTank = 25, maximum charging of heat tank
    # minChgHeatTank = 26, minimum charging of heat tank
    # upDisHeatTank = 27, maximum discharging of heat tank
    # minDisHeatTank = 28, minimum discharging of heat tank
    # upTotHeatTank = 29, maximum total heat stored in the heat tank
    # upStaHeatTank = 30, hourly charging/discharging status of heat tank, upper limit '1'
    # minStaHeatTank = 31, hourly charging/discharging status of heat tank,  lower limit '0'
    # upCapHeatTank = 32, maximum rated capacity of heat tank
    # minCapHeatTank = 33, minimum rated capacity of heat tank

    # upInstAreaPV = 34, maximum installation area of PV panels
    # upCapPV = 35, maximum hourly electricity generation from PV panels related to rated capacity
    # upIrPV = 36, maximum hourly electricity generation from PV panels related to solar irradiation
    # upInstAreaSolThem = 37,maximum installation area of solar thermal
    # upCapSolThem = 38, maximum hourly heat generation from solar thermal related to rated capacity
    # upIrSolThem = 39, maximum hourly heat generation from solar thermal related to solar irradiation

    # upCapEleBoil = 40, maximum rated capacity of electric boiler
    # minCapEleBoil = 41, minimum rated capacity of electric boiler
    # upCapNGBoil = 42, maximum rated capacity of natural gas boiler
    # minCapNGBoil = 43, minimum rated capacity of natural gas boiler
    # upGenNGBoil = 44, maximum heat generation from natural gas boiler
    # upGenEleBoil = 45,maximum heat generation from electric boiler
    # upHrEleBoil = 46, maximum hourly heat generation from electric boiler
    # upHrNGBoil = 47, maximum hourly heat generation from natural gas boiler

    # upCoolNGChill = 48, maximum hourly cooling generation from natural gas chiller
    # minCoolNGChill = 49, minimum hourly cooling generation from natural gas chiller

    # upCapNGChill = 54, maximum rated capacity of natural gas chiller
    # minCapNGChill = 55, minimum rated capacity of natural gas chiller

    # upHrCoolNGChill = 59, maximum hourly cooling generation of natural gas chiller
    # upHrCoolABSChill = 60, maximum hourly cooling generation of heat fired absorption chiller
    # upCapABSChill = 61, maximum rated cooling capacity of heat fired absorption chiller
    # minCapABSChill = 62, minimum rated cooling capacity of heat fired absorption chiller
    # upHrCoolEleChill = 63, maximum hourly cooling generation of electric chiller
    # upCapEleChill = 64, maximum rated cooling capacity of electric chiller
    # minCapEleChill = 65, minimum rated cooling capacity of electric chiller

    # upHrThemHP = 66, maximum hourly thermal generation of heat pump
    # upHrStaHP = 70, heating/cooling status of heat pump, upper limit '1'
    # minHrStaHP = 71, heating/cooling status of heat pump,lower limit '0'
    # upCapHP = 72, maximum rated capacity of heat pump
    # minCapHP = 73, minimum rated capacity of heat pump
    # upHrHeatHP = 76, maximum hourly heating generation of heat pump
    # minHrHeatHP = 76, minimum hourly heating generation of heat pump
    # upHrCoolHP = 77, maximum hourly cooling generation of heat pump
    # minHrCoolHP = 77, minimum hourly cooling generation of heat pump
    # '''
    # upInstStaCHP = 78, number of centralized CHP in district network
    # upHrEleCHP = 79, maximum hourly electricity generation from CHP
    # minHrEleCHP = 80, minimum hourly electricity generation from CHP

    # 1, installation requirement
    idxInstIneqCons = {}
    countOpeEqCons = 0

    # PV
    if candidateTech['PV'] == 1 and len(nonzeroIndEle):
        countOpeEqCons += 1
        idxInstIneqCons['upInstAreaPV'] = countOpeEqCons
        countOpeEqCons += 1
        idxInstIneqCons['minInstAreaPV'] = countOpeEqCons

    # solar thermal
    if candidateTech['solThem'] == 1 and len(nonzeroIndHeat):
        countOpeEqCons += 1
        idxInstIneqCons['upInstAreaSolThem'] = countOpeEqCons
        countOpeEqCons += 1
        idxInstIneqCons['minInstAreaSolThem'] = countOpeEqCons

    # electric boiler
    if candidateTech['eleBoil'] == 1 and len(nonzeroIndHeat):
        countOpeEqCons += 1
        idxInstIneqCons['upCapEleBoil'] = countOpeEqCons
        countOpeEqCons += 1
        idxInstIneqCons['minCapEleBoil'] = countOpeEqCons
        countOpeEqCons += 1
        idxInstIneqCons['upGenEleBoil'] = countOpeEqCons

    # natural gas boiler
    if candidateTech['NGBoil'] == 1 and len(nonzeroIndHeat):
        countOpeEqCons += 1
        idxInstIneqCons['upCapNGBoil'] = countOpeEqCons
        countOpeEqCons += 1
        idxInstIneqCons['minCapNGBoil'] = countOpeEqCons
        countOpeEqCons += 1
        idxInstIneqCons['upGenNGBoil'] = countOpeEqCons

    # natural gas chiller
    if candidateTech['NGChill'] == 1 and len(nonzeroIndCool):
        countOpeEqCons += 1
        idxInstIneqCons['upCapNGChill'] = countOpeEqCons
        countOpeEqCons += 1
        idxInstIneqCons['minCapNGChill'] = countOpeEqCons

    # heat fired absorption chiller
    if candidateTech['ABSChill'] == 1 and len(nonzeroIndCool):
        countOpeEqCons += 1
        idxInstIneqCons['upCapABSChill'] = countOpeEqCons
        countOpeEqCons += 1
        idxInstIneqCons['minCapABSChill'] = countOpeEqCons

    # electric chiller
    if candidateTech['eleChill'] == 1 and len(nonzeroIndCool):
        countOpeEqCons += 1
        idxInstIneqCons['upCapEleChill'] = countOpeEqCons
        countOpeEqCons += 1
        idxInstIneqCons['minCapEleChill'] = countOpeEqCons

    # heat pump
    if candidateTech['HP'] == 1 and (
            (parHP['coolSta'] == 1 and len(nonzeroIndCool)) or (parHP['heatSta'] == 1 and len(nonzeroIndHeat))):
        countOpeEqCons += 1
        idxInstIneqCons['upCapHP'] = countOpeEqCons
        countOpeEqCons += 1
        idxInstIneqCons['minCapHP'] = countOpeEqCons

    # electric battery
    if candidateTech['eleBat'] == 1 and len(nonzeroIndEle):
        countOpeEqCons += 1
        idxInstIneqCons['upCapEleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxInstIneqCons['minCapEleBat'] = countOpeEqCons

    # heat tank
    if candidateTech['heatTank'] == 1 and len(nonzeroIndHeat):
        countOpeEqCons += 1
        idxInstIneqCons['upCapHeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxInstIneqCons['minCapHeatTank'] = countOpeEqCons

    # cool tank
    if candidateTech['coolTank'] == 1 and len(nonzeroIndCool):
        countOpeEqCons += 1
        idxInstIneqCons['upCapCoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxInstIneqCons['minCapCoolTank'] = countOpeEqCons

    numInstIneqCons = len(idxInstIneqCons)

    # CHP
    # 12/14/15, upInstTypeCHP: max installation number for each type
    # upInstStaCHP: max investment number of CHP for all types
    # len(idxInstIneqCons['upInstTypeCHP']) = np.sum(parCHP['numInst'])
    if candidateTech['CHP'] == 1:
        countOpeEqCons += 1
        idxInstIneqCons['upInstStaCHP'] = countOpeEqCons
        countOpeEqCons += 1
        idxInstIneqCons['upInstTypeCHP'] = np.arange(countOpeEqCons, countOpeEqCons + parCHP['numType'])
        numInstIneqCons += 1 + parCHP['numType']

    for name, contents in idxInstIneqCons.items():
        idxInstIneqCons[name] = contents - 1

    # numInstIneqCons = len(idxInstIneqCons)

    ''' 2, operational requirements '''
    idxOpeIneqCons = {}
    countOpeEqCons = 0

    # electric grid
    if candidateTech['eleGrid'] == 1 and len(nonzeroIndEle):
        countOpeEqCons += 1
        idxOpeIneqCons['maxPurEleGrid'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['maxSalEleGrid'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minPurEleGrid'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minSalEleGrid'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upEleExchg'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minEleExchg'] = countOpeEqCons

    # heat industry
    if candidateTech['heatInds'] == 1 and len(nonzeroIndHeat):
        countOpeEqCons += 1
        idxOpeIneqCons['upHeatInds'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minHeatInds'] = countOpeEqCons

    # heat sale
    if candidateTech['heatSal'] == 1 and len(nonzeroIndHeat):
        countOpeEqCons += 1
        idxOpeIneqCons['upHeatSal'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minHeatSal'] = countOpeEqCons

    if candidateTech['heatSal'] == 1 and candidateTech['heatInds'] == 1 and len(nonzeroIndHeat):
        countOpeEqCons += 1
        idxOpeIneqCons['upHeatExchg'] = countOpeEqCons

    # electric battery
    if candidateTech['eleBat'] == 1 and len(nonzeroIndEle):
        countOpeEqCons += 1
        idxOpeIneqCons['upInEleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minInEleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upOutEleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minOutEleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upChgEleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minChgEleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upDisEleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minDisEleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upTotEleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minTotEleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upStaEleBat'] = countOpeEqCons

        # added on 10/30/15, linearization of energy storage model
        # upIn
        countOpeEqCons += 1
        idxOpeIneqCons['upInX1EleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upInX2EleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upInX12EleBat'] = countOpeEqCons
        # minIn
        countOpeEqCons += 1
        idxOpeIneqCons['minInX1EleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minInX2EleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minInX12EleBat'] = countOpeEqCons
        # upOut
        countOpeEqCons += 1
        idxOpeIneqCons['upOutX1EleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upOutX2EleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upOutX12EleBat'] = countOpeEqCons
        # minOut
        countOpeEqCons += 1
        idxOpeIneqCons['minOutX1EleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minOutX2EleBat'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minOutX12EleBat'] = countOpeEqCons

    # heat tank
    if candidateTech['heatTank'] == 1 and len(nonzeroIndHeat):
        countOpeEqCons += 1
        idxOpeIneqCons['upInHeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minInHeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upOutHeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minOutHeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upChgHeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minChgHeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upDisHeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minDisHeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upTotHeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minTotHeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upStaHeatTank'] = countOpeEqCons

        # added on 10/30/15, linearization of energy storage model
        # upIn
        countOpeEqCons += 1
        idxOpeIneqCons['upInX1HeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upInX2HeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upInX12HeatTank'] = countOpeEqCons
        # minIn
        countOpeEqCons += 1
        idxOpeIneqCons['minInX1HeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minInX2HeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minInX12HeatTank'] = countOpeEqCons
        # upOut
        countOpeEqCons += 1
        idxOpeIneqCons['upOutX1HeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upOutX2HeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upOutX12HeatTank'] = countOpeEqCons
        # minOut
        countOpeEqCons += 1
        idxOpeIneqCons['minOutX1HeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minOutX2HeatTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minOutX12HeatTank'] = countOpeEqCons

    # cool tank
    if candidateTech['coolTank'] == 1 and len(nonzeroIndCool):
        countOpeEqCons += 1
        idxOpeIneqCons['upInCoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minInCoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upOutCoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minOutCoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upChgCoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minChgCoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upDisCoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minDisCoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upTotCoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minTotCoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upStaCoolTank'] = countOpeEqCons

        # added on 10/16/15, linearization of energy storage model
        # upIn
        countOpeEqCons += 1
        idxOpeIneqCons['upInX1CoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upInX2CoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upInX12CoolTank'] = countOpeEqCons
        # minIn
        countOpeEqCons += 1
        idxOpeIneqCons['minInX1CoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minInX2CoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minInX12CoolTank'] = countOpeEqCons
        # upOut
        countOpeEqCons += 1
        idxOpeIneqCons['upOutX1CoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upOutX2CoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upOutX12CoolTank'] = countOpeEqCons
        # minOut
        countOpeEqCons += 1
        idxOpeIneqCons['minOutX1CoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minOutX2CoolTank'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['minOutX12CoolTank'] = countOpeEqCons

    # PV
    if candidateTech['PV'] == 1 and len(nonzeroIndEle):
        countOpeEqCons += 1
        idxOpeIneqCons['upCapPV'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upIrPV'] = countOpeEqCons

    # solar thermal
    if candidateTech['solThem'] == 1 and len(nonzeroIndHeat):
        countOpeEqCons += 1
        idxOpeIneqCons['upCapSolThem'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upIrSolThem'] = countOpeEqCons

    # electric boiler
    if candidateTech['eleBoil'] == 1 and len(nonzeroIndHeat):
        countOpeEqCons += 1
        idxOpeIneqCons['upHrEleBoil'] = countOpeEqCons

    # natural gas boiler
    if candidateTech['NGBoil'] == 1 and len(nonzeroIndHeat):
        countOpeEqCons += 1
        idxOpeIneqCons['upHrNGBoil'] = countOpeEqCons

    # natural gas chiller
    if candidateTech['NGChill'] == 1 and len(nonzeroIndCool):
        countOpeEqCons += 1
        idxOpeIneqCons['upHrCoolNGChill'] = countOpeEqCons

    # heat-fired absorption chiller
    if candidateTech['ABSChill'] == 1 and len(nonzeroIndCool):
        countOpeEqCons += 1
        idxOpeIneqCons['upHrCoolABSChill'] = countOpeEqCons

    # electric chiller
    if candidateTech['eleChill'] == 1 and len(nonzeroIndCool):
        countOpeEqCons += 1
        idxOpeIneqCons['upHrCoolEleChill'] = countOpeEqCons

    # heat pump
    if candidateTech['HP'] == 1 and (parHP['heatSta'] or parHP['coolSta']):
        countOpeEqCons += 1
        idxOpeIneqCons['upHrThemHP'] = countOpeEqCons
        countOpeEqCons += 1
        idxOpeIneqCons['upHrStaHP'] = countOpeEqCons

        # 09/17/2015, heating status with heating load
        if parHP['heatSta'] == 1 and len(nonzeroIndHeat):
            countOpeEqCons += 1
            idxOpeIneqCons['upHrHeatHP'] = countOpeEqCons
            countOpeEqCons += 1
            idxOpeIneqCons['minHrHeatHP'] = countOpeEqCons

            # modifies on 09/10/2015, linearization of heat pump model
            # heating status
            countOpeEqCons += 1
            idxOpeIneqCons['upHeatZHP'] = countOpeEqCons
            countOpeEqCons += 1
            idxOpeIneqCons['minHeatZHP'] = countOpeEqCons
            countOpeEqCons += 1
            idxOpeIneqCons['upHeatZXHP'] = countOpeEqCons
            countOpeEqCons += 1
            idxOpeIneqCons['minHeatZXHP'] = countOpeEqCons
            countOpeEqCons += 1
            idxOpeIneqCons['upHeatZAHP'] = countOpeEqCons
            countOpeEqCons += 1
            idxOpeIneqCons['minHeatZAHP'] = countOpeEqCons
            countOpeEqCons += 1
            idxOpeIneqCons['upHeatZAXHP'] = countOpeEqCons

        # cooling status with cooling load
        if parHP['coolSta'] == 1 and len(nonzeroIndCool):
            countOpeEqCons += 1
            idxOpeIneqCons['upHrCoolHP'] = countOpeEqCons
            countOpeEqCons += 1
            idxOpeIneqCons['minHrCoolHP'] = countOpeEqCons

            # cooling status
            countOpeEqCons += 1
            idxOpeIneqCons['upCoolZHP'] = countOpeEqCons
            countOpeEqCons += 1
            idxOpeIneqCons['minCoolZHP'] = countOpeEqCons
            countOpeEqCons += 1
            idxOpeIneqCons['upCoolZXHP'] = countOpeEqCons
            countOpeEqCons += 1
            idxOpeIneqCons['minCoolZXHP'] = countOpeEqCons
            countOpeEqCons += 1
            idxOpeIneqCons['upCoolZAHP'] = countOpeEqCons
            countOpeEqCons += 1
            idxOpeIneqCons['minCoolZAHP'] = countOpeEqCons
            countOpeEqCons += 1
            idxOpeIneqCons['upCoolZAXHP'] = countOpeEqCons

    del countOpeEqCons

    numHrIneq = len(idxOpeIneqCons)  # for each hour, the number of equal constraints except those of CHP plants

    for name, contents in idxOpeIneqCons.items():
        idxOpeIneqCons[name] = contents - 1

    # 3, requirements related to types of CHP
    # maxEleCHP, added on 09/28/2015,
    # -it means the maximum hourly electricity output from CHP is related to the installation status of CHP,
    #  and should be limited to the individual installation capacity
    #
    # 'upHrEleCHP' means the maximum hourly electricity output is related to the hourly operation status of CHP
    idxIneqConsCHP = {}
    if candidateTech['CHP'] == 1:
        idxIneqConsCHP = dict(upHrEleCHP=0,
                              minHrEleCHP=1,
                              maxEleCHP=2)

    numIneqConsPerTypeCHP = len(idxIneqConsCHP)
    numIneqConsCHP = 0
    if candidateTech['CHP'] == 1:
        numIneqConsCHP = numIneqConsPerTypeCHP * np.sum(parCHP['numInst'])

    # =========================================================================================================================
    if candidateTech['CHP'] == 0:
        totEqCons, idxEqCons, idxEqConsCHP = cal_idx_cons(numInstEqCons, numHrEq, numEqConsCHP, parTime, idxOpeEqCons,
                                                          idxEqConsCHP, numEqConsPerTypeCHP)
    elif candidateTech['CHP'] == 1:
        totEqCons, idxEqCons, idxEqConsCHP = cal_idx_cons(numInstEqCons, numHrEq, numEqConsCHP, parTime, idxOpeEqCons,
                                                          idxEqConsCHP, numEqConsPerTypeCHP, parCHP)

    idxEndChgSto = {}
    # set the following active if end of charge condition is required
    if numEndChgStoCons != 0 and parTime['numHour'] == 24:
        idxEqEndChgSto = np.asarray([numEndChgStoCons * j + totEqCons + numEndChgStoCons * parTime['numDay'] * i
                                     for i in range(parTime['numMonth'])
                                     for j in range(parTime['numDay'])])

        for name, contents in endChgSto.items():
            idxEndChgSto[name] = contents + idxEqEndChgSto

        totEqCons += numEndChgStoCons * parTime['numMonth'] * parTime['numDay']

    # idxEqConsCHP = idxEq.idxEqConsCHP
    # idxEqCons = idxEq.idxEqCons
    # totNumHrEqCons = numHrEq + numEqConsCHP  # for each hour, the number of equal constraints, including those of CHP plants
    # totEqCons = totNumHrEqCons * parTime['numMonth'] * parTime['numDay'] * parTime[
    #    'numHour']  # total number of equality constraints during the whole time horizon

    # index of equality constraint
    # numEqCons = np.asarray(
    #    [totNumHrEqCons * k + totNumHrEqCons * parTime['numHour'] * j + totNumHrEqCons * parTime['numHour'] * parTime[
    #        'numDay'] * i for i in
    #     range(parTime['numMonth']) for j in range(parTime['numDay']) for k in range(parTime['numHour'])])
    # numEqCons = numEqCons.reshape((numMonth, numDay, numHour))

    # for name, contents in idxEqCons.items():
    #    idxEqCons[name] = contents + numEqCons

    # numEqConsMat = numEqCons.reshape((parTime['numMonth'], parTime['numDay'], parTime['numHour']))
    # idxEqPerTypeCHP = np.sort(
    #    np.asarray(numEqConsPerTypeCHP * m + numHrEq + numEqConsMat[i, j, k] for m in range(parCHP['typeCHP']) for i in
    #               range(parTime['numMonth']) for j in range(parTime['numDay']) for k in range(parTime['numHour'])))
    # for name, contents in idxEqConsCHP.items():
    #    idxEqConsCHP[name] = contents + idxEqPerTypeCHP
    # =========================================================================================================================
    if candidateTech['CHP'] == 1:
        totIneqCons, idxIneqCons, idxIneqConsCHP = cal_idx_cons(numInstIneqCons, numHrIneq, numIneqConsCHP, parTime,
                                                                idxOpeIneqCons,
                                                                idxIneqConsCHP, numIneqConsPerTypeCHP, parCHP)
    elif candidateTech['CHP'] == 0:
        totIneqCons, idxIneqCons, idxIneqConsCHP = cal_idx_cons(numInstIneqCons, numHrIneq, numIneqConsCHP, parTime,
                                                                idxOpeIneqCons,
                                                                idxIneqConsCHP, numIneqConsPerTypeCHP)

    # 08/07/2015, add inequality constraints for max monthly demand charge
    if candidateTech['demandCharge'] == 1:
        idxIneqMaxDemand = totIneqCons
        totIneqCons = totIneqCons + parTime['totHrStep']

    # 09/24/2015
    if candidateTech['CHP'] == 1:
        # if len(zeroIndEle)>0:
        # zeroidxEqConsCHP = zeroRowCHP(idxEqConsCHP, parTime, parCHP, zeroIndEle, zeroIndHeat, zeroIndCool)
        # zeroidxIneqConsCHP = zeroRowCHP(idxIneqConsCHP, parTime, parCHP, zeroIndEle, zeroIndHeat, zeroIndCool)

        # if len(nonzeroIndEle)>0 and len(nonzeroIndEle)<parTime['totHrStep']:
        idxEqConsCHP = nonzero_row_chp(idxEqConsCHP, parTime, parCHP, nonzeroIndEle, nonzeroIndHeat, nonzeroIndCool)
        idxIneqConsCHP = nonzero_row_chp(idxIneqConsCHP, parTime, parCHP, nonzeroIndEle, nonzeroIndHeat, nonzeroIndCool)

    # idxIneqConsCHP = idxIneq.idxEqConsCHP
    # idxEqCons = idxEq.idxEqCons
    # totNumHrIneqCons = numHrIneq + numIneqConsCHP  # for each hour, the number of equal constraints, including those of CHP plants
    # totIneqCons = totNumHrIneqCons * parTime['numMonth'] * parTime['numDay'] * parTime[
    #    'numHour']  # total number of equality constraints during the whole time horizon

    # index of equality constraint
    # numIneqCons = np.asarray(
    #    [totNumHrIneqCons * k + totNumHrIneqCons * parTime['numHour'] * j + totNumHrIneqCons * parTime['numHour'] * parTime[
    #        'numDay'] * i for i in
    #     range(parTime['numMonth']) for j in range(parTime['numDay']) for k in range(parTime['numHour'])])
    # numEqCons = numEqCons.reshape((numMonth, numDay, numHour))

    # for name, contents in idxIneqCons.items():
    #    idxIneqCons[name] = contents + numIneqCons

    # numIneqConsMat = numIneqCons.reshape((parTime['numMonth'], parTime['numDay'], parTime['numHour']))
    # idxIneqPerTypeCHP = np.sort(
    #    np.asarray(numIneqConsPerTypeCHP * m + numHrIneq + numIneqConsMat[i, j, k] for m in range(parCHP['typeCHP']) for i in
    #               range(parTime['numMonth']) for j in range(parTime['numDay']) for k in range(parTime['numHour'])))
    # for name, contents in idxIneqConsCHP.items():
    #    idxIneqConsCHP[name] = contents + idxIneqPerTypeCHP
    # =======================================================================================================================

    logging.info("Begin building the objective function ......................")
    # TODO: 08/11/2015, capital recovery factor (CRF), not including CHP
    # TODO: lifeYear is used for calculating CRF.
    # To get the annualized capital cost, the capital recovery factor (CRF) of each equipment is used.
    CRF = np.asarray(
        [parTime['longIntr'] * (1 + parTime['longIntr']) ** lifeYear[i] / ((1 + parTime['longIntr']) ** lifeYear[i] - 1)
         for i in range(len(lifeYear))])

    # investment cost
    f = np.zeros(totVars, dtype=np.float32)  # initialize the objective function
    logging.debug("total variables: totVar({}): {}".format(totVars, totVars))

    # the point of numpy is that arrays are compact in memory,
    # so a numpy.float32 array only takes 4 bytes per element (plus a tiny bit of constant overhead for the whole array)

    # 08/04/2015, max monthly demand
    if candidateTech['demandCharge'] == 1:
        if carbOnly == 0:
            f[idxMaxDemand:totVars] = rateMonthDemandCharge

    if candidateTech['CHP'] == 1:
        # 08/11/2015, different life time for CHP types
        CRFCHP = np.asarray([parTime['longIntr'] * (1 + parTime['longIntr']) ** parCHP['lifeYear'][i] / (
                (1 + parTime['longIntr']) ** parCHP['lifeYear'][i] - 1)
                             for i in range(len(parCHP['lifeYear']))])
        if carbOnly == 0:
            f[numContInstVar + numBinInstVar + np.asarray(range(np.sum(parCHP['numInst'])))] = [
                CRFCHP[i] * (parCHP['varInst'][i] * parCHP['capGen'][i] + parCHP['fixInst'][i])
                for i in range(parCHP['numType']) for j in range(parCHP['numInst'][i])]  # 3, CHP

    indexContTech = {}
    indexContTech['binInst'] = []
    indexContTech['capInst'] = []
    indexContTech['LifeYr'] = []
    indexContTech['fixInst'] = []
    indexContTech['varInst'] = []

    # electric boiler
    if candidateTech['eleBoil'] == 1 and len(nonzeroIndHeat):
        indexContTech['binInst'] = [idxBinInst['eleBoil']]
        indexContTech['capInst'] = [idxContInstVar['eleBoil']]
        indexContTech['LifeYr'] = [idxLifeYr['eleBoil']]
        indexContTech['fixInst'] = [parEleBoil['fixIns']]
        indexContTech['varInst'] = [parEleBoil['varIns']]

    # natural gas boiler
    if candidateTech['NGBoil'] == 1 and len(nonzeroIndHeat):
        indexContTech['binInst'] = np.append(indexContTech['binInst'], [idxBinInst['NGBoil']])
        indexContTech['capInst'] = np.append(indexContTech['capInst'], [idxContInstVar['NGBoil']])
        indexContTech['LifeYr'] = np.append(indexContTech['LifeYr'], [idxLifeYr['NGBoil']])
        indexContTech['fixInst'] = np.append(indexContTech['fixInst'], [parNGBoil['fixIns']])
        indexContTech['varInst'] = np.append(indexContTech['varInst'], [parNGBoil['varIns']])

    # heat tank
    if candidateTech['heatTank'] == 1 and len(nonzeroIndHeat):
        indexContTech['binInst'] = np.append(indexContTech['binInst'], [idxBinInst['heatTank']])
        indexContTech['capInst'] = np.append(indexContTech['capInst'], [idxContInstVar['heatTank']])
        indexContTech['LifeYr'] = np.append(indexContTech['LifeYr'], [idxLifeYr['heatTank']])
        indexContTech['fixInst'] = np.append(indexContTech['fixInst'], [parHeatTank['fixIns']])
        indexContTech['varInst'] = np.append(indexContTech['varInst'], [parHeatTank['varIns']])

    # cool tank
    if candidateTech['coolTank'] == 1 and len(nonzeroIndCool):
        indexContTech['binInst'] = np.append(indexContTech['binInst'], [idxBinInst['coolTank']])
        indexContTech['capInst'] = np.append(indexContTech['capInst'], [idxContInstVar['coolTank']])
        indexContTech['LifeYr'] = np.append(indexContTech['LifeYr'], [idxLifeYr['coolTank']])
        indexContTech['fixInst'] = np.append(indexContTech['fixInst'], [parCoolTank['fixIns']])
        indexContTech['varInst'] = np.append(indexContTech['varInst'], [parCoolTank['varIns']])

    # electric battery
    if candidateTech['eleBat'] == 1 and len(nonzeroIndEle):
        indexContTech['binInst'] = np.append(indexContTech['binInst'], [idxBinInst['eleBat']])
        indexContTech['capInst'] = np.append(indexContTech['capInst'], [idxContInstVar['eleBat']])
        indexContTech['LifeYr'] = np.append(indexContTech['LifeYr'], [idxLifeYr['eleBat']])
        indexContTech['fixInst'] = np.append(indexContTech['fixInst'], [parEleBat['fixIns']])
        indexContTech['varInst'] = np.append(indexContTech['varInst'], [parEleBat['varIns']])

    # heat pump
    if candidateTech['HP'] == 1 and (
            (parHP['coolSta'] == 1 and len(nonzeroIndCool)) or (parHP['heatSta'] == 1 and len(nonzeroIndHeat))):
        indexContTech['binInst'] = np.append(indexContTech['binInst'], [idxBinInst['HP']])
        indexContTech['capInst'] = np.append(indexContTech['capInst'], [idxContInstVar['HP']])
        indexContTech['LifeYr'] = np.append(indexContTech['LifeYr'], [idxLifeYr['HP']])
        indexContTech['fixInst'] = np.append(indexContTech['fixInst'], [parHP['fixIns']])
        indexContTech['varInst'] = np.append(indexContTech['varInst'], [parHP['varIns']])

    # natural gas chiller
    if candidateTech['NGChill'] == 1 and len(nonzeroIndCool):
        indexContTech['binInst'] = np.append(indexContTech['binInst'], [idxBinInst['NGChill']])
        indexContTech['capInst'] = np.append(indexContTech['capInst'], [idxContInstVar['NGChill']])
        indexContTech['LifeYr'] = np.append(indexContTech['LifeYr'], [idxLifeYr['NGChill']])
        indexContTech['fixInst'] = np.append(indexContTech['fixInst'], [parNGChill['fixIns']])
        indexContTech['varInst'] = np.append(indexContTech['varInst'], [parNGChill['varIns']])

    # heat fired absorption chiller
    if candidateTech['ABSChill'] == 1 and len(nonzeroIndCool):
        indexContTech['binInst'] = np.append(indexContTech['binInst'], [idxBinInst['ABSChill']])
        indexContTech['capInst'] = np.append(indexContTech['capInst'], [idxContInstVar['ABSChill']])
        indexContTech['LifeYr'] = np.append(indexContTech['LifeYr'], [idxLifeYr['ABSChill']])
        indexContTech['fixInst'] = np.append(indexContTech['fixInst'], [parABSChill['fixIns']])
        indexContTech['varInst'] = np.append(indexContTech['varInst'], [parABSChill['varIns']])

    # electric chiller
    if candidateTech['eleChill'] == 1 and len(nonzeroIndCool):
        indexContTech['binInst'] = np.append(indexContTech['binInst'], [idxBinInst['eleChill']])
        indexContTech['capInst'] = np.append(indexContTech['capInst'], [idxContInstVar['eleChill']])
        indexContTech['LifeYr'] = np.append(indexContTech['LifeYr'], [idxLifeYr['eleChill']])
        indexContTech['fixInst'] = np.append(indexContTech['fixInst'], [parEleChill['fixIns']])
        indexContTech['varInst'] = np.append(indexContTech['varInst'], [parEleChill['varIns']])

    # PV
    if candidateTech['PV'] == 1 and len(nonzeroIndEle):
        indexContTech['binInst'] = np.append(indexContTech['binInst'], [idxBinInst['PV']])
        indexContTech['capInst'] = np.append(indexContTech['capInst'], [idxContInstVar['PV']])
        indexContTech['LifeYr'] = np.append(indexContTech['LifeYr'], [idxLifeYr['PV']])
        indexContTech['fixInst'] = np.append(indexContTech['fixInst'], [parPV['fixIns']])
        indexContTech['varInst'] = np.append(indexContTech['varInst'], [parPV['varIns'] * parPV['unitOut']])

    # solar thermal
    if candidateTech['solThem'] == 1 and len(nonzeroIndHeat):
        indexContTech['binInst'] = np.append(indexContTech['binInst'], [idxBinInst['solThem']])
        indexContTech['capInst'] = np.append(indexContTech['capInst'], [idxContInstVar['solThem']])
        indexContTech['LifeYr'] = np.append(indexContTech['LifeYr'], [idxLifeYr['solThem']])
        indexContTech['fixInst'] = np.append(indexContTech['fixInst'], [parSolThem['fixIns']])
        indexContTech['varInst'] = np.append(indexContTech['varInst'], [parSolThem['varIns'] * parSolThem['unitOut']])

    # f = fd.oovars('coefficiency of objective function')
    # indexContTech['binInst'] = map(int, indexContTech['binInst'])
    # indexContTech['LifeYr'] = map(int, indexContTech['LifeYr'])
    # indexContTech['capInst'] = map(int, indexContTech['capInst'])

    if len(indexContTech['binInst']):
        if carbOnly == 0:
            f[np.asarray(indexContTech['binInst'])] = CRF[indexContTech['LifeYr']] * np.asarray(
                indexContTech['fixInst'])

    # variable installation cost
    # assert isinstance(indexContTech.varInst, object)  # ???
    if len(indexContTech['capInst']):
        if carbOnly == 0:
            f[np.asarray(indexContTech['capInst'])] = CRF[indexContTech['LifeYr']] * np.asarray(
                indexContTech['varInst'])  # ???

    # -------------------------------------------------------------------------------------------------------------------
    # added on 09/30/2015
    # Take into account the effect of different number of weekday and weekend days
    if sampleTimeFlag is None:
        sampleTimeFlag = 0

    if parTime['numDay'] == 2 and sampleTimeFlag == 1:
        if parTime['numMonth'] < 1:
            temp_a = np.repeat([1, 1], parTime['numHour']) # only 48 hours.
        else:
            temp_a = np.repeat([21, 9], parTime['numHour'])
    # elif parTime['numDay']==7:
    else:
        temp_a = np.repeat(np.ones(parTime['numDay']), parTime['numHour'])

    temp_b = npmtlb.repmat(temp_a, 1, parTime['numMonth'])
    if len(np.shape(temp_b)) > 1:
        # temp_b = np.squeeze(temp_b)
        temp_b = temp_b.reshape((parTime['totHrStep'],))

    # natural gas boiler
    if candidateTech['NGBoil'] == 1 and len(nonzeroIndHeat):
        parNGBoil['weight_mtn'] = parNGBoil['mtnPriceNGBoil'] * temp_b
        parNGBoil['weight_ng'] = parNGBoil['gasPriceNGBoil'] * temp_b
        if carbOnly == 0:
            f[idxContOpeVar['gasNGBoil'][nonzeroIndHeat]] = (parTime['stepHr'] * parNGBoil['gasPriceNGBoil'][
                nonzeroIndHeat] + parNGBoil['mtnPriceNGBoil'][nonzeroIndHeat]) * temp_b[nonzeroIndHeat]
        if taxCO2Flag == 1:
            if carbOnly == 0:
                f[idxContOpeVar['gasNGBoil'][nonzeroIndHeat]] += parTime['stepHr'] * emiFac['natGas'] * taxCO2[
                    'natGas'] * temp_b[nonzeroIndEle]
            else:
                f[idxContOpeVar['gasNGBoil'][nonzeroIndHeat]] += parTime['stepHr'] * emiFac['natGas'] * temp_b[
                    nonzeroIndEle]

    # natural gas chiller
    if candidateTech['NGChill'] == 1 and len(nonzeroIndCool):
        parNGChill['weight_mtn'] = parNGChill['mtnPriceNGChill'] * temp_b
        parNGChill['weight_ng'] = parNGChill['gasPriceNGChill'] * temp_b
        if carbOnly == 0:
            f[idxContOpeVar['gasNGChill'][nonzeroIndCool]] = (parTime['stepHr'] * parNGChill['gasPriceNGChill'][
                nonzeroIndCool] + parNGChill['mtnPriceNGChill'][nonzeroIndCool]) * temp_b[nonzeroIndCool]
        if taxCO2Flag == 1:
            if carbOnly == 0:
                f[idxContOpeVar['gasNGChill'][nonzeroIndCool]] += parTime['stepHr'] * emiFac['natGas'] * taxCO2[
                    'natGas'] * temp_b[nonzeroIndCool]
            else:
                f[idxContOpeVar['gasNGChill'][nonzeroIndCool]] += parTime['stepHr'] * emiFac['natGas'] * temp_b[
                    nonzeroIndCool]

    # PV
    if candidateTech['PV'] == 1 and len(nonzeroIndEle):
        parPV['weight_mtn'] = parPV['mtnPricePV'] * temp_b
        if carbOnly == 0:
            f[idxContOpeVar['elePV'][nonzeroIndEle]] = parPV['mtnPricePV'][nonzeroIndEle] * temp_b[
                nonzeroIndEle]  # parTime['stepHr'] *
        if taxCO2Flag == 1:
            if carbOnly == 0:
                f[idxContInstVar['PV']] += emiFac['PV'] * taxCO2['PV']
            else:
                f[idxContInstVar['PV']] += emiFac['PV']

    # electric battery
    if candidateTech['eleBat'] == 1 and len(nonzeroIndEle):
        parEleBat['weight_mtn'] = parEleBat['mtnPriceEleBat'] * temp_b
        if carbOnly == 0:
            f[idxContOpeVar['totEleBat'][nonzeroIndEle]] = parEleBat['mtnPriceEleBat'][nonzeroIndEle] * temp_b[
                nonzeroIndEle]  # parTime['stepHr'] *

    # heat tank

    if candidateTech['heatTank'] == 1 and len(nonzeroIndHeat):
        parHeatTank['weight_mtn'] = parHeatTank['mtnPriceHeatTank'] * temp_b
        if carbOnly == 0:
            f[idxContOpeVar['totHeatTank'][nonzeroIndHeat]] = parHeatTank['mtnPriceHeatTank'][nonzeroIndHeat] * temp_b[
                nonzeroIndHeat]  # parTime['stepHr'] *
        if taxCO2Flag == 1:
            if carbOnly == 0:
                f[idxContInstVar['heatTank']] += emiFac['heatTank'] * taxCO2['heatTank']
            else:
                f[idxContInstVar['heatTank']] += emiFac['heatTank']

    # cool tank
    if candidateTech['coolTank'] == 1 and len(nonzeroIndCool):
        parCoolTank['weight_mtn'] = parCoolTank['mtnPriceCoolTank'] * temp_b
        if carbOnly == 0:
            f[idxContOpeVar['totCoolTank'][nonzeroIndCool]] = parCoolTank['mtnPriceCoolTank'][nonzeroIndCool] * temp_b[
                nonzeroIndCool]  # parTime['stepHr'] *
        if taxCO2Flag == 1:
            if carbOnly == 0:
                f[idxContInstVar['coolTank']] += emiFac['coolTank'] * taxCO2['coolTank']
            else:
                f[idxContInstVar['coolTank']] += emiFac['coolTank'] * taxCO2['coolTank']

    parWeight = {'weight_tot': np.squeeze(temp_b)}
    # electric grid
    if candidateTech['eleGrid'] == 1 and len(nonzeroIndEle):
        parEleGrid['weight_tot'] = np.squeeze(temp_b)
        parEleGrid['weight_tot_low'] = np.squeeze(parEleGrid['purPriceEleGrid_low'] * temp_b)
        parEleGrid['weight_tot_med'] = np.squeeze(parEleGrid['purPriceEleGrid_med'] * temp_b)
        parEleGrid['weight_tot_high'] = np.squeeze(parEleGrid['purPriceEleGrid_high'] * temp_b)

        parEleGrid['weight_cost'] = np.squeeze(parEleGrid['purPriceEleGrid'] * temp_b)
        parEleGrid['weight_cost_low'] = np.squeeze(
            parEleGrid['purPriceEleGrid'] * parEleGrid['purPriceEleGrid_low'] * temp_b)
        parEleGrid['weight_cost_med'] = np.squeeze(
            parEleGrid['purPriceEleGrid'] * parEleGrid['purPriceEleGrid_med'] * temp_b)
        parEleGrid['weight_cost_high'] = np.squeeze(
            parEleGrid['purPriceEleGrid'] * parEleGrid['purPriceEleGrid_high'] * temp_b)
        if carbOnly == 0:
            f[idxContOpeVar['purEleGrid'][nonzeroIndEle]] = parTime['stepHr'] * parEleGrid['purPriceEleGrid'][
                nonzeroIndEle] * temp_b[nonzeroIndEle]
            f[idxContOpeVar['salEleGrid'][nonzeroIndEle]] = -parTime['stepHr'] * parEleGrid['salPriceEleGrid'][
                nonzeroIndEle] * temp_b[nonzeroIndEle]
        # 02/04/2016
        if taxCO2Flag == 1:
            if carbOnly == 0:
                f[idxContOpeVar['purEleGrid'][nonzeroIndEle]] += parTime['stepHr'] * emiFac['ele'] * taxCO2['ele'] * \
                                                                 temp_b[nonzeroIndEle]
            else:
                f[idxContOpeVar['purEleGrid'][nonzeroIndEle]] += parTime['stepHr'] * emiFac['ele'] * temp_b[
                    nonzeroIndEle]

    # heat sale
    if candidateTech['heatSal'] == 1 and len(nonzeroIndHeat):
        f[idxContOpeVar['heatSal'][nonzeroIndHeat]] = -parTime['stepHr'] * parHeat['salPriceHeat'][nonzeroIndHeat] * \
                                                      temp_b[nonzeroIndHeat]

    # heat sale
    if candidateTech['eleChill'] == 1 and len(nonzeroIndEle):
        parEleChill['weight_mtn'] = parEleChill['mtnPriceEleChill'] * temp_b
        if carbOnly == 0:
            f[idxContOpeVar['eleEleChill'][nonzeroIndEle]] = parEleChill['mtnPriceEleChill'][nonzeroIndEle] * temp_b[
                nonzeroIndEle]

    # heat industry
    if candidateTech['heatInds'] == 1 and len(nonzeroIndHeat):
        parHeat['weight_cost'] = parHeat['purPriceInds'] * temp_b
        if carbOnly == 0:
            f[idxContOpeVar['heatInds'][nonzeroIndHeat]] = parTime['stepHr'] * parHeat['purPriceInds'][nonzeroIndHeat] * \
                                                           temp_b[nonzeroIndHeat]
        if taxCO2Flag == 1:
            if carbOnly == 0:
                f[idxContOpeVar['heatInds'][nonzeroIndHeat]] += emiFac['heatInds'] * taxCO2['heatInds'] * temp_b[
                    nonzeroIndHeat]
            else:
                f[idxContOpeVar['heatInds'][nonzeroIndHeat]] += emiFac['heatInds'] * temp_b[nonzeroIndHeat]

    # heat fired absorption chiller
    if candidateTech['ABSChill'] == 1 and len(nonzeroIndCool):
        parABSChill['weight_mtn'] = parABSChill['mtnPriceABSChill'] * temp_b
        if carbOnly == 0:
            f[idxContOpeVar['coolABSChill'][nonzeroIndCool]] = parABSChill['mtnPriceABSChill'][nonzeroIndCool] * temp_b[
                nonzeroIndCool]  # parTime['stepHr'] *

    # solar thermal
    if candidateTech['solThem'] == 1 and len(nonzeroIndHeat):
        parSolThem['weight_mtn'] = parSolThem['mtnPriceSolThem'] * temp_b
        if carbOnly == 0:
            f[idxContOpeVar['heatSolThem'][nonzeroIndHeat]] = parSolThem['mtnPriceSolThem'][nonzeroIndHeat] * temp_b[
                nonzeroIndHeat]  # parTime['stepHr'] *
        if taxCO2Flag == 1:
            if carbOnly == 0:
                f[idxContInstVar['solThem']] += emiFac['solThem'] * taxCO2['solThem']
            else:
                f[idxContInstVar['solThem']] += emiFac['solThem']

    # heat pump
    if candidateTech['HP'] == 1:
        parHP['weight_mtn'] = parHP['mtnPriceHP'] * temp_b
        if len(nonzeroIndHeat):
            if carbOnly == 0:
                f[idxContOpeVar['heatHP'][nonzeroIndHeat]] = parHP['mtnPriceHP'][nonzeroIndHeat] * temp_b[
                    nonzeroIndHeat]  # parTime['stepHr'] *
                # add the below item in objective in order to limit the electricity input of heat pump
                f[idxContOpeVar['coolHP'][zeroIndCool]] = parHP['mtnPriceHP'][zeroIndCool] * temp_b[
                    zeroIndCool]  # parTime['stepHr'] *
        elif len(nonzeroIndCool):
            if carbOnly == 0:
                f[idxContOpeVar['coolHP'][nonzeroIndCool]] = parHP['mtnPriceHP'][nonzeroIndCool] * temp_b[
                    nonzeroIndCool]  # parTime['stepHr'] *
                f[idxContOpeVar['heatHP'][zeroIndHeat]] = parHP['mtnPriceHP'][zeroIndHeat] * temp_b[
                    zeroIndHeat]  # parTime['stepHr'] *

    # -----------------------------------------------------------------------------------------------------------------------
    # operation & maintenance cost due to natural gas consumption
    # f[0][idxVarGasCHP] = stepHr * (gasPriceCHP[i, j, k] + mtnPriceCHP[i, j, k])

    if candidateTech['CHP'] == 1:
        # create coefficient matrix for gas consumption variable of CHP in the objective function
        temp = []
        # temp=np.append(temp,(np.matlib.repmat(stepHr * (gasPriceCHP[i, j, k] + mtnPriceCHP[i, j, k]),parCHP['typeCHP'],1) for i in
        # range(numMonth) for j in range(numDay) for k in range(numHour)))
        temp_c = temp_b.reshape(parTime['numMonth'], parTime['numDay'], parTime['numHour'])
        weight_mtn_chp, weight_cost_chp = [], []
        for i in range(parTime['numMonth']):
            for j in range(parTime['numDay']):
                for k in range(parTime['numHour']):
                    n1 = npmtlb.repmat(
                        (parTime['stepHr'] * parCHP['gasPriceCHP'][i, j, k] + parCHP['mtnPriceCHP'][i, j, k]) * temp_c[
                            i, j, k], np.sum(parCHP['numInst']), 1)
                    n2 = (parCHP['mtnPriceCHP'][i, j, k]) * temp_c[i, j, k]
                    n3 = (parTime['stepHr'] * parCHP['gasPriceCHP'][i, j, k]) * temp_c[i, j, k]
                    temp = np.append(temp, n1)
                    weight_mtn_chp = np.append(weight_mtn_chp, n2)
                    weight_cost_chp = np.append(weight_cost_chp, n3)
        '''
        # convert numpy ndarray into matrix
        # http://stackoverflow.com/questions/17443620/convert-a-2d-numpy-array-to-a-2d-numpy-matrix
        temp = np.asmatrix(temp)
        # convert matrix into dictionary
        # http://stackoverflow.com/questions/19991338/creating-dictionary-from-numpy-array
        temp = dict(enumerate(temp))
        '''
        # remove zero demand time period, return a dictionary
        temp = nonzero_row_chp(temp, parTime, parCHP, nonzeroIndEle, nonzeroIndHeat, nonzeroIndCool)

        # if len(nonzeroIndEle) and parCHP['eleSta']==1:

        if isinstance(temp, np.ndarray):
            parCHP['weight_mtn'] = weight_mtn_chp
            parCHP['weight_cost'] = weight_cost_chp
            if carbOnly == 0:
                f[idxContOpeVarCHP['gasCHP']] = temp
        elif isinstance(temp, dict):
            parCHP['weight_mtn'] = weight_mtn_chp[0]
            parCHP['weight_cost'] = weight_cost_chp[0]
            if carbOnly == 0:
                f[idxContOpeVarCHP['gasCHP']] = temp[0]

        if taxCO2Flag == 1:
            if carbOnly == 0:
                f[idxContOpeVarCHP['gasCHP']] += parTime['stepHr'] * emiFac['natGas'] * taxCO2['natGas']
            else:
                f[idxContOpeVarCHP['gasCHP']] += parTime['stepHr'] * emiFac['natGas']

    logging.debug("index of continuous operational variables CHP: {}".format(idxContOpeVarCHP))
    logging.debug("objective coeffients: f({}). {}".format(len(f), f))
    logging.info("objective function is built successfully! ")

    # --------------------------------------------------------------------------------------------------------------
    # ---------------------------Building equality constraints matrix ----------------------------------------------

    logging.info("Begin building the equality constraints matrix.........................")

    # 'initialize equality constraints'
    # hrAeq = np.zeros((totNumHrEqCons, numHrOpeVar))  # coefficient of equality constraints each hour
    # hrbeq = np.zeros(totNumHrEqCons)  # right hand side of equality constraints each hour
    if sparseProbFlag == 0:
        Aeq = np.zeros((totEqCons, totVars),
                       dtype=np.float32)  # coefficient of equality constraints during the whole time horizon
    elif sparseProbFlag == 1:
        Aeq = sps.lil_matrix((totEqCons, totVars), dtype=np.float32)  # imported from scipy.sparse
    beq = np.zeros(totEqCons, dtype=np.float32)  # right hand side of equality constraints during the whole time horizon

    # 'index of equality constraint for electricity/heat balance'
    # CHP
    if candidateTech['CHP'] == 1:
        if len(idxEqConsCHP):
            Aeq[idxEqConsCHP['eleCHP'], idxContOpeVarCHP['eleCHP']] = 1  # electricity from CHP

        if len(nonzeroIndEle) and parCHP['eleSta'] == 1:
            Aeq[np.repeat(idxEqCons['eleBal'][nonzeroIndEle], np.sum(parCHP['numInst'])), idxContOpeVarCHP[
                'eleCHP']] = 1  # electricity from CHP
            # 'equality constraint of CHP for converting natural gas to electricity'

            # Aeq[idxEqConsCHP['eleCHP'], idxContOpeVarCHP['gasCHP']] = np.matlib.repmat(
            #    [-parCHP['eleEff'][m] for m in range(parCHP['numType'])], 1, parTime['totHrStep'])  # natural gas consumption
            Aeq[idxEqConsCHP['eleCHP'], idxContOpeVarCHP['gasCHP']] = np.tile(
                [-parCHP['eleEff'][i] for i in range(parCHP['numType']) for j in range(parCHP['numInst'][i])],
                (1, len(nonzeroIndEle)))

        elif len(nonzeroIndHeat) and parCHP['heatSta'] == 1:
            # electricity from CHP
            Aeq[np.repeat(idxEqCons['eleBal'][nonzeroIndHeat], np.sum(parCHP['numInst'])), idxContOpeVarCHP[
                'eleCHP']] = 1
            Aeq[idxEqConsCHP['eleCHP'], idxContOpeVarCHP['gasCHP']] = np.tile(
                [-parCHP['eleEff'][i] for i in range(parCHP['numType']) for j in range(parCHP['numInst'][i])],
                (1, len(nonzeroIndHeat)))

        elif len(nonzeroIndCool) and parCHP['coolSta'] == 1:
            # electricity from CHP
            Aeq[np.repeat(idxEqCons['eleBal'][nonzeroIndCool], np.sum(parCHP['numInst'])), idxContOpeVarCHP[
                'eleCHP']] = 1
            Aeq[idxEqConsCHP['eleCHP'], idxContOpeVarCHP['gasCHP']] = np.tile(
                [-parCHP['eleEff'][i] for i in range(parCHP['numType']) for j in range(parCHP['numInst'][i])],
                (1, len(nonzeroIndCool)))
        # '''example: the size of row index should be consistent with that of coloum index
        # aa=Aeq
        # aa[[0,16,0,16],[23,26,29,31]]=1
        # '''
        if len(idxEqConsCHP):
            Aeq[idxEqConsCHP['heatCHP'], idxContOpeVarCHP['heatCHP']] = 1  # heat from CHP

        if len(nonzeroIndHeat) and parCHP['heatSta'] == 1:
            Aeq[np.repeat(idxEqCons['heatBal'][nonzeroIndHeat], np.sum(parCHP['numInst'])), idxContOpeVarCHP[
                'heatCHP']] = 1  # heat from CHP

            # 'equality constraint of CHP for correlation between electricity and heat generation'

            Aeq[idxEqConsCHP['heatCHP'], idxContOpeVarCHP['eleCHP']] = npmtlb.repmat(
                [-parCHP['heatEff'][i] for i in range(parCHP['numType']) for j in range(parCHP['numInst'][i])], 1,
                len(nonzeroIndHeat))  # electricity from CHP
        '''
        elif len(nonzeroIndEle) and parCHP['eleSta']==1:
            # if CHP is not for heating purpose, it does not need to provide heating energy during all nonzeroIdxHeat period
            Aeq[np.repeat(idxEqCons['heatBal'][nonzeroIndEle], np.sum(parCHP['numInst'])), idxContOpeVarCHP['heatCHP']] = 1  # heat from CHP

            # 'equality constraint of CHP for correlation between electricity and heat generation'
            Aeq[idxEqConsCHP['heatCHP'], idxContOpeVarCHP['eleCHP']] = npmtlb.repmat(
                                [-parCHP['heatEff'][i] for i in range(parCHP['numType']) for j in range(parCHP['numInst'][i])], 1, len(nonzeroIndEle))  # electricity from CHP


        elif len(nonzeroIndCool) and parCHP['coolSta']==1:
            # if CHP is not for heating purpose, it does not need to provide heating energy during all nonzeroIdxHeat period
            Aeq[np.repeat(idxEqCons['heatBal'][nonzeroIndCool], np.sum(parCHP['numInst'])), idxContOpeVarCHP['heatCHP']] = 1  # heat from CHP

            # 'equality constraint of CHP for correlation between electricity and heat generation'
            Aeq[idxEqConsCHP['heatCHP'], idxContOpeVarCHP['eleCHP']] = npmtlb.repmat(
                                [-parCHP['heatEff'][i] for i in range(parCHP['numType']) for j in range(parCHP['numInst'][i])], 1, len(nonzeroIndCool))  # electricity from CHP
        '''
    # 'equality constraints for electricity balance'
    # ''' which option is time efficient among the three choices???
    # option 1

    # electrical grid
    if candidateTech['eleGrid'] == 1 and len(nonzeroIndEle):
        Aeq[idxEqCons['eleBal'][nonzeroIndEle], idxContOpeVar['purEleGrid'][
            nonzeroIndEle]] = 1  # electricity purchasing from power grid
        Aeq[idxEqCons['eleBal'][nonzeroIndEle], idxContOpeVar['salEleGrid'][
            nonzeroIndEle]] = -1  # electricity sold back to power grid

    # PV
    if candidateTech['PV'] == 1 and len(nonzeroIndEle):
        Aeq[idxEqCons['eleBal'][nonzeroIndEle], idxContOpeVar['elePV'][nonzeroIndEle]] = 1  # electricity from PV

    # electric battery
    if candidateTech['eleBat'] == 1 and len(nonzeroIndEle):
        Aeq[idxEqCons['eleBal'][nonzeroIndEle], idxContOpeVar['fromEleBat'][
            nonzeroIndEle]] = 1  # electricity released from electric storage
        Aeq[idxEqCons['eleBal'][nonzeroIndEle], idxContOpeVar['stoEleBat'][
            nonzeroIndEle]] = -1  # electricity supplied to electric storage

    # electric boiler
    if candidateTech['eleBoil'] == 1 and len(nonzeroIndHeat):
        Aeq[idxEqCons['eleBal'][nonzeroIndEle], idxContOpeVar['eleEleBoil'][
            nonzeroIndEle]] = -1  # electricity consumption by electric boiler

    # heat pump
    # there is possibility that both nonzeroIndHeat and nonzeroIndCool are nonempty
    # there could be the case when some periods are associated with both heating and cooling demand
    if candidateTech['HP'] == 1:
        if len(nonzeroIndHeat):
            Aeq[idxEqCons['eleBal'][nonzeroIndHeat], idxContOpeVar['eleHP'][
                nonzeroIndHeat]] = -1  # electricity consumption by heat pump
        if len(nonzeroIndCool):
            Aeq[idxEqCons['eleBal'][nonzeroIndCool], idxContOpeVar['eleHP'][nonzeroIndCool]] = -1

    # electric chiller
    if candidateTech['eleChill'] == 1 and len(nonzeroIndCool):
        Aeq[idxEqCons['eleBal'][nonzeroIndCool], idxContOpeVar['eleEleChill'][
            nonzeroIndCool]] = -1  # electricity consumption by electric chiller

    if len(nonzeroIndEle):
        beq[idxEqCons['eleBal'][nonzeroIndEle]] = eleLoad[nonzeroIndEle]  # electricity consumption at customer side
        # remove the electricity balance constraint with zero electric demand
        # beq = popzeroElement(beq,zeroIndEle, nonzeroIndEle, idxEqCons['eleBal'])
        # removing corresponding rows from Aeq
        # Aeq = popzeroElement(Aeq,zeroIndEle, nonzeroIndEle, idxEqCons['eleBal'])

    # 'equality constraints for heat balance'



    # option 1
    # heat industry
    if candidateTech['heatInds'] == 1 and len(nonzeroIndHeat):
        Aeq[idxEqCons['heatBal'][nonzeroIndHeat], idxContOpeVar['heatInds'][
            nonzeroIndHeat]] = 1  # industry waste heat, heatInds

    # heat pump
    if candidateTech['HP'] == 1 and len(nonzeroIndHeat) and parHP['heatSta'] == 1:
        Aeq[idxEqCons['heatBal'][nonzeroIndHeat], idxContOpeVar['heatHP'][nonzeroIndHeat]] = 1  # heat from heat pump

    # electric boiler
    if candidateTech['eleBoil'] == 1 and len(nonzeroIndHeat):
        Aeq[idxEqCons['heatBal'][nonzeroIndHeat], idxContOpeVar['heatEleBoil'][
            nonzeroIndHeat]] = 1  # heat from electric boiler

    # natural gas boiler
    if candidateTech['NGBoil'] == 1 and len(nonzeroIndHeat):
        Aeq[idxEqCons['heatBal'][nonzeroIndHeat], idxContOpeVar['heatNGBoil'][
            nonzeroIndHeat]] = 1  # heat from natural gas boiler

    # solar thermal
    if candidateTech['solThem'] == 1 and len(nonzeroIndHeat):
        Aeq[idxEqCons['heatBal'][nonzeroIndHeat], idxContOpeVar['heatSolThem'][
            nonzeroIndHeat]] = 1  # heat from solar thermal

    # heat tank
    if candidateTech['heatTank'] == 1 and len(nonzeroIndHeat):
        Aeq[idxEqCons['heatBal'][nonzeroIndHeat], idxContOpeVar['fromHeatTank'][
            nonzeroIndHeat]] = 1  # heat released from thermal storage
        Aeq[idxEqCons['heatBal'][nonzeroIndHeat], idxContOpeVar['stoHeatTank'][
            nonzeroIndHeat]] = -1  # heat supplied to thermal storage

    # heat fired absorption chiller
    # 09/20/2015, it is not necessary that ABSchill is used whenever heating load exists
    # it only works to supply cooling load with heating energy input
    # heating balance exists even if no heating demand is needed
    # because ABSchill needs heating input for cooling purpose during summer season
    # for ABSchill, it appears in the heating balance based on cooling demand
    if candidateTech['ABSChill'] == 1 and len(nonzeroIndCool):
        Aeq[idxEqCons['heatBal'][nonzeroIndCool], idxContOpeVar['heatABSChill'][
            nonzeroIndCool]] = -1  # heat consumed by heat fired absorption chiller

    # heat sale
    if candidateTech['heatSal'] == 1 and len(nonzeroIndHeat):
        Aeq[idxEqCons['heatBal'][nonzeroIndHeat], idxContOpeVar['heatSal'][nonzeroIndHeat]] = -1
    # '''
    # option 2
    # Aeq[idxEqCons['heatBal'], [idxContOpeVar['heatInds'], idxContOpeVar['heatHP'], idxContOpeVar['heatEleBoil'],
    #                           idxContOpeVar['heatNGBoil'], idxContOpeVar['heatSolThem'], idxContOpeVar['fromHeatTank'],
    #                           idxContOpeVar['stoHeatTank'], idxContOpeVar['heatABSChill'], idxContOpeVar['heatSal']]] = [
    #    [1] * parTime['totHrStep'], [1] * parTime['totHrStep'], [1] * parTime['totHrStep'], [1] * parTime['totHrStep'],
    #    [1] * parTime['totHrStep'], [1] * parTime['totHrStep'], [-1] * parTime['totHrStep'], [-1] * parTime['totHrStep'],
    #    [-1] * parTime['totHrStep']]

    if len(nonzeroIndHeat):
        if pipeNetwFlag == 0:
            # heat consumption by customers and loss by pipeline
            beq[idxEqCons['heatBal'][nonzeroIndHeat]] = heatLoad[nonzeroIndHeat]
        elif pipeNetwFlag != 0:
            # modified on 11/23/15, calculate thermal loss based on pipeline network
            beq[idxEqCons['heatBal'][nonzeroIndHeat]] = heatLoad[nonzeroIndHeat] + \
                                                        (pipeHeatLoss[nonzeroIndHeat] if pipeNetwFlag else 0)

    # 'cooling balance'
    # '''
    # option 1
    # heat pump
    if candidateTech['HP'] == 1 and len(nonzeroIndCool) and parHP['coolSta'] == 1:
        Aeq[idxEqCons['coolBal'][nonzeroIndCool], idxContOpeVar['coolHP'][nonzeroIndCool]] = 1  # cooling from heat pump

    # electric chiller
    if candidateTech['eleChill'] == 1 and len(nonzeroIndCool):
        Aeq[idxEqCons['coolBal'][nonzeroIndCool], idxContOpeVar['coolEleChill'][
            nonzeroIndCool]] = 1  # cooling from electric chiller

    # heat fired absorption chiller
    if candidateTech['ABSChill'] == 1 and len(nonzeroIndCool):
        Aeq[idxEqCons['coolBal'][nonzeroIndCool], idxContOpeVar['coolABSChill'][
            nonzeroIndCool]] = 1  # cooling from heat fired absorption chiller

    # natural gas chiller
    if candidateTech['NGChill'] == 1 and len(nonzeroIndCool):
        Aeq[idxEqCons['coolBal'][nonzeroIndCool], idxContOpeVar['coolNGChill'][
            nonzeroIndCool]] = 1  # cooling from natural gas chiller

    # cool tank
    if candidateTech['coolTank'] == 1 and len(nonzeroIndCool):
        Aeq[idxEqCons['coolBal'][nonzeroIndCool], idxContOpeVar['fromCoolTank'][
            nonzeroIndCool]] = 1  # cool released from thermal storage
        Aeq[idxEqCons['coolBal'][nonzeroIndCool], idxContOpeVar['stoCoolTank'][
            nonzeroIndCool]] = -1  # cool supplied to thermal storage
    # '''
    # option 2
    # Aeq[idxEqCons['coolBal'], [idxContOpeVar['coolHP'], idxContOpeVar['coolEleChill'], idxContOpeVar['coolABSChill'],
    #                           idxContOpeVar['coolNGChill']]] = [[1] * parTime['totHrStep'], [1] * parTime['totHrStep'],
    #                                                             [1] * parTime['totHrStep'], [1] * parTime['totHrStep']]

    # cooling consumption by customers and loss by pipeline
    if len(nonzeroIndCool):
        if pipeNetwFlag == 0:
            beq[idxEqCons['coolBal'][nonzeroIndCool]] = coolLoad[nonzeroIndCool]
        elif pipeNetwFlag != 0:
            beq[idxEqCons['coolBal'][nonzeroIndCool]] = coolLoad[nonzeroIndCool] + \
                                                        (pipeCoolLoss[nonzeroIndCool] if pipeNetwFlag else 0)

    # 'natural gas boiler'
    if candidateTech['NGBoil'] == 1 and len(nonzeroIndHeat):
        Aeq[idxEqCons['NGBoil'][nonzeroIndHeat], [idxContOpeVar['heatNGBoil'][nonzeroIndHeat],
                                                  idxContOpeVar['gasNGBoil'][nonzeroIndHeat]]] = npmtlb.repmat(
            [1, -parNGBoil['themEff']], len(nonzeroIndHeat), 1).transpose()

    # 'electric boiler'
    if candidateTech['eleBoil'] == 1 and len(nonzeroIndHeat):
        Aeq[idxEqCons['eleBoil'][nonzeroIndHeat], [idxContOpeVar['eleEleBoil'][nonzeroIndHeat],
                                                   idxContOpeVar['heatEleBoil'][nonzeroIndHeat]]] = npmtlb.repmat(
            [-parEleBoil['themEff'], 1], len(nonzeroIndHeat), 1).transpose()

    #  natural gas fired chiller: equality constraint for cooling to heat
    if candidateTech['NGChill'] == 1 and len(nonzeroIndCool):
        Aeq[idxEqCons['coolNGChill'][nonzeroIndCool], [idxContOpeVar['coolNGChill'][nonzeroIndCool],
                                                       idxContOpeVar['gasNGChill'][nonzeroIndCool]]] = npmtlb.repmat(
            [1, -parNGChill['coolCOP']], len(nonzeroIndCool), 1).transpose()

    # 'heat fired absorption chiller'

    if candidateTech['ABSChill'] == 1 and len(nonzeroIndCool):
        Aeq[idxEqCons['ABSChill'][nonzeroIndCool], [idxContOpeVar['coolABSChill'][nonzeroIndCool],
                                                    idxContOpeVar['heatABSChill'][nonzeroIndCool]]] = npmtlb.repmat(
            [1, -parABSChill['coolCOP']], len(nonzeroIndCool), 1).transpose()
    # 'electric chiller'
    if candidateTech['eleChill'] == 1 and len(nonzeroIndCool):
        Aeq[idxEqCons['eleChill'][nonzeroIndCool], [idxContOpeVar['coolEleChill'][nonzeroIndCool],
                                                    idxContOpeVar['eleEleChill'][nonzeroIndCool]]] = npmtlb.repmat(
            [1, -parEleChill['coolCOP']], len(nonzeroIndCool), 1).transpose()
    # ===========================================================================================================
    # calling function 'calc_idx_sto' to calculate the index of total energy storage at hour h-1
    # 10/06/2015, removing variables with zero heating demand
    # if candidateTech['heatTank'] == 1 and len(nonzeroIndHeat):
    #    idxPreTotHeatTank = calc_idx_sto(idxContOpe['totHeatTank'], parTime, numVarMat)

    # if candidateTech['coolTank'] == 1 and len(nonzeroIndCool):
    #    idxPreTotCoolTank = calc_idx_sto(idxContOpe['totCoolTank'], parTime, numVarMat)

    # calling function 'calc_idx_sto' to calculate the index of total energy storage at hour h-1
    if candidateTech['eleBat'] == 1 and len(nonzeroIndEle):
        idxPreTotEleBat = calc_idx_sto(idxContOpe['totEleBat'], parTime, numVarMat)

        # if candidateTech['eleBat'] == 1 and len(nonzeroIndEle):
        Aeq[idxEqCons["dynEleBat"], [idxContOpeVar['totEleBat'],
                                     idxContOpeVar['stoEleBat'],
                                     idxContOpeVar['fromEleBat']]] = npmtlb.repmat(
            [-1 * parTime['stepHr'], parEleBat['chgEff'] * parTime['stepHr'],
             -1 / float(parEleBat['disEff']) * parTime['stepHr']], parTime['totHrStep'], 1).transpose()

        # beq[idxEqCons["eleBat"]]= [(parEleBat['decayFac']-1)*iniTotSto for i in
        #               range(parTime['numMonth']) for j in range(parTime['numDay']) for k in range(parTime['numHour'] if k = 0)]

        # modify on 1/20/2016
        # [Aeq, beq] = dymChg(idxEqCons["dynEleBat"], parTime, Aeq, beq, (parEleBat['decayFac']-1) * parEleBat['iniTot'],
        #                     -(parEleBat['decayFac']-1) * parTime['stepHr'], idxPreTotEleBat)
        # [idxContInstVar['eleBat'] for i in range(parTime['totHrStep'])]
        [Aeq, beq] = dym_chg(idxEqCons["dynEleBat"], idxContInstVar['eleBat'], parTime, Aeq, beq,
                             (1 - parEleBat['decayFac']) * parEleBat['iniSoc'],
                             (1 - parEleBat['decayFac']) * parTime['stepHr'], idxPreTotEleBat, parEleBat['mode'])

        # set the following active if end of charge condition is required
        if parTime['numHour'] == 24 and parEleBat['mode'] == 'cycleCharge':
            #    [Aeq, beq] = endChg(idxEndChgSto["endEleBat"], parTime, Aeq, beq, idxContOpeVar['totEleBat'], parEleBat['iniTot'])
            [Aeq, beq] = end_chg(idxEndChgSto["endEleBat"], idxContInstVar['eleBat'], parTime, Aeq, beq,
                                 idxContOpeVar['totEleBat'], parEleBat['iniSoc'])
    # ============================================================================================================================
    # 'dynamics of inventory level of the thermal storage, similar to that for electric battary'
    # 10/06/2015, removing variables with zero heating demand
    if candidateTech['heatTank'] == 1 and len(nonzeroIndHeat):
        idxPreTotHeatTank = calc_idx_sto(idxContOpe['totHeatTank'], parTime, numVarMat)

        Aeq[idxEqCons['dynHeatTank'], [idxContOpeVar['totHeatTank'],
                                       idxContOpeVar['stoHeatTank'],
                                       idxContOpeVar['fromHeatTank']]] = npmtlb.repmat(
            [-1 * parTime['stepHr'], parHeatTank['chgEff'] * parTime['stepHr'],
             -1 / float(parHeatTank['disEff']) * parTime['stepHr']], parTime['totHrStep'], 1).transpose()

        # [Aeq, beq] = dymChg(idxEqCons["dynHeatTank"], parTime, Aeq, beq, (parHeatTank['decayFac'] - 1) * parHeatTank['iniTot'],
        #                    -(parHeatTank['decayFac'] - 1) * parTime['stepHr'], idxPreTotHeatTank)
        [Aeq, beq] = dym_chg(idxEqCons["dynHeatTank"], idxContInstVar['heatTank'], parTime, Aeq, beq,
                             -(parHeatTank['decayFac'] - 1) * parHeatTank['iniSoc'],
                             -(parHeatTank['decayFac'] - 1) * parTime['stepHr'], idxPreTotHeatTank, parHeatTank['mode'])

        # set the following active if end of charge condition is required
        if parTime['numHour'] == 24 and parHeatTank['mode'] == 'cycleCharge':
            # [Aeq, beq] = endChg(idxEndChgSto["endHeatTank"], parTime, Aeq, beq, idxContOpeVar['totHeatTank'], parHeatTank['iniTot'])
            [Aeq, beq] = end_chg(idxEndChgSto["endHeatTank"], idxContInstVar['heatTank'], parTime, Aeq, beq,
                                 idxContOpeVar['totHeatTank'], parHeatTank['iniSoc'])

    # cooling storage
    if candidateTech['coolTank'] == 1 and len(nonzeroIndCool):
        idxPreTotCoolTank = calc_idx_sto(idxContOpe['totCoolTank'], parTime, numVarMat)

        Aeq[idxEqCons['dynCoolTank'], [idxContOpeVar['totCoolTank'],
                                       idxContOpeVar['stoCoolTank'],
                                       idxContOpeVar['fromCoolTank']]] = npmtlb.repmat(
            [-1 * parTime['stepHr'], parCoolTank['chgEff'] * parTime['stepHr'],
             -1 / float(parCoolTank['disEff']) * parTime['stepHr']], parTime['totHrStep'], 1).transpose()

        # [Aeq, beq] = dymChg(idxEqCons["dynCoolTank"], parTime, Aeq, beq, (parCoolTank['decayFac'] - 1) * parCoolTank['iniTot'],
        #                    -(parCoolTank['decayFac'] - 1) * parTime['stepHr'], idxPreTotCoolTank)
        [Aeq, beq] = dym_chg(idxEqCons["dynCoolTank"], idxContInstVar['coolTank'], parTime, Aeq, beq,
                             -(parCoolTank['decayFac'] - 1) * parCoolTank['iniSoc'],
                             -(parCoolTank['decayFac'] - 1) * parTime['stepHr'], idxPreTotCoolTank, parCoolTank['mode'])

        # set the following active if end of charge condition is required
        if parTime['numHour'] == 24 and parCoolTank['mode'] == 'cycleCharge':
            # [Aeq, beq] = endChg(idxEndChgSto["endCoolTank"], parTime, Aeq, beq, idxContOpeVar['totCoolTank'], parCoolTank['iniTot'])
            [Aeq, beq] = end_chg(idxEndChgSto["endCoolTank"], idxContInstVar['coolTank'], parTime, Aeq, beq,
                                 idxContOpeVar['totCoolTank'], parCoolTank['iniSoc'])

    print("Equality constraints matrix built successfully!\n")



    # ------------------------------------------------------------------------------------------------------
    # ---------------------------Building inequality constraints matrix ------------------------------------

    print("begin building inequality constraints matrix ......................")
    # 'initialize inequality constraints'
    if sparseProbFlag == 0:
        Aineq = np.zeros((totIneqCons, totVars), dtype=np.float32)
    elif sparseProbFlag == 1:
        Aineq = sps.lil_matrix((totIneqCons, totVars), dtype=np.float32)
    bineq = np.zeros(totIneqCons, dtype=np.float32)

    # 08/07/2015, max monthly demand charge
    idxVarMaxDemand = []
    if candidateTech['demandCharge'] == 1:
        idxIneqConMaxDemand = np.asarray(
            [idxIneqMaxDemand + i * parTime['numHour'] * parTime['numDay'] + j * parTime['numHour'] + k
             for i in range(parTime['numMonth'])
             for j in range(parTime['numDay'])
             for k in range(parTime['numHour'])])

        idxVarMaxDemand = np.arange(idxMaxDemand, idxMaxDemand + parTime['numMonth'])
        # electricity purchase
        Aineq[idxIneqConMaxDemand, idxContOpeVar['purEleGrid']] = 1
        Aineq[idxIneqConMaxDemand, np.repeat(idxVarMaxDemand, parTime['numDay'] * parTime['numHour'])] = -1

    # inequality constraints of electric grid
    # maximum purchasing electricity from power grid
    # A[idxIneqCons['purEleGrid'], [idxContOpeVar['purEleGrid'], [idxBinOpeVar['purEleGrid'] for i in range(len(idxIneqCons['purEleGrid']))]]] = [
    #  [1] * parTime['totHrStep'], [- parEleGrid['maxPur']] * parTime['totHrStep']]

    if candidateTech['eleGrid'] == 1 and len(nonzeroIndEle):
        Aineq[idxIneqCons['maxPurEleGrid'][nonzeroIndEle], [idxContOpeVar['purEleGrid'][nonzeroIndEle],
                                                            idxBinOpeVar['purEleGrid'][nonzeroIndEle]]] = npmtlb.repmat(
            [1, -parEleGrid['maxPur']], len(nonzeroIndEle), 1).transpose()

        # minimum purchasing electricity from power grid
        Aineq[idxIneqCons['minPurEleGrid'][nonzeroIndEle], [idxContOpeVar['purEleGrid'][nonzeroIndEle],
                                                            idxBinOpeVar['purEleGrid'][nonzeroIndEle]]] = npmtlb.repmat(
            [-1, parEleGrid['minPur']], len(nonzeroIndEle), 1).transpose()

        Aineq[idxIneqCons['maxSalEleGrid'][nonzeroIndEle], [idxContOpeVar['salEleGrid'][nonzeroIndEle],
                                                            idxBinOpeVar['salEleGrid'][nonzeroIndEle]]] = npmtlb.repmat(
            [1, -parEleGrid['maxSal']], len(nonzeroIndEle), 1).transpose()

        # minimum selling electricity to power grid
        Aineq[idxIneqCons['minSalEleGrid'][nonzeroIndEle], [idxContOpeVar['salEleGrid'][nonzeroIndEle],
                                                            idxBinOpeVar['salEleGrid'][nonzeroIndEle]]] = npmtlb.repmat(
            [-1, parEleGrid['minSal']], len(nonzeroIndEle), 1).transpose()

        # upper bound for electricity exchange status
        Aineq[idxIneqCons['upEleExchg'][nonzeroIndEle], [idxBinOpeVar['salEleGrid'][nonzeroIndEle],
                                                         idxBinOpeVar['purEleGrid'][nonzeroIndEle]]] = npmtlb.repmat(
            [1, 1], len(nonzeroIndEle), 1).transpose()

        bineq[idxIneqCons['upEleExchg'][nonzeroIndEle]] = [parEleGrid['outType']] * len(nonzeroIndEle)
        # upper bound for electricity exchange status
        Aineq[idxIneqCons['minEleExchg'][nonzeroIndEle], [idxBinOpeVar['salEleGrid'][nonzeroIndEle],
                                                          idxBinOpeVar['purEleGrid'][nonzeroIndEle]]] = npmtlb.repmat(
            [-1, -1], len(nonzeroIndEle), 1).transpose()

    # inequality constraints of industry waste heat
    if candidateTech['heatInds'] == 1 and len(nonzeroIndHeat):
        Aineq[idxIneqCons['upHeatInds'][nonzeroIndHeat], [idxContOpeVar['heatInds'][nonzeroIndHeat],
                                                          idxBinOpeVar['heatInds'][nonzeroIndHeat]]] = npmtlb.repmat(
            [1, -parHeat['maxPur']], len(nonzeroIndHeat), 1).transpose()

        Aineq[idxIneqCons['minHeatInds'][nonzeroIndHeat], [idxContOpeVar['heatInds'][nonzeroIndHeat],
                                                           idxBinOpeVar['heatInds'][nonzeroIndHeat]]] = npmtlb.repmat(
            [-1, parHeat['minPur']], len(nonzeroIndHeat), 1).transpose()

    # inequality constraints of selling heat
    if candidateTech['heatSal'] == 1 and len(nonzeroIndHeat):
        Aineq[idxIneqCons['upHeatSal'][nonzeroIndHeat], [idxContOpeVar['heatSal'][nonzeroIndHeat],
                                                         idxBinOpeVar['heatSal'][nonzeroIndHeat]]] = npmtlb.repmat(
            [1, -parHeat['maxSal']], len(nonzeroIndHeat), 1).transpose()

        Aineq[idxIneqCons['minHeatSal'][nonzeroIndHeat], [idxContOpeVar['heatSal'][nonzeroIndHeat],
                                                          idxBinOpeVar['heatSal'][nonzeroIndHeat]]] = npmtlb.repmat(
            [-1, parHeat['minSal']], len(nonzeroIndHeat), 1).transpose()

    if candidateTech['heatInds'] == 1 and candidateTech['heatSal'] == 1 and len(nonzeroIndHeat):
        Aineq[idxIneqCons['upHeatExchg'][nonzeroIndHeat], [idxBinOpeVar['heatSal'][nonzeroIndHeat],
                                                           idxBinOpeVar['heatInds'][nonzeroIndHeat]]] = npmtlb.repmat(
            [1, 1], len(nonzeroIndHeat), 1).transpose()

        bineq[idxIneqCons['upHeatExchg'][nonzeroIndHeat]] = [parHeat['outType']] * len(nonzeroIndHeat)
    # ========================================================================================================================
    'electric battery'
    # inequality constraints of electric battery: net electricity sent to battery for storage
    if candidateTech['eleBat'] == 1 and len(nonzeroIndEle):
        # added on  10/30/15
        # upInEleBat
        Aineq[idxIneqCons['upInEleBat'], [idxContOpeVar['stoEleBat'], idxContOpeVar['upInYEleBat']]] = npmtlb.repmat(
            [parEleBat['chgEff'] * parTime['stepHr'], -1], parTime['totHrStep'], 1).transpose()

        # upInX1
        Aineq[idxIneqCons['upInX1EleBat'], [idxContOpeVar['upInYEleBat'], idxBinOpeVar['chgEleBat']]] = npmtlb.repmat(
            [1, -parEleBat['maxChg']], parTime['totHrStep'], 1).transpose()

        # upInX2
        Aineq[idxIneqCons['upInX2EleBat'], [idxContOpeVar['upInYEleBat'],
                                            [idxContInstVar['eleBat'] for i in
                                             range(parTime['totHrStep'])]]] = npmtlb.repmat(
            [1, -parEleBat['maxFracChg'] * parEleBat['unitOut']], parTime['totHrStep'], 1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['upInX2EleBat'], parTime, Aineq, bineq, -parEleBat['maxFracChg'] * parEleBat['iniTot'],
        #                    parEleBat['maxFracChg'] * parTime['stepHr'], idxPreTotEleBat)
        [Aineq, bineq] = dym_chg(idxIneqCons['upInX2EleBat'], idxContInstVar['eleBat'], parTime, Aineq, bineq,
                                 parEleBat['maxFracChg'] * parEleBat['iniSoc'],
                                 parEleBat['maxFracChg'] * parTime['stepHr'], idxPreTotEleBat, parEleBat['mode'])
        # upInX12
        Aineq[idxIneqCons['upInX12EleBat'], [idxContOpeVar['upInYEleBat'], idxBinOpeVar['chgEleBat'],
                                             [idxContInstVar['eleBat'] for i in
                                              range(parTime['totHrStep'])]]] = npmtlb.repmat(
            [-1, parEleBat['maxChg'], parEleBat['maxFracChg'] * parEleBat['unitOut']], parTime['totHrStep'],
            1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['upInX12EleBat'], parTime, Aineq, bineq, parEleBat['maxFracChg'] * parEleBat['iniTot'],
        #                    -parEleBat['maxFracChg'] * parTime['stepHr'], idxPreTotEleBat)
        [Aineq, bineq] = dym_chg(idxIneqCons['upInX12EleBat'], idxContInstVar['eleBat'], parTime, Aineq, bineq,
                                 -parEleBat['maxFracChg'] * parEleBat['iniSoc'],
                                 -parEleBat['maxFracChg'] * parTime['stepHr'], idxPreTotEleBat, parEleBat['mode'])

        bineq[idxIneqCons['upInX12EleBat']] = bineq[idxIneqCons['upInX12EleBat']] + parEleBat['maxChg']

        # minInEleBat
        Aineq[idxIneqCons['minInEleBat'], [idxContOpeVar['stoEleBat'], idxContOpeVar['minInYEleBat']]] = npmtlb.repmat(
            [-parEleBat['chgEff'] * parTime['stepHr'], 1], parTime['totHrStep'], 1).transpose()

        # minInX1
        Aineq[idxIneqCons['minInX1EleBat'], [idxContOpeVar['minInYEleBat'], idxBinOpeVar['chgEleBat']]] = npmtlb.repmat(
            [1, -parEleBat['minChg']], parTime['totHrStep'], 1).transpose()

        # minInX2
        Aineq[idxIneqCons['minInX2EleBat'], [idxContOpeVar['minInYEleBat'],
                                             [idxContInstVar['eleBat'] for i in
                                              range(parTime['totHrStep'])]]] = npmtlb.repmat(
            [1, -parEleBat['minFracChg'] * parEleBat['unitOut']], parTime['totHrStep'], 1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['minInX2EleBat'], parTime, Aineq, bineq, -parEleBat['minFracChg'] * parEleBat['iniTot'],
        #                    parEleBat['minFracChg'] * parTime['stepHr'], idxPreTotEleBat)
        [Aineq, bineq] = dym_chg(idxIneqCons['minInX2EleBat'], idxContInstVar['eleBat'], parTime, Aineq, bineq,
                                 parEleBat['minFracChg'] * parEleBat['iniSoc'],
                                 parEleBat['minFracChg'] * parTime['stepHr'], idxPreTotEleBat, parEleBat['mode'])

        # minInX12
        Aineq[idxIneqCons['minInX12EleBat'], [idxContOpeVar['minInYEleBat'], idxBinOpeVar['chgEleBat'],
                                              [idxContInstVar['eleBat'] for i in
                                               range(parTime['totHrStep'])]]] = npmtlb.repmat(
            [-1, parEleBat['minChg'], parEleBat['minFracChg'] * parEleBat['unitOut']], parTime['totHrStep'],
            1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['minInX12EleBat'], parTime, Aineq, bineq, parEleBat['minFracChg'] * parEleBat['iniTot'],
        #                    -parEleBat['minFracChg'] * parTime['stepHr'], idxPreTotEleBat)
        [Aineq, bineq] = dym_chg(idxIneqCons['minInX12EleBat'], idxContInstVar['eleBat'], parTime, Aineq, bineq,
                                 -parEleBat['minFracChg'] * parEleBat['iniSoc'],
                                 -parEleBat['minFracChg'] * parTime['stepHr'], idxPreTotEleBat, parEleBat['mode'])

        bineq[idxIneqCons['minInX12EleBat']] = bineq[idxIneqCons['minInX12EleBat']] + parEleBat['minChg']

        # inequality constraints of electric battery: gross electricity released from battery
        # upOutEleBat
        Aineq[idxIneqCons['upOutEleBat'], [idxContOpeVar['fromEleBat'], idxContOpeVar['upOutYEleBat']]] = npmtlb.repmat(
            [1.0 / parEleBat['disEff'] * parTime['stepHr'], -1], parTime['totHrStep'], 1).transpose()

        # upOutX1
        Aineq[idxIneqCons['upOutX1EleBat'], [idxContOpeVar['upOutYEleBat'], idxBinOpeVar['disEleBat']]] = npmtlb.repmat(
            [1, -parEleBat['maxDis']], parTime['totHrStep'], 1).transpose()

        # upOutX2
        Aineq[idxIneqCons['upOutX2EleBat'], [idxContOpeVar['upOutYEleBat']]] = npmtlb.repmat([1], parTime['totHrStep'],
                                                                                             1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['upOutX2EleBat'], parTime, Aineq, bineq, parEleBat['maxFracDis'] * parEleBat['iniTot'],
        #                    -parEleBat['maxFracDis'] * parTime['stepHr'], idxPreTotEleBat)
        [Aineq, bineq] = dym_chg(idxIneqCons['upOutX2EleBat'], idxContInstVar['eleBat'], parTime, Aineq, bineq,
                                 -parEleBat['maxFracDis'] * parEleBat['iniSoc'],
                                 -parEleBat['maxFracDis'] * parTime['stepHr'], idxPreTotEleBat, parEleBat['mode'])

        # upOutX12
        Aineq[
            idxIneqCons['upOutX12EleBat'], [idxContOpeVar['upOutYEleBat'], idxBinOpeVar['disEleBat']]] = npmtlb.repmat(
            [-1, parEleBat['maxDis']], parTime['totHrStep'], 1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['upOutX12EleBat'], parTime, Aineq, bineq, -parEleBat['maxFracDis'] * parEleBat['iniTot'],
        #                    parEleBat['maxFracDis'] * parTime['stepHr'], idxPreTotEleBat)
        [Aineq, bineq] = dym_chg(idxIneqCons['upOutX12EleBat'], idxContInstVar['eleBat'], parTime, Aineq, bineq,
                                 parEleBat['maxFracDis'] * parEleBat['iniSoc'],
                                 parEleBat['maxFracDis'] * parTime['stepHr'], idxPreTotEleBat, parEleBat['mode'])

        bineq[idxIneqCons['upOutX12EleBat']] = bineq[idxIneqCons['upOutX12EleBat']] + parEleBat['maxDis']

        # minOutEleBat
        Aineq[
            idxIneqCons['minOutEleBat'], [idxContOpeVar['fromEleBat'], idxContOpeVar['minOutYEleBat']]] = npmtlb.repmat(
            [-1.0 / parEleBat['disEff'] * parTime['stepHr'], 1], parTime['totHrStep'], 1).transpose()

        # minOutX1
        Aineq[
            idxIneqCons['minOutX1EleBat'], [idxContOpeVar['minOutYEleBat'], idxBinOpeVar['disEleBat']]] = npmtlb.repmat(
            [1, -parEleBat['minDis']], parTime['totHrStep'], 1).transpose()

        # minOutX2
        Aineq[idxIneqCons['minOutX2EleBat'], [idxContOpeVar['minOutYEleBat']]] = npmtlb.repmat([1],
                                                                                               parTime['totHrStep'],
                                                                                               1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['minOutX2EleBat'], parTime, Aineq, bineq, parEleBat['minFracDis'] * parEleBat['iniTot'],
        #                    -parEleBat['minFracDis'] * parTime['stepHr'], idxPreTotEleBat)
        [Aineq, bineq] = dym_chg(idxIneqCons['minOutX2EleBat'], idxContInstVar['eleBat'], parTime, Aineq, bineq,
                                 -parEleBat['minFracDis'] * parEleBat['iniSoc'],
                                 -parEleBat['minFracDis'] * parTime['stepHr'], idxPreTotEleBat, parEleBat['mode'])

        # minOutX12
        Aineq[idxIneqCons['minOutX12EleBat'], [idxContOpeVar['minOutYEleBat'],
                                               idxBinOpeVar['disEleBat']]] = npmtlb.repmat(
            [-1, parEleBat['minDis']], parTime['totHrStep'], 1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['minOutX12EleBat'], parTime, Aineq, bineq, -parEleBat['minFracDis'] * parEleBat['iniTot'],
        #                    parEleBat['minFracDis'] * parTime['stepHr'], idxPreTotEleBat)
        [Aineq, bineq] = dym_chg(idxIneqCons['minOutX12EleBat'], idxContInstVar['eleBat'], parTime, Aineq, bineq,
                                 parEleBat['minFracDis'] * parEleBat['iniSoc'],
                                 parEleBat['minFracDis'] * parTime['stepHr'], idxPreTotEleBat, parEleBat['mode'])

        bineq[idxIneqCons['minOutX12EleBat']] = bineq[idxIneqCons['minOutX12EleBat']] + parEleBat['minDis']

        # inequality constraints of electric battery: upper and lower bound for charging related to binary status
        # upChgEleBat
        Aineq[idxIneqCons['upChgEleBat'], [idxContOpeVar['stoEleBat'],
                                           idxBinOpeVar['chgEleBat']]] = npmtlb.repmat(
            [parEleBat['chgEff'] * parTime['stepHr'], -parEleBat['maxIn']], parTime['totHrStep'], 1).transpose()
        # minChgEleBat
        Aineq[idxIneqCons['minChgEleBat'], [idxContOpeVar['stoEleBat'],
                                            idxBinOpeVar['chgEleBat']]] = npmtlb.repmat(
            [-parEleBat['chgEff'] * parTime['stepHr'], parEleBat['minIn']], parTime['totHrStep'], 1).transpose()

        # inequality constraints of electric battery: upper and lower bound for discharging related to binary status
        # upDisEleBat
        Aineq[idxIneqCons['upDisEleBat'], [idxContOpeVar['fromEleBat'],
                                           idxBinOpeVar['disEleBat']]] = npmtlb.repmat(
            [1.0 / parEleBat['disEff'] * parTime['stepHr'], -parEleBat['maxOut']], parTime['totHrStep'], 1).transpose()
        # minDisEleBat
        Aineq[idxIneqCons['minDisEleBat'], [idxContOpeVar['fromEleBat'],
                                            idxBinOpeVar['disEleBat']]] = npmtlb.repmat(
            [-1.0 / parEleBat['disEff'] * parTime['stepHr'], parEleBat['minOut']], parTime['totHrStep'], 1).transpose()

        # inequality constraints of electric battery: upper and lower limit of hourly total electricity stored in battery
        Aineq[idxIneqCons['upTotEleBat'], [idxContOpeVar['totEleBat'],
                                           [idxContInstVar['eleBat'] for i in
                                            range(parTime['totHrStep'])]]] = npmtlb.repmat(
            [1 * parTime['stepHr'], -parEleBat['maxSoc'] * parEleBat['unitOut']], parTime['totHrStep'], 1).transpose()

        Aineq[idxIneqCons['minTotEleBat'], [idxContOpeVar['totEleBat'],
                                            [idxContInstVar['eleBat'] for i in
                                             range(parTime['totHrStep'])]]] = npmtlb.repmat(
            [-1 * parTime['stepHr'], parEleBat['minSoc'] * parEleBat['unitOut']], parTime['totHrStep'], 1).transpose()

        # inequality constraints of electric battery:  upper and lower limit of battery capacity

        Aineq[idxInstIneqCons['upCapEleBat'], [idxContInstVar['eleBat'],
                                               idxBinInst['eleBat']]] = [1 * parEleBat['unitOut'], -parEleBat['maxCap']]

        Aineq[idxInstIneqCons['minCapEleBat'], [idxContInstVar['eleBat'],
                                                idxBinInst['eleBat']]] = [-1 * parEleBat['unitOut'],
                                                                          parEleBat['minCap']]

        # inequality constraints of electric battery:  upper limit of battery charging/discharging status
        Aineq[idxIneqCons['upStaEleBat'], [idxBinOpeVar['chgEleBat'],
                                           idxBinOpeVar['disEleBat']]] = npmtlb.repmat(
            [1, 1], parTime['totHrStep'], 1).transpose()

        bineq[idxIneqCons['upStaEleBat']] = 1
    # =======================================================================================================================
    'heat tank'
    # inequality constraints of heat tank: net electricity sent to battery for storage
    if candidateTech['heatTank'] == 1 and len(nonzeroIndHeat):
        # added on  10/16/15
        # upInHeatTank
        Aineq[idxIneqCons['upInHeatTank'], [idxContOpeVar['stoHeatTank'],
                                            idxContOpeVar['upInYHeatTank']]] = npmtlb.repmat(
            [parHeatTank['chgEff'] * parTime['stepHr'], -1], parTime['totHrStep'], 1).transpose()

        # upInX1
        Aineq[idxIneqCons['upInX1HeatTank'], [idxContOpeVar['upInYHeatTank'],
                                              idxBinOpeVar['chgHeatTank']]] = npmtlb.repmat(
            [1, -parHeatTank['maxChg']], parTime['totHrStep'], 1).transpose()

        # upInX2
        Aineq[idxIneqCons['upInX2HeatTank'], [idxContOpeVar['upInYHeatTank'],
                                              [idxContInstVar['heatTank'] for i in
                                               range(parTime['totHrStep'])]]] = npmtlb.repmat(
            [1, -parHeatTank['maxFracChg'] * parHeatTank['unitOut']], parTime['totHrStep'], 1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['upInX2HeatTank'], parTime, Aineq, bineq, -parHeatTank['maxFracChg'] * parHeatTank['iniTot'],
        #                    parHeatTank['maxFracChg'] * parTime['stepHr'], idxPreTotHeatTank)
        [Aineq, bineq] = dym_chg(idxIneqCons['upInX2HeatTank'], idxContInstVar['heatTank'], parTime, Aineq, bineq,
                                 parHeatTank['maxFracChg'] * parHeatTank['iniSoc'],
                                 parHeatTank['maxFracChg'] * parTime['stepHr'], idxPreTotHeatTank)
        # upInX12
        Aineq[idxIneqCons['upInX12HeatTank'], [idxContOpeVar['upInYHeatTank'], idxBinOpeVar['chgHeatTank'],
                                               [idxContInstVar['heatTank'] for i in
                                                range(parTime['totHrStep'])]]] = npmtlb.repmat(
            [-1, parHeatTank['maxChg'], parHeatTank['maxFracChg'] * parHeatTank['unitOut']], parTime['totHrStep'],
            1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['upInX12HeatTank'], parTime, Aineq, bineq, parHeatTank['maxFracChg'] * parHeatTank['iniTot'],
        #                    -parHeatTank['maxFracChg'] * parTime['stepHr'], idxPreTotHeatTank)
        [Aineq, bineq] = dym_chg(idxIneqCons['upInX12HeatTank'], idxContInstVar['heatTank'], parTime, Aineq, bineq,
                                 -parHeatTank['maxFracChg'] * parHeatTank['iniSoc'],
                                 -parHeatTank['maxFracChg'] * parTime['stepHr'], idxPreTotHeatTank)

        bineq[idxIneqCons['upInX12HeatTank']] = bineq[idxIneqCons['upInX12HeatTank']] + parHeatTank['maxChg']

        # minInHeatTank
        Aineq[idxIneqCons['minInHeatTank'], [idxContOpeVar['stoHeatTank'],
                                             idxContOpeVar['minInYHeatTank']]] = npmtlb.repmat(
            [-parHeatTank['chgEff'] * parTime['stepHr'], 1], parTime['totHrStep'], 1).transpose()

        # minInX1
        Aineq[idxIneqCons['minInX1HeatTank'], [idxContOpeVar['minInYHeatTank'],
                                               idxBinOpeVar['chgHeatTank']]] = npmtlb.repmat(
            [1, -parHeatTank['minChg']], parTime['totHrStep'], 1).transpose()

        # minInX2
        Aineq[idxIneqCons['minInX2HeatTank'], [idxContOpeVar['minInYHeatTank'],
                                               [idxContInstVar['heatTank'] for i in
                                                range(parTime['totHrStep'])]]] = npmtlb.repmat(
            [1, -parHeatTank['minFracChg'] * parHeatTank['unitOut']], parTime['totHrStep'], 1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['minInX2HeatTank'], parTime, Aineq, bineq, -parHeatTank['minFracChg'] * parHeatTank['iniTot'],
        #                    parHeatTank['minFracChg'] * parTime['stepHr'], idxPreTotHeatTank)
        [Aineq, bineq] = dym_chg(idxIneqCons['minInX2HeatTank'], idxContInstVar['heatTank'], parTime, Aineq, bineq,
                                 parHeatTank['minFracChg'] * parHeatTank['iniSoc'],
                                 parHeatTank['minFracChg'] * parTime['stepHr'], idxPreTotHeatTank)

        # minInX12
        Aineq[idxIneqCons['minInX12HeatTank'], [idxContOpeVar['minInYHeatTank'], idxBinOpeVar['chgHeatTank'],
                                                [idxContInstVar['heatTank'] for i in
                                                 range(parTime['totHrStep'])]]] = npmtlb.repmat(
            [-1, parHeatTank['minChg'], parHeatTank['minFracChg'] * parHeatTank['unitOut']], parTime['totHrStep'],
            1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['minInX12HeatTank'], parTime, Aineq, bineq, parHeatTank['minFracChg'] * parHeatTank['iniTot'],
        #                    -parHeatTank['minFracChg'] * parTime['stepHr'], idxPreTotHeatTank)
        [Aineq, bineq] = dym_chg(idxIneqCons['minInX12HeatTank'], idxContInstVar['heatTank'], parTime, Aineq, bineq,
                                 -parHeatTank['minFracChg'] * parHeatTank['iniSoc'],
                                 -parHeatTank['minFracChg'] * parTime['stepHr'], idxPreTotHeatTank)

        bineq[idxIneqCons['minInX12HeatTank']] = bineq[idxIneqCons['minInX12HeatTank']] + parHeatTank['minChg']

        # inequality constraints of heat tank: gross electricity released from battery
        # upOutHeatTank
        Aineq[idxIneqCons['upOutHeatTank'], [idxContOpeVar['fromHeatTank'],
                                             idxContOpeVar['upOutYHeatTank']]] = npmtlb.repmat(
            [1.0 / parHeatTank['disEff'] * parTime['stepHr'], -1], parTime['totHrStep'], 1).transpose()

        # upOutX1
        Aineq[idxIneqCons['upOutX1HeatTank'], [idxContOpeVar['upOutYHeatTank'],
                                               idxBinOpeVar['disHeatTank']]] = npmtlb.repmat(
            [1, -parHeatTank['maxDis']], parTime['totHrStep'], 1).transpose()

        # upOutX2
        Aineq[idxIneqCons['upOutX2HeatTank'], [idxContOpeVar['upOutYHeatTank']]] = npmtlb.repmat([1],
                                                                                                 parTime['totHrStep'],
                                                                                                 1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['upOutX2HeatTank'], parTime, Aineq, bineq, parHeatTank['maxFracDis'] * parHeatTank['iniTot'],
        #                    -parHeatTank['maxFracDis'] * parTime['stepHr'], idxPreTotHeatTank)
        [Aineq, bineq] = dym_chg(idxIneqCons['upOutX2HeatTank'], idxContInstVar['heatTank'], parTime, Aineq, bineq,
                                 -parHeatTank['maxFracDis'] * parHeatTank['iniSoc'],
                                 -parHeatTank['maxFracDis'] * parTime['stepHr'], idxPreTotHeatTank)

        # upOutX12
        Aineq[idxIneqCons['upOutX12HeatTank'], [idxContOpeVar['upOutYHeatTank'],
                                                idxBinOpeVar['disHeatTank']]] = npmtlb.repmat(
            [-1, parHeatTank['maxDis']], parTime['totHrStep'], 1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['upOutX12HeatTank'], parTime, Aineq, bineq, -parHeatTank['maxFracDis'] * parHeatTank['iniTot'],
        #                    parHeatTank['maxFracDis'] * parTime['stepHr'], idxPreTotHeatTank)
        [Aineq, bineq] = dym_chg(idxIneqCons['upOutX12HeatTank'], idxContInstVar['heatTank'], parTime, Aineq, bineq,
                                 parHeatTank['maxFracDis'] * parHeatTank['iniSoc'],
                                 parHeatTank['maxFracDis'] * parTime['stepHr'], idxPreTotHeatTank)

        bineq[idxIneqCons['upOutX12HeatTank']] = bineq[idxIneqCons['upOutX12HeatTank']] + parHeatTank['maxDis']

        # minOutHeatTank
        Aineq[idxIneqCons['minOutHeatTank'], [idxContOpeVar['fromHeatTank'],
                                              idxContOpeVar['minOutYHeatTank']]] = npmtlb.repmat(
            [-1.0 / parHeatTank['disEff'] * parTime['stepHr'], 1], parTime['totHrStep'], 1).transpose()

        # minOutX1
        Aineq[idxIneqCons['minOutX1HeatTank'], [idxContOpeVar['minOutYHeatTank'],
                                                idxBinOpeVar['disHeatTank']]] = npmtlb.repmat(
            [1, -parHeatTank['minDis']], parTime['totHrStep'], 1).transpose()

        # minOutX2
        Aineq[idxIneqCons['minOutX2HeatTank'], [idxContOpeVar['minOutYHeatTank']]] = npmtlb.repmat([1],
                                                                                                   parTime['totHrStep'],
                                                                                                   1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['minOutX2HeatTank'], parTime, Aineq, bineq, parHeatTank['minFracDis'] * parHeatTank['iniTot'],
        #                    -parHeatTank['minFracDis'] * parTime['stepHr'], idxPreTotHeatTank)
        [Aineq, bineq] = dym_chg(idxIneqCons['minOutX2HeatTank'], idxContInstVar['heatTank'], parTime, Aineq, bineq,
                                 -parHeatTank['minFracDis'] * parHeatTank['iniSoc'],
                                 -parHeatTank['minFracDis'] * parTime['stepHr'], idxPreTotHeatTank)

        # minOutX12
        Aineq[idxIneqCons['minOutX12HeatTank'], [idxContOpeVar['minOutYHeatTank'],
                                                 idxBinOpeVar['disHeatTank']]] = npmtlb.repmat(
            [-1, parHeatTank['minDis']], parTime['totHrStep'], 1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['minOutX12HeatTank'], parTime, Aineq, bineq, -parHeatTank['minFracDis'] * parHeatTank['iniTot'],
        #                    parHeatTank['minFracDis'] * parTime['stepHr'], idxPreTotHeatTank)
        [Aineq, bineq] = dym_chg(idxIneqCons['minOutX12HeatTank'], idxContInstVar['heatTank'], parTime, Aineq, bineq,
                                 parHeatTank['minFracDis'] * parHeatTank['iniSoc'],
                                 parHeatTank['minFracDis'] * parTime['stepHr'], idxPreTotHeatTank)

        bineq[idxIneqCons['minOutX12HeatTank']] = bineq[idxIneqCons['minOutX12HeatTank']] + parHeatTank['minDis']

        # inequality constraints of heat tank: upper and lower bound for charging related to binary status
        Aineq[idxIneqCons['upChgHeatTank'], [idxContOpeVar['stoHeatTank'],
                                             idxBinOpeVar['chgHeatTank']]] = npmtlb.repmat(
            [parHeatTank['chgEff'] * parTime['stepHr'], -parHeatTank['maxIn']], parTime['totHrStep'], 1).transpose()

        Aineq[idxIneqCons['minChgHeatTank'], [idxContOpeVar['stoHeatTank'],
                                              idxBinOpeVar['chgHeatTank']]] = npmtlb.repmat(
            [-parHeatTank['chgEff'] * parTime['stepHr'], parHeatTank['minIn']], parTime['totHrStep'], 1).transpose()

        # inequality constraints of heat tank: upper and lower bound for discharging related to binary status
        Aineq[idxIneqCons['upDisHeatTank'], [idxContOpeVar['fromHeatTank'],
                                             idxBinOpeVar['disHeatTank']]] = npmtlb.repmat(
            [1.0 / parHeatTank['disEff'] * parTime['stepHr'], -parHeatTank['maxOut']], parTime['totHrStep'],
            1).transpose()

        Aineq[idxIneqCons['minDisHeatTank'], [idxContOpeVar['fromHeatTank'],
                                              idxBinOpeVar['disHeatTank']]] = npmtlb.repmat(
            [-1.0 / parHeatTank['disEff'] * parTime['stepHr'], parHeatTank['minOut']], parTime['totHrStep'],
            1).transpose()

        # inequality constraints of heat tank: upper and lower limit of hourly total electricity stored in battery
        Aineq[idxIneqCons['upTotHeatTank'], [idxContOpeVar['totHeatTank'],
                                             [idxContInstVar['heatTank'] for i in
                                              range(parTime['totHrStep'])]]] = npmtlb.repmat(
            [1 * parTime['stepHr'], -parHeatTank['maxSoc'] * parHeatTank['unitOut']], parTime['totHrStep'],
            1).transpose()
        # added 08/11/2015
        Aineq[idxIneqCons['minTotHeatTank'], [idxContOpeVar['totHeatTank'],
                                              [idxContInstVar['heatTank'] for i in
                                               range(parTime['totHrStep'])]]] = npmtlb.repmat(
            [-1 * parTime['stepHr'], parHeatTank['minSoc'] * parHeatTank['unitOut']], parTime['totHrStep'],
            1).transpose()

        # A[idxIneqCons['minTotHeatTank'],[idxContOpeVar['totEleBat'],idxContInstVar['eleBat']]]= [[-1]* parTime['totHrStep'], [parEleBat['minSoc']] * parTime['totHrStep']]

        # inequality constraints of heat tank:  upper and lower limit of battery capacity

        Aineq[idxInstIneqCons['upCapHeatTank'], [idxContInstVar['heatTank'],
                                                 idxBinInst['heatTank']]] = [1 * parHeatTank['unitOut'],
                                                                             -parHeatTank['maxCap']]

        Aineq[idxInstIneqCons['minCapHeatTank'], [idxContInstVar['heatTank'],
                                                  idxBinInst['heatTank']]] = [-1 * parHeatTank['unitOut'],
                                                                              parHeatTank['minCap']]

        # inequality constraints of heat tank:  upper limit of battery charging/discharging status
        Aineq[idxIneqCons['upStaHeatTank'], [idxBinOpeVar['chgHeatTank'],
                                             idxBinOpeVar['disHeatTank']]] = npmtlb.repmat(
            [1, 1], parTime['totHrStep'], 1).transpose()

        bineq[idxIneqCons['upStaHeatTank']] = 1
    # ======================================================================================================
    'cooling tank'
    # inequality constraints of heat tank: net electricity sent to battery for storage
    if candidateTech['coolTank'] == 1 and len(nonzeroIndCool):
        # added on  10/16/15
        # upInCoolTank
        Aineq[idxIneqCons['upInCoolTank'], [idxContOpeVar['stoCoolTank'],
                                            idxContOpeVar['upInYCoolTank']]] = npmtlb.repmat(
            [parCoolTank['chgEff'] * parTime['stepHr'], -1], parTime['totHrStep'], 1).transpose()

        # upInX1
        Aineq[idxIneqCons['upInX1CoolTank'], [idxContOpeVar['upInYCoolTank'],
                                              idxBinOpeVar['chgCoolTank']]] = npmtlb.repmat(
            [1, -parCoolTank['maxChg']], parTime['totHrStep'], 1).transpose()

        # upInX2
        Aineq[idxIneqCons['upInX2CoolTank'], [idxContOpeVar['upInYCoolTank'],
                                              [idxContInstVar['coolTank'] for i in
                                               range(parTime['totHrStep'])]]] = npmtlb.repmat(
            [1, -parCoolTank['maxFracChg'] * parCoolTank['unitOut']], parTime['totHrStep'], 1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['upInX2CoolTank'], parTime, Aineq, bineq, -parCoolTank['maxFracChg'] * parCoolTank['iniTot'],
        #                    parCoolTank['maxFracChg'] * parTime['stepHr'], idxPreTotCoolTank)
        [Aineq, bineq] = dym_chg(idxIneqCons['upInX2CoolTank'], idxContInstVar['coolTank'], parTime, Aineq, bineq,
                                 parCoolTank['maxFracChg'] * parCoolTank['iniSoc'],
                                 parCoolTank['maxFracChg'] * parTime['stepHr'], idxPreTotCoolTank)

        # upInX12
        Aineq[idxIneqCons['upInX12CoolTank'], [idxContOpeVar['upInYCoolTank'], idxBinOpeVar['chgCoolTank'],
                                               [idxContInstVar['coolTank'] for i in
                                                range(parTime['totHrStep'])]]] = npmtlb.repmat(
            [-1, parCoolTank['maxChg'], parCoolTank['maxFracChg'] * parCoolTank['unitOut']], parTime['totHrStep'],
            1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['upInX12CoolTank'], parTime, Aineq, bineq, parCoolTank['maxFracChg'] * parCoolTank['iniTot'],
        #                    -parCoolTank['maxFracChg'] * parTime['stepHr'], idxPreTotCoolTank)
        [Aineq, bineq] = dym_chg(idxIneqCons['upInX12CoolTank'], idxContInstVar['coolTank'], parTime, Aineq, bineq,
                                 -parCoolTank['maxFracChg'] * parCoolTank['iniSoc'],
                                 -parCoolTank['maxFracChg'] * parTime['stepHr'], idxPreTotCoolTank)

        bineq[idxIneqCons['upInX12CoolTank']] = bineq[idxIneqCons['upInX12CoolTank']] + parCoolTank['maxChg']

        # minInCoolTank
        Aineq[idxIneqCons['minInCoolTank'], [idxContOpeVar['stoCoolTank'],
                                             idxContOpeVar['minInYCoolTank']]] = npmtlb.repmat(
            [-parCoolTank['chgEff'] * parTime['stepHr'], 1], parTime['totHrStep'], 1).transpose()

        # minInX1
        Aineq[idxIneqCons['minInX1CoolTank'], [idxContOpeVar['minInYCoolTank'],
                                               idxBinOpeVar['chgCoolTank']]] = npmtlb.repmat(
            [1, -parCoolTank['minChg']], parTime['totHrStep'], 1).transpose()

        # minInX2
        Aineq[idxIneqCons['minInX2CoolTank'], [idxContOpeVar['minInYCoolTank'],
                                               [idxContInstVar['coolTank'] for i in
                                                range(parTime['totHrStep'])]]] = npmtlb.repmat(
            [1, -parCoolTank['minFracChg'] * parCoolTank['unitOut']], parTime['totHrStep'], 1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['minInX2CoolTank'], parTime, Aineq, bineq, -parCoolTank['minFracChg'] * parCoolTank['iniTot'],
        #                    parCoolTank['minFracChg'] * parTime['stepHr'], idxPreTotCoolTank)
        [Aineq, bineq] = dym_chg(idxIneqCons['minInX2CoolTank'], idxContInstVar['coolTank'], parTime, Aineq, bineq,
                                 parCoolTank['minFracChg'] * parCoolTank['iniSoc'],
                                 parCoolTank['minFracChg'] * parTime['stepHr'], idxPreTotCoolTank)

        # minInX12
        Aineq[idxIneqCons['minInX12CoolTank'], [idxContOpeVar['minInYCoolTank'], idxBinOpeVar['chgCoolTank'],
                                                [idxContInstVar['coolTank'] for i in
                                                 range(parTime['totHrStep'])]]] = npmtlb.repmat(
            [-1, parCoolTank['minChg'], parCoolTank['minFracChg'] * parCoolTank['unitOut']], parTime['totHrStep'],
            1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['minInX12CoolTank'], parTime, Aineq, bineq, parCoolTank['minFracChg'] * parCoolTank['iniTot'],
        #                    -parCoolTank['minFracChg'] * parTime['stepHr'], idxPreTotCoolTank)
        [Aineq, bineq] = dym_chg(idxIneqCons['minInX12CoolTank'], idxContInstVar['coolTank'], parTime, Aineq, bineq,
                                 -parCoolTank['minFracChg'] * parCoolTank['iniSoc'],
                                 -parCoolTank['minFracChg'] * parTime['stepHr'], idxPreTotCoolTank)

        bineq[idxIneqCons['minInX12CoolTank']] = bineq[idxIneqCons['minInX12CoolTank']] + parCoolTank['minChg']

        # inequality constraints of heat tank: gross electricity released from battery
        # upOutCoolTank
        Aineq[idxIneqCons['upOutCoolTank'], [idxContOpeVar['fromCoolTank'],
                                             idxContOpeVar['upOutYCoolTank']]] = npmtlb.repmat(
            [1.0 / parCoolTank['disEff'] * parTime['stepHr'], -1], parTime['totHrStep'], 1).transpose()

        # upOutX1
        Aineq[idxIneqCons['upOutX1CoolTank'], [idxContOpeVar['upOutYCoolTank'],
                                               idxBinOpeVar['disCoolTank']]] = npmtlb.repmat(
            [1, -parCoolTank['maxDis']], parTime['totHrStep'], 1).transpose()

        # upOutX2
        Aineq[idxIneqCons['upOutX2CoolTank'], [idxContOpeVar['upOutYCoolTank']]] = npmtlb.repmat([1],
                                                                                                 parTime['totHrStep'],
                                                                                                 1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['upOutX2CoolTank'], parTime, Aineq, bineq, parCoolTank['maxFracDis'] * parCoolTank['iniTot'],
        #                    -parCoolTank['maxFracDis'] * parTime['stepHr'], idxPreTotCoolTank)
        [Aineq, bineq] = dym_chg(idxIneqCons['upOutX2CoolTank'], idxContInstVar['coolTank'], parTime, Aineq, bineq,
                                 -parCoolTank['maxFracDis'] * parCoolTank['iniSoc'],
                                 -parCoolTank['maxFracDis'] * parTime['stepHr'], idxPreTotCoolTank)

        # upOutX12
        Aineq[idxIneqCons['upOutX12CoolTank'], [idxContOpeVar['upOutYCoolTank'],
                                                idxBinOpeVar['disCoolTank']]] = npmtlb.repmat(
            [-1, parCoolTank['maxDis']], parTime['totHrStep'], 1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['upOutX12CoolTank'], parTime, Aineq, bineq, -parCoolTank['maxFracDis'] * parCoolTank['iniTot'],
        #                    parCoolTank['maxFracDis'] * parTime['stepHr'], idxPreTotCoolTank)
        [Aineq, bineq] = dym_chg(idxIneqCons['upOutX12CoolTank'], idxContInstVar['coolTank'], parTime, Aineq, bineq,
                                 parCoolTank['maxFracDis'] * parCoolTank['iniSoc'],
                                 parCoolTank['maxFracDis'] * parTime['stepHr'], idxPreTotCoolTank)

        bineq[idxIneqCons['upOutX12CoolTank']] = bineq[idxIneqCons['upOutX12CoolTank']] + parCoolTank['maxDis']

        # minOutCoolTank
        Aineq[idxIneqCons['minOutCoolTank'], [idxContOpeVar['fromCoolTank'],
                                              idxContOpeVar['minOutYCoolTank']]] = npmtlb.repmat(
            [-1.0 / parCoolTank['disEff'] * parTime['stepHr'], 1], parTime['totHrStep'], 1).transpose()

        # minOutX1
        Aineq[idxIneqCons['minOutX1CoolTank'], [idxContOpeVar['minOutYCoolTank'],
                                                idxBinOpeVar['disCoolTank']]] = npmtlb.repmat(
            [1, -parCoolTank['minDis']], parTime['totHrStep'], 1).transpose()

        # minOutX2
        Aineq[idxIneqCons['minOutX2CoolTank'], [idxContOpeVar['minOutYCoolTank']]] = npmtlb.repmat([1],
                                                                                                   parTime['totHrStep'],
                                                                                                   1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['minOutX2CoolTank'], parTime, Aineq, bineq, parCoolTank['minFracDis'] * parCoolTank['iniTot'],
        #                    -parCoolTank['minFracDis'] * parTime['stepHr'], idxPreTotCoolTank)
        [Aineq, bineq] = dym_chg(idxIneqCons['minOutX2CoolTank'], idxContInstVar['coolTank'], parTime, Aineq, bineq,
                                 -parCoolTank['minFracDis'] * parCoolTank['iniSoc'],
                                 -parCoolTank['minFracDis'] * parTime['stepHr'], idxPreTotCoolTank)
        # minOutX12
        Aineq[idxIneqCons['minOutX12CoolTank'], [idxContOpeVar['minOutYCoolTank'],
                                                 idxBinOpeVar['disCoolTank']]] = npmtlb.repmat(
            [-1, parCoolTank['minDis']], parTime['totHrStep'], 1).transpose()

        # [Aineq, bineq] = dymChg(idxIneqCons['minOutX12CoolTank'], parTime, Aineq, bineq, -parCoolTank['minFracDis'] * parCoolTank['iniTot'],
        #                    parCoolTank['minFracDis'] * parTime['stepHr'], idxPreTotCoolTank)
        [Aineq, bineq] = dym_chg(idxIneqCons['minOutX12CoolTank'], idxContInstVar['coolTank'], parTime, Aineq, bineq,
                                 parCoolTank['minFracDis'] * parCoolTank['iniSoc'],
                                 parCoolTank['minFracDis'] * parTime['stepHr'], idxPreTotCoolTank)

        bineq[idxIneqCons['minOutX12CoolTank']] = bineq[idxIneqCons['minOutX12CoolTank']] + parCoolTank['minDis']

        # inequality constraints of heat tank: upper and lower bound for charging related to binary status
        Aineq[idxIneqCons['upChgCoolTank'], [idxContOpeVar['stoCoolTank'],
                                             idxBinOpeVar['chgCoolTank']]] = npmtlb.repmat(
            [parCoolTank['chgEff'] * parTime['stepHr'], -parCoolTank['maxIn']], parTime['totHrStep'], 1).transpose()

        Aineq[idxIneqCons['minChgCoolTank'], [idxContOpeVar['stoCoolTank'],
                                              idxBinOpeVar['chgCoolTank']]] = npmtlb.repmat(
            [-parCoolTank['chgEff'] * parTime['stepHr'], parCoolTank['minIn']], parTime['totHrStep'], 1).transpose()

        # inequality constraints of heat tank: upper and lower bound for discharging related to binary status
        Aineq[idxIneqCons['upDisCoolTank'], [idxContOpeVar['fromCoolTank'],
                                             idxBinOpeVar['disCoolTank']]] = npmtlb.repmat(
            [1.0 / parCoolTank['disEff'] * parTime['stepHr'], -parCoolTank['maxOut']], parTime['totHrStep'],
            1).transpose()

        Aineq[idxIneqCons['minDisCoolTank'], [idxContOpeVar['fromCoolTank'],
                                              idxBinOpeVar['disCoolTank']]] = npmtlb.repmat(
            [-1.0 / parCoolTank['disEff'] * parTime['stepHr'], parCoolTank['minOut']], parTime['totHrStep'],
            1).transpose()

        # inequality constraints of heat tank: upper and lower limit of hourly total electricity stored in battery
        Aineq[idxIneqCons['upTotCoolTank'], [idxContOpeVar['totCoolTank'],
                                             [idxContInstVar['coolTank'] for i in
                                              range(parTime['totHrStep'])]]] = npmtlb.repmat(
            [1 * parTime['stepHr'], -parCoolTank['maxSoc'] * parCoolTank['unitOut']], parTime['totHrStep'],
            1).transpose()
        # added 08/11/2015
        Aineq[idxIneqCons['minTotCoolTank'], [idxContOpeVar['totCoolTank'],
                                              [idxContInstVar['coolTank'] for i in
                                               range(parTime['totHrStep'])]]] = npmtlb.repmat(
            [-1 * parTime['stepHr'], parCoolTank['minSoc'] * parCoolTank['unitOut']], parTime['totHrStep'],
            1).transpose()

        # inequality constraints of heat tank:  upper and lower limit of battery capacity

        Aineq[idxInstIneqCons['upCapCoolTank'], [idxContInstVar['coolTank'],
                                                 idxBinInst['coolTank']]] = [1 * parCoolTank['unitOut'],
                                                                             -parCoolTank['maxCap']]

        Aineq[idxInstIneqCons['minCapCoolTank'], [idxContInstVar['coolTank'],
                                                  idxBinInst['coolTank']]] = [-1 * parCoolTank['unitOut'],
                                                                              parCoolTank['minCap']]

        # inequality constraints of heat tank:  upper limit of battery charging/discharging status
        Aineq[idxIneqCons['upStaCoolTank'], [idxBinOpeVar['chgCoolTank'],
                                             idxBinOpeVar['disCoolTank']]] = npmtlb.repmat(
            [1, 1], parTime['totHrStep'], 1).transpose()

        bineq[idxIneqCons['upStaCoolTank']] = 1
    # ======================================================================================================
    #'CHP'
    if candidateTech['CHP'] == 1:
        # maximum investment number of CHP
        idxInstVarCHP = np.asarray(range(idxInstCHP, np.sum(parCHP['numInst']) + idxInstCHP))
        # if (parCHP['coolSta']==1 and len(nonzeroIndCool)) or (parCHP['heatSta']==1 and len(nonzeroIndHeat)) or (parCHP['eleSta']==1 and len(nonzeroIndEle)):
        Aineq[np.repeat(idxInstIneqCons['upInstStaCHP'], np.sum(parCHP['numInst'])), idxInstVarCHP] = [1] * np.sum(
            parCHP['numInst'])
        bineq[idxInstIneqCons['upInstStaCHP']] = parCHP['numInv']

        # 12/15/15, maximum installation number of each type of CHP
        for i in np.arange(parCHP['numType']):
            if i == 0:
                idxInstTypeCHP = np.asarray(range(idxInstCHP, np.sum(parCHP['numInst'][0:i + 1]) + idxInstCHP))
            else:
                idxInstTypeCHP = np.asarray(
                    range(idxInstCHP + np.sum(parCHP['numInst'][0:i]), np.sum(parCHP['numInst'][0:i + 1]) + idxInstCHP))

            Aineq[np.repeat(idxInstIneqCons['upInstTypeCHP'][i], (parCHP['numInst'][i])), idxInstTypeCHP] = [1] * (
                parCHP['numInst'][i])
            bineq[idxInstIneqCons['upInstTypeCHP'][i]] = parCHP['numInst'][i]

        # hourly electricity generation from CHP
        # the column matrix are both in array
        # np.append is applied to create a single matrix with two separate arrays

        # in this case, CHP is used to provide heating energy for electricity purpose
        if len(nonzeroIndEle) and parCHP['eleSta'] == 1:  # and parCHP['heatSta']==0
            # rows: 80
            # right hand side: 2 * 80
            # columns: 1st column: (80,); 2nd column: (80,)
            Aineq[idxIneqConsCHP['maxEleCHP'], [idxContOpeVarCHP['eleCHP'],
                                                npmtlb.repmat(idxInstVarCHP, 1, len(nonzeroIndEle))[0]]] = np.append(
                npmtlb.repmat([1], 1, len(nonzeroIndEle) * np.sum(parCHP['numInst'])),
                npmtlb.repmat(np.negative([parCHP['capGen'][i]
                                           for i in range(parCHP['numType'])
                                           for j in range(parCHP['numInst'][i])]), 1, len(nonzeroIndEle)), axis=0)

            Aineq[idxIneqConsCHP['upHrEleCHP'], [idxContOpeVarCHP['eleCHP'], idxBinOpeVarCHP['CHP']]] = np.append(
                npmtlb.repmat([1], 1, len(nonzeroIndEle) * np.sum(parCHP['numInst'])),
                npmtlb.repmat(np.negative([parCHP['capGen'][i]
                                           for i in range(parCHP['numType'])
                                           for j in range(parCHP['numInst'][i])]), 1, len(nonzeroIndEle)), axis=0)

            Aineq[idxIneqConsCHP['minHrEleCHP'], [idxContOpeVarCHP['eleCHP'], idxBinOpeVarCHP['CHP']]] = np.append(
                npmtlb.repmat(np.negative([1]), 1, len(nonzeroIndEle) * np.sum(parCHP['numInst'])),
                npmtlb.repmat([parCHP['minHrEle'][i]
                               for i in range(parCHP['numType'])
                               for j in range(parCHP['numInst'][i])], 1, len(nonzeroIndEle)), axis=0)

            bineq[idxIneqConsCHP['minHrEleCHP']] = npmtlb.repmat(
                [parCHP['minHrEle'][i] - np.prod([parCHP['capGen'][i], parCHP['parLoad'][i]], axis=0)
                 for i in range(parCHP['numType'])
                 for j in range(parCHP['numInst'][i])], 1, len(nonzeroIndEle))
        # in this case, CHP is used for heating purpose
        elif len(nonzeroIndHeat) and parCHP['heatSta'] == 1:  # and parCHP['eleSta']==0
            Aineq[idxIneqConsCHP['maxEleCHP'], [idxContOpeVarCHP['eleCHP'],
                                                npmtlb.repmat(idxInstVarCHP, 1, len(nonzeroIndHeat))[0]]] = np.append(
                npmtlb.repmat([1], 1, len(nonzeroIndHeat) * np.sum(parCHP['numInst'])),
                npmtlb.repmat(np.negative([parCHP['capGen'][i]
                                           for i in range(parCHP['numType'])
                                           for j in range(parCHP['numInst'][i])]), 1, len(nonzeroIndHeat)), axis=0)

            Aineq[idxIneqConsCHP['upHrEleCHP'], [idxContOpeVarCHP['eleCHP'], idxBinOpeVarCHP['CHP']]] = np.append(
                npmtlb.repmat([1], 1, len(nonzeroIndHeat) * np.sum(parCHP['numInst'])),
                npmtlb.repmat(np.negative([parCHP['capGen'][i]
                                           for i in range(parCHP['numType'])
                                           for j in range(parCHP['numInst'][i])]), 1, len(nonzeroIndHeat)), axis=0)
            # '''example, see 'test1'
            # aa=np.zeros((10, 10))
            # aa[[4,5,6,7,8,9], np.matlib.repmat([2,3], 1,3)]=np.matlib.repmat([1,2], 1,3)
            # aa[[4,5,6,7,8,9], [4,5,6,7,8,9]]=  np.matlib.repmat([1], 1,6)
            # the coloum matrix are both in list
            # aa[[4,5,6,7,8,9], [[4,5,6,7,8,9],np.matlib.repmat([2,3], 1,3).tolist()[0]]]=  np.append(np.matlib.repmat([4], 1,6), np.matlib.repmat([5,6], 1,3), axis=0)
            # '''
            Aineq[idxIneqConsCHP['minHrEleCHP'], [idxContOpeVarCHP['eleCHP'], idxBinOpeVarCHP['CHP']]] = np.append(
                npmtlb.repmat(np.negative([1]), 1, len(nonzeroIndHeat) * np.sum(parCHP['numInst'])),
                npmtlb.repmat([parCHP['minHrEle'][i]
                               for i in range(parCHP['numType'])
                               for j in range(parCHP['numInst'][i])], 1, len(nonzeroIndHeat)), axis=0)

            bineq[idxIneqConsCHP['minHrEleCHP']] = npmtlb.repmat(
                [parCHP['minHrEle'][i] - np.prod([parCHP['capGen'][i], parCHP['parLoad'][i]], axis=0)
                 for i in range(parCHP['numType'])
                 for j in range(parCHP['numInst'][i])], 1, len(nonzeroIndHeat))

        # in this case, CHP is used to provide heating energy for cooling purpose
        elif len(nonzeroIndCool) and parCHP['coolSta'] == 1:
            Aineq[idxIneqConsCHP['maxEleCHP'], [idxContOpeVarCHP['eleCHP'],
                                                npmtlb.repmat(idxInstVarCHP, 1, len(nonzeroIndCool))[0]]] = np.append(
                npmtlb.repmat([1], 1, len(nonzeroIndCool) * np.sum(parCHP['numInst'])),
                npmtlb.repmat(np.negative([parCHP['capGen'][i]
                                           for i in range(parCHP['numType'])
                                           for j in range(parCHP['numInst'][i])]), 1, len(nonzeroIndCool)), axis=0)

            Aineq[idxIneqConsCHP['upHrEleCHP'], [idxContOpeVarCHP['eleCHP'], idxBinOpeVarCHP['CHP']]] = np.append(
                npmtlb.repmat([1], 1, len(nonzeroIndCool) * np.sum(parCHP['numInst'])),
                npmtlb.repmat(np.negative([parCHP['capGen'][i]
                                           for i in range(parCHP['numType'])
                                           for j in range(parCHP['numInst'][i])]), 1, len(nonzeroIndCool)), axis=0)

            Aineq[idxIneqConsCHP['minHrEleCHP'], [idxContOpeVarCHP['eleCHP'], idxBinOpeVarCHP['CHP']]] = np.append(
                npmtlb.repmat(np.negative([1]), 1, len(nonzeroIndCool) * np.sum(parCHP['numInst'])),
                npmtlb.repmat([parCHP['minHrEle'][i]
                               for i in range(parCHP['numType'])
                               for j in range(parCHP['numInst'][i])], 1, len(nonzeroIndCool)), axis=0)

            bineq[idxIneqConsCHP['minHrEleCHP']] = npmtlb.repmat(
                [parCHP['minHrEle'][i] - np.prod([parCHP['capGen'][i], parCHP['parLoad'][i]], axis=0)
                 for i in range(parCHP['numType'])
                 for j in range(parCHP['numInst'][i])], 1, len(nonzeroIndCool))

    # PV installation requirement
    if candidateTech['PV'] == 1 and len(nonzeroIndEle):
        Aineq[idxInstIneqCons['upInstAreaPV'], [idxContInstVar['PV'],
                                                idxBinInst['PV']]] = [1, -parPV['maxInstArea']]

        Aineq[idxInstIneqCons['minInstAreaPV'], [idxContInstVar['PV'],
                                                 idxBinInst['PV']]] = [-1, parPV['minInstArea']]
    # PV hourly operation requirement
    if candidateTech['PV'] == 1 and len(nonzeroIndEle):
        Aineq[idxIneqCons['upCapPV'][nonzeroIndEle], [idxContOpeVar['elePV'][nonzeroIndEle],
                                                      [idxContInstVar['PV'] for i in
                                                       range(len(nonzeroIndEle))]]] = npmtlb.repmat(
            [1, -parPV['unitOut']], len(nonzeroIndEle), 1).transpose()

        tempPV = -parPV['solIrradiation'][nonzeroIndEle] * parPV['perfRatio'] * parPV['solYield']
        tempPV = tempPV.reshape((1, len(nonzeroIndEle)))
        Aineq[idxIneqCons['upIrPV'][nonzeroIndEle], [idxContOpeVar['elePV'][nonzeroIndEle],
                                                     [idxContInstVar['PV'] for i in
                                                      range(len(nonzeroIndEle))]]] = np.append(
            npmtlb.repmat([1], 1, len(nonzeroIndEle)), tempPV, axis=0)

    # solar thermal: similar to PV
    if candidateTech['solThem'] == 1 and len(nonzeroIndHeat):
        Aineq[idxInstIneqCons['upInstAreaSolThem'], [idxContInstVar['solThem'],
                                                     idxBinInst['solThem']]] = [1, -parSolThem['maxInstArea']]

        Aineq[idxInstIneqCons['minInstAreaSolThem'], [idxContInstVar['solThem'],
                                                      idxBinInst['solThem']]] = [-1, parSolThem['minInstArea']]

    if candidateTech['solThem'] == 1 and len(nonzeroIndHeat):
        Aineq[idxIneqCons['upCapSolThem'][nonzeroIndHeat], [idxContOpeVar['heatSolThem'][nonzeroIndHeat],
                                                            [idxContInstVar['solThem'] for i in
                                                             range(len(nonzeroIndHeat))]]] = npmtlb.repmat(
            [1, -parSolThem['unitOut']], len(nonzeroIndHeat), 1).transpose()

        tempSolThem = -parSolThem['solIrradiation'][nonzeroIndHeat] * parSolThem['irrEff']
        tempSolThem = tempSolThem.reshape((1, len(nonzeroIndHeat)))
        Aineq[idxIneqCons['upIrSolThem'][nonzeroIndHeat], [idxContOpeVar['heatSolThem'][nonzeroIndHeat],
                                                           [idxContInstVar['solThem'] for i in
                                                            range(len(nonzeroIndHeat))]]] = np.append(
            npmtlb.repmat([1], 1, len(nonzeroIndHeat)), tempSolThem, axis=0)

    # electric boiler
    if candidateTech['eleBoil'] == 1 and len(nonzeroIndHeat):
        Aineq[idxInstIneqCons['upCapEleBoil'], [idxContInstVar['eleBoil'],
                                                idxBinInst['eleBoil']]] = [1, -parEleBoil['maxInstCap']]

        Aineq[idxInstIneqCons['minCapEleBoil'], [idxContInstVar['eleBoil'],
                                                 idxBinInst['eleBoil']]] = [-1, parEleBoil['minInstCap']]

    if candidateTech['eleBoil'] == 1 and len(nonzeroIndHeat):
        # 09/18/2015, why do we need repeat the sequence of constraint here
        Aineq[np.repeat(idxInstIneqCons['upGenEleBoil'], len(nonzeroIndHeat)), [
            idxContOpeVar['heatEleBoil'][nonzeroIndHeat],
            [idxBinInst['eleBoil'] for i in range(len(nonzeroIndHeat))]]] = npmtlb.repmat(
            [1, -parEleBoil['maxTotGen']], len(nonzeroIndHeat), 1).transpose()

        Aineq[idxIneqCons['upHrEleBoil'][nonzeroIndHeat], [idxContOpeVar['heatEleBoil'][nonzeroIndHeat],
                                                           [idxContInstVar['eleBoil'] for i in
                                                            range(len(nonzeroIndHeat))]]] = npmtlb.repmat(
            [1, -1], len(nonzeroIndHeat), 1).transpose()

    # natural gas boiler: similar to electric boiler
    if candidateTech['NGBoil'] == 1 and len(nonzeroIndHeat):
        Aineq[idxInstIneqCons['upCapNGBoil'], [idxContInstVar['NGBoil'],
                                               idxBinInst['NGBoil']]] = [1, -parNGBoil['maxInstCap']]

        Aineq[idxInstIneqCons['minCapNGBoil'], [idxContInstVar['NGBoil'],
                                                idxBinInst['NGBoil']]] = [-1, parNGBoil['minInstCap']]

    if candidateTech['NGBoil'] == 1 and len(nonzeroIndHeat):
        Aineq[np.repeat(idxInstIneqCons['upGenNGBoil'], len(nonzeroIndHeat)), [
            idxContOpeVar['heatNGBoil'][nonzeroIndHeat],
            [idxBinInst['NGBoil'] for i in range(len(nonzeroIndHeat))]]] = npmtlb.repmat(
            [1, -parNGBoil['maxTotGen']], len(nonzeroIndHeat), 1).transpose()

        Aineq[idxIneqCons['upHrNGBoil'][nonzeroIndHeat], [idxContOpeVar['heatNGBoil'][nonzeroIndHeat],
                                                          [idxContInstVar['NGBoil'] for i in
                                                           range(len(nonzeroIndHeat))]]] = npmtlb.repmat(
            [1, -1], len(nonzeroIndHeat), 1).transpose()

    # natural gas chiller: operation requirement
    if candidateTech['NGChill'] == 1 and len(nonzeroIndCool):
        Aineq[idxIneqCons['upHrCoolNGChill'][nonzeroIndCool], [idxContOpeVar['coolNGChill'][nonzeroIndCool],
                                                               [idxContInstVar['NGChill'] for i in
                                                                range(len(nonzeroIndCool))]]] = npmtlb.repmat(
            [1, -1], len(nonzeroIndCool), 1).transpose()

    # natural gas chiller: installation requirement
    if candidateTech['NGChill'] == 1 and len(nonzeroIndCool):
        Aineq[idxInstIneqCons['upCapNGChill'], [idxContInstVar['NGChill'],
                                                idxBinInst['NGChill']]] = [1, -parNGChill['maxCap']]

        Aineq[idxInstIneqCons['minCapNGChill'], [idxContInstVar['NGChill'],
                                                 idxBinInst['NGChill']]] = [-1, parNGChill['minCap']]

    # heat fired absorption chiller:operation requirement
    if candidateTech['ABSChill'] == 1 and len(nonzeroIndCool):
        Aineq[idxIneqCons['upHrCoolABSChill'][nonzeroIndCool], [idxContOpeVar['coolABSChill'][nonzeroIndCool],
                                                                [idxContInstVar['ABSChill'] for i in
                                                                 range(len(nonzeroIndCool))]]] = npmtlb.repmat(
            [1, -1], len(nonzeroIndCool), 1).transpose()

    # heat fired absorption chiller: installation requirement
    # if there is no cooling demand, ABSchill is not installed
    if candidateTech['ABSChill'] == 1 and len(nonzeroIndCool):
        Aineq[idxInstIneqCons['upCapABSChill'], [idxContInstVar['ABSChill'],
                                                 idxBinInst['ABSChill']]] = [1, -parABSChill['maxCap']]

        Aineq[idxInstIneqCons['minCapABSChill'], [idxContInstVar['ABSChill'],
                                                  idxBinInst['ABSChill']]] = [-1, parABSChill['minCap']]

    # electric chiller:operation requirement
    if candidateTech['eleChill'] == 1 and len(nonzeroIndCool):
        Aineq[idxIneqCons['upHrCoolEleChill'][nonzeroIndCool], [idxContOpeVar['coolEleChill'][nonzeroIndCool],
                                                                [idxContInstVar['eleChill'] for i in
                                                                 range(len(nonzeroIndCool))]]] = npmtlb.repmat(
            [1, -1], len(nonzeroIndCool), 1).transpose()
        # bineq = popzeroElement(bineq,zeroIndCool, nonzeroIndCool, idxIneqCons['upHrCoolEleChill'])
        # Aineq = popzeroElement(Aineq,zeroIndCool, nonzeroIndCool, idxIneqCons['upHrCoolEleChill'])

    # electric chiller: installtion requirements
    if candidateTech['eleChill'] == 1 and len(nonzeroIndCool):
        Aineq[idxInstIneqCons['upCapEleChill'], [idxContInstVar['eleChill'],
                                                 idxBinInst['eleChill']]] = [1, -parEleChill['maxCap']]

        Aineq[idxInstIneqCons['minCapEleChill'], [idxContInstVar['eleChill'],
                                                  idxBinInst['eleChill']]] = [-1, parEleChill['minCap']]

    # heat pump
    if candidateTech['HP'] == 1 and parHP['heatSta'] == 1 and len(nonzeroIndHeat):
        # added on 09/20/2015, linearization of heat pump model
        # heating status
        # upHeatZHP
        Aineq[idxIneqCons['upHeatZHP'], idxContOpeVar['heatZHP']] = 1
        bineq[idxIneqCons['upHeatZHP']] = parHP['maxHeat'] - parHP['heatCOP'] * parHP['eleEff'] * parHP['minEle']
        # minHeatZHP
        Aineq[idxIneqCons['minHeatZHP'], idxContOpeVar['heatZHP']] = -1
        bineq[idxIneqCons['minHeatZHP']] = -np.min(
            [0, parHP['minHeat'] - parHP['heatCOP'] * parHP['eleEff'] * parHP['maxEle']])
        # upHeatZXHP
        Aineq[idxIneqCons['upHeatZXHP'], idxContOpeVar['heatZHP']] = 1
        Aineq[idxIneqCons['upHeatZXHP'], idxBinOpeVar['heatHP']] = -(
                parHP['maxHeat'] - parHP['heatCOP'] * parHP['eleEff'] * parHP['minEle'])
        # minHeatZXHP
        Aineq[idxIneqCons['minHeatZXHP'], idxContOpeVar['heatZHP']] = -1
        Aineq[idxIneqCons['minHeatZXHP'], idxBinOpeVar['heatHP']] = parHP['minHeat'] - parHP['heatCOP'] * parHP[
            'eleEff'] * parHP['maxEle']
        # upHeatZAHP
        Aineq[idxIneqCons['upHeatZAHP'], idxContOpeVar['heatZHP']] = 1
        Aineq[idxIneqCons['upHeatZAHP'], idxBinOpeVar['heatHP']] = -(
                parHP['minHeat'] - parHP['heatCOP'] * parHP['eleEff'] * parHP['maxEle'])
        Aineq[idxIneqCons['upHeatZAHP'], idxContOpeVar['heatHP']] = -1
        Aineq[idxIneqCons['upHeatZAHP'], idxContOpeVar['eleHP']] = parHP['heatCOP'] * parHP['eleEff']
        bineq[idxIneqCons['upHeatZAHP']] = -(parHP['minHeat'] - parHP['heatCOP'] * parHP['eleEff'] * parHP['maxEle'])
        # minHeatZAHP
        Aineq[idxIneqCons['minHeatZAHP'], idxContOpeVar['heatZHP']] = -1
        Aineq[idxIneqCons['minHeatZAHP'], idxBinOpeVar['heatHP']] = parHP['maxHeat'] - parHP['heatCOP'] * parHP[
            'eleEff'] * parHP['minEle']
        Aineq[idxIneqCons['minHeatZAHP'], idxContOpeVar['heatHP']] = 1
        Aineq[idxIneqCons['minHeatZAHP'], idxContOpeVar['eleHP']] = -parHP['heatCOP'] * parHP['eleEff']
        bineq[idxIneqCons['minHeatZAHP']] = parHP['maxHeat'] - parHP['heatCOP'] * parHP['eleEff'] * parHP['minEle']
        # upHeatZAXHP
        Aineq[idxIneqCons['upHeatZAXHP'], idxContOpeVar['heatZHP']] = 1
        Aineq[idxIneqCons['upHeatZAXHP'], idxBinOpeVar['heatHP']] = parHP['maxHeat'] - parHP['heatCOP'] * parHP[
            'eleEff'] * parHP['minEle']
        Aineq[idxIneqCons['upHeatZAXHP'], idxContOpeVar['heatHP']] = -1
        Aineq[idxIneqCons['upHeatZAXHP'], idxContOpeVar['eleHP']] = parHP['heatCOP'] * parHP['eleEff']
        bineq[idxIneqCons['upHeatZAXHP']] = parHP['maxHeat'] - parHP['heatCOP'] * parHP['eleEff'] * parHP['minEle']

    if candidateTech['HP'] == 1 and parHP['coolSta'] == 1 and len(nonzeroIndCool):
        # cooling status
        # upCoolZHP
        Aineq[idxIneqCons['upCoolZHP'], idxContOpeVar['coolZHP']] = 1
        bineq[idxIneqCons['upCoolZHP']] = parHP['maxCool'] - parHP['coolCOP'] * parHP['eleEff'] * parHP['minEle']
        # minCoolZHP
        Aineq[idxIneqCons['minCoolZHP'], idxContOpeVar['coolZHP']] = -1
        bineq[idxIneqCons['minCoolZHP']] = -np.min(
            [0, parHP['minCool'] - parHP['coolCOP'] * parHP['eleEff'] * parHP['maxEle']])
        # upCoolZXHP
        Aineq[idxIneqCons['upCoolZXHP'], idxContOpeVar['coolZHP']] = 1
        Aineq[idxIneqCons['upCoolZXHP'], idxBinOpeVar['coolHP']] = -(
                parHP['maxCool'] - parHP['coolCOP'] * parHP['eleEff'] * parHP['minEle'])
        # minCoolZXHP
        Aineq[idxIneqCons['minCoolZXHP'], idxContOpeVar['coolZHP']] = -1
        Aineq[idxIneqCons['minCoolZXHP'], idxBinOpeVar['coolHP']] = parHP['minCool'] - parHP['coolCOP'] * parHP[
            'eleEff'] * parHP['maxEle']
        # upCoolZAHP
        Aineq[idxIneqCons['upCoolZAHP'], idxContOpeVar['coolZHP']] = 1
        Aineq[idxIneqCons['upCoolZAHP'], idxBinOpeVar['coolHP']] = -(
                parHP['minCool'] - parHP['coolCOP'] * parHP['eleEff'] * parHP['maxEle'])
        Aineq[idxIneqCons['upCoolZAHP'], idxContOpeVar['coolHP']] = -1
        Aineq[idxIneqCons['upCoolZAHP'], idxContOpeVar['eleHP']] = parHP['coolCOP'] * parHP['eleEff']
        bineq[idxIneqCons['upCoolZAHP']] = -(parHP['minCool'] - parHP['coolCOP'] * parHP['eleEff'] * parHP['maxEle'])
        # minCoolZAHP
        Aineq[idxIneqCons['minCoolZAHP'], idxContOpeVar['coolZHP']] = -1
        Aineq[idxIneqCons['minCoolZAHP'], idxBinOpeVar['coolHP']] = parHP['maxCool'] - parHP['coolCOP'] * parHP[
            'eleEff'] * parHP['minEle']
        Aineq[idxIneqCons['minCoolZAHP'], idxContOpeVar['coolHP']] = 1
        Aineq[idxIneqCons['minCoolZAHP'], idxContOpeVar['eleHP']] = -parHP['coolCOP'] * parHP['eleEff']
        bineq[idxIneqCons['minCoolZAHP']] = parHP['maxCool'] - parHP['coolCOP'] * parHP['eleEff'] * parHP['minEle']
        # upCoolZAXHP
        Aineq[idxIneqCons['upCoolZAXHP'], idxContOpeVar['coolZHP']] = 1
        Aineq[idxIneqCons['upCoolZAXHP'], idxBinOpeVar['coolHP']] = parHP['maxCool'] - parHP['coolCOP'] * parHP[
            'eleEff'] * parHP['minEle']
        Aineq[idxIneqCons['upCoolZAXHP'], idxContOpeVar['coolHP']] = -1
        Aineq[idxIneqCons['upCoolZAXHP'], idxContOpeVar['eleHP']] = parHP['coolCOP'] * parHP['eleEff']
        bineq[idxIneqCons['upCoolZAXHP']] = parHP['maxCool'] - parHP['coolCOP'] * parHP['eleEff'] * parHP['minEle']

    # heat pump: installation
    if candidateTech['HP'] == 1 and (len(nonzeroIndCool) or len(nonzeroIndHeat)):
        Aineq[idxInstIneqCons['upCapHP'], [idxContInstVar['HP'],
                                           idxBinInst['HP']]] = [1, -parHP['maxCap']]

        Aineq[idxInstIneqCons['minCapHP'], [idxContInstVar['HP'],
                                            idxBinInst['HP']]] = [-1, parHP['minCap']]

    # heat pump: operation
    if candidateTech['HP'] == 1:
        '''                                  
        Aineq[idxIneqCons['upHrThemHP'], [idxContOpeVar['coolHP'],
                                      idxContOpeVar['heatHP'],
                                     [idxContInstVar['HP'] for i in range(parTime['totHrStep'])]]] = npmtlb.repmat(
                                     [1, 1, -1], parTime['totHrStep'], 1).transpose()
        '''
        # cooling status
        if parHP['coolSta'] == 1 and len(nonzeroIndCool):
            Aineq[idxIneqCons['upHrThemHP'][nonzeroIndCool], [idxContOpeVar['coolHP'][nonzeroIndCool],
                                                              [idxContInstVar['HP'] for i in
                                                               range(len(nonzeroIndCool))]]] = npmtlb.repmat(
                [1, -1], len(nonzeroIndCool), 1).transpose()

            Aineq[idxIneqCons['upHrStaHP'][nonzeroIndCool], [idxBinOpeVar['coolHP'][nonzeroIndCool]]] = npmtlb.repmat(
                [1], len(nonzeroIndCool), 1).transpose()

            bineq[idxIneqCons['upHrStaHP'][nonzeroIndCool]] = parHP['outType']

            # heating status
        if parHP['heatSta'] == 1 and len(nonzeroIndHeat):
            Aineq[idxIneqCons['upHrThemHP'][nonzeroIndHeat], [idxContOpeVar['heatHP'][nonzeroIndHeat],
                                                              [idxContInstVar['HP'] for i in
                                                               range(len(nonzeroIndHeat))]]] = npmtlb.repmat(
                [1, -1], len(nonzeroIndHeat), 1).transpose()

            Aineq[idxIneqCons['upHrStaHP'][nonzeroIndHeat], [idxBinOpeVar['heatHP'][nonzeroIndHeat]]] = npmtlb.repmat(
                [1], len(nonzeroIndHeat), 1).transpose()

            bineq[idxIneqCons['upHrStaHP'][nonzeroIndHeat]] = parHP['outType']
        '''                             
        Aineq[idxIneqCons['upHrStaHP'], [idxBinOpeVar['heatHP'],
                                     idxBinOpeVar['coolHP']]] = npmtlb.repmat(
                                    [1, 1], parTime['totHrStep'],1).transpose()
        '''

    # heat pump:  heating status
    if candidateTech['HP'] == 1 and parHP['heatSta'] == 1 and len(nonzeroIndHeat):
        Aineq[idxIneqCons['upHrHeatHP'][nonzeroIndHeat], [idxContOpeVar['heatHP'][nonzeroIndHeat],
                                                          idxBinOpeVar['heatHP'][nonzeroIndHeat]]] = npmtlb.repmat(
            [1, -parHP['maxHeat']], len(nonzeroIndHeat), 1).transpose()

        Aineq[idxIneqCons['minHrHeatHP'][nonzeroIndHeat], [idxContOpeVar['heatHP'][nonzeroIndHeat],
                                                           idxBinOpeVar['heatHP'][nonzeroIndHeat]]] = npmtlb.repmat(
            [-1, parHP['minHeat']], len(nonzeroIndHeat), 1).transpose()
    # heat pump:  cooling status
    if candidateTech['HP'] == 1 and parHP['coolSta'] == 1 and len(nonzeroIndCool):
        # upHrCoolHP
        Aineq[idxIneqCons['upHrCoolHP'][nonzeroIndCool], [idxContOpeVar['coolHP'][nonzeroIndCool],
                                                          idxBinOpeVar['coolHP'][nonzeroIndCool]]] = npmtlb.repmat(
            [1, -parHP['maxCool']], len(nonzeroIndCool), 1).transpose()
        # bineq = popzeroElement(bineq,zeroIndCool, nonzeroIndCool, idxIneqCons['upHrCoolHP'])
        # Aineq = popzeroElement(Aineq,zeroIndCool, nonzeroIndCool, idxIneqCons['upHrCoolHP'])

        # minHrCoolHP
        Aineq[idxIneqCons['minHrCoolHP'][nonzeroIndCool], [idxContOpeVar['coolHP'][nonzeroIndCool],
                                                           idxBinOpeVar['coolHP'][nonzeroIndCool]]] = npmtlb.repmat(
            [-1, parHP['minCool']], len(nonzeroIndCool), 1).transpose()
        # bineq = popzeroElement(bineq,zeroIndCool, nonzeroIndCool, idxIneqCons['minHrCoolHP'])
        # Aineq = popzeroElement(Aineq,zeroIndCool, nonzeroIndCool, idxIneqCons['minHrCoolHP'])

    print("Ineqaulity constraints matrix is successfully built! ")

    # #for i in xrange(5):
    #    for j in xrange(8):
    #        A[i, j] = -8 + np.sin(8 * i) + np.cos(15 * j)
    #    b[i] = -150 + 80 * np.sin(80 * i)
    # --------------------------------------------------------------------------------------------------------------------
    print("Begin building the index for integer variables ...........................")
    # _optionalData = ['f','x0','A', 'Aeq', 'b', 'beq', 'lb', 'ub', 'intVars', 'boolVars']
    # allowedGoals = ['minimum', 'min', 'max', 'maximum']
    # index of integer installation variables
    # indexing starts from ZERO!
    # staIdx = numContInstVar
    # endIdx = totNumInstVar
    # intVars = np.arange(staIdx, endIdx)  # integer installation variables
    # intVars = intVars.tolist()
    # index of integer operational variables
    # staIdx = numVar + totNumContOpeVar
    # staIdx = (totNumInstVar + totNumContOpeVar)
    # endIdx = staIdx + numBinOpeVar
    intVars = []
    # installation binary variables
    intInstVar = np.arange(numContInstVar, numContInstVar + totNumBinInstVar)
    intVars = np.append(intVars, intInstVar)
    # operational binary variables
    intOpeVar = [
        np.arange(numVarMat[i][j][k] + totNumContOpeVar, numVarMat[i][j][k] + totNumContOpeVar + totNumBinOpeVar)
        for i in range(parTime['numMonth'])
        for j in range(parTime['numDay'])
        for k in range(parTime['numHour'])]

    for i in range(parTime['totHrStep']):
        intVars = np.hstack((intVars, intOpeVar[i]))  # integer operation variables

    intVars = intVars.astype(int)
    logging.info("Integer index successfully built! ")
    # -----------------------------------------------------------------------------------------------------------------------
    logging.info("Being building the upper and lower bound matrix ...................")

    lb = np.zeros(totVars)
    ub = bigM * np.ones(totVars)
    # this interface 'lb,ub' is left of users to set up the decision on whether the technology is selected or not
    # lb[intVars] = 0
    ub[intVars] = 1

    # specify lower and upper bounds on a certain tech
    idxIntOpe = {}
    for name, contents in idxBinOpe.items():
        idxIntOpe[name] = numVarMat + totNumContOpeVar + contents
    if not (itemForceSelect is None):
        for name in itemForceSelect.keys():
            if itemForceSelect[name] == 1 and candidateTech[name] == 1:
                lb[idxBinInst[name]] = 1

    # set the decision for purchasing electricity from grid 'OFF'
    # when parCHP['eleSta']=1, electricity is allowed to be purchased, all the electricity will be provided from power grid, not from CHP
    if candidateTech['eleGrid'] == 1 and len(nonzeroIndEle):  # and parCHP['eleSta']==1
        ub[idxIntOpe['purEleGrid']] = 1

    if candidateTech['PV'] == 1 and len(nonzeroIndEle):
        ub[idxContInstVar['PV']] = parPV['maxInstArea']

    if candidateTech['solThem'] == 1 and len(nonzeroIndHeat):
        ub[idxContInstVar['solThem']] = parSolThem['maxInstArea']

    # set the status of natural gas chiller to be 'OFF'
    if candidateTech['NGChill'] == 1 and len(nonzeroIndCool):
        ub[idxBinInst['NGChill']] = 1

    # set the status of electric boiler to be 'OFF'
    # ub[idxBinInst['eleBoil']] = 0

    # 08/24/2015, set the status of cooling tank to be 'OFF'
    if candidateTech['coolTank'] == 1 and len(nonzeroIndCool):
        lb[idxBinInst['coolTank']] = 0

    if candidateTech['eleBat'] == 1 and len(nonzeroIndEle):
        ub[idxBinInst['eleBat']] = 1

    # set the status of natural gas boiler to be 'OFF'
    if candidateTech['NGBoil'] == 1 and len(nonzeroIndHeat):
        ub[idxBinInst['NGBoil']] = 1

    # 09/04/2015, set heating status of heat pump to be 'OFF'
    if candidateTech['HP'] == 1 and len(nonzeroIndHeat) and parHP['heatSta'] == 1:
        ub[idxContOpeVar['heatZHP']] = 0
    #    ub[idxBinOpeVar['heatHP']] = 0

    # 09/04/2015, set cooling status of heat pump to be 'ON'
    if candidateTech['HP'] == 1 and len(nonzeroIndCool) and parHP['coolSta'] == 1:
        ub[idxContOpeVar['coolZHP']] = 0
    #    lb[idxBinOpeVar['coolHP']] = 1

    if candidateTech['CHP'] == 1 and parCHP['heatSta'] == 0:
        ub[idxContOpeVar['heatCHP']] = 0

    print("upper and lower bound matrix successfully built!")

    print("Aeq matrix(before reduction):")

    print(repr(Aeq))

    print("Aineq matrix(before reduction):")
    print(repr(Aineq))


    # =========================================================================
    # 09/20/2015, in this case, if we oculd find out all the zero rows in Aeq, then we can delete the corresponding rows in beq
    # therefore, we do not need to find out what is the set of indEqDel
    # see remove_zero_rows_sparse0(X)
    # before removing rows, len([unique_nonzero_indice, indEqDel])=len(beq)
    # for rows, Aeq is related to beq
    # for columns, Aeq is related to Aineq
    # http://stackoverflow.com/questions/13352280/slicing-sparse-matrices-in-scipy-which-types-work-best
    #  to slice rows I want to slice CSR
    print("\nBeing reducing nonexistent columns and rows in equality and inequality constraints matrix......")
    indDelInt = []
    unique_nonzero_col_indice = []
    if probReduceFlag == 1:
        unique_nonzero_row_eqindice = np.unique(Aeq.nonzero()[0])
        # 1st condition indicates that there are zero rows in Aeq
        # 2nd condition indicates that Aeq is not zero matrix
        if len(unique_nonzero_row_eqindice) < Aeq.shape[0] and len(unique_nonzero_row_eqindice) > 0:
            if sps.issparse(Aeq):
                Aeq = Aeq.tocsr()[unique_nonzero_row_eqindice, :]
            else:
                Aeq = Aeq[unique_nonzero_row_eqindice, :]
            # Aeq = Aeq.tocsr()[unique_nonzero_row_eqindice,:]
            beq = beq[unique_nonzero_row_eqindice]

        unique_nonzero_row_ineqindice = np.unique(Aineq.nonzero()[0])
        if len(unique_nonzero_row_ineqindice) < Aineq.shape[0] and len(unique_nonzero_row_ineqindice) > 0:
            if sps.issparse(Aineq):
                Aineq = Aineq.tocsr()[unique_nonzero_row_ineqindice, :]
            else:
                Aineq = Aineq[unique_nonzero_row_ineqindice, :]
            # Aineq = Aineq.tocsr()[unique_nonzero_row_ineqindice,:]
            bineq = bineq[unique_nonzero_row_ineqindice]

        # removing zero columns
        unique_nonzero_col_eqindice = np.unique(Aeq.nonzero()[1])
        unique_nonzero_col_ineqindice = np.unique(Aineq.nonzero()[1])
        unique_nonzero_col_indice = np.concatenate((unique_nonzero_col_eqindice, unique_nonzero_col_ineqindice), axis=0)
        unique_nonzero_col_indice = np.unique(unique_nonzero_col_indice)

        # find out rows with zero elements in Aeq but nonzero elements in Aineq
        nonZeroColIneq_zeroColEq = sorted(list(set(unique_nonzero_col_indice) - set(unique_nonzero_col_eqindice)))
        # find out rows with zero elements in Aineq but nonzero elements in Aeq
        nonZeroColEq_zeroColIneq = sorted(list(set(unique_nonzero_col_indice) - set(unique_nonzero_col_ineqindice)))

        # find out the zero columns in both Aeq and Aineq
        # full matrix
        # zeroColEq_zeroColIneq=sorted(list(set(range(len(Aeq[0]))) - set(unique_nonzero_col_indice)))
        # sparse matrix
        zeroColEq_zeroColIneq = sorted(list(set(range(Aeq.shape[1])) - set(unique_nonzero_col_indice)))
        # mat._shape = (mat._shape[0] - 1, mat._shape[1])

        # removing zero elements from all the matrix related to variables
        # after removing zero columns, the index of variables is changed accordingly
        # to slice columns I want to use CSC
        # indDelInt = list()
        if len(unique_nonzero_col_indice) < Aeq.shape[1] and len(unique_nonzero_col_indice) > 0:
            if sps.issparse(Aineq) and sps.issparse(Aeq):
                Aineq = Aineq.tocsc()[:, unique_nonzero_col_indice]
                Aeq = Aeq.tocsc()[:, unique_nonzero_col_indice]
            else:
                Aineq = Aineq[:, unique_nonzero_col_indice]
                Aeq = Aeq[:, unique_nonzero_col_indice]
            # Aineq = Aineq.tocsc()[:, unique_nonzero_col_indice]
            # Aeq = Aeq.tocsc()[:, unique_nonzero_col_indice]
            f = f[unique_nonzero_col_indice]
            lb = lb[unique_nonzero_col_indice]
            ub = ub[unique_nonzero_col_indice]

            # zero integer variables that need to be removed from intVars
            indDelInt = sorted(list(set(intVars).intersection(zeroColEq_zeroColIneq)))
            # when len(indDelInt)=0 and len(zeroColEq_zeroColIneq)>0, it means no integer variables are deleted
            if len(indDelInt):
                intVars = sorted(list(set(intVars) - set(indDelInt)))

            # http://stackoverflow.com/questions/8251541/numpy-for-every-element-in-one-array-find-the-index-in-another-array
            # we need to find  the index of every element of intVars in unique_nonzero_col_indice
            # because the index of variable in intVars should be changed due to the deletion of zero columns in Aeq, Aineq
            # indices = [i for i, x in enumerate(my_list) if x == "whatever"]
            indices = []
            for iy in intVars:
                indices = np.append(indices, np.where(unique_nonzero_col_indice == iy)[0])
            intVars = indices.astype(int)
    # 09/18/2015, there are some problems with the removing of zero energy demand requirement since it should be done at once
    # if we remove the constraint for many times, the sequence would be changed, leading to error in the latter action
    print("nonexistent columns and rows in equality and ineqaulity matrix are removed successfully!")

    tol = smallM
    if sps.issparse(Aineq) and sps.issparse(Aeq):
        pass
    else:
        Aeq[abs(Aeq) < tol] = 0
        Aineq[abs(Aineq) < tol] = 0
        beq[abs(beq) < tol] = 0
        bineq[abs(bineq) < tol] = 0


    print("Aeq matrix(after reduction):")
    print(repr(Aeq))

    print("Aineq matrix(after reduction):")
    print(repr(Aineq))

    numNonZeroAeq = len(np.transpose(np.nonzero(Aeq)))
    totElementAeq = (float(len(beq)) * float(len(ub)))
    print("number of nonzero and total elements in Aeq: %i/%i" % (numNonZeroAeq, totElementAeq))

    if totElementAeq != 0:
        sparsityAeq = numNonZeroAeq / totElementAeq
        print('sparsity of Aeq(after reduction): %f' % sparsityAeq)

    numNonZeroAineq = len(np.nonzero(Aineq)[0])
    totElementAineq = (float(len(bineq)) * float(len(ub)))
    print("number of nonzero and total elements in Aineq: %i/%i" % (numNonZeroAineq, totElementAineq))

    if totElementAineq != 0:
        sparsityAineq = numNonZeroAineq / totElementAineq
        print('sparsity of Aineq(after reduction): %f' % sparsityAineq)

    print('number of equality constraints after reduction (the row of Aeq): %i' % len(beq))
    print('number of inequality constraints after reduction (the row of Aineq): %i' % len(bineq))

    print("number of total variable before and after removing zero columns: %i/%i" % (totVars, len(ub)))
    # print('the number of total variable after removing zero columns: %i' % len(ub))
    print("number of integer variable before and after removing zero columns: %i/%i\n" % (
        (len(intVars) + len(indDelInt)), len(intVars)))


    milpProb = {'Aeq': Aeq,
                'beq': beq,
                'Aineq': Aineq,
                'bineq': bineq,
                'intVars': intVars,
                'lb': lb,
                'ub': ub,
                'f': f,
                'oriTotVar': totVars}

    nonzeroIndLoad = {'ele': nonzeroIndEle,
                      'cool': nonzeroIndCool,
                      'heat': nonzeroIndHeat,
                      'elePre': oriNonzeroIndEle,
                      'coolPre': oriNonzeroIndCool,
                      'heatPre': oriNonzeroIndHeat
                      }

    return eleLoad, coolLoad, heatLoad, pipeHeatLoss, pipeCoolLoss, milpProb, candidateTech, unique_nonzero_col_indice, \
           nonzeroIndLoad, idxContOpeVar, idxBinOpeVar, oriIdxContOpeVarCHP, oriIdxBinOpeVarCHP, idxInstCHP, idxBinInst, idxContInstVar, \
           idxVarMaxDemand, mainPipeHeatLoss, mainPipeCoolLoss, bldgHeat, bldgCool, parTime, parCHP, parEleGrid, parPV, parSolThem, parEleBoil, \
           parNGBoil, parHeatTank, parCoolTank, parEleBat, parHP, parNGChill, parABSChill, parEleChill, parHeat, parWeight


# @profile(precision=4)
def get_sol(idxVar, optSol, flag, dimX, dimY, dimZ):
    # flag parameter is used to decide whether the solution should be shaped by year/month/day
    # dimX, dimY, dimZ are month, day, hour for valContOpeVar & valBinOpeVar
    valVar = {}
    if type(optSol) is list:
        optSol = np.asarray(optSol)

    for name, contents in idxVar.items():
        valVar[name] = optSol[contents]
        if flag == 1:
           valVar[name] = valVar[name].reshape(dimX, dimY, dimZ)

    return valVar


# added on 09/28/2015 to process the binary and continuous operation variables for CHP
def get_ope_sol_chp(idxOpeVar, optSol, reshapeFlag, paraTime, paraCHP):
    valOpeVarCHP = get_sol(idxOpeVar, optSol, reshapeFlag, paraTime['numMonth'], paraTime['numDay'],
                          paraTime['numHour'] *  np.sum(paraCHP['numInst']))
    # 09/25/2015, reorganize the solution for each type of CHP
    solOpeVarCHP = {}
    # solBinOpeVarCHP = {}
    for name, contents in valOpeVarCHP.items():
        solOpeVarCHP[name] = np.zeros((paraTime['totHrStep'], np.sum(paraCHP['numInst'])))

        temp = valOpeVarCHP[name]

        for i in range(paraTime['numMonth']):
            for j in range(paraTime['numDay']):
                temp1 = temp[i,j,:].reshape((paraTime['numHour'], np.sum(paraCHP['numInst'])))

                startIdx = j * paraTime['numHour'] + i * paraTime['numHour'] * paraTime['numDay']
                endIdx = startIdx + paraTime['numHour']
                solOpeVarCHP[name][startIdx:endIdx, :] = temp1
                '''
                for m in range(np.sum(paraCHP['numInst'])):
                    # temp2=np.reshape(temp1[:,m],1,parTime['numHour'])
                    # solContOpeVarCHP[name][m,:]=np.append(solContOpeVarCHP[name][m,:],temp1[:,m])

                    solOpeVarCHP[name][startIdx:endIdx, m] = temp1[:, m]
                '''
    return valOpeVarCHP, solOpeVarCHP

def run(pipeNetwFlag=None, parThemPipe=None, ctrStr=None, eleLoad=None,coolLoad=None,
        heatLoad=None, bigM=None, smallM=None, parCHP=None, parTime=None, LenLifeYr=None, CandidateTech1=None,
        CandidateTech2=None, forceSelect=None, parPV=None, parSolThem=None, parEleBoil=None, parNGBoil=None,
        parHeatTank=None, parCoolTank=None, parEleBat=None, parHP=None, parNGChill=None, parABSChill=None,
        parEleChill=None, parEleGrid=None, parHeat=None, rateMonthDemandCharge=None, lossCalMode=None,
        numbldgCluster=None, mainPipeLink=None, parMainPipe=None, emiFac=None, taxCO2=None, solIrradiation=None,
        parBasic=None):

    assert parTime is not None, "parameter parTime can be None."

    parTime["totHrStep"] = parTime['numMonth'] * parTime['numDay'] * parTime['numHour']

    ############################# CHP ####################################################################
    if parCHP is not None:
        # gasPriceCHP: $/kWh
        parCHP['gasPriceCHP'] = np.ones((parTime['numMonth'], parTime['numDay'], parTime['numHour']), dtype=np.float32) *  parCHP['unitGasPriceCHP']

        # $35/kWe - $55 /kWe, average $40/kWe
        parCHP['mtnPriceCHP'] = np.ones((parTime['numMonth'], parTime['numDay'], parTime['numHour']), dtype=np.float32) * parCHP['unitMtnPriceCHP']



    #################################  PV  ##################################################################

    parPV['solIrradiation'] = solIrradiation

    parPV['mtnPricePV'] = (np.ones((parTime['numMonth'], parTime['numDay'], parTime['numHour']), dtype=np.float32) *
                           parPV['unitMtnPrice'])

    # TODO: why the factor here is 0.098?
    tot_area = np.sum(parBasic['off_area']) + np.sum(parBasic['com_area']) + np.sum(parBasic['hotel_area']) + \
               np.sum(parBasic['res_area'])

    # parPV['maxInstArea'] = np.min(np.array([tot_area * 0.098, parPV['maxInstArea']]))
    parPV['maxInstArea'] = min(tot_area, parPV['maxInstArea'])

    ############################Solar thermal ###############################################################

    # TODO: a vector can be created directly instead of a high dimension array.
    parSolThem['mtnPriceSolThem'] = (np.ones((parTime['numMonth'], parTime['numDay'], parTime['numHour']),
                                             dtype=np.float32) * parSolThem['unitMtnPrice'])

    parSolThem['solIrradiation'] = solIrradiation
    # TODO: why need to multiply 0.167 here?
    parSolThem['maxInstArea'] = min(tot_area * 0.167, parSolThem['maxInstArea'])

    #################################### NG Boiler ########################################################
    # TODO: convert gasPriceNGBoil and mtnPriceNGBoild to vector directly.
    parNGBoil['gasPriceNGBoil'] = (np.ones((parTime['numMonth'], parTime['numDay'], parTime['numHour']),
                                           dtype=np.float32) * parNGBoil['unitGasPriceNGBoil'])
    parNGBoil['mtnPriceNGBoil'] = (np.ones((parTime['numMonth'], parTime['numDay'], parTime['numHour']),
                                           dtype=np.float32) * parNGBoil['unitMtnPriceNGBoil'])

    ##################################### Heat Tank #######################################################
    parHeatTank['mtnPriceHeatTank'] = (
            np.ones((parTime['numMonth'], parTime['numDay'], parTime['numHour']),
                    dtype=np.float32) * parHeatTank['unitMtnPriceHeatTank'])

    ## Cool tank
    parCoolTank['mtnPriceCoolTank'] = (np.ones((parTime['numMonth'], parTime['numDay'], parTime['numHour']),
                    dtype=np.float32) * parCoolTank['unitMtnPriceCoolTank'])

    ## EleBat
    parEleBat['mtnPriceEleBat'] = (
            np.ones((parTime['numMonth'], parTime['numDay'], parTime['numHour']),
                    dtype=np.float32) * parEleBat['unitMtnPriceEleBat'])
    ## HP
    parHP['mtnPriceHP'] = (np.ones((parTime['numMonth'], parTime['numDay'], parTime['numHour']),
                                       dtype=np.float32) * parHP['unitMtnPriceHP'])

    parHP['maxCap'] = min(tot_area * 0.0068, parHP['maxCap'])

    ## NG Chiller
    parNGChill['gasPriceNGChill'] = (np.ones((parTime['numMonth'], parTime['numDay'], parTime['numHour']),
                    dtype=np.float32) * parNGChill['unitGasPriceNGChill'])

    parNGChill['mtnPriceNGChill'] = (np.ones((parTime['numMonth'], parTime['numDay'], parTime['numHour']),
                    dtype=np.float32) * parNGChill['unitMtnPriceNGChill'])

    ## absorption chiller fired by heat
    parABSChill['mtnPriceABSChill'] = (np.ones((parTime['numMonth'] * parTime['numDay'] * parTime['numHour']),
                                               dtype=np.float32) * parABSChill['unitMtnPriceABSChill'])
    ## electric chiller
    parEleChill['mtnPriceEleChill'] = (np.ones((parTime['numMonth'] * parTime['numDay'] * parTime['numHour']),
                    dtype=np.float32) * parEleChill['unitMtnPriceEleChill'])

    ## Heat sale
    parHeat['purPriceInds'] = (np.ones((parTime['numMonth'], parTime['numDay'], parTime['numHour']),
                    dtype=np.float32) * parHeat['unitPurPriceInds'])

    parHeat['salPriceHeat'] = (np.ones((parTime['numMonth'], parTime['numDay'], parTime['numHour']),
                    dtype=np.float32) * parHeat['unitSalPriceHeat'])

    # build the MILP problem
    logging.info("begin building the MILP problem...............")
    # note: we need to return parCHP, parTime even though it is included in the input because some new parameters are added in probOptDES
    eleLoad, coolLoad, heatLoad, pipeHeatLoss, pipeCoolLoss, milpProb, candidateTech, unique_nonzero_col_indice, nonzeroIndLoad, \
    idxContOpeVar, idxBinOpeVar, oriIdxContOpeVarCHP, oriIdxBinOpeVarCHP, idxInstCHP, idxBinInst, idxContInstVar, \
    idxVarMaxDemand, mainPipeHeatLoss, mainPipeCoolLoss, bldgHeat, bldgCool, parTime, parCHP, parEleGrid, parPV, parSolThem, parEleBoil, \
    parNGBoil, parHeatTank, parCoolTank, parEleBat, parHP, parNGChill, parABSChill, parEleChill, parHeat, parWeight = build(
        pipeNetwFlag=pipeNetwFlag, parThemPipe=parThemPipe,
        ctrStr=ctrStr, eleLoad=eleLoad, coolLoad=coolLoad, heatLoad=heatLoad, bigM=bigM,
        smallM=smallM, itemParCHP=parCHP, itemParTime=parTime, itemLenLifeYr=LenLifeYr,
        itemCandidateTech1=CandidateTech1,
        itemCandidateTech2=CandidateTech2, itemForceSelect=forceSelect, itemParPV=parPV, itemParSolThem=parSolThem,
        itemParEleBoil=parEleBoil, itemParNGBoil=parNGBoil, itemParHeatTank=parHeatTank,
        itemParCoolTank=parCoolTank, itemParEleBat=parEleBat, itemParHP=parHP, itemParNGChill=parNGChill,
        itemParABSChill=parABSChill, itemParEleChill=parEleChill,
        itemParEleGrid=parEleGrid, itemParHeat=parHeat, rateMonthDemandCharge=rateMonthDemandCharge, emiFac=emiFac,
        taxCO2=taxCO2, lossCalMode=lossCalMode, numbldgCluster=numbldgCluster, mainPipeLink=mainPipeLink,
        parMainPipe=parMainPipe)

    Aeq = milpProb['Aeq']
    Aineq = milpProb['Aineq']
    beq = milpProb['beq']
    bineq = milpProb['bineq']
    lb = milpProb['lb']
    ub = milpProb['ub']
    intVars = milpProb['intVars']
    f = milpProb['f']
    oriTotVar = milpProb['oriTotVar']

    nonzeroIndEle = nonzeroIndLoad['ele']
    nonzeroIndCool = nonzeroIndLoad['cool']
    nonzeroIndHeat = nonzeroIndLoad['heat']

    oriNonzeroIndEle = nonzeroIndLoad['elePre']
    oriNonzeroIndCool = nonzeroIndLoad['coolPre']
    oriNonzeroIndHeat = nonzeroIndLoad['heatPre']

    # ###################################################################### #
    #                                                                        #
    #    O P T I M I Z E   P R O B L E M                                     #
    #                                                                        #
    # ###################################################################### #
    # solve the MILP problem
    operSummary, instSummary, optimSummary = {}, {}, {}

    print("the MILP problem is successfully built!")
    print("being solving the MILP problem ................")

    if (ctrStr is None) or (not ('plotFlag' in ctrStr)):
        raise Exception("ctrStr can not be None.")

    mySolverInterface = ctrStr['mySolverInterface']
    mySolver = ctrStr['mySolver']
    mpsWriteFlag = ctrStr['mpsWriteFlag']
    mpsGenMed = ctrStr['mpsGenMed']

    optVal, optObj = gurobi_solver.solve(Aeq=Aeq, Aineq=Aineq, beq=beq, bineq=bineq,vlb=lb,vub=ub, xint=intVars,
                                         f=f, goal='min')

    optimSummary['optVal'], optimSummary['optimalValue'] = optVal, optObj

    if type(optVal) is list:
        optVal = np.asarray(optVal)

    if len(optVal) == 0 or math.isnan(float(optObj)):
        logging.warning("infeasible problem ... ")
        success_flag = 0
        return [], [], [], [], [], [], [], [], success_flag
    else:
        logging.info("the problem is successfully solved!\n")
        success_flag = 1
        pass

    # we should reformat the solution to the original index before removing zero columns
    if len(unique_nonzero_col_indice) < oriTotVar and len(unique_nonzero_col_indice) > 0:
        oriOptSol = np.zeros(oriTotVar)
        # assigning optVal to oriOptSol according to unique_nonzero_col_indice
        for i in range(len(optVal)):
            oriOptSol[unique_nonzero_col_indice[i]] = optVal[i]
        optVal = oriOptSol


    # ---------------------------------------------------------------------
    #                                                                        #
    #    Q U E R Y   S O L U T I O N                                         #
    #                                                                        #
    # ---------------------------------------------------------------------
    # We store all results in a dictionary so that we can easily access　them by name.
    # reorganize solutions after optimization
    #
    # valContOpeVarCHP: optimal value of continuous operation variable for CHP
    # valBinInst: optimal value of binary installation variables
    # valContInst: optimal value of continuous installation variables (capacity)


    # for valContOpeVar & valBinOpeVar, reshape flag is '1'

    valContOpeVar = get_sol(idxContOpeVar, optVal, 0, parTime['numMonth'], parTime['numDay'], parTime['numHour'])
    valBinOpeVar = get_sol(idxBinOpeVar, optVal, 0, parTime['numMonth'], parTime['numDay'], parTime['numHour'])

    # no reshape is needed for valBinInst & valContInst, so the last three parameters are empty,
    # and the reshape flag is set to '0'
    valBinInst = get_sol(idxBinInst, optVal, 0, [], [], [])
    valContInst = get_sol(idxContInstVar, optVal, 0, [], [], [])

    # for CHP, the reshape size is different from that for valContOpeVar & valBinOpeVar
    valBinInstTypeCHP = np.zeros((1, parCHP['numType']))
    valBinInstCHP = []
    if candidateTech['CHP'] == 1:
        valContOpeVarCHP, solContOpeVarCHP = get_ope_sol_chp(oriIdxContOpeVarCHP, optVal, 1, parTime, parCHP)
        valBinOpeVarCHP, solBinOpeVarCHP = get_ope_sol_chp(oriIdxBinOpeVarCHP, optVal, 1, parTime, parCHP)

        valBinInstCHP = optVal[np.arange(0, np.sum(parCHP['numInst'])) + idxInstCHP]

        # calculate the install number of each type of CHP

        for i in range(parCHP['numType']):
            if i == 0:
                startIdx = 0
                endIdx = parCHP['numInst'][i]
            else:
                startIdx = np.sum(parCHP['numInst'][0:i])
                endIdx = parCHP['numInst'][i] + startIdx

            valBinInstTypeCHP[0, i] = np.sum(valBinInstCHP[startIdx:endIdx])

        valBinInstTypeCHP = np.squeeze(valBinInstTypeCHP)
        instSummary['valBinInstTypeCHP'] = valBinInstTypeCHP
        instSummary['valBinInstCHP'] = valBinInstCHP

    logging.info("begin reorganizing the results...................")

    # Here is the load information.
    # Outputs/Operation/eleLoad.in, Outputs/Operation/heatLoad.in, Outputs/Operation/coolLoad.in
    operSummary['eleLoad'], operSummary['heatLoad'], operSummary['coolLoad'] = eleLoad, heatLoad, coolLoad


    ngTot, initInv, ngTot_cost, mtnTot = 0, 0, 0, 0
    ng_month, ng_month_cost, ele_month, ele_month_cost = np.zeros(parTime['numMonth']), np.zeros(
        parTime['numMonth']), np.zeros(parTime['numMonth']), np.zeros(parTime['numMonth'])
    ele_month_low, ele_month_med, ele_month_high = np.zeros(parTime['numMonth']), np.zeros(
        parTime['numMonth']), np.zeros(parTime['numMonth'])
    ele_month_cost_low, ele_month_cost_med, ele_month_cost_high = np.zeros(parTime['numMonth']), np.zeros(
        parTime['numMonth']), np.zeros(parTime['numMonth'])

    if candidateTech['PV'] == 1 and valBinInst['PV'] == 1:
        elePV = valContOpeVar['elePV']
        # Outputs/Operation/elePV.out
        operSummary['elePV'] = elePV

        elePV_month = np.squeeze(np.sum(
            (elePV * parWeight['weight_tot']).reshape(parTime['numMonth'], parTime['numDay'], parTime['numHour']),
            axis=(1, 2)))
        # Outputs/Operation/elePV_month.out
        operSummary['elePV_month'] = elePV_month


        areaPV = valContInst['PV']
        print('the installation area of solar PV is (m^2): %f\n' % areaPV)
        # Outputs/Installations/areaPV.out
        instSummary['areaPV'] = areaPV

        initInv += valContInst['PV'] * parPV['varIns'] + valBinInst['PV'] * parPV['fixIns']
        mtnTot += np.sum(elePV * parPV['weight_mtn'])

    if candidateTech['solThem'] == 1 and valBinInst['solThem'] == 1:
        heatSolThem = valContOpeVar['heatSolThem']
        operSummary['heatSolThem'] = heatSolThem

        areaSolThem = valContInst['solThem']
        print('the installation area of solar thermal is (m^2): %f\n' % areaSolThem)
        instSummary['areaSolThem'] = areaSolThem

        initInv += valContInst['solThem'] * parSolThem['varIns'] + valBinInst['solThem'] * parSolThem['fixIns']
        mtnTot += np.sum(heatSolThem * parSolThem['weight_mtn'])

    if candidateTech['NGBoil'] == 1 and valBinInst['NGBoil'] == 1:
        capNGBoil = valContInst['NGBoil']
        instSummary['capNGBoil'] = capNGBoil

        print('the installation capacity of natural gas boiler is : %i\n' % int(capNGBoil))
        gasNGBoil = valContOpeVar['gasNGBoil']
        heatNGBoil = valContOpeVar['heatNGBoil']
        operSummary['gasNGBoil'], operSummary['heatNGBoil'] = gasNGBoil, heatNGBoil

        initInv += valContInst['NGBoil'] * parNGBoil['varIns'] + valBinInst['NGBoil'] * parNGBoil['fixIns']
        ngTot += np.sum(valContOpeVar['gasNGBoil'] * parWeight['weight_tot'])
        ngTot_cost += np.sum(valContOpeVar['gasNGBoil'] * parNGBoil['weight_ng'])
        mtnTot += np.sum(heatNGBoil * parNGBoil['weight_mtn'])
        ng_month += np.squeeze(np.sum(
            (valContOpeVar['gasNGBoil'] * parWeight['weight_tot']).reshape(parTime['numMonth'], parTime['numDay'],
                                                                           parTime['numHour']), axis=(1, 2)))
        ng_month_cost += np.squeeze(np.sum(
            (valContOpeVar['gasNGBoil'] * parNGBoil['weight_ng']).reshape(parTime['numMonth'], parTime['numDay'],
                                                                          parTime['numHour']), axis=(1, 2)))

    if candidateTech['NGChill'] == 1 and valBinInst['NGChill'] == 1:
        capNGChill = valContInst['NGChill']
        print('the installation capacity of natural gas chiller is (kW): %i\n' % int(capNGChill))
        instSummary['capNGChill'] = capNGChill

        gasNGChill = valContOpeVar['gasNGChill']
        coolNGChill = valContOpeVar['coolNGChill']
        operSummary['gasNGChill'], operSummary['coolNGChill'] = gasNGChill, coolNGChill

        initInv += valContInst['NGChill'] * parNGChill['varIns'] + valBinInst['NGChill'] * parNGChill['fixIns']
        ngTot += np.sum(valContOpeVar['gasNGChill'] * parWeight['weight_tot'])
        ngTot_cost += np.sum(valContOpeVar['gasNGChill'] * parNGChill['weight_ng'])
        mtnTot += np.sum(coolNGChill * parNGChill['weight_mtn'])
        ng_month += np.squeeze(np.sum(
            (valContOpeVar['gasNGChill'] * parWeight['weight_tot']).reshape(parTime['numMonth'], parTime['numDay'],
                                                                            parTime['numHour']), axis=(1, 2)))
        ng_month_cost += np.squeeze(np.sum(
            (valContOpeVar['gasNGChill'] * parNGChill['weight_ng']).reshape(parTime['numMonth'], parTime['numDay'],
                                                                            parTime['numHour']), axis=(1, 2)))

    if candidateTech['CHP'] == 1:
        eleCHP = solContOpeVarCHP['eleCHP']
        gasCHP = solContOpeVarCHP['gasCHP']
        heatCHP = solContOpeVarCHP['heatCHP']
        opeStaCHP = solBinOpeVarCHP['CHP']
        print('the installation decision for CHP types:')
        print(valBinInstTypeCHP)
        totEleCHP = np.sum(eleCHP, 1)
        totGasCHP = np.sum(gasCHP, 1)
        totHeatCHP = np.sum(heatCHP, 1)
        gasCHP_all = np.sum(gasCHP, axis=1)
        totCHPcap = np.sum(valBinInstTypeCHP * parCHP['capGen'])
        instSummary['totCapCHP'] = totCHPcap

        initInv += np.sum(valBinInstTypeCHP * parCHP['capGen'] * parCHP['varInst']) + np.sum(
            valBinInstTypeCHP * parCHP['fixInst'])
        ngTot += np.sum(gasCHP_all * parWeight['weight_tot'])
        ngTot_cost += np.sum(gasCHP_all * parCHP['weight_cost'])
        mtnTot += np.sum(gasCHP_all * parCHP['weight_mtn'])

        ng_month += np.squeeze(np.sum(
            (gasCHP_all * parWeight['weight_tot']).reshape(parTime['numMonth'], parTime['numDay'], parTime['numHour']),
            axis=(1, 2)))
        ng_month_cost += np.squeeze(np.sum(
            (gasCHP_all * parCHP['weight_cost']).reshape(parTime['numMonth'], parTime['numDay'], parTime['numHour']),
            axis=(1, 2)))
        operSummary['totEleCHP'], operSummary['totGasCHP'], operSummary['totHeatCHP'] = totEleCHP, totGasCHP, totHeatCHP

        operSummary['eleCHP'], operSummary['gasCHP'], operSummary['heatCHP'], operSummary[
            'opeStaCHP'] = eleCHP, gasCHP, heatCHP, opeStaCHP


    if candidateTech['eleChill'] == 1 and valBinInst['eleChill'] == 1:
        coolEleChill = valContOpeVar['coolEleChill']
        eleEleChill = valContOpeVar['eleEleChill']
        capEleChill = valContInst['eleChill']
        logging.info('the installation capacity of electric chiller is (kW): %i\n' % int(capEleChill))
        instSummary['capEleChill'] = capEleChill
        operSummary['coolEleChill'], operSummary['eleEleChill'] = coolEleChill, eleEleChill

        initInv += valContInst['eleChill'] * parEleChill['varIns'] + valBinInst['eleChill'] * parEleChill['fixIns']
        mtnTot += np.sum(eleEleChill * parEleChill['weight_mtn'])

    if candidateTech['HP'] == 1 and valBinInst['HP'] == 1:
        capHP = valContInst['HP']
        print('the installation capacity of heat pump is (kW): %i\n' % int(capHP))
        instSummary['capHP'] = capHP

        initInv += valContInst['HP'] * parHP['varIns'] + valBinInst['HP'] * parHP['fixIns']
        eleHP = valContOpeVar['eleHP']
        mtnTot += np.sum(eleHP * parHP['weight_mtn'])
        operSummary['eleHP'] = eleHP

        if parHP['heatSta'] == 1 and len(nonzeroIndHeat):
            heatHP = valContOpeVar['heatHP']
            operSummary['heatHP'] = heatHP

        if parHP['coolSta'] == 1 and len(nonzeroIndCool):
            coolHP = valContOpeVar['coolHP']
            operSummary['coolHP'] = coolHP


    eleTot = 0
    if candidateTech['eleGrid'] == 1 and len(nonzeroIndEle):
        purEleGrid = valContOpeVar['purEleGrid']
        salEleGrid = valContOpeVar['salEleGrid']
        operSummary['purEleGrid'], operSummary['salEleGrid'] = purEleGrid, salEleGrid

        eleTot = np.sum(purEleGrid * parWeight['weight_tot'])
        eleTot_cost = np.sum(purEleGrid * parEleGrid['weight_cost'])
        ele_month += np.squeeze(np.sum(
            (purEleGrid * parWeight['weight_tot']).reshape(parTime['numMonth'], parTime['numDay'], parTime['numHour']),
            axis=(1, 2)))
        ele_month_cost += np.squeeze(np.sum(
            (purEleGrid * parEleGrid['weight_cost']).reshape(parTime['numMonth'], parTime['numDay'],
                                                             parTime['numHour']), axis=(1, 2)))
        ele_month_low += np.squeeze(np.sum(
            (purEleGrid * parEleGrid['weight_tot_low']).reshape(parTime['numMonth'], parTime['numDay'],
                                                                parTime['numHour']), axis=(1, 2)))
        ele_month_cost_low += np.squeeze(np.sum(
            (purEleGrid * parEleGrid['weight_cost_low']).reshape(parTime['numMonth'], parTime['numDay'],
                                                                 parTime['numHour']), axis=(1, 2)))
        ele_month_med += np.squeeze(np.sum(
            (purEleGrid * parEleGrid['weight_tot_med']).reshape(parTime['numMonth'], parTime['numDay'],
                                                                parTime['numHour']), axis=(1, 2)))
        ele_month_cost_med += np.squeeze(np.sum(
            (purEleGrid * parEleGrid['weight_cost_med']).reshape(parTime['numMonth'], parTime['numDay'],
                                                                 parTime['numHour']), axis=(1, 2)))
        ele_month_high += np.squeeze(np.sum(
            (purEleGrid * parEleGrid['weight_tot_high']).reshape(parTime['numMonth'], parTime['numDay'],
                                                                 parTime['numHour']), axis=(1, 2)))
        ele_month_cost_high += np.squeeze(np.sum(
            (purEleGrid * parEleGrid['weight_cost_high']).reshape(parTime['numMonth'], parTime['numDay'],
                                                                  parTime['numHour']), axis=(1, 2)))

    if candidateTech['heatSal'] == 1:
        heatSal = valContOpeVar['heatSal']
        operSummary['heatSal'] = heatSal


    carbHeat = 0
    if candidateTech['heatInds'] == 1:
        heatInds = valContOpeVar['heatInds']
        operSummary['heatInds'] = heatInds

        heatInds_month = np.squeeze(np.sum(
            (heatInds * parWeight['weight_tot']).reshape(parTime['numMonth'], parTime['numDay'], parTime['numHour']),
            axis=(1, 2)))
        optimSummary['heatInds_month'] = heatInds_month

        heatInds_cost_month = np.squeeze(np.sum(
            (heatInds * parHeat['weight_cost']).reshape(parTime['numMonth'], parTime['numDay'], parTime['numHour']),
            axis=(1, 2)))
        optimSummary['heatInds_cost_month'] = heatInds_cost_month

        heatInds_cost_tot = np.sum((heatInds * parHeat['weight_cost']))
        optimSummary['heatIndsTot_cost'] = heatInds_cost_tot

        heatInds_tot = np.sum((heatInds * parWeight['weight_tot']))
        optimSummary['heatindsTot'] = heatInds_tot

        carbHeat = np.sum(heatInds * parWeight['weight_tot']) * emiFac['heatInds']
        optimSummary['carbHeat'] = carbHeat


    if candidateTech['ABSChill'] == 1 and valBinInst['ABSChill'] == 1:
        coolABSChill = valContOpeVar['coolABSChill']
        heatABSChill = valContOpeVar['heatABSChill']
        capABSChill = valContInst['ABSChill']
        logging.info('The installation capacity of absorption chiller is (kW): %i\n' % int(capABSChill))
        instSummary['capABSChill'] = capABSChill

        operSummary['coolABSChill'], operSummary['heatABSChill'] = coolABSChill, heatABSChill

        initInv += valContInst['ABSChill'] * parABSChill['varIns'] + valBinInst['ABSChill'] * parABSChill['fixIns']
        mtnTot += np.sum(coolABSChill * parABSChill['weight_mtn'])

    # electric boiler
    if candidateTech['eleBoil'] == 1 and np.allclose(valBinInst['eleBoil'], 1):
        capEleBoil = valContInst['eleBoil']
        print('the installation capacity of electric boiler is (kW): %i\n' % int(capEleBoil))

        eleEleBoil = valContOpeVar['eleEleBoil']
        heatEleBoil = valContOpeVar['heatEleBoil']
        instSummary['capEleBoil'] = capEleBoil

        operSummary['eleEleBoil'], operSummary['heatEleBoil'] = eleEleBoil, heatEleBoil

        initInv += valContInst['eleBoil'] * parEleBoil['varIns'] + valBinInst['eleBoil'] * parEleBoil['fixIns']


    # 08/07/2015, get solution for max demand charge
    valMaxDemand = np.zeros((parTime['numMonth'], 1))
    ele_dc_month = np.zeros((parTime['numMonth'], 1))

    if candidateTech['demandCharge'] == 1:
        valMaxDemand = optVal[idxVarMaxDemand]
        ele_dc_month = np.squeeze(
            np.max((purEleGrid).reshape(parTime['numMonth'], parTime['numDay'], parTime['numHour']),
                   axis=(1, 2))) * rateMonthDemandCharge
        eleTot_cost += np.sum(ele_dc_month)  # added demand charge
    operSummary['valMaxDemand'] = valMaxDemand
    optimSummary['valMaxDemand_month'] = ele_dc_month


    if candidateTech['heatTank'] == 1 and valBinInst['heatTank'] == 1:
        capHeatTank = valContInst['heatTank']
        print('the installation capacity of heat tank is (kWh): %i\n' % int(capHeatTank))
        initInv += valContInst['heatTank'] * parHeatTank['varIns'] + valBinInst['heatTank'] * parHeatTank['fixIns']

        chgHeatTank = valBinOpeVar['chgHeatTank']
        disHeatTank = valBinOpeVar['disHeatTank']
        chgHeatTank = map(int, chgHeatTank)
        disHeatTank = map(int, disHeatTank)

        fromHeatTank = valContOpeVar['fromHeatTank']
        stoHeatTank = valContOpeVar['stoHeatTank']
        totHeatTank = valContOpeVar['totHeatTank']
        inHeatTank = stoHeatTank * parHeatTank['chgEff']
        outHeatTank = fromHeatTank / float(parHeatTank['disEff'])
        socHeatTank = totHeatTank / valContInst['heatTank']
        mtnTot += np.sum(totHeatTank * parHeatTank['weight_mtn'])
        # 02/03/2016
        # charge velocity
        preTotHeatTank = np.hstack((capHeatTank * parHeatTank['iniSoc'], totHeatTank[:parTime['totHrStep'] - 1]))
        fracChgHeatTank = inHeatTank * parTime['stepHr'] / (capHeatTank - preTotHeatTank)
        # discharge velocity
        fracDisHeatTank = outHeatTank * parTime['stepHr'] / (preTotHeatTank)
        operSummary['fracChgHeatTank'], operSummary['fracDisHeatTank'] = fracChgHeatTank, fracDisHeatTank

        instSummary['capHeatTank'] = capHeatTank

        operSummary['chgHeatTank'], operSummary['disHeatTank'] = chgHeatTank, disHeatTank

        operSummary['fromHeatTank'], operSummary['stoHeatTank'] = fromHeatTank, stoHeatTank

        operSummary['totHeatTank'], operSummary['inHeatTank'] = totHeatTank, inHeatTank

        operSummary['outHeatTank'], operSummary['socHeatTank'] = outHeatTank, socHeatTank


    if candidateTech['eleBat'] == 1 and valBinInst['eleBat'] == 1:
        capEleBat = valContInst['eleBat']
        print('the installation capacity of electric battery is (kWh): %i\n' % int(capEleBat))
        instSummary['capEleBat'] = capEleBat

        initInv += valContInst['eleBat'] * parEleBat['varIns'] + valBinInst['eleBat'] * parEleBat['fixIns']

        chgEleBat = valBinOpeVar['chgEleBat']
        disEleBat = valBinOpeVar['disEleBat']

        chgEleBat = map(int, chgEleBat)
        disEleBat = map(int, disEleBat)

        fromEleBat = valContOpeVar['fromEleBat']
        stoEleBat = valContOpeVar['stoEleBat']
        totEleBat = valContOpeVar['totEleBat']
        inEleBat = stoEleBat * parEleBat['chgEff']
        outEleBat = fromEleBat / float(parEleBat['disEff'])
        socEleBat = totEleBat / valContInst['eleBat']
        mtnTot += np.sum(totEleBat * parEleBat['weight_mtn'])
        # 02/03/2016
        # charge velocity
        preTotEleBat = np.hstack((capEleBat * parEleBat['iniSoc'], totEleBat[:parTime['totHrStep'] - 1]))
        fracChgEleBat = inEleBat * parTime['stepHr'] / (capEleBat - preTotEleBat)
        # discharge velocity
        fracDisEleBat = outEleBat * parTime['stepHr'] / (preTotEleBat)
        operSummary['fracChgEleBat'], operSummary['fracDisEleBat'] = fracChgEleBat, fracDisEleBat

        operSummary['chgEleBat'], operSummary['disEleBat'] = chgEleBat, disEleBat

        operSummary['fromEleBat'], operSummary['stoEleBat'] = fromEleBat, stoEleBat

        operSummary['totEleBat'], operSummary['inEleBat'] = totEleBat, inEleBat

        operSummary['outEleBat'], operSummary['socEleBat'] = outEleBat, socEleBat


    if candidateTech['coolTank'] == 1 and valBinInst['coolTank'] == 1:
        capCoolTank = valContInst['coolTank']
        print('the installation capacity of cool tank is (kWh): %i\n' % int(capCoolTank))
        instSummary['capCoolTank'] = capCoolTank

        initInv += valContInst['coolTank'] * parCoolTank['varIns'] + valBinInst['coolTank'] * parCoolTank['fixIns']

        chgCoolTank = valBinOpeVar['chgCoolTank']
        disCoolTank = valBinOpeVar['disCoolTank']
        chgCoolTank = map(int, chgCoolTank)
        disCoolTank = map(int, disCoolTank)

        fromCoolTank = valContOpeVar['fromCoolTank']
        stoCoolTank = valContOpeVar['stoCoolTank']
        totCoolTank = valContOpeVar['totCoolTank']
        inCoolTank = stoCoolTank * parCoolTank['chgEff']
        outCoolTank = fromCoolTank / float(parCoolTank['disEff'])
        socCoolTank = totCoolTank / valContInst['coolTank']
        mtnTot += np.sum(totCoolTank * parCoolTank['weight_mtn'])

        # 02/03/2016
        # charge velocity
        preTotCoolTank = np.hstack((capCoolTank * parCoolTank['iniSoc'], totCoolTank[:parTime['totHrStep'] - 1]))
        fracChgCoolTank = inCoolTank * parTime['stepHr'] / (capCoolTank - preTotCoolTank)
        # discharge velocity
        fracDisCoolTank = outCoolTank * parTime['stepHr'] / (preTotCoolTank)
        operSummary['fracChgCoolTank'], operSummary['fracDisCoolTank'] = fracChgCoolTank, fracDisCoolTank

        operSummary['chgCoolTank'], operSummary['disCoolTank'] = chgCoolTank, disCoolTank

        operSummary['fromCoolTank'], operSummary['stoCoolTank'] = fromCoolTank, stoCoolTank

        operSummary['totCoolTank'], operSummary['inCoolTank'] = totCoolTank, inCoolTank

        operSummary['outCoolTank'], operSummary['socCoolTank'] = outCoolTank, socCoolTank

    print("results are successfully organized!\n")

    print("the installation decision for candidate technologies is \n")
    for name, contents in valBinInst.items():
        print(name, contents)

    # CO2 emission (kgCO2/kWh)
    # how about CO2 emission from electricity storage
    if emiFac is None:
        raise Exception("emiFac can not None.")

    # CO2 emission from power grid
    carbEle = 0
    if candidateTech['eleGrid'] == 1:
        carbEle = np.sum(valContOpeVar['purEleGrid'] * parTime['stepHr'] * parEleGrid['weight_tot']) * emiFac['ele']
        optimSummary['carbEle'] = carbEle
    # CO2 emission from natural gas boiler
    carbNGBoil = 0
    if candidateTech['NGBoil'] == 1:
        carbNGBoil = np.sum(valContOpeVar['gasNGBoil'] * parTime['stepHr'] * parWeight['weight_tot']) * emiFac['natGas']
        optimSummary['carbNGBoil'] = carbNGBoil

    # CO2 emission from natural gas chiller
    carbNGChill = 0
    if candidateTech['NGChill'] == 1:
        carbNGChill = np.sum(valContOpeVar['gasNGChill'] * parTime['stepHr'] * parWeight['weight_tot']) * emiFac[
            'natGas']
        optimSummary['carbNGChill'] = carbNGChill

    # CO2 emission from CHP
    carbCHP = 0
    if candidateTech['CHP'] == 1:
        carbCHP = np.sum(gasCHP_all * parTime['stepHr'] * parWeight['weight_tot']) * emiFac['natGas']
        optimSummary['carbCHP'] = carbCHP

    # CO2 emission from PV system, kg/m^2
    carbPV = 0
    if candidateTech['PV'] == 1:
        carbPV = emiFac['PV'] * valBinInst['PV']
        # carbPV = np.sum(valContOpeVar['elePV'] * parTime['stepHr']) * emiFac['PV']
        optimSummary['carbPV'] = carbPV

    # CO2 emission from solar thermal system
    carbSolThem = 0
    if candidateTech['solThem'] == 1:
        # carbSolThem = np.sum(valContOpeVar['heatSolThem'] * parTime['stepHr']) * emiFac['solThem']
        carbSolThem = emiFac['solThem'] * valBinInst['solThem']
        optimSummary['carbSolThem'] = carbSolThem

    # 08/11/2015, CO2 emission from heating tank
    carbHeatTank = 0
    if candidateTech['heatTank'] == 1:
        carbHeatTank = emiFac['heatTank'] * valBinInst['heatTank']
        optimSummary['carbHeatTank'] = carbHeatTank

    # 08/11/2015, CO2 emission from cooling tank
    carbCoolTank = 0
    if candidateTech['coolTank'] == 1:
        carbCoolTank = emiFac['coolTank'] * valBinInst['coolTank']
        optimSummary['carbCoolTank'] = carbCoolTank

    optimSummary['NGtot'], optimSummary['NGtot_cost'] = ngTot, ngTot_cost

    optimSummary['mtn_cost'], optimSummary['initInv'] = mtnTot, initInv

    optimSummary['eleTotGrid'], optimSummary['eleTotGrid_cost'] = eleTot, eleTot_cost

    optimSummary['ele_monthly'], optimSummary['ele_monthly_cost'] = ele_month, ele_month_cost

    optimSummary['ele_monthly_low'], optimSummary['ele_monthly_cost_low'] = ele_month_low, ele_month_cost_low

    optimSummary['ele_monthly_med'], optimSummary['ele_monthly_cost_med'] = ele_month_med, ele_month_cost_med

    optimSummary['ele_monthly_high'], optimSummary['ele_monthly_cost_high'] = ele_month_high, ele_month_cost_high

    optimSummary['ng_monthly'], optimSummary['ng_monthly_cost'] = ng_month, ng_month_cost

    optimSummary['totCarbEmi'] = carbEle + carbNGBoil + carbNGChill + carbCHP + carbPV + carbSolThem + carbHeatTank +\
                                carbCoolTank + carbHeat
    optimSummary['totCarbEmi_ng'] = carbNGBoil + carbNGChill + carbCHP
    optimSummary['totCarbEmi_ele'] = carbEle

    return operSummary, instSummary, optimSummary, success_flag
