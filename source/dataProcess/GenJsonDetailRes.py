import  json
import  random
import  numpy as np
import  os

def get_resouces_cons(gas_path,ele_path,boil_path,output_file):
    gas_cons=[]
    ele_cons = []
    boil_cons = []

    with open(gas_path,'rU') as f:
        for line in f.readlines():
            gas_cons.append(round(float(line.strip()),2))

    with open(ele_path,'rU') as f:
        for line in f.readlines():
            ele_cons.append(round(float(line.strip()),2))

    with open(boil_path,'rU') as f:
        for line in f.readlines():
            boil_cons.append(round(random.uniform(0,2)*float(line.strip()),2))

    result = {}
    result['x_data'] = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    result['gas_cons']= gas_cons
    result['ele_cons'] = ele_cons
    result['boil_cons'] = boil_cons

    with open(output_file,'w+') as f:
        json.dump(result,f)

def get_running_costs(gas_path,ele_high_path,ele_low_path,ele_med_path,max_demand_path,heat_inds_path,output_file):
    gas__cost = []
    ele_high_cost = []
    ele_low_cost = []
    ele_med_cost = []
    max_demand_cost = []
    heat_inds_cost = []

    if os.path.isfile(gas_path):
        with open(gas_path, 'rU') as f:
            for line in f.readlines():
                gas__cost.append(round(float(line.strip()), 2))

    if os.path.isfile(ele_high_path):
        with open(ele_high_path, 'rU') as f:
            for line in f.readlines():
                ele_high_cost.append(round(float(line.strip()),2))

    if os.path.isfile(ele_low_path):
        with open(ele_low_path, 'rU') as f:
            for line in f.readlines():
                ele_low_cost.append(round(float(line.strip()),2))

    if os.path.isfile(ele_med_path):
        with open(ele_med_path, 'rU') as f:
            for line in f.readlines():
                ele_med_cost.append(round(float(line.strip()),2))

    if os.path.isfile(max_demand_path):
        with open(max_demand_path, 'rU') as f:
            for line in f.readlines():
                max_demand_cost.append(round(float(line.strip()),2))

    if os.path.isfile(heat_inds_path):
        with open(heat_inds_path, 'rU') as f:
            for line in f.readlines():
                heat_inds_cost.append(round(float(line.strip()),2))

    result = {}
    legend = []
    result['x_data'] = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    y_data=[]
    if len(gas__cost) == 12:
        result['gas_cost'] = gas__cost
        legend.append("NG")
        y_data.append(gas__cost)
    if len(ele_high_cost) == 12:
        result['ele_high_cost'] = ele_high_cost
        legend.append("Peak electricity")
        y_data.append(ele_high_cost)
    if len(ele_low_cost) == 12:
        result['ele_low_cost'] = ele_low_cost
        legend.append("Valley electricity")
        y_data.append(ele_low_cost)
    if len(ele_med_cost) == 12:
        result['ele_med_cost'] = ele_med_cost
        legend.append("Other electricity")
        y_data.append(ele_med_cost)
    if len(max_demand_cost) == 12:
        result['max_demand_cost'] = max_demand_cost
        legend.append("Demand charge")
        y_data.append(max_demand_cost)
    if len(heat_inds_cost) == 12:
        result['heat_inds_cost'] = heat_inds_cost
        legend.append("Heat")
        y_data.append(heat_inds_cost)

    result['legend'] = legend
    result['y_data'] = y_data
    with open(output_file,'w+') as f:
        json.dump(result,f)







def get_running_data(ele_load_path,heat_load_path,cool_load_path,ele_load_boil_path,ele_load_chill_path,ele_load_hp_path,\
                     ele_load_sto_bat_path,ele_load_sal_grid_path,ele_load_pv_path,ele_load_chp_path,ele_load_from_ele_bat_path,\
                     ele_load_pur_ele_grid_path,\
                     heat_load_sto_heat_tank_path,heat_load_heat_sal_path,heat_load_heat_ngboil_path,heat_load_heat_eleboil_path,\
                     heat_load_to_heat_chp_path,heat_load_heat_solthem_path,heat_load_from_heat_tank_path,heat_load_heat_inds_path,\
                     cool_load_sto_cool_tank_path,cool_load_cool_hp_path,cool_load_abschill_path,cool_load_elechill_path,cool_load_ngchill_path,\
                     cool_load_from_cool_tank_path,output_file):
    ele_load=[] #电负荷
    heat_load = [] #热负荷
    cool_load = []  #冷负荷
    with open(ele_load_path,'rU') as f:
        for line in f.readlines():
            ele_load.append(round(float(line.strip()),2))

    with open(heat_load_path,'rU') as f:
        for line in f.readlines():
            heat_load.append(round(float(line.strip()),2))

    with open(cool_load_path,'rU') as f:
        for line in f.readlines():
            cool_load.append(round(float(line.strip()),2))

    ele_load_boil = [] #电力需求 锅炉耗电
    ele_load_chill = [] #电力需求 冷机耗电
    ele_load_hp = [] # 电力需求 热泵耗电
    ele_load_sto_bat = [] #电力需求 电池储电
    ele_load_sal_grid = [] #电力需求 向电网卖电
    ele_load_pv = [] #电力供给 光伏发电
    ele_load_chp = [] #电力供给 CHP发电
    ele_load_from_ele_bat = [] #电力供给 电池放电
    ele_load_pur_ele_grid = [] #电力供给 电网供电
    with open(ele_load_boil_path,'rU') as f:
        for line in f.readlines():
            ele_load_boil.append(round(float(line.strip()),2))
    with open(ele_load_chill_path,'rU') as f:
        for line in f.readlines():
            ele_load_chill.append(round(float(line.strip()),2))
    with open(ele_load_hp_path,'rU') as f:
        for line in f.readlines():
            ele_load_hp.append(round(float(line.strip()),2))

    with open(ele_load_sto_bat_path,'rU') as f:
        for line in f.readlines():
            ele_load_sto_bat.append(round(float(line.strip()),2))
    with open(ele_load_sal_grid_path,'rU') as f:
        for line in f.readlines():
            ele_load_sal_grid.append(round(float(line.strip()),2))
    with open(ele_load_pv_path,'rU') as f:
        for line in f.readlines():
            ele_load_pv.append(round(float(line.strip()),2))
    with open(ele_load_chp_path,'rU') as f:
        for line in f.readlines():
            ele_load_chp.append(round(float(line.strip()),2))
    with open(ele_load_from_ele_bat_path,'rU') as f:
        for line in f.readlines():
            ele_load_from_ele_bat.append(round(float(line.strip()),2))
    with open(ele_load_pur_ele_grid_path,'rU') as f:
        for line in f.readlines():
            ele_load_pur_ele_grid.append(round(float(line.strip()),2))

    # heat_load_sto_heat_tank_path,heat_load_heat_sal_path,heat_load_heat_ngboil_path,heat_load_heat_eleboil_path,\
    # heat_load_to_heat_chp_path,heat_load_heat_solthem_path,heat_load_from_heat_tank_path,heat_load_heat_inds_path,\
    heat_load_sto_heat_tank = [] #热力需求 蓄热罐储热
    heat_load_heat_sal = [] #热力需求 卖给热网
    heat_load_heat_ngboil = [] #热力供给 燃气锅炉
    heat_load_heat_eleboil = [] #热力供给 电锅炉
    heat_load_to_heat_chp = []#热力供给 CHP
    heat_load_heat_solthem = [] #热力供给 太阳能热水
    heat_load_from_heat_tank = [] #热力供给 蓄热罐释放
    heat_load_heat_inds = [] #热网供热
    with open(heat_load_sto_heat_tank_path,'rU') as f:
        for line in f.readlines():
            heat_load_sto_heat_tank.append(round(float(line.strip()),2))

    with open(heat_load_heat_sal_path,'rU') as f:
        for line in f.readlines():
            heat_load_heat_sal.append(round(float(line.strip()),2))

    with open(heat_load_heat_ngboil_path,'rU') as f:
        for line in f.readlines():
            heat_load_heat_ngboil.append(round(float(line.strip()),2))

    with open(heat_load_heat_eleboil_path,'rU') as f:
        for line in f.readlines():
            heat_load_heat_eleboil.append(round(float(line.strip()),2))

    with open(heat_load_to_heat_chp_path,'rU') as f:
        for line in f.readlines():
            heat_load_to_heat_chp.append(round(float(line.strip()),2))

    with open(heat_load_heat_solthem_path,'rU') as f:
        for line in f.readlines():
            heat_load_heat_solthem.append(round(float(line.strip()),2))

    with open(heat_load_from_heat_tank_path,'rU') as f:
        for line in f.readlines():
            heat_load_from_heat_tank.append(round(float(line.strip()),2))

    with open(heat_load_heat_inds_path,'rU') as f:
        for line in f.readlines():
            heat_load_heat_inds.append(round(float(line.strip()),2))



    #cool_load_sto_cool_tank_path, cool_load_cool_hp_path, cool_load_abschill_path, cool_load_elechill_path, cool_load_ngchill_path, \
    #cool_load_from_cool_tank_path
    cool_load_sto_cool_tank = [] #冷量需求 蓄冷罐储冷
    cool_load_cool_hp = [] #冷量供给 热泵
    cool_load_abschill = [] #冷量供给 吸收式冷机
    cool_load_elechill = [] #冷量供给 电制冷机
    cool_load_ngchill = [] #冷量供给 天然气冷机
    cool_load_from_cool_tank = [] #冷量供给 冷罐释放
    with open(cool_load_sto_cool_tank_path,'rU') as f:
        for line in f.readlines():
            cool_load_sto_cool_tank.append(round(float(line.strip()),2))

    with open(cool_load_cool_hp_path,'rU') as f:
        for line in f.readlines():
            cool_load_cool_hp.append(round(float(line.strip()),2))

    with open(cool_load_abschill_path,'rU') as f:
        for line in f.readlines():
            cool_load_abschill.append(round(float(line.strip()),2))

    with open(cool_load_elechill_path,'rU') as f:
        for line in f.readlines():
            cool_load_elechill.append(round(float(line.strip()),2))

    with open(cool_load_ngchill_path,'rU') as f:
        for line in f.readlines():
            cool_load_ngchill.append(round(float(line.strip()),2))

    with open(cool_load_from_cool_tank_path,'rU') as f:
        for line in f.readlines():
            cool_load_from_cool_tank.append(round(float(line.strip()),2))

    x_data = []
    for i in range(1, 577):
        x_data.append(i)

    result = {}
    result['x_data'] = x_data
    result['ele_load_total'] = ele_load
    result['heat_load_total'] = heat_load
    result['cool_load_total'] = cool_load


    ele_load_value_supply={}
    ele_load_value_demand={}
    ele_load_value_demand['ele_ele_boil'] = ele_load_boil
    ele_load_value_demand['ele_ele_chill'] = ele_load_chill
    ele_load_value_demand['ele_hp'] = ele_load_hp
    ele_load_value_demand['sto_ele_bat'] = ele_load_sto_bat
    ele_load_value_demand['sal_ele_grid'] = ele_load_sal_grid
    ele_demand_sub_total_arr = np.array(ele_load_boil) + np.array(ele_load_chill) + np.array(ele_load_hp) + np.array(ele_load_sto_bat) + \
        np.array(ele_load_sal_grid)
    ele_load_value_demand['sub_total'] = [round(i,2) for i in ele_demand_sub_total_arr.tolist()]

    ele_load_value_supply['ele_pv'] = ele_load_pv
    ele_load_value_supply['tot_ele_chp'] = ele_load_chp
    ele_load_value_supply['from_ele_bat'] = ele_load_from_ele_bat
    ele_load_value_supply['pur_ele_grid'] = ele_load_pur_ele_grid
    ele_suppy_sub_total_arr = np.array(ele_load_pv) + np.array(ele_load_chp) + np.array(ele_load_from_ele_bat) + np.array(ele_load_pur_ele_grid)
    ele_load_value_supply['sub_total'] = [round(i,2) for i in ele_suppy_sub_total_arr.tolist()]

    result['ele_load_demand'] = ele_load_value_demand
    result['ele_load_supply'] = ele_load_value_supply

    heat_load_value_demand = {}
    heat_load_value_supply = {}
    heat_load_value_demand['sto_heat_tank'] = heat_load_sto_heat_tank
    heat_load_value_demand['heat_sal'] = heat_load_heat_sal
    heat_demand_sub_total_arr = np.array(heat_load_sto_heat_tank) + np.array(heat_load_heat_sal)
    heat_load_value_demand['sub_total'] = [round(i,2) for i in heat_demand_sub_total_arr.tolist()]

    heat_load_value_supply['heat_ng_boil'] = heat_load_heat_ngboil
    heat_load_value_supply['heat_ele_boil'] = heat_load_heat_eleboil
    heat_load_value_supply['tot_heat_chp'] = heat_load_to_heat_chp
    heat_load_value_supply['heat_sol_them'] = heat_load_heat_solthem
    heat_load_value_supply['from_heat_tank'] = heat_load_from_heat_tank
    heat_load_value_supply['heat_load_heat_inds'] = heat_load_heat_inds
    heat_supply_sub_total_arr = np.array(heat_load_heat_ngboil) + np.array(heat_load_heat_eleboil) + np.array(heat_load_to_heat_chp)+\
        np.array(heat_load_heat_solthem) + np.array(heat_load_from_heat_tank)+np.array(heat_load_heat_inds)
    heat_load_value_supply['sub_total'] = [round(i,2) for i in heat_supply_sub_total_arr.tolist()]
    result['heat_load_demand'] = heat_load_value_demand
    result['heat_load_supply'] = heat_load_value_supply


    cool_load_value_demand = {}
    cool_load_value_supply = {}
    cool_load_value_demand['sto_cool_tank'] = cool_load_sto_cool_tank
    cool_load_value_demand['sub_total'] = [round(i,2) for i in cool_load_sto_cool_tank]

    cool_load_value_supply['cool_hp'] = cool_load_cool_hp
    cool_load_value_supply['cool_abs_chill'] = cool_load_abschill
    cool_load_value_supply['cool_ele_chill'] = cool_load_elechill
    cool_load_value_supply['cool_ng_chill'] = cool_load_ngchill
    cool_load_value_supply['from_cool_tank'] = cool_load_from_cool_tank
    cool_supply_sub_total_arr = np.array(cool_load_cool_hp) + np.array(cool_load_abschill) + np.array(cool_load_elechill) \
                                + np.array(cool_load_ngchill) + np.array(cool_load_from_cool_tank)
    cool_load_value_supply['sub_total'] = [round(i,2) for i in cool_supply_sub_total_arr.tolist()]

    result['cool_load_demand'] = cool_load_value_demand
    result['cool_load_supply'] = cool_load_value_supply

    with open(output_file, 'w+') as f:
        json.dump(result, f)




def get_running_data2(ele_load_path,heat_load_path,cool_load_path,ele_load_boil_path,ele_load_chill_path,ele_load_hp_path,\
                     ele_load_sto_bat_path,ele_load_sal_grid_path,ele_load_pv_path,ele_load_chp_path,ele_load_from_ele_bat_path,\
                     ele_load_pur_ele_grid_path,\
                     heat_load_sto_heat_tank_path,heat_load_heat_sal_path,heat_load_heat_ngboil_path,heat_load_heat_eleboil_path,\
                     heat_load_to_heat_chp_path,heat_load_heat_solthem_path,heat_load_from_heat_tank_path,heat_load_heat_inds_path,\
                     cool_load_sto_cool_tank_path,cool_load_cool_hp_path,cool_load_abschill_path,cool_load_elechill_path,cool_load_ngchill_path,\
                     cool_load_from_cool_tank_path,output_file):
    ele_load=[] #电负荷
    heat_load = [] #热负荷
    cool_load = []  #冷负荷
    with open(ele_load_path,'rU') as f:
        for line in f.readlines():
            ele_load.append(round(float(line.strip()),2))

    with open(heat_load_path,'rU') as f:
        for line in f.readlines():
            heat_load.append(round(float(line.strip()),2))

    with open(cool_load_path,'rU') as f:
        for line in f.readlines():
            cool_load.append(round(float(line.strip()),2))

    ele_load_boil = [] #电力需求 锅炉耗电
    ele_load_chill = [] #电力需求 冷机耗电
    ele_load_hp = [] # 电力需求 热泵耗电
    ele_load_sto_bat = [] #电力需求 电池储电
    ele_load_sal_grid = [] #电力需求 向电网卖电
    ele_load_pv = [] #电力供给 光伏发电
    ele_load_chp = [] #电力供给 CHP发电
    ele_load_from_ele_bat = [] #电力供给 电池放电
    ele_load_pur_ele_grid = [] #电力供给 电网供电
    with open(ele_load_boil_path,'rU') as f:
        for line in f.readlines():
            ele_load_boil.append(round(float(line.strip()),2))
    with open(ele_load_chill_path,'rU') as f:
        for line in f.readlines():
            ele_load_chill.append(round(float(line.strip()),2))
    with open(ele_load_hp_path,'rU') as f:
        for line in f.readlines():
            ele_load_hp.append(round(float(line.strip()),2))

    with open(ele_load_sto_bat_path,'rU') as f:
        for line in f.readlines():
            ele_load_sto_bat.append(round(float(line.strip()),2))
    with open(ele_load_sal_grid_path,'rU') as f:
        for line in f.readlines():
            ele_load_sal_grid.append(round(float(line.strip()),2))
    with open(ele_load_pv_path,'rU') as f:
        for line in f.readlines():
            ele_load_pv.append(round(float(line.strip()),2))
    with open(ele_load_chp_path,'rU') as f:
        for line in f.readlines():
            ele_load_chp.append(round(float(line.strip()),2))
    with open(ele_load_from_ele_bat_path,'rU') as f:
        for line in f.readlines():
            ele_load_from_ele_bat.append(round(float(line.strip()),2))
    with open(ele_load_pur_ele_grid_path,'rU') as f:
        for line in f.readlines():
            ele_load_pur_ele_grid.append(round(float(line.strip()),2))

    # heat_load_sto_heat_tank_path,heat_load_heat_sal_path,heat_load_heat_ngboil_path,heat_load_heat_eleboil_path,\
    # heat_load_to_heat_chp_path,heat_load_heat_solthem_path,heat_load_from_heat_tank_path,heat_load_heat_inds_path,\
    heat_load_sto_heat_tank = [] #热力需求 蓄热罐储热
    heat_load_heat_sal = [] #热力需求 卖给热网
    heat_load_heat_ngboil = [] #热力供给 燃气锅炉
    heat_load_heat_eleboil = [] #热力供给 电锅炉
    heat_load_to_heat_chp = []#热力供给 CHP
    heat_load_heat_solthem = [] #热力供给 太阳能热水
    heat_load_from_heat_tank = [] #热力供给 蓄热罐释放
    heat_load_heat_inds = [] #热网供热
    with open(heat_load_sto_heat_tank_path,'rU') as f:
        for line in f.readlines():
            heat_load_sto_heat_tank.append(round(float(line.strip()),2))

    with open(heat_load_heat_sal_path,'rU') as f:
        for line in f.readlines():
            heat_load_heat_sal.append(round(float(line.strip()),2))

    with open(heat_load_heat_ngboil_path,'rU') as f:
        for line in f.readlines():
            heat_load_heat_ngboil.append(round(float(line.strip()),2))

    with open(heat_load_heat_eleboil_path,'rU') as f:
        for line in f.readlines():
            heat_load_heat_eleboil.append(round(float(line.strip()),2))

    with open(heat_load_to_heat_chp_path,'rU') as f:
        for line in f.readlines():
            heat_load_to_heat_chp.append(round(float(line.strip()),2))

    with open(heat_load_heat_solthem_path,'rU') as f:
        for line in f.readlines():
            heat_load_heat_solthem.append(round(float(line.strip()),2))

    with open(heat_load_from_heat_tank_path,'rU') as f:
        for line in f.readlines():
            heat_load_from_heat_tank.append(round(float(line.strip()),2))

    with open(heat_load_heat_inds_path,'rU') as f:
        for line in f.readlines():
            heat_load_heat_inds.append(round(float(line.strip()),2))



    #cool_load_sto_cool_tank_path, cool_load_cool_hp_path, cool_load_abschill_path, cool_load_elechill_path, cool_load_ngchill_path, \
    #cool_load_from_cool_tank_path
    cool_load_sto_cool_tank = [] #冷量需求 蓄冷罐储冷
    cool_load_cool_hp = [] #冷量供给 热泵
    cool_load_abschill = [] #冷量供给 吸收式冷机
    cool_load_elechill = [] #冷量供给 电制冷机
    cool_load_ngchill = [] #冷量供给 天然气冷机
    cool_load_from_cool_tank = [] #冷量供给 冷罐释放
    with open(cool_load_sto_cool_tank_path,'rU') as f:
        for line in f.readlines():
            cool_load_sto_cool_tank.append(round(float(line.strip()),2))

    with open(cool_load_cool_hp_path,'rU') as f:
        for line in f.readlines():
            cool_load_cool_hp.append(round(float(line.strip()),2))

    with open(cool_load_abschill_path,'rU') as f:
        for line in f.readlines():
            cool_load_abschill.append(round(float(line.strip()),2))

    with open(cool_load_elechill_path,'rU') as f:
        for line in f.readlines():
            cool_load_elechill.append(round(float(line.strip()),2))

    with open(cool_load_ngchill_path,'rU') as f:
        for line in f.readlines():
            cool_load_ngchill.append(round(float(line.strip()),2))

    with open(cool_load_from_cool_tank_path,'rU') as f:
        for line in f.readlines():
            cool_load_from_cool_tank.append(round(float(line.strip()),2))

    x_data = []
    for i in range(1, 577):
        x_data.append(i)

    result = {}
    result['x_data'] = x_data
    result['ele_load_total'] = ele_load
    result['heat_load_total'] = heat_load
    result['cool_load_total'] = cool_load


    ele_load_value_supply={}
    ele_load_value_demand={}
    ele_load_value_demand['ele_ele_boil'] = ele_load_boil
    ele_load_value_demand['ele_ele_chill'] = ele_load_chill
    ele_load_value_demand['ele_hp'] = ele_load_hp
    ele_load_value_demand['sto_ele_bat'] = ele_load_sto_bat
    ele_load_value_demand['sal_ele_grid'] = ele_load_sal_grid
    ele_demand_sub_total_arr = np.array(ele_load_boil) + np.array(ele_load_chill) + np.array(ele_load_hp) + np.array(ele_load_sto_bat) + \
        np.array(ele_load_sal_grid)
    ele_load_value_demand['sub_total'] = [round(i,2) for i in ele_demand_sub_total_arr.tolist()]

    ele_load_value_supply['ele_pv'] = (np.array(ele_load_pv)*(-1)).tolist()
    ele_load_value_supply['tot_ele_chp'] = (np.array(ele_load_chp)*(-1)).tolist()
    ele_load_value_supply['from_ele_bat'] = (np.array(ele_load_from_ele_bat)*(-1)).tolist()
    ele_load_value_supply['pur_ele_grid'] = (np.array(ele_load_pur_ele_grid)*(-1)).tolist()
    ele_suppy_sub_total_arr = np.array(ele_load_pv) + np.array(ele_load_chp) + np.array(ele_load_from_ele_bat) + np.array(ele_load_pur_ele_grid)
    ele_load_value_supply['sub_total'] = [round(i,2) for i in ele_suppy_sub_total_arr.tolist()]

    result['ele_load_demand'] = ele_load_value_demand
    result['ele_load_supply'] = ele_load_value_supply

    heat_load_value_demand = {}
    heat_load_value_supply = {}
    heat_load_value_demand['sto_heat_tank'] = heat_load_sto_heat_tank
    heat_load_value_demand['heat_sal'] = heat_load_heat_sal
    heat_demand_sub_total_arr = np.array(heat_load_sto_heat_tank) + np.array(heat_load_heat_sal)
    heat_load_value_demand['sub_total'] = [round(i,2) for i in heat_demand_sub_total_arr.tolist()]

    heat_load_value_supply['heat_ng_boil'] = heat_load_heat_ngboil
    heat_load_value_supply['heat_ele_boil'] = heat_load_heat_eleboil
    heat_load_value_supply['tot_heat_chp'] = heat_load_to_heat_chp
    heat_load_value_supply['heat_sol_them'] = heat_load_heat_solthem
    heat_load_value_supply['from_heat_tank'] = heat_load_from_heat_tank
    heat_load_value_supply['heat_load_heat_inds'] = heat_load_heat_inds
    heat_supply_sub_total_arr = np.array(heat_load_heat_ngboil) + np.array(heat_load_heat_eleboil) + np.array(heat_load_to_heat_chp)+\
        np.array(heat_load_heat_solthem) + np.array(heat_load_from_heat_tank)+np.array(heat_load_heat_inds)
    heat_load_value_supply['sub_total'] = [round(i,2) for i in heat_supply_sub_total_arr.tolist()]
    result['heat_load_demand'] = heat_load_value_demand
    result['heat_load_supply'] = heat_load_value_supply


    cool_load_value_demand = {}
    cool_load_value_supply = {}
    cool_load_value_demand['sto_cool_tank'] = cool_load_sto_cool_tank
    cool_load_value_demand['sub_total'] = [round(i,2) for i in cool_load_sto_cool_tank]

    cool_load_value_supply['cool_hp'] = cool_load_cool_hp
    cool_load_value_supply['cool_abs_chill'] = cool_load_abschill
    cool_load_value_supply['cool_ele_chill'] = cool_load_elechill
    cool_load_value_supply['cool_ng_chill'] = cool_load_ngchill
    cool_load_value_supply['from_cool_tank'] = cool_load_from_cool_tank
    cool_supply_sub_total_arr = np.array(cool_load_cool_hp) + np.array(cool_load_abschill) + np.array(cool_load_elechill) \
                                + np.array(cool_load_ngchill) + np.array(cool_load_from_cool_tank)
    cool_load_value_supply['sub_total'] = [round(i,2) for i in cool_supply_sub_total_arr.tolist()]

    result['cool_load_demand'] = cool_load_value_demand
    result['cool_load_supply'] = cool_load_value_supply

    with open(output_file, 'w+') as f:
        json.dump(result, f)



def get_running_data_ele(ele_load_ele_path, ele_load_boil_path,ele_load_chill_path,ele_load_hp_path,\
                     ele_load_sto_bat_path,ele_load_sal_grid_path,ele_load_pv_path,ele_load_chp_path,ele_load_from_ele_bat_path,\
                     ele_load_pur_ele_grid_path,output_file):

    """
    Extract the operation information about electricity load.
    """
    ele_load_ele = [] #电力需求 电负荷
    ele_load_boil = [] #电力需求 锅炉耗电
    ele_load_chill = [] #电力需求 冷机耗电
    ele_load_hp = [] # 电力需求 热泵耗电
    ele_load_sto_bat = [] #电力需求 电池储电
    ele_load_sal_grid = [] #电力需求 向电网卖电

    ele_load_pv = [] #电力供给 光伏发电
    ele_load_chp = [] #电力供给 CHP发电
    ele_load_from_ele_bat = [] #电力供给 电池放电
    ele_load_pur_ele_grid = [] #电力供给 电网供电

    legend = []
    ele_load = []
    category = []

    if os.path.isfile(ele_load_ele_path):
        with open(ele_load_ele_path,'rU') as f:
            for line in f.readlines():
                ele_load_ele.append(round(float(line.strip()),2))
        legend.append("Electric load")
        ele_load.append((np.array(ele_load_ele)*(-1)).tolist())
        category.append("demand")


    if os.path.isfile(ele_load_boil_path):
        with open(ele_load_boil_path,'rU') as f:
            for line in f.readlines():
                ele_load_boil.append(round(float(line.strip()),2))
        legend.append("Electric boiler")
        ele_load.append((np.array(ele_load_boil)*(-1)).tolist())
        category.append("demand")


    if os.path.isfile(ele_load_chill_path):
        with open(ele_load_chill_path,'rU') as f:
            for line in f.readlines():
                ele_load_chill.append(round(float(line.strip()),2))
        legend.append("Electric chiller")
        ele_load.append((np.array(ele_load_chill) * (-1)).tolist())
        category.append("demand")

    if os.path.isfile(ele_load_hp_path):
        with open(ele_load_hp_path,'rU') as f:
            for line in f.readlines():
                ele_load_hp.append(round(float(line.strip()),2))
        legend.append("Heat pump")
        ele_load.append((np.array(ele_load_hp) * (-1)).tolist())
        category.append("demand")

    if os.path.isfile(ele_load_sto_bat_path):
        with open(ele_load_sto_bat_path,'rU') as f:
            for line in f.readlines():
                ele_load_sto_bat.append(round(float(line.strip()),2))
        legend.append("Battery charge")
        ele_load.append((np.array(ele_load_sto_bat) * (-1)).tolist())
        category.append("demand")

    if os.path.isfile(ele_load_sal_grid_path):
        with open(ele_load_sal_grid_path,'rU') as f:
            for line in f.readlines():
                ele_load_sal_grid.append(round(float(line.strip()),2))
        legend.append("Grid export")
        ele_load.append((np.array(ele_load_sal_grid) * (-1)).tolist())
        category.append("demand")

    if os.path.isfile(ele_load_pv_path):
        with open(ele_load_pv_path,'rU') as f:
            for line in f.readlines():
                ele_load_pv.append(round(float(line.strip()),2))
        legend.append("PV")
        ele_load.append(ele_load_pv)
        category.append("supply")

    if os.path.isfile(ele_load_chp_path):
        with open(ele_load_chp_path,'rU') as f:
            for line in f.readlines():
                ele_load_chp.append(round(float(line.strip()),2))
        legend.append("CHP")
        ele_load.append(ele_load_chp)
        category.append("supply")

    if os.path.isfile(ele_load_from_ele_bat_path):
        with open(ele_load_from_ele_bat_path,'rU') as f:
            for line in f.readlines():
                ele_load_from_ele_bat.append(round(float(line.strip()),2))
        legend.append("Battery discharge")
        ele_load.append(ele_load_from_ele_bat)
        category.append("supply")

    if os.path.isfile(ele_load_pur_ele_grid_path):
        with open(ele_load_pur_ele_grid_path,'rU') as f:
            for line in f.readlines():
                ele_load_pur_ele_grid.append(round(float(line.strip()),2))
        legend.append("Grid import")
        ele_load.append(ele_load_pur_ele_grid)
        category.append("supply")



    x_data = []
    mon_num = {
        1: "Jan",
        2: "Feb",
        3: "Mar",
        4: "Apr",
        5: "May",
        6: "Jun",
        7: "Jul",
        8: "Aug",
        9: "Sep",
        10: "Oct",
        11: "Nov",
        12: "Dec"
    }
    for i in range(1, 13):  # month
        for j in range(1, 3):  # workday or weekend
            for k in range(1, 25):  # hours
                if k == 1:
                    if j == 1:
                        x_data.append(mon_num.get(i) + "-workday")
                    elif j == 2:
                        x_data.append(mon_num.get(i) + "-weekend")
                else:
                    x_data.append(k)

    result = {}
    result['x_data'] = x_data
    result['legend'] = legend
    result['ele_load'] = ele_load
    result['category'] = category

    with open(output_file, 'w+') as f:
        json.dump(result, f)



def get_running_data_heat(heat_load_heat_path, heat_load_sto_heat_tank_path,heat_load_heat_sal_path,heat_load_abs_chiller_path,heat_load_heat_ngboil_path,heat_load_heat_eleboil_path,\
                     heat_load_to_heat_chp_path,heat_load_heat_solthem_path,heat_load_from_heat_tank_path,heat_load_heat_inds_path,output_file):


    # heat_load_sto_heat_tank_path,heat_load_heat_sal_path,heat_load_heat_ngboil_path,heat_load_heat_eleboil_path,\
    # heat_load_to_heat_chp_path,heat_load_heat_solthem_path,heat_load_from_heat_tank_path,heat_load_heat_inds_path,\
    heat_load_heat = []  #热力需求 热负荷
    heat_load_sto_heat_tank = [] #热力需求 蓄热罐储热
    heat_load_heat_sal = [] #热力需求 卖给热网
    heat_load_abs_chiller=[]#热力需求  abs chiller

    heat_load_heat_ngboil = [] #热力供给 燃气锅炉
    heat_load_heat_eleboil = [] #热力供给 电锅炉
    heat_load_to_heat_chp = []#热力供给 CHP
    heat_load_heat_solthem = [] #热力供给 太阳能热水
    heat_load_from_heat_tank = [] #热力供给 蓄热罐释放
    heat_load_heat_inds = [] #热网供热

    legend = []
    category = []
    y_data = []

    if os.path.isfile(heat_load_heat_path):
        with open(heat_load_heat_path, 'rU') as f:
            for line in f.readlines():
                heat_load_heat.append(round(float(line.strip()), 2))
        legend.append("Heat load")
        category.append("demand")
        y_data.append((np.array(heat_load_heat) * (-1)).tolist())

    if os.path.isfile(heat_load_sto_heat_tank_path):
        with open(heat_load_sto_heat_tank_path,'rU') as f:
            for line in f.readlines():
                heat_load_sto_heat_tank.append(round(float(line.strip()),2))
        legend.append("Storage charge")
        category.append("demand")
        y_data.append((np.array(heat_load_sto_heat_tank)*(-1)).tolist())

    if os.path.isfile(heat_load_heat_sal_path):
        with open(heat_load_heat_sal_path,'rU') as f:
            for line in f.readlines():
                heat_load_heat_sal.append(round(float(line.strip()),2))
        legend.append("Heat export")
        category.append("demand")
        y_data.append((np.array(heat_load_heat_sal)*(-1)).tolist())

    if os.path.isfile(heat_load_abs_chiller_path):
        with open(heat_load_abs_chiller_path,'rU') as f:
            for line in f.readlines():
                heat_load_abs_chiller.append(round(float(line.strip()),2))
        legend.append("Absorption Chiller")
        category.append("demand")
        y_data.append((np.array(heat_load_abs_chiller)*(-1)).tolist())



    if os.path.isfile(heat_load_heat_ngboil_path):
        with open(heat_load_heat_ngboil_path,'rU') as f:
            for line in f.readlines():
                heat_load_heat_ngboil.append(round(float(line.strip()),2))
        legend.append("NG boiler")
        category.append("supply")
        y_data.append(heat_load_heat_ngboil)

    if os.path.isfile(heat_load_heat_eleboil_path):
        with open(heat_load_heat_eleboil_path,'rU') as f:
            for line in f.readlines():
                heat_load_heat_eleboil.append(round(float(line.strip()),2))
        legend.append("Electric boiler")
        category.append("supply")
        y_data.append(heat_load_heat_eleboil)

    if os.path.isfile(heat_load_to_heat_chp_path):
        with open(heat_load_to_heat_chp_path,'rU') as f:
            for line in f.readlines():
                heat_load_to_heat_chp.append(round(float(line.strip()),2))
        legend.append("CHP")
        category.append("supply")
        y_data.append(heat_load_to_heat_chp)

    if os.path.isfile(heat_load_heat_solthem_path):
        with open(heat_load_heat_solthem_path,'rU') as f:
            for line in f.readlines():
                heat_load_heat_solthem.append(round(float(line.strip()),2))
        legend.append("Solar thermal")
        category.append("supply")
        y_data.append(heat_load_heat_solthem)

    if os.path.isfile(heat_load_from_heat_tank_path):
        with open(heat_load_from_heat_tank_path,'rU') as f:
            for line in f.readlines():
                heat_load_from_heat_tank.append(round(float(line.strip()),2))
        legend.append("Storage discharge")
        category.append("supply")
        y_data.append(heat_load_from_heat_tank)

    if os.path.isfile(heat_load_heat_inds_path):
        with open(heat_load_heat_inds_path,'rU') as f:
            for line in f.readlines():
                heat_load_heat_inds.append(round(float(line.strip()),2))
        legend.append("Heat import")
        category.append("supply")
        y_data.append(heat_load_heat_inds)



    x_data = []
    mon_num = {
        1: "Jan",
        2: "Feb",
        3: "Mar",
        4: "Apr",
        5: "May",
        6: "Jun",
        7: "Jul",
        8: "Aug",
        9: "Sep",
        10: "Oct",
        11: "Nov",
        12: "Dec"
    }
    for i in range(1, 13):  # month
        for j in range(1, 3):  # workday or weekend
            for k in range(1, 25):  # hours
                if k == 1:
                    if j == 1:
                        x_data.append(mon_num.get(i) + "-workday")
                    elif j == 2:
                        x_data.append(mon_num.get(i) + "-weekend")
                else:
                    x_data.append(k)

    result = {}
    result['x_data'] = x_data
    result['y_data'] = y_data
    result['category'] = category
    result['legend'] = legend


    with open(output_file, 'w+') as f:
        json.dump(result, f)



def get_running_data_cool(cool_load_cool_path,cool_load_sto_cool_tank_path,cool_load_cool_hp_path,cool_load_abschill_path,cool_load_elechill_path,cool_load_ngchill_path,\
                     cool_load_from_cool_tank_path,output_file):




    #cool_load_sto_cool_tank_path, cool_load_cool_hp_path, cool_load_abschill_path, cool_load_elechill_path, cool_load_ngchill_path, \
    #cool_load_from_cool_tank_path
    cool_load_cool = [] #冷量需求 冷负荷
    cool_load_sto_cool_tank = [] #冷量需求 蓄冷罐储冷
    cool_load_cool_hp = [] #冷量供给 热泵
    cool_load_abschill = [] #冷量供给 吸收式冷机
    cool_load_elechill = [] #冷量供给 电制冷机
    cool_load_ngchill = [] #冷量供给 天然气冷机
    cool_load_from_cool_tank = [] #冷量供给 冷罐释放

    legend = []
    y_data = []
    category = []

    if os.path.isfile(cool_load_cool_path):
        with open(cool_load_cool_path, 'rU') as f:
            for line in f.readlines():
                cool_load_cool.append(round(float(line.strip()), 2))
        legend.append("Cooling load")
        category.append("demand")
        y_data.append((np.array(cool_load_cool)*(-1)).tolist())

    if os.path.isfile(cool_load_sto_cool_tank_path):
        with open(cool_load_sto_cool_tank_path,'rU') as f:
            for line in f.readlines():
                cool_load_sto_cool_tank.append(round(float(line.strip()),2))
        legend.append("Storage charge")
        category.append("demand")
        y_data.append(np.array((cool_load_sto_cool_tank)*(-1)).tolist())

    if os.path.isfile(cool_load_cool_hp_path):
        with open(cool_load_cool_hp_path,'rU') as f:
            for line in f.readlines():
                cool_load_cool_hp.append(round(float(line.strip()),2))
        legend.append("Heat pump")
        category.append("supply")
        y_data.append(cool_load_cool_hp)

    if os.path.isfile(cool_load_abschill_path):
        with open(cool_load_abschill_path,'rU') as f:
            for line in f.readlines():
                cool_load_abschill.append(round(float(line.strip()),2))
        legend.append("Absorption chiller")
        category.append("supply")
        y_data.append(cool_load_abschill)


    if os.path.isfile(cool_load_elechill_path):
        with open(cool_load_elechill_path,'rU') as f:
            for line in f.readlines():
                cool_load_elechill.append(round(float(line.strip()),2))
        legend.append("Electric chiller")
        category.append("supply")
        y_data.append(cool_load_elechill)

    if os.path.isfile(cool_load_ngchill_path):
        with open(cool_load_ngchill_path,'rU') as f:
            for line in f.readlines():
                cool_load_ngchill.append(round(float(line.strip()),2))
        legend.append("NG chiller")
        category.append("supply")
        y_data.append(cool_load_ngchill)


    if os.path.isfile(cool_load_from_cool_tank_path):
        with open(cool_load_from_cool_tank_path,'rU') as f:
            for line in f.readlines():
                cool_load_from_cool_tank.append(round(float(line.strip()),2))
        legend.append("Storage discharge")
        category.append("supply")
        y_data.append(cool_load_from_cool_tank)

    mon_num = {
        1: "Jan",
        2: "Feb",
        3: "Mar",
        4: "Apr",
        5: "May",
        6: "Jun",
        7: "Jul",
        8: "Aug",
        9: "Sep",
        10: "Oct",
        11: "Nov",
        12: "Dec"
    }

    x_data = []
    for i in range(1, 13):  # month
        for j in range(1, 3):  # workday or weekend
            for k in range(1, 25):  # hours
                if k == 1:
                    if j == 1:
                        x_data.append(mon_num.get(i) + "-workday")
                    elif j == 2:
                        x_data.append(mon_num.get(i) + "-weekend")
                else:
                    x_data.append(k)

    result = {}
    result['x_data'] = x_data
    result["y_data"] = y_data
    result["legend"] = legend
    result["category"] = category


    with open(output_file, 'w+') as f:
        json.dump(result, f)


def test_get_resource_cons():
    gas_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/ng_monthly.out"
    ele_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/ele_monthly.out"
    boil_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/ng_monthly.out"
    output_file = "./detailResult/detail_result_resource_cons.json"
    get_resouces_cons(gas_path,ele_path,boil_path,output_file)

def test_get_running_costs():
    gas_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/ng_monthly_cost.out"
    ele_high_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/ele_monthly_cost_high.out"
    ele_low_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/ele_monthly_cost_low.out"
    ele_med_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/ele_monthly_cost_med.out"
    max_demand_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/valMaxDemand_month.out"
    heat_inds_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/heatInds_cost_month.out"
    output_file = "./detailResult/detail_result_running_costs.json"

    get_running_costs(gas_path, ele_high_path, ele_low_path, ele_med_path, max_demand_path, heat_inds_path, output_file)

def test_get_running_data():
    ele_load_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Algorithm/eleLoad.out'
    heat_load_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Algorithm/heatLoad.out'
    cool_load_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Algorithm/coolLoad.out'

    ele_load_boil_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/eleEleBoil.out'
    ele_load_chill_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/eleEleChill.out'
    ele_load_hp_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/eleHP.out'
    ele_load_sto_bat_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/stoEleBat.out'
    ele_load_sal_grid_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/salEleGrid.out'
    ele_load_pv_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/elePV.out'
    ele_load_chp_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/totEleCHP.out'
    ele_load_from_ele_bat_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/fromEleBat.out'
    ele_load_pur_ele_grid_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/purEleGrid.out'

    heat_load_sto_heat_tank_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/stoHeatTank.out'
    heat_load_heat_sal_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/heatSal.out'
    heat_load_heat_ngboil_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/heatNGBoil.out'
    heat_load_heat_eleboil_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/heatEleBoil.out'
    heat_load_to_heat_chp_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/totHeatCHP.out'
    heat_load_heat_solthem_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/heatSolThem.out'
    heat_load_from_heat_tank_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/fromHeatTank.out'
    heat_load_heat_inds_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/heatInds.out'

    cool_load_sto_cool_tank_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/stoCoolTank.out'
    cool_load_cool_hp_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/coolHP.out'
    cool_load_abschill_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/coolABSChill.out'
    cool_load_elechill_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/coolEleChill.out'
    cool_load_ngchill_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/coolNGChill.out'
    cool_load_from_cool_tank_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/fromCoolTank.out'

    output_file = './detailResult/detail_result_running_data.json'


    get_running_data(ele_load_path, heat_load_path, cool_load_path, ele_load_boil_path, ele_load_chill_path,
                     ele_load_hp_path, \
                     ele_load_sto_bat_path, ele_load_sal_grid_path, ele_load_pv_path, ele_load_chp_path,
                     ele_load_from_ele_bat_path, \
                     ele_load_pur_ele_grid_path, \
                     heat_load_sto_heat_tank_path, heat_load_heat_sal_path, heat_load_heat_ngboil_path,
                     heat_load_heat_eleboil_path, \
                     heat_load_to_heat_chp_path, heat_load_heat_solthem_path, heat_load_from_heat_tank_path,
                     heat_load_heat_inds_path, \
                     cool_load_sto_cool_tank_path, cool_load_cool_hp_path, cool_load_abschill_path,
                     cool_load_elechill_path, cool_load_ngchill_path, \
                     cool_load_from_cool_tank_path, output_file)



def test_get_running_data2():
    ele_load_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Algorithm/eleLoad.out'
    heat_load_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Algorithm/heatLoad.out'
    cool_load_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Algorithm/coolLoad.out'

    ele_load_boil_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/eleEleBoil.out'
    ele_load_chill_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/eleEleChill.out'
    ele_load_hp_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/eleHP.out'
    ele_load_sto_bat_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/stoEleBat.out'
    ele_load_sal_grid_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/salEleGrid.out'
    ele_load_pv_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/elePV.out'
    ele_load_chp_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/totEleCHP.out'
    ele_load_from_ele_bat_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/fromEleBat.out'
    ele_load_pur_ele_grid_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/purEleGrid.out'

    heat_load_sto_heat_tank_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/stoHeatTank.out'
    heat_load_heat_sal_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/heatSal.out'
    heat_load_heat_ngboil_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/heatNGBoil.out'
    heat_load_heat_eleboil_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/heatEleBoil.out'
    heat_load_to_heat_chp_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/totHeatCHP.out'
    heat_load_heat_solthem_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/heatSolThem.out'
    heat_load_from_heat_tank_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/fromHeatTank.out'
    heat_load_heat_inds_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/heatInds.out'

    cool_load_sto_cool_tank_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/stoCoolTank.out'
    cool_load_cool_hp_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/coolHP.out'
    cool_load_abschill_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/coolABSChill.out'
    cool_load_elechill_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/coolEleChill.out'
    cool_load_ngchill_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/coolNGChill.out'
    cool_load_from_cool_tank_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/fromCoolTank.out'

    output_file = './detailResult/detail_result_running_data.json'


    get_running_data2(ele_load_path, heat_load_path, cool_load_path, ele_load_boil_path, ele_load_chill_path,
                     ele_load_hp_path, \
                     ele_load_sto_bat_path, ele_load_sal_grid_path, ele_load_pv_path, ele_load_chp_path,
                     ele_load_from_ele_bat_path, \
                     ele_load_pur_ele_grid_path, \
                     heat_load_sto_heat_tank_path, heat_load_heat_sal_path, heat_load_heat_ngboil_path,
                     heat_load_heat_eleboil_path, \
                     heat_load_to_heat_chp_path, heat_load_heat_solthem_path, heat_load_from_heat_tank_path,
                     heat_load_heat_inds_path, \
                     cool_load_sto_cool_tank_path, cool_load_cool_hp_path, cool_load_abschill_path,
                     cool_load_elechill_path, cool_load_ngchill_path, \
                     cool_load_from_cool_tank_path, output_file)

def test_get_running_data_ele():
    ele_load_ele_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/eleLoad.in'
    ele_load_boil_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/eleEleBoil.out'
    ele_load_chill_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/eleEleChill.out'
    ele_load_hp_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/eleHP.out'
    ele_load_sto_bat_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/stoEleBat.out'
    ele_load_sal_grid_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/salEleGrid.out'
    ele_load_pv_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/elePV.out'
    ele_load_chp_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/totEleCHP.out'
    ele_load_from_ele_bat_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/fromEleBat.out'
    ele_load_pur_ele_grid_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/purEleGrid.out'


    output_file = './detailResult/detail_result_running_data_ele.json'


    get_running_data_ele(ele_load_ele_path, ele_load_boil_path, ele_load_chill_path, ele_load_hp_path, \
                     ele_load_sto_bat_path, ele_load_sal_grid_path, ele_load_pv_path, ele_load_chp_path,
                     ele_load_from_ele_bat_path,  ele_load_pur_ele_grid_path , output_file)



def test_get_running_data_heat():
    heat_load_heat_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/heatLoad.in'
    heat_load_sto_heat_tank_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/stoHeatTank.out'
    heat_load_heat_sal_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/heatSal.out'
    heat_load_abs_chiller_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/heatABSChill.out'
    heat_load_heat_ngboil_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/heatNGBoil.out'
    heat_load_heat_eleboil_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/heatEleBoil.out'
    heat_load_to_heat_chp_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/totHeatCHP.out'
    heat_load_heat_solthem_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/heatSolThem.out'
    heat_load_from_heat_tank_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/fromHeatTank.out'
    heat_load_heat_inds_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/heatInds.out'

    output_file = './detailResult/detail_result_running_data_heat.json'


    get_running_data_heat(heat_load_heat_path,heat_load_sto_heat_tank_path, heat_load_heat_sal_path,heat_load_abs_chiller_path, heat_load_heat_ngboil_path, heat_load_heat_eleboil_path,heat_load_to_heat_chp_path, heat_load_heat_solthem_path, heat_load_from_heat_tank_path,
                     heat_load_heat_inds_path, output_file)

def test_get_running_data_cool():
    cool_load_cool_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/coolLoad.in'
    cool_load_sto_cool_tank_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/stoCoolTank.out'
    cool_load_cool_hp_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/coolHP.out'
    cool_load_abschill_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/coolABSChill.out'
    cool_load_elechill_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/coolEleChill.out'
    cool_load_ngchill_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/coolNGChill.out'
    cool_load_from_cool_tank_path = '/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/fromCoolTank.out'

    output_file = './detailResult/detail_result_running_data_cool.json'


    get_running_data_cool(cool_load_cool_path,cool_load_sto_cool_tank_path, cool_load_cool_hp_path, cool_load_abschill_path,
                     cool_load_elechill_path, cool_load_ngchill_path, \
                     cool_load_from_cool_tank_path, output_file)


if __name__ ==  '__main__':
    #test_get_resource_cons()
    #test_get_running_costs()
    #test_get_running_data2()
    test_get_running_data_ele()
    test_get_running_data_heat()
    test_get_running_data_cool()
    print('Over')