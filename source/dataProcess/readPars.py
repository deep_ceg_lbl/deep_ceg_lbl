# -*- coding: utf-8 -*-
# !/usr/bin/env python

""" Short description of this Python module.
Longer description of this module.
This program is for: .
"""

__authors__ = ["Qin Tian<qtian.whu@gmail.com>", "Ming Jin<jinming@berkeley.edu>", "Wei Feng<weifeng@lbl.gov>"]
__contact__ = "Wei Feng"
__copyright__ = "Copyright 2018, CHINA ENERGY GROUP@LBNL"
__date__ = "18-1-22"
__deprecated__ = False
__email__ = "weifeng@lbl.gov"
__license__ = "GPLv3"
__maintainer__ = "Qin Tian, Ming Jing"
__status__ = "Production"
__version__ = "2.0.0"

import csv
import os
import numpy as np
import numpy.matlib as npmtlb
import ast
import traceback


def readPars(currentPath=None):
    # Initialization
    # parameters/algorithm/default_vals.csv
    # bigM, smallM
    # 1.00E+06, 1.00E-06
    bigM, smallM = 10 ** 6, 10 ** (-6)

    # parameters/technology/itemParTime.csv
    # numDay, numMonth, stepHr, numHour, longIntr
    # 2, 12, 1, 24, 0.1
    itemParTime = {'numHour': 24,
                   'numDay': 2,
                   'numMonth': 12,
                   'stepHr': 1,
                   'longIntr': 0.1}

    itemParTime['totHrStep'] = itemParTime['numMonth'] * itemParTime['numDay'] * itemParTime['numHour']




    # parameters/algorithm/ctrStr.csv
    # plotFlag, mpsWriteFlag, sampleTimeFlag, sparseProbFlag, mpsGenMed, mySolverInterface, probReduceFlag, taxCO2Flag, mySolver, carbOnly
    # 0,           1        ,        1      ,       1       , gurobi   ,      gurobi      ,        1      ,       0   ,     mps , 0
    ctrStr = dict(probReduceFlag=1,
                  sparseProbFlag=1,
                  sampleTimeFlag=1,
                  taxCO2Flag=0,
                  carbOnly=0,
                  mySolverInterface='gurobi',
                  # 'Openopt(glpk, cplex, lpsolve), NEOS(cplex, cbc), cylp(cbc),cmd(cbc, cplex, scip, glpk),cplex(mps,matrix)'
                  mySolver='mps',  # 'cplex, glpk, lpsolve,scip,cbc'
                  mpsWriteFlag=1,
                  mpsGenMed='gurobi',  # cplex,Openopt
                  plotFlag=0,
                  )

    # parameters/technology/itemLenLifeYr.csv
    # 15,20,15,10,15,15,15,15,15,17,18,17
    itemLenLifeYr = [25, 20, 15, 10, 15, 15, 15, 15, 15, 17, 18, 17]

    # TODO: Merge the below dicts itemCandidateTech1 and itemCandidateTech2.
    # parameters/problem/itemCandidateTech1.csv
    # PV, solThem, NGBoil, NGChill, heatTank, eleBat, eleChill, HP, eleBoil, ABSChill, coolTank, wind
    # 1,     1   ,   1   ,    1   ,    0    ,   0   ,    1    ,  0,     1  ,    1    ,     0   ,   0
    itemCandidateTech1 = {
        'PV': 1,
        'solThem': 1,
        'NGBoil': 1,
        'NGChill': 1,
        'heatTank': 0,
        'eleBat': 0,
        'eleChill': 1,
        'HP': 0,
        'eleBoil': 1,
        'ABSChill': 1,
        'coolTank': 0,
        'wind': 0, }

    # parameters/problem/itemCandidateTech2.csv
    # CHP, eleGrid, heatInds, heatSal, demandCharge
    #  1 ,     1  ,    0    ,    0   ,      0
    itemCandidateTech2 = {
        'CHP': 1,
        'eleGrid': 1,
        'heatInds': 0,
        'heatSal': 0,
        'demandCharge': 0}

    # parameters/problem/forceSelect.csv
    # PV, solThem, NGBoil, NGChill, heatTank, eleBat, eleChill, HP, eleBoil, ABSChill, coolTank, wind
    # 1 ,    0   ,    0  ,    0   ,     0   ,    0  ,    0    ,  0,     0  ,     0   ,    0    ,   0
    forceSelect = {
        'PV': 1,
        'solThem': 0,
        'NGBoil': 0,
        'NGChill': 0,
        'heatTank': 0,
        'eleBat': 0,
        'eleChill': 0,
        'HP': 0,
        'eleBoil': 0,
        'ABSChill': 0,
        'coolTank': 0,
        'wind': 0,
    }

    # parameters/problem/basic.csv
    # location,  off_area,  com_area,  hotel_area,  res_area
    # 1       ,   [10000],   []   ,       [10000],   [10000]
    parBasic = {
        'location': 1,
        'off_area': [10000],
        'com_area': [],
        'res_area': [10000],
        'hotel_area': [10000]
    }

    totArea = np.sum(parBasic['off_area']) + np.sum(parBasic['com_area']) + np.sum(
                 parBasic['hotel_area']) + np.sum(parBasic['res_area'])



    rootDir = os.path.dirname(os.path.dirname(os.getcwd()))
    currentPath = rootDir + '/data/parameters/'
    currentPathData = rootDir + '/data/data/'


    #------------ irradiance data  -----------------#

    # TODO the code below will be deleted. also what is the difference bewteen daySolIrradiation and solIrradiation?
    daySolIrradiation = np.asarray(
        [0, 0, 0, 0, 0, 0, 0, 0.050, 0.120, 0.300, 0.400, 0.550, 0.600, 0.500, 0.400, 0.220, 0.100, 0, 0, 0, 0, 0, 0,
         0])

    solIrradiation = npmtlb.repmat(daySolIrradiation[0: itemParTime['numHour']], 1,
                                   itemParTime['numMonth'] * itemParTime['numDay'])
    # TODO: the above code will be deleted.

    try:
        location = ['shenzhen', 'beijing', 'shanghai'][int(parBasic['location'])]
        if itemParTime['numDay'] == 2:  # 576 case
            assert (itemParTime['numMonth'] == 12 and itemParTime['numHour'] == 24)
            loadPath = currentPathData + 'irr_data/' + location + '_576.txt'
            daySolIrradiation = np.loadtxt(loadPath)
            solIrradiation = np.loadtxt(loadPath)
        else:
            assert (itemParTime['numDay'] == 30 and itemParTime['numMonth'] == 12 and itemParTime['numHour'] == 24)
            loadPath = currentPathData + 'irr_data/' + location + '.txt'
            daySolIrradiation = np.loadtxt(loadPath)
            solIrradiation = np.loadtxt(loadPath)
        np.savetxt(currentPath + 'technology/daySolIrradiation.out', daySolIrradiation, delimiter=',')
        # print ('Read out Solar irradiation!')
        # print daySolIrradiation
    except:
        print('Did not read out solar irradiation data!')

    # -------------------------------------------------------------------------------------
    # -------------------------------------- PV--------------------------------------------

    # parameters/technology/itemParPV.csv
    # unitOut, minInstArea, solYield, varIns, maxInstArea, perfRatio, fixIns, unitMtnPrice
    # 0.15   ,     20     ,    0.15 ,  12000,     3920   ,    0.75  ,    0  ,       0

    # itemParPV:
    # varIns :capital cost of solar PV panels, $/kW
    # unitOutPV : rated electricity generation per unit area of PV panel system, kW/m^2
    # perfRatio : performance ratio / irradiation efficiency
    # solYield: solar panel yield
    # E_PV <= A_PV * perfRatio * solYield * unitOut

    itemParPV = dict(fixIns=0,  # $/kW
                     varIns=12000,  # $/kW
                     unitOut=0.15,  # TODO: kW/m^2, this needs to be confirmed
                     maxInstArea=3920,  # m^2
                     minInstArea=20,  # m^2
                     perfRatio=0.75,
                     solYield=0.15,
                     unitMtnPrice=0)

    itemParPV['solIrradiation'] = solIrradiation

    # TODO: an array(564,) or array(8760,) can be created directly here.
    itemParPV['mtnPricePV'] = (np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
                                       dtype=np.float32) * itemParPV['unitMtnPrice'])
    # TODO: what does the below code mean? why the factor here is 0.098?
    itemParPV['maxInstArea'] = np.min(np.array([totArea * 0.098, itemParPV['maxInstArea']]))

    # -----------------------------------------------------------------------------------------------------------
    # ------------------------------------Solar Thermal----------------------------------------------------------

    # parameters/technology/itemParSolThem
    # unitOut, minInstArea, varIns, irrEff , maxInstArea ,fixIns , unitMtnPrice
    #  0.1   ,   0.000001 ,  3200 ,  0.69  ,    6680     ,  6400 ,   0

    # itemParSolThem:
    # varIns : capital cost of solar thermal, $/kW
    # unitOutSolThem :rated electricity generation per unit area of PV panel system, kW/m^2
    # H_SolThem <= A_SolThem * irrEff * unitOut

    itemParSolThem = dict(fixIns=6400,  # $/kW
                          varIns=3200,  # $/kW
                          unitOut=0.1,  # kW/m^2
                          maxInstArea=6680,  # m^2
                          minInstArea=0.000001,  # m^2
                          irrEff=0.69,
                          unitMtnPrice=0)
    # TODO: a vector can be created directly instead of a high dimension array.
    itemParSolThem['mtnPriceSolThem'] = (np.ones((itemParTime['numMonth'], itemParTime['numDay'],
                                                  itemParTime['numHour']), dtype=np.float32) *
                                         itemParSolThem['unitMtnPrice'])

    itemParSolThem['solIrradiation'] = solIrradiation
    # TODO: why need to multiply 0.167 here?
    itemParSolThem['maxInstArea'] = np.min(np.array([totArea * 0.167, itemParSolThem['maxInstArea']]))

    # --------------------------------------------------------------------------------------------------
    # -----------------------------------------CHP------------------------------------------------------

    # parameters/technology/itemParCHP.csv
    # numInst,          eleEff,                    varInst  ,            capGen       ,     fixInst ,coolSta,
    # "[1,1,1,1]", "[0.4,0.45,0.5,0.25]", "[5000,8000,10000,3000]",  "[200,100,150,230]", "[0,0,0,0]",   1,

    # heatSta, eleSta,        parLoad,          heatEff,           lifeYear,     numInv,unitGasPriceCHP,unitMtnPriceCHP
    #   1,       1   , "[0.2,0.25,0.3,0.1]", "[1.2,1.45,1.5,1]", "[17,17,17,17]",   4  ,  0.3          ,     0

    # itemParCHP
    # define the number & capacity of discrete CHP technologies
    # typeCHP : number of types of CHP plant
    # capCostCHP : capital cost of CHP plant, $/kW
    # capGenCHP : rated capacity of CHP, kW
    # eleEffCHP : electrical efficiency of CHP
    # heatEffCHP :factor relating electricity generation to heat supply from CHP
    #     CHP efficiency data see http://www.epa.gov/chp/basic/methods.html
    #     'heatSta', 'coolSta', used to detect when CHP should be considered
    #     when CHP is considered for heating purpose, heatSta =1
    #     when CHP is considered for electricity purpose, eleSta =1
    #     when CHP is used for cooling purpose, providing heating for ABSChiller, coolSta = 1
    # H_SolThem <= A_SolThem * irrEff * unitOut

    itemParCHP = {'varInst': [5000,8000,10000,3000],  # $/kW
                  'fixInst': [0,0,0,0],  # $
                  'capGen': [200,100,150,230],  # kW
                  'eleEff': [0.4, 0.45, 0.5, 0.25],
                  'heatEff': [1.2, 1.45, 1.5, 1],
                  'parLoad': [0.2,0.25,0.3,0.1],
                  'lifeYear': [17,17,17,17],
                  'numInst': [1, 1, 1, 1],  # maximum number of installed CHP for each type
                  'numInv': 4,
                  'heatSta': 1,
                  'coolSta': 1,
                  'eleSta': 1,
                  'unitGasPriceCHP':0.3,
                  'unitMtnPriceCHP':0
    }

    # gasPriceCHP: $/kWh
    itemParCHP['gasPriceCHP'] = np.ones(
        (itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']), dtype=np.float32) *\
                                itemParCHP['unitGasPriceCHP']
    # $35/kWe - $55 /kWe, average $40/kWe
    itemParCHP['mtnPriceCHP'] = np.ones(
        (itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']), dtype=np.float32) * \
                                itemParCHP['unitMtnPriceCHP']

    # -----------------------------------------------------------------------------------------------------------
    # ---------------------------------------------------- Ele Boil----------------------------------------------

    # maxInstCap ,minInstCap, varIns, themEff, fixIns, maxTotGen , unitMtnPriceEleBoil
    # 100000000,  0.000001  ,   300 ,   0.9  ,   5000,  100000000,       0

    itemParEleBoil = dict(fixIns=5000,
                          varIns=300,
                          themEff=0.9,
                          maxInstCap=100000000,
                          minInstCap=0.000001,
                          maxTotGen=100000000,
                          unitMtnPriceEleBoil=0)

    # -----------------------------------------------------------------------------------------------------------
    # ---------------------------------------------NG Boil-------------------------------------------------------

    # maxInstCap,minInstCap,varIns,themEff,fixIns,maxTotGen,unitGasPriceNGBoil,unitMtnPriceNGBoil
    # 100000000 , 0.000001 ,  300 ,  0.82 , 5000 ,100000000,        0.3       ,        0

    itemParNGBoil = dict(fixIns=5000,
                         varIns=300,
                         themEff=0.82,
                         maxInstCap=100000000,
                         minInstCap=0.000001,
                         maxTotGen=100000000,
                         unitGasPriceNGBoil=0.3,
                         unitMtnPriceNGBoil=0)

    # TODO: convert gasPriceNGBoil and mtnPriceNGBoild to vector directly.
    itemParNGBoil['gasPriceNGBoil'] = (
            np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
                    dtype=np.float32) * itemParNGBoil['unitGasPriceNGBoil'])
    itemParNGBoil['mtnPriceNGBoil'] = (
            np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
                    dtype=np.float32) * itemParNGBoil['unitMtnPriceNGBoil'])

    # -------------------------------------------------------------------------------------------------
    # -------------------------------------- Heat Tank ------------------------------------------------

    # minFracDis,maxFracDis,unitOut,minSoc,maxFracChg,minFracChg,fixIns,varIns,decayFac,iniSoc,  minCap ,
    #   0.15    ,   0.25   ,    1  ,  0.15,    0.25  ,    0.1   ,  5000,   200,  0.01  ,  0.9 , 0.000001,

    # mode,        chgEff,  maxCap,  maxSoc, disEff, unitMtnPriceHeatTank
    # priceFollow, 0.9,    100000000,   1,    0.8  ,         0

    itemParHeatTank = dict(fixIns=5000,
                           varIns=200,
                           chgEff=0.9,
                           disEff=0.8,
                           decayFac=0.01,
                           iniSoc=0.9,
                           maxCap=100000000,  # m^3
                           minCap=0.000001,  # m^3
                           maxFracChg=0.25,
                           minFracChg=0.1,
                           maxFracDis=0.25,
                           minFracDis=0.15,
                           maxSoc=1,
                           minSoc=0.15,
                           unitOut=1,  # kWh/m^3
                           mode='priceFollow',  # cycleCharge
                           unitMtnPriceHeatTank=0
                           )
    itemParHeatTank['mtnPriceHeatTank'] = (
            np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
                    dtype=np.float32) * itemParHeatTank['unitMtnPriceHeatTank'])

    # ---------------------------------------------------------------------------------------------------
    # ------------------------------------ Cool Tank -----------------------------------------------------

    # minFracDis, maxFracDis, unitOut, minSoc , maxFracChg, minFracChg, fixIns, varIns, decayFac, iniSoc,
    # 0.05      ,    0.35   ,    1   ,   0.15 ,     0.25  ,    0.1    ,  5000 ,   200 ,   0.01  ,   0.9 ,

    # minCap  ,       mode , chgEff,   maxCap , maxSoc, disEff, unitMtnPriceCoolTank
    # 0.000001, priceFollow,  0.85 , 100000000,   1   ,   0.75,        0.32
    itemParCoolTank = dict(fixIns=5000,
                           varIns=200,
                           chgEff=0.85,
                           disEff=0.75,
                           decayFac=0.01,
                           iniSoc=0.9,
                           maxCap=100000000,  # kWh
                           minCap=0.000001,  # kWh
                           maxFracChg=0.25,
                           minFracChg=0.1,
                           maxFracDis=0.35,
                           minFracDis=0.05,
                           maxSoc=1,
                           minSoc=0.15,
                           unitOut=1,  # this parameter is used to convert capacity from non-kwh to kwh
                           mode='priceFollow',  # cycleCharge'
                           unitMtnPriceCoolTank=0.32
                           )

    itemParCoolTank['mtnPriceCoolTank'] = (
            np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
                    dtype=np.float32) * itemParCoolTank['unitMtnPriceCoolTank'])

    # ------------------------------------------------------------------------------------------------------
    # ----------------------------------------------- EleBat -----------------------------------------------

    # minFracDis,varIns, decayFac, minCap  , maxSoc, maxFracDis, iniSoc, unitOut, minSoc, maxFracChg,
    # 0.05      , 2000 ,  0.001  , 0.000001,  1    ,    0.35   ,   0.9 ,     1  ,    0.2,    0.37   ,

    # mode,       chgEff,  maxCap  , minFracChg, fixIns, disEff, unitMtnPriceEleBat
    # priceFollow, 0.9  , 100000000,    0.02   ,  10000,  0.91 ,        0

    itemParEleBat = dict(fixIns=10000,  # $
                         varIns=2000,  # $/kWh
                         chgEff=0.9,
                         disEff=0.91,
                         decayFac=0.001,
                         iniSoc=0.9,
                         maxFracChg=0.37,
                         minFracChg=0.02,
                         maxFracDis=0.35,
                         minFracDis=0.05,
                         maxSoc=1,
                         minSoc=0.2,
                         unitOut=1,
                         maxCap=100000000,  # kWh
                         minCap=0.000001,  # kWh
                         mode='priceFollow',  #cycleCharge'
                         unitMtnPriceEleBat=0
                         )

    itemParEleBat['mtnPriceEleBat'] = (
            np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
                    dtype=np.float32) * itemParEleBat['unitMtnPriceEleBat'])

    # --------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------HP-------------------------------------------------

    # heatSta, outType, maxHeat, maxEle , eleEff, fixIns,  minCool , minEle  , varIns, minHeat  , coolSta,
    #   1    ,    1   , 2916000,  900000,   0.9 ,  5000 ,  0.000001, 0.000001,  1500 ,  0.000001,     1  ,

    # minCap  ,  heatCOP,  maxCap,  coolCOP,  maxCool, unitMtnPriceHP
    # 0.000001,   3.24  ,    272 ,   3.82  ,  3838000,       0

    itemParHP = dict(fixIns=5000,
                     varIns=1500,
                     heatCOP=3.24,
                     coolCOP=3.82,
                     eleEff=0.9,
                     maxCap=272,  # kW
                     minCap=0.000001,
                     outType=1,  # could be '0,1,2', '1' means either 'coolSta' or 'heatSta' is 'ON';
                     #  '2' means both 'coolSta' and 'heatSta' are 'ON'
                     coolSta=1,  # 09/06/2015, determine the cooling status of HP
                     heatSta=1,  # determine the heating status of HP
                     maxHeat=2916000,  # added 08/12/2015, maximum heating generation from heat pump
                     minHeat=0.000001,  # minimum heating generation from heat pump
                     maxCool=3838000,  # maximum cooling generation from heat pump
                     minCool=0.000001,  # minimum cooling generation from heat pump
                     maxEle=900000,
                     # added 09/10/2015, maximum electricity consumption for heat pump,
                     # used for linearization of heat pump model,
                     minEle=0.000001,  # minimum electricity consumption for heat pump
                     unitMtnPriceHP=0
                     )

    itemParHP['mtnPriceHP'] = (np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
                                       dtype=np.float32) * itemParHP['unitMtnPriceHP'])
    itemParHP['maxCap'] = np.min(np.array([totArea * 0.0068, itemParHP['maxCap']]))

    # -------------------------------------------------------------------------------------------------------
    # -----------------------------------------NG Chill -----------------------------------------------------

    # minCap  , maxCap  , varIns, themEff, coolCOP, fixIns, unitGasPriceNGChill,unitMtnPriceNGChill
    # 0.000001,100000000,  3500 ,  0.52  ,  1.2   ,  5000 ,        0.3         ,       0

    itemParNGChill = dict(fixIns=5000,
                          varIns=3500,
                          themEff=0.52,
                          coolCOP=1.2,
                          maxCap=100000000,
                          minCap=0.000001,
                          unitGasPriceNGChill=0.3,
                          unitMtnPriceNGChill=0)

    itemParNGChill['gasPriceNGChill'] = (
            np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
                    dtype=np.float32) * itemParNGChill['unitGasPriceNGChill'])
    itemParNGChill['mtnPriceNGChill'] = (
            np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
                    dtype=np.float32) * itemParNGChill['unitMtnPriceNGChill'])

    # ---------------------------------------------------------------------------------------------
    # ----------------------------------ABS Chill -------------------------------------------------

    #  maxCap  , minCap  , coolCOP, fixIns, varIns, unitMtnPriceABSChill
    # 100000000, 0.000001,    1   ,  15000,   3000,           0

    itemParABSChill = dict(fixIns=15000,
                           varIns=3000,
                           coolCOP=1.0,
                           maxCap=100000000,  # cooling capacity, kW
                           minCap=0.000001,
                           unitMtnPriceABSChill=0)
    itemParABSChill['mtnPriceABSChill'] = (
            np.ones((itemParTime['numMonth'] * itemParTime['numDay'] * itemParTime['numHour']),
                    dtype=np.float32) * itemParABSChill['unitMtnPriceABSChill'])

    # --------------------------------------------------------------------------------------------
    # -----------------------------------------Ele Chill------------------------------------------

    # maxCap   ,   minCap, coolCOP,fixIns, varIns, unitMtnPriceEleChill
    # 100000000, 0.000001,   5.47 , 5000 ,   1000,         0

    itemParEleChill = dict(fixIns=5000,
                           varIns=1000,
                           coolCOP=5.47,
                           maxCap=100000000,  # cooling capacity, kW
                           minCap=0.000001,
                           unitMtnPriceEleChill=0)
    itemParEleChill['mtnPriceEleChill'] = (
            np.ones((itemParTime['numMonth']*itemParTime['numDay']*itemParTime['numHour']),
                    dtype=np.float32) * itemParEleChill['unitMtnPriceEleChill'])

    # ------------------------------------------------------------------------------------------
    # ---------------------------------Ele Grid-------------------------------------------------

    # maxSal ,  maxPur  , outType,   minSal,   minPur, sellPrice, demandCharge
    # 0.00001, 100000000,    1   , 0.000001, 0.000001,     0.5  ,      0

    dayPurPriceEleGrid = np.genfromtxt(currentPath + 'technology/dayPurPriceEleGrid.csv', delimiter=',')
    daySellPriceEleGrid = np.genfromtxt(currentPath + 'technology/daySellPriceEleGrid.csv', delimiter=',')
    dayPurPriceEleGrid_low = np.genfromtxt(currentPath + 'technology/dayPurPriceEleGrid_low.csv')
    dayPurPriceEleGrid_med = np.genfromtxt(currentPath + 'technology/dayPurPriceEleGrid_med.csv')
    dayPurPriceEleGrid_high = np.genfromtxt(currentPath + 'technology/dayPurPriceEleGrid_high.csv')

    itemParEleGrid = dict(maxPur=100000000,  # bigM
                          maxSal=0.00001,
                          minPur=0.000001,
                          minSal=0.000001,
                          outType=1,
                          sellPrice=0.5,
                          demandCharge=0)

    itemParEleGrid['purPriceEleGrid'] = npmtlb.repmat(dayPurPriceEleGrid[0: itemParTime['numHour']], 1,
                                                      itemParTime['numMonth'] * itemParTime['numDay'])
    itemParEleGrid['purPriceEleGrid_low'] = npmtlb.repmat(dayPurPriceEleGrid_low[0: itemParTime['numHour']], 1,
                                                          itemParTime['numMonth'] * itemParTime['numDay'])
    itemParEleGrid['purPriceEleGrid_med'] = npmtlb.repmat(dayPurPriceEleGrid_med[0: itemParTime['numHour']], 1,
                                                          itemParTime['numMonth'] * itemParTime['numDay'])
    itemParEleGrid['purPriceEleGrid_high'] = npmtlb.repmat(dayPurPriceEleGrid_high[0: itemParTime['numHour']],
                                                           1, itemParTime['numMonth'] * itemParTime['numDay'])

    if itemParEleGrid['outType'] == 2:
        itemParEleGrid['salPriceEleGrid'] = npmtlb.repmat(daySellPriceEleGrid[0: itemParTime['numHour']], 1,
                                                          itemParTime['numMonth'] * itemParTime['numDay'])
    else:
        itemParEleGrid['salPriceEleGrid'] = (
            np.zeros((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']), dtype=np.float32))

    rateMonthDemandCharge = np.atleast_1d(itemParEleGrid['demandCharge'])

    # -----------------------------------------------------------------------------------------------------------
    # ----------------------------------------Heat sal--------------------------------------------------------------

    # maxSal    ,    maxPur, outType,   minSal,   minPur, unitPurPriceInds,unitSalPriceHeat
    # 100000000 , 100000000,   1    , 0.000001, 0.000001,       0.18      ,     0.1

    itemParHeat = dict(maxPur=100000000,
                       maxSal=100000000,
                       minPur=0.000001,
                       minSal=0.000001,
                       outType=1,
                       unitPurPriceInds=0.18,
                       unitSalPriceHeat=0.1)

    itemParHeat['purPriceInds'] = (
            np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
                    dtype=np.float32) * itemParHeat['unitPurPriceInds'])
    itemParHeat['salPriceHeat'] = (
            np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
                    dtype=np.float32) * itemParHeat['unitSalPriceHeat'])




    rateMonthDemandCharge = np.asarray([8.23])




    # -------------------------------------------pipeline parameters-----------------------------------------------------------
    # determine whether the pipeline network should be taken into account for thermal loss calculate

    # parameters/problem/pipePar.csv
    # lossCalMode, pipeNetwFlag, numbldgCluster
    # loadFollow, 1, 3
    pipeNetwFlag = 1  # determine whether the pipeline network should be taken into account for thermal loss calculate

    lossCalMode = 'loadFollow'  # calculate pipeline loss according to the delivered load or the lengh of the pipeline 'loadFollow' , 'lengthFollow'

    numbldgCluster = 3 # numbldgCluster: number of buildings connected to the pipeline network

    # parameters/problem/parThemPipe.csv
    parThemPipe = {'coolDistEff': 0.85,  # distribution efficiency of pipeline for cooling energy
                   'heatDistEff': 0.9,
                   }
    # 5.00E-01,3.00E-01,2.00E-01
    bldgHeatPercent = [0.5, 0.3, 0.2]

    # 5.00E-01, 3.00E-01, 2.00E-01
    bldgCoolPercent = [0.5, 0.3, 0.2]


    # mainPipeLink: connection of buildings with primary pipeline section,
    # size is parMainPipe['numMainSec']* numbldgCluster
    #mainPipeLink = np.genfromtxt(currentPath + 'problem/mainPipeLink.csv')

    mainPipeLink = np.array([
        [1, 0, 0],
        [0, 1, 0],
        [0, 0, 1],
        [0, 1, 1],
        [1, 1, 1]
         ])
    print(mainPipeLink)
    # parMainPipe: properties of primary pipeline section, keys = ['lenMainSec','lenLossCoef','heatLossCoef','coolLossCoef']
    #              parMainPipe['lenMainSec'] : size is 1 * parMainPipe['numMainSec']
    #              parMainPipe['lenLossCoef']: size is 1 * parMainPipe['numMainSec']
    #              parMainPipe['heatLossCoef']: size is 1 * parMainPipe['numMainSec']
    #              parMainPipe['coolLossCoef']: size is 1 * parMainPipe['numMainSec']

    # parameters/problem/parMainPipe.csv

    #          lenLossCoef          ,      heatLossCoef                  ,
    # "[0.1, 0.2, 0.12, 0.23, 0.13]", "[0.01, 0.02, 0.012, 0.023, 0.013]",

    #          coolLossCoef,                      lenMainSec
    # "[0.015, 0.014, 0.013, 0.016, 0.021]","[44, 39, 15, 61, 40]"
    parMainPipe = {'lenMainSec': [44, 39, 15, 61, 40],  # m
                   'lenLossCoef': [0.1, 0.2, 0.12, 0.23, 0.13],
                   'heatLossCoef': [0.01, 0.02, 0.012, 0.023, 0.013],
                   'coolLossCoef': [0.015, 0.014, 0.013, 0.016, 0.021],
                   }

    # parScndPipe: properties of secondary pipeline section, keys = ['lenScndSec','lenLossCoef','heatLossCoef','coolLossCoef']
    #              parScndPipe['lenScndSec'] : size is 1 * parScndPipe['numScndSec']
    #              parScndPipe['lenLossCoef']: size is 1 * parScndPipe['numScndSec']
    #              parScndPipe['heatLossCoef']: size is 1 * parScndPipe['numScndSec']
    #              parScndPipe['coolLossCoef']: size is 1 * parScndPipe['numScndSec']


    # parameters/problem/parScndPipe.csv

    #            lenLossCoef              ,            lenScndSec     ,

    # "[0.1, 0.2, 0.12, 0.23, 0.13, 0.14]", "[45, 10, 5, 100, 35, 11]",
    #           heatLossCoef                      ,              coolLossCoef
    # "[0.015, 0.025, 0.015, 0.025, 0.015, 0.014]", "[0.025, 0.024, 0.023, 0.026, 0.031, 0.017]"

    parScndPipe = {'lenScndSec': [45, 10, 5, 100, 35, 11],
                   'lenLossCoef': [0.1, 0.2, 0.12, 0.23, 0.13, 0.14],
                   'heatLossCoef': [0.015, 0.025, 0.015, 0.025, 0.015, 0.014],
                   'coolLossCoef': [0.025, 0.024, 0.023, 0.026, 0.031, 0.017]}

    with open(currentPath + 'problem/parScndPipe.csv', 'rU') as f:
        # parScndPipe: properties of secondary pipeline section, keys = ['lenScndSec','lenLossCoef','heatLossCoef','coolLossCoef']
        #              parScndPipe['lenScndSec'] : size is 1 * parScndPipe['numScndSec']
        #              parScndPipe['lenLossCoef']: size is 1 * parScndPipe['numScndSec']
        #              parScndPipe['heatLossCoef']: size is 1 * parScndPipe['numScndSec']
        #              parScndPipe['coolLossCoef']: size is 1 * parScndPipe['numScndSec']
        reader = csv.reader(f)
        pars = next(reader)
        vals = next(reader)
        vals = [ast.literal_eval(x) for x in vals]
        parScndPipe = dict(zip(pars, vals))

    # Load methods.
    # parameters/algorithm/loadMethod.csv
    # loadFileFlag,loadFileName,idxInitHeatLoad,idxIniCoolLoad,idxIniEleLoad,incHeatLoad,incCoolLoad,incEleLoad
    #     3       ,    None    ,      0        ,      0       ,      0      ,      1    ,      1    ,     1

    # loadFile: 2 means the load data is read from excel file
    #           1 means the load data is read from the current file
    #           0 means load data is input with matrix directly
    # idxIniHeatLoad : choose from which row to select the heating load in the excel when loadFile=1
    loadFileFlag = 3
    loadFileName = None
    idxIniHeatLoad = 0
    idxIniCoolLoad = 0
    idxIniEleLoad = 0

    incHeatLoad = 1
    incCoolLoad = 1
    incEleLoad = 1

    eleLoad, coolLoad, heatLoad = None, None, None

    if incHeatLoad:
        heatLoad = None
    else:
        if pipeNetwFlag == 0:
            heatLoad = np.zeros((1, itemParTime['numMonth'] * itemParTime['numDay'] * itemParTime['numHour']))
        else:
            heatLoad = [np.zeros(
                (1, itemParTime['numMonth'] * itemParTime['numDay'] * itemParTime['numHour']))] * numbldgCluster

    if incCoolLoad:
        coolLoad = None
    else:
        if pipeNetwFlag == 0:
            coolLoad = np.zeros((1, itemParTime['numMonth'] * itemParTime['numDay'] * itemParTime['numHour']))
        else:
            coolLoad = [np.zeros(
                (1, itemParTime['numMonth'] * itemParTime['numDay'] * itemParTime['numHour']))] * numbldgCluster

    if incEleLoad:
        eleLoad = None
    else:
        if pipeNetwFlag == 0:
            eleLoad = np.zeros((1, itemParTime['numMonth'] * itemParTime['numDay'] * itemParTime['numHour']))
        else:
            eleLoad = [np.zeros(
                (1, itemParTime['numMonth'] * itemParTime['numDay'] * itemParTime['numHour']))] * numbldgCluster









    # PV, natGas , coolTank, solThem, heatTank,  ele , heatInds
    # 0 ,  0.187 ,     0   ,     0  ,     0   , 0.853,  0.187
    emiFac = {'ele': 0.853,  # carbon intensity of electricity from Grid
              'solThem': 0,  # 0.022, carbon intensity of heat from solar thermal
              'PV': 0,  # 101 , carbon intensity of electricity from PV, kgCO2/m^2
              'natGas': 0.187,  # carbon intensity of heating and cooling from natural gas
              'heatTank': 0,  # 821.3, kgCO2/m^3
              'coolTank': 0,  # 821.3, kgCO2/m^3
              'heatInds': 0.187
              }

    # PV ,  natGas, coolTank, solThem, heatTank, ele, heatInds
    #  0 ,    0.9 ,     0   ,    0   ,      0  ,  1 ,    0
    taxCO2 = {'ele': 1,  # carbon intensity of electricity from Grid
              'solThem': 0,  # carbon intensity of heat from solar thermal
              'PV': 0,  # carbon intensity of electricity from PV, kgCO2/m^2
              'natGas': 0.9,  # carbon intensity of heating and cooling from natural gas
              'heatTank': 0,  # kgCO2/m^3
              'coolTank': 0,  # kgCO2/m^3
              'heatInds': 0
              }


    # try:
        # if currentPath is None:
        #     rootDir = os.path.dirname(os.path.dirname(os.getcwd()))
        #     currentPath = rootDir + '/data/parameters/'
        #     currentPathData = rootDir + '/data/data/'

        # with open(currentPath+'algorithm/default_vals.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = map(float,vals)
        #     default_vals = dict(zip(pars,vals))
        #     bigM = default_vals['bigM']
        #     smallM = default_vals['smallM']

        # with open(currentPath+'technology/itemParTime.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     longInd = pars.index('longIntr')
        #     valInd = range(len(vals))
        #     #del valInd[longInd]
        #     for i in valInd:
        #         vals[i] = int(vals[i])
        #     vals[longInd] = float(vals[longInd])
        #     itemParTime = dict(zip(pars,vals))

        # with open(currentPath+'problem/pipePar.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     modeInd = pars.index('lossCalMode')
        #     valInd = range(len(vals))
        #     del valInd[modeInd]
        #     for i in valInd:
        #         vals[i] = float(vals[i])
        #     pipePar = dict(zip(pars,vals))
        #     pipeNetwFlag = pipePar['pipeNetwFlag'] # determine whether the pipeline network should be taken into account for thermal loss calculate
        #     lossCalMode = pipePar['lossCalMode'] # calculate pipeline loss according to the delivered load or the lengh of the pipeline 'loadFollow' , 'lengthFollow'
        #     numbldgCluster = pipePar['numbldgCluster'] # lossCalMode: 'loadFollow, lengthFollow' numbldgCluster: number of buildings connected to the pipeline network

        # with open(currentPath + 'algorithm/loadMethod.csv', 'rU') as f:
        #     # loadFile: 2 means the load data is read from excel file
        #     #           1 means the load data is read from the current file
        #     #           0 means load data is input with matrix directly
        #     # idxIniHeatLoad : choose from which row to select the heating load in the excel when loadFile=1
        #     myheader = ['loadFileFlag', 'loadFileName', 'idxInitHeatLoad', 'idxIniCoolLoad', 'idxIniEleLoad',
        #                 'incHeatLoad', 'incCoolLoad', 'incEleLoad']
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     check = sum(1 for i, j in zip(myheader, pars) if i != j)
        #     if check > 0:
        #         raise Exception('The parameters are not the correct order in loadMethod!')
        #     loadFileFlag = int(float(vals[0]))
        #     if vals[1] == 'None':
        #         loadFileName = None
        #     else:
        #         loadFileName = vals[1]
        #     incHeatLoad = int(float(vals[5]))
        #     idxIniHeatLoad = int(float(vals[2]))
        #     if incHeatLoad:
        #         heatLoad = None
        #     else:
        #         if pipeNetwFlag == 0:
        #             heatLoad = np.zeros((1, itemParTime['numMonth'] * itemParTime['numDay'] * itemParTime['numHour']))
        #         else:
        #             heatLoad = [np.zeros(
        #                 (1, itemParTime['numMonth'] * itemParTime['numDay'] * itemParTime['numHour']))] * numbldgCluster
        #     incCoolLoad = int(float(vals[6]))
        #     idxIniCoolLoad = int(float(vals[3]))
        #     if incCoolLoad:
        #         coolLoad = None
        #     else:
        #         if pipeNetwFlag == 0:
        #             coolLoad = np.zeros((1, itemParTime['numMonth'] * itemParTime['numDay'] * itemParTime['numHour']))
        #         else:
        #             coolLoad = [np.zeros(
        #                 (1, itemParTime['numMonth'] * itemParTime['numDay'] * itemParTime['numHour']))] * numbldgCluster
        #
        #     incEleLoad = int(float(vals[7]))
        #     idxIniEleLoad = int(float(vals[4]))
        #     if incEleLoad:
        #         eleLoad = None
        #     else:
        #         if pipeNetwFlag == 0:
        #             eleLoad = np.zeros((1, itemParTime['numMonth'] * itemParTime['numDay'] * itemParTime['numHour']))
        #         else:
        #             eleLoad = [np.zeros(
        #                 (1, itemParTime['numMonth'] * itemParTime['numDay'] * itemParTime['numHour']))] * numbldgCluster

        # with open(currentPath + 'algorithm/ctrStr.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals[pars.index('plotFlag')] = int(vals[pars.index('plotFlag')])
        #     vals[pars.index('mpsWriteFlag')] = int(vals[pars.index('mpsWriteFlag')])
        #     vals[pars.index('sampleTimeFlag')] = int(vals[pars.index('sampleTimeFlag')])
        #     vals[pars.index('sparseProbFlag')] = int(vals[pars.index('sparseProbFlag')])
        #     vals[pars.index('probReduceFlag')] = int(vals[pars.index('probReduceFlag')])
        #     vals[pars.index('taxCO2Flag')] = int(vals[pars.index('taxCO2Flag')])
        #     vals[pars.index('carbOnly')] = int(vals[pars.index('carbOnly')])
        #     ctrStr = dict(zip(pars, vals))

        # with open(currentPath + 'problem/basic.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = [ast.literal_eval(x) for x in vals]
        #     parBasic = dict(zip(pars, vals))
        #     totArea = np.sum(parBasic['off_area']) + np.sum(parBasic['com_area']) + np.sum(
        #         parBasic['hotel_area']) + np.sum(parBasic['res_area'])

        # try:
        #     location = ['shenzhen', 'beijing', 'shanghai'][int(parBasic['location'])]
        #     if itemParTime['numDay'] == 2:  # 576 case
        #         assert (itemParTime['numMonth'] == 12 and itemParTime['numHour'] == 24)
        #         loadPath = currentPathData + 'irr_data/' + location + '_576.txt'
        #         daySolIrradiation = np.loadtxt(loadPath)
        #         solIrradiation = np.loadtxt(loadPath)
        #     else:
        #         assert (itemParTime['numDay'] == 30 and itemParTime['numMonth'] == 12 and itemParTime['numHour'] == 24)
        #         loadPath = currentPathData + 'irr_data/' + location + '.txt'
        #         daySolIrradiation = np.loadtxt(loadPath)
        #         solIrradiation = np.loadtxt(loadPath)
        #     np.savetxt(currentPath + 'technology/daySolIrradiation.out', daySolIrradiation, delimiter=',')
        #     # print ('Read out Solar irradiation!')
        #     # print daySolIrradiation
        # except:
        #     print('Did not read out solar irradiation data!')
            # print dayPurPriceEleGrid

        # with open(currentPath + 'technology/itemLenLifeYr.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     vals = next(reader)
        #     itemLenLifeYr = map(float, vals)

        # with open(currentPath + 'problem/itemCandidateTech1.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = map(float, vals)
        #     itemCandidateTech1 = dict(zip(pars, vals))

        # with open(currentPath + 'problem/itemCandidateTech2.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = map(float, vals)
        #     itemCandidateTech2 = dict(zip(pars, vals))

        # with open(currentPath + 'problem/forceSelect.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = map(float, vals)
        #     forceSelect = dict(zip(pars, vals))

        # with open(currentPath + 'technology/itemParPV.csv', 'rU') as f:
        #     # itemParPV:
        #     # varIns :capital cost of solar PV panels, $/kW
        #     # unitOutPV : rated electricity generation per unit area of PV panel system, kW/m^2
        #     # perfRatio : performance ratio / irradiation efficiency
        #     # solYield: solar panel yield
        #     # E_PV <= A_PV * perfRatio * solYield * unitOut
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = map(float, vals)
        #     itemParPV = dict(zip(pars, vals))
        #     itemParPV['solIrradiation'] = solIrradiation
        #     itemParPV['mtnPricePV'] = (np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
        #                                        dtype=np.float32) * itemParPV['unitMtnPrice'])
        #     itemParPV['maxInstArea'] = np.min(np.array([totArea * 0.098, itemParPV['maxInstArea']]))

        # with open(currentPath + 'technology/itemParSolThem.csv', 'rU') as f:
        #     # itemParSolThem:
        #     # varIns : capital cost of solar thermal, $/kW
        #     # unitOutSolThem :rated electricity generation per unit area of PV panel system, kW/m^2
        #     # H_SolThem <= A_SolThem * irrEff * unitOut
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = map(float, vals)
        #     itemParSolThem = dict(zip(pars, vals))
        #     itemParSolThem['mtnPriceSolThem'] = (
        #             np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
        #                     dtype=np.float32) * itemParSolThem['unitMtnPrice'])
        #     # itemParSolThem['solIrradiation'] = (np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']), dtype=np.float32) * 0)
        #     itemParSolThem['solIrradiation'] = solIrradiation
        #     itemParSolThem['maxInstArea'] = np.min(np.array([totArea * 0.167, itemParSolThem['maxInstArea']]))

        # with open(currentPath + 'technology/itemParCHP.csv', 'rU') as f:
        #     # itemParCHP
        #     # define the number & capacity of discrete CHP technologies
        #     # typeCHP : number of types of CHP plant
        #     # capCostCHP : capital cost of CHP plant, $/kW
        #     # capGenCHP : rated capacity of CHP, kW
        #     # eleEffCHP : electrical efficiency of CHP
        #     # heatEffCHP :factor relating electricity generation to heat supply from CHP
        #     #     CHP efficiency data see http://www.epa.gov/chp/basic/methods.html
        #     #     'heatSta', 'coolSta', used to detect when CHP should be considered
        #     #     when CHP is considered for heating purpose, heatSta =1
        #     #     when CHP is considered for electricity purpose, eleSta =1
        #     #     when CHP is used for cooling purpose, providing heating for ABSChiller, coolSta = 1
        #     # H_SolThem <= A_SolThem * irrEff * unitOut
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = [ast.literal_eval(x) for x in vals]
        #     itemParCHP = dict(zip(pars, vals))
        #     # gasPriceCHP: $/kWh
        #     itemParCHP['gasPriceCHP'] = np.ones(
        #         (itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']), dtype=np.float32) * \
        #                                 itemParCHP['unitGasPriceCHP']
        #     # $35/kWe - $55 /kWe, average $40/kWe
        #     itemParCHP['mtnPriceCHP'] = np.ones(
        #         (itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']), dtype=np.float32) * \
        #                                 itemParCHP['unitMtnPriceCHP']

        # with open(currentPath + 'technology/itemParEleBoil.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = map(float, vals)
        #     itemParEleBoil = dict(zip(pars, vals))

        # with open(currentPath + 'technology/itemParNGBoil.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = map(float, vals)
        #     itemParNGBoil = dict(zip(pars, vals))
        #     itemParNGBoil['gasPriceNGBoil'] = (
        #             np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
        #                     dtype=np.float32) * itemParNGBoil['unitGasPriceNGBoil'])
        #     itemParNGBoil['mtnPriceNGBoil'] = (
        #             np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
        #                     dtype=np.float32) * itemParNGBoil['unitMtnPriceNGBoil'])

        # with open(currentPath + 'technology/itemParHeatTank.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     modeInd = pars.index('mode')
        #     valInd = range(len(vals))
        #     del valInd[modeInd]
        #     for i in valInd:
        #         vals[i] = float(vals[i])
        #     itemParHeatTank = dict(zip(pars, vals))
        #     itemParHeatTank['mtnPriceHeatTank'] = (
        #             np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
        #                     dtype=np.float32) * itemParHeatTank['unitMtnPriceHeatTank'])

        # with open(currentPath + 'technology/itemParCoolTank.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     modeInd = pars.index('mode')
        #     valInd = range(len(vals))
        #     del valInd[modeInd]
        #     for i in valInd:
        #         vals[i] = float(vals[i])
        #     itemParCoolTank = dict(zip(pars, vals))
        #     itemParCoolTank['mtnPriceCoolTank'] = (
        #             np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
        #                     dtype=np.float32) * itemParCoolTank['unitMtnPriceCoolTank'])

        # with open(currentPath + 'technology/itemParEleBat.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     modeInd = pars.index('mode')
        #     valInd = range(len(vals))
        #     del valInd[modeInd]
        #     for i in valInd:
        #         vals[i] = float(vals[i])
        #     itemParEleBat = dict(zip(pars, vals))
        #     itemParEleBat['mtnPriceEleBat'] = (
        #             np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
        #                     dtype=np.float32) * itemParEleBat['unitMtnPriceEleBat'])
        #     # itemParEleBat['mtnPriceEleBat'] = itemParEleBat['mtnPriceEleBat'].reshape(1, itemParTime['totHrStep'])
        #     # itemParEleBat['mtnPriceEleBat']  = np.squeeze(itemParEleBat['mtnPriceEleBat'])

        # with open(currentPath + 'technology/itemParHP.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = map(float, vals)
        #     itemParHP = dict(zip(pars, vals))
        #     itemParHP['mtnPriceHP'] = (np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
        #                                        dtype=np.float32) * itemParHP['unitMtnPriceHP'])
        #     itemParHP['maxCap'] = np.min(np.array([totArea * 0.0068, itemParHP['maxCap']]))


        # with open(currentPath + 'technology/itemParNGChill.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = map(float, vals)
        #     itemParNGChill = dict(zip(pars, vals))
        #     itemParNGChill['gasPriceNGChill'] = (
        #             np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
        #                     dtype=np.float32) * itemParNGChill['unitGasPriceNGChill'])
        #     itemParNGChill['mtnPriceNGChill'] = (
        #             np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
        #                     dtype=np.float32) * itemParNGChill['unitMtnPriceNGChill'])

        # with open(currentPath + 'technology/itemParABSChill.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = map(float, vals)
        #     itemParABSChill = dict(zip(pars, vals))
        #     itemParABSChill['mtnPriceABSChill'] = (
        #             np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
        #                     dtype=np.float32) * itemParABSChill['unitMtnPriceABSChill'])

        # with open(currentPath + 'technology/itemParEleChill.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = map(float, vals)
        #     itemParEleChill = dict(zip(pars, vals))
        #     itemParEleChill['mtnPriceEleChill'] = (
        #             np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
        #                     dtype=np.float32) * itemParEleChill['unitMtnPriceEleChill'])

        # try:
        #     dayPurPriceEleGrid = np.genfromtxt(currentPath + 'technology/dayPurPriceEleGrid.csv', delimiter=',')
        #     daySellPriceEleGrid = np.genfromtxt(currentPath + 'technology/daySellPriceEleGrid.csv', delimiter=',')
        #     outFile = os.path.join(currentPath, '../Outputs/Algorithm/')
        #     if not os.path.exists(outFile):
        #         os.makedirs(outFile)
        #     np.savetxt(outFile + 'dayPurPriceEleGrid.out', dayPurPriceEleGrid, delimiter=',')
        #     np.savetxt(outFile + 'daySellPriceEleGrid.out', dayPurPriceEleGrid, delimiter=',')
        #     dayPurPriceEleGrid_low = np.genfromtxt(currentPath + 'technology/dayPurPriceEleGrid_low.csv', delimiter=',')
        #     np.savetxt(outFile + 'dayPurPriceEleGrid_low.out', dayPurPriceEleGrid_low, delimiter=',')
        #     dayPurPriceEleGrid_med = np.genfromtxt(currentPath + 'technology/dayPurPriceEleGrid_med.csv', delimiter=',')
        #     np.savetxt(outFile + 'dayPurPriceEleGrid_med.out', dayPurPriceEleGrid_med, delimiter=',')
        #     dayPurPriceEleGrid_high = np.genfromtxt(currentPath + 'technology/dayPurPriceEleGrid_high.csv',
        #                                             delimiter=',')
        #     np.savetxt(outFile + 'dayPurPriceEleGrid_high.out', dayPurPriceEleGrid_high, delimiter=',')
        # except:
        #     print('Did not read out Electricity purchase price!')
        #     # print dayPurPriceEleGrid

        # with open(currentPath + 'technology/itemParEleGrid.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = map(float, vals)
        #     itemParEleGrid = dict(zip(pars, vals))
        #     itemParEleGrid['purPriceEleGrid'] = npmtlb.repmat(dayPurPriceEleGrid[0: itemParTime['numHour']], 1,
        #                                                       itemParTime['numMonth'] * itemParTime['numDay'])
        #     itemParEleGrid['purPriceEleGrid_low'] = npmtlb.repmat(dayPurPriceEleGrid_low[0: itemParTime['numHour']], 1,
        #                                                           itemParTime['numMonth'] * itemParTime['numDay'])
        #     itemParEleGrid['purPriceEleGrid_med'] = npmtlb.repmat(dayPurPriceEleGrid_med[0: itemParTime['numHour']], 1,
        #                                                           itemParTime['numMonth'] * itemParTime['numDay'])
        #     itemParEleGrid['purPriceEleGrid_high'] = npmtlb.repmat(dayPurPriceEleGrid_high[0: itemParTime['numHour']],
        #                                                            1, itemParTime['numMonth'] * itemParTime['numDay'])
        #
        #     if itemParEleGrid['outType'] == 2:
        #         itemParEleGrid['salPriceEleGrid'] = npmtlb.repmat(daySellPriceEleGrid[0: itemParTime['numHour']], 1,
        #                                                           itemParTime['numMonth'] * itemParTime['numDay'])
        #     else:
        #         itemParEleGrid['salPriceEleGrid'] = (
        #             np.zeros((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
        #                      dtype=np.float32))
        #     rateMonthDemandCharge = np.atleast_1d(itemParEleGrid['demandCharge'])
        #     # print 'Added rate month demand charge '
        #     # print rateMonthDemandCharge


        # with open(currentPath + 'technology/itemParHeat.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = map(float, vals)
        #     itemParHeat = dict(zip(pars, vals))
        #     itemParHeat['purPriceInds'] = (
        #             np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
        #                     dtype=np.float32) * itemParHeat['unitPurPriceInds'])
        #     itemParHeat['salPriceHeat'] = (
        #             np.ones((itemParTime['numMonth'], itemParTime['numDay'], itemParTime['numHour']),
        #                     dtype=np.float32) * itemParHeat['unitSalPriceHeat'])

        # with open(currentPath + 'problem/parThemPipe.csv', 'rU') as f:
        #     # 01/27/2016, this parameter is only needed when the pipeline network is not considered (pipeNetwFlag=0)
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = map(float, vals)
        #     parThemPipe = dict(zip(pars, vals))

        # with open(currentPathData + 'rateMonthDemandCharge.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     vals = next(reader)
        #     rateMonthDemandCharge = np.asarray([float(i) for i in vals if len(i)])

        # with open(currentPath + 'problem/parMainPipe.csv', 'rU') as f:
        #     # parMainPipe: properties of primary pipeline section, keys = ['lenMainSec','lenLossCoef','heatLossCoef','coolLossCoef']
        #     #              parMainPipe['lenMainSec'] : size is 1 * parMainPipe['numMainSec']
        #     #              parMainPipe['lenLossCoef']: size is 1 * parMainPipe['numMainSec']
        #     #              parMainPipe['heatLossCoef']: size is 1 * parMainPipe['numMainSec']
        #     #              parMainPipe['coolLossCoef']: size is 1 * parMainPipe['numMainSec']
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = [ast.literal_eval(x) for x in vals]
        #     parMainPipe = dict(zip(pars, vals))

        # with open(currentPath + 'problem/parScndPipe.csv', 'rU') as f:
        #     # parScndPipe: properties of secondary pipeline section, keys = ['lenScndSec','lenLossCoef','heatLossCoef','coolLossCoef']
        #     #              parScndPipe['lenScndSec'] : size is 1 * parScndPipe['numScndSec']
        #     #              parScndPipe['lenLossCoef']: size is 1 * parScndPipe['numScndSec']
        #     #              parScndPipe['heatLossCoef']: size is 1 * parScndPipe['numScndSec']
        #     #              parScndPipe['coolLossCoef']: size is 1 * parScndPipe['numScndSec']
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = [ast.literal_eval(x) for x in vals]
        #     parScndPipe = dict(zip(pars, vals))

        # with open(currentPath + 'problem/emiFac.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = map(float, vals)
        #     emiFac = dict(zip(pars, vals))

        # with open(currentPath + 'problem/taxCO2.csv', 'rU') as f:
        #     reader = csv.reader(f)
        #     pars = next(reader)
        #     vals = next(reader)
        #     vals = map(float, vals)
        #     taxCO2 = dict(zip(pars, vals))

        # mainPipeLink: connection of buildings with primary pipeline section,
        # size is parMainPipe['numMainSec']* numbldgCluster
        #mainPipeLink = np.genfromtxt(currentPath + 'problem/mainPipeLink.csv', delimiter=',')

        # with open(currentPathData + 'bldgCoolPercent.csv', 'rU') as f:
        #     # bldgCoolPercent: ditribution of cool load in buildings, size is 1 * numbldgCluster
        #     reader = csv.reader(f)
        #     vals = next(reader)
        #     bldgCoolPercent = np.asarray([float(i) for i in vals if len(i)])

        # with open(currentPathData + 'bldgHeatPercent.csv', 'rU') as f:
        #     # bldgHeatPercent: distribution of heat load in buildings, size is 1 * numbldgCluster
        #     reader = csv.reader(f)
        #     vals = next(reader)
        #     bldgHeatPercent = np.asarray([float(i) for i in vals if len(i)])


    # except IOError as e:
    #     print("I/O error({0}): {1}".format(e.errno, e.strerror))
    # except:  # handle other exceptions
    #     print("Unexpected error:", sys.exc_info()[0])

    try:
        location = ['shenzhen', 'beijing', 'shanghai'][int(parBasic['location'])]
        loadPath = currentPathData + 'building_loads/' + location + '/'

        off_eleoald_text = np.loadtxt(loadPath + 'off_eleLoad_576.in', delimiter=',')

        eleLoad_off = np.squeeze(np.loadtxt(loadPath + 'off_eleLoad_576.in', delimiter=','))
            #.reshape(1, itemParTime[
            #'numMonth'] * itemParTime['numDay'] * itemParTime['numHour'])
        eleLoad_com = np.squeeze(np.loadtxt(loadPath + 'com_eleLoad_576.in', delimiter=','))
        eleLoad_hotel = np.squeeze(np.loadtxt(loadPath + 'hotel_eleLoad_576.in', delimiter=','))
        eleLoad_res = np.squeeze(np.loadtxt(loadPath + 'res_eleLoad_576.in', delimiter=','))

        heatLoad_off = np.squeeze(np.loadtxt(loadPath + 'off_heatLoad_576.in', delimiter=','))
        heatLoad_com = np.squeeze(np.loadtxt(loadPath + 'com_heatLoad_576.in', delimiter=','))
        heatLoad_hotel = np.squeeze(np.loadtxt(loadPath + 'hotel_heatLoad_576.in', delimiter=','))

        heatLoad_res = np.squeeze(np.loadtxt(loadPath + 'res_heatLoad_576.in', delimiter=','))

        coolLoad_off = np.squeeze(np.loadtxt(loadPath + 'off_coolLoad_576.in', delimiter=','))
        coolLoad_com = np.squeeze(np.loadtxt(loadPath + 'com_coolLoad_576.in', delimiter=','))
        coolLoad_hotel = np.squeeze(np.loadtxt(loadPath + 'hotel_coolLoad_576.in', delimiter=','))
        coolLoad_res = np.squeeze(np.loadtxt(loadPath + 'res_coolLoad_576.in', delimiter=','))

        eleLoad = np.zeros(itemParTime['totHrStep'])
        coolLoad = np.zeros(itemParTime['totHrStep'])
        heatLoad = np.zeros(itemParTime['totHrStep'])

        if pipeNetwFlag == 0:
            com_area, hotel_area, off_area, res_area = np.sum(parBasic['com_area']), np.sum(
                parBasic['hotel_area']), np.sum(parBasic['off_area']), np.sum(parBasic['res_area'])
            eleLoad = (
                    eleLoad_com * com_area + eleLoad_hotel * hotel_area + eleLoad_off * off_area + eleLoad_res * res_area)
            heatLoad = (
                    heatLoad_com * com_area + heatLoad_hotel * hotel_area + heatLoad_off * off_area + heatLoad_res * res_area)
            coolLoad = (
                    coolLoad_com * com_area + coolLoad_hotel * hotel_area + coolLoad_off * off_area + coolLoad_res * res_area)

        else:

            # we store the building loads by orderly iterating over: office, commercial, hotel, residential
            for i in range(len(parBasic['off_area'])):
                eleLoad += eleLoad_off * parBasic['off_area'][i]
                heatLoad += heatLoad_off * parBasic['off_area'][i]
                coolLoad += coolLoad_off * parBasic['off_area'][i]
            ##
            for i in range(len(parBasic['com_area'])):
                eleLoad += eleLoad_com * parBasic['com_area'][i]
                heatLoad += heatLoad_com * parBasic['com_area'][i]
                coolLoad += coolLoad_com * parBasic['com_area'][i]
            ##
            for i in range(len(parBasic['hotel_area'])):
                eleLoad += eleLoad_hotel * parBasic['hotel_area'][i]
                heatLoad += heatLoad_hotel * parBasic['hotel_area'][i]
                coolLoad += coolLoad_hotel * parBasic['hotel_area'][i]
            ##
            for i in range(len(parBasic['res_area'])):
                eleLoad += eleLoad_res * parBasic['res_area'][i]
                heatLoad += heatLoad_res * parBasic['res_area'][i]
                coolLoad += coolLoad_res * parBasic['res_area'][i]

    except Exception as e:
        print(e)
        traceback.print_exc()
        print("cannot handle loads")

    return loadFileFlag, loadFileName, pipeNetwFlag, parThemPipe, ctrStr, eleLoad, coolLoad, heatLoad, bigM, \
           smallM, itemParCHP, itemParTime, itemLenLifeYr, itemCandidateTech1, itemCandidateTech2, forceSelect, itemParPV, \
           itemParSolThem, itemParEleBoil, itemParNGBoil, itemParHeatTank, itemParCoolTank, itemParEleBat, \
           itemParHP, itemParNGChill, itemParABSChill, itemParEleChill, itemParEleGrid, itemParHeat, rateMonthDemandCharge, \
           lossCalMode, numbldgCluster, bldgHeatPercent, bldgCoolPercent, mainPipeLink, parMainPipe, parScndPipe, emiFac, \
           taxCO2, parBasic, idxIniHeatLoad, idxIniCoolLoad, idxIniEleLoad


if __name__ == "__main__":
    readPars()
