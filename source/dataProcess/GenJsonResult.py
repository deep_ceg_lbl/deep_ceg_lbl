
import os
import json
import random




def get_load(ele_path, cool_path, heat_path, out_file):
    if ele_path is None or cool_path is None or heat_path is None or out_file is None:
        print("Please check the file whether it exists or not.")
        return

    result = {}
    all_data = {}

    all_data['ele_load'] = get_load_data(ele_path)
    all_data['cool_load'] = get_load_data(cool_path)
    all_data['heat_load'] = get_load_data(heat_path)

    mon_num = {
        1: "Jan",
        2: "Feb",
        3: "Mar",
        4: "Apr",
        5: "May",
        6: "Jun",
        7: "Jul",
        8: "Aug",
        9: "Sep",
        10: "Oct",
        11: "Nov",
        12: "Dec"
    }

    x_data_all = []
    for i in range(1, 13): #month
        for j in range(1,3): #workday or weekend
            for k in range(1,25): # hours
                if k == 1:
                    if j ==1:
                        x_data_all.append(mon_num.get(i)+"-workday")
                    elif j == 2:
                        x_data_all.append(mon_num.get(i) + "-weekend")
                else :
                    x_data_all.append(k)


    all_data['x_data'] = x_data_all
    result['all_data'] = all_data

    #Get workday data and weekend data.
    x_data_half =[]
    for i in range(1, 12): #month
            for k in range(1,24): # hours
                if k == 1:
                    x_data_half.append(mon_num.get(i))
                else :
                    x_data_half.append(k)

    workday_data = {}
    weekend_data = {}
    workday_data['ele_load'], weekend_data['ele_load']=get_load_data_wkd_wkn(ele_path)
    workday_data['cool_load'], weekend_data['cool_load'] = get_load_data_wkd_wkn(cool_path)
    workday_data['heat_load'], weekend_data['heat_load'] = get_load_data_wkd_wkn(heat_path)
    workday_data['x_data'] = x_data_half
    weekend_data['x_data'] = x_data_half

    result['workday_data'] = workday_data
    result['weekend_data'] = weekend_data

    with open(out_file, 'w+') as f:
        json.dump(result, f)


def get_load_data(src_path):
    if src_path is None:
        return None
    data = []
    with open(src_path, 'rU') as f:
        for line in f.readlines():
            line = line.strip()
            data.append(round(float(line),2))
    return data


#Get the workday data and weekend data.
def get_load_data_wkd_wkn(src_path):
    if src_path is None:
        return None
    i = 0
    workday_data = []
    weekend_data = []
    with open(src_path, 'rU') as f:
        for line in f.readlines():
            line = line.strip()
            if(i // 24) % 2 == 0:
                workday_data.append(round(float(line), 2))
            else:
                weekend_data.append(round(float(line), 2))
            i = i + 1
    #print(i)
    #print(workday_data)
    #print(len(workday_data))
    #print(weekend_data)
    #print(len(weekend_data))
    return workday_data,weekend_data

def get_running_costs(running_cost_ele_path,running_cost_gas_path,running_cost_mtn_path):
    ele_cost = None
    gas_cost = None
    mtn_cost = None
    with open(running_cost_ele_path,'rU') as f:
        for line in f.readlines():
            ele_cost = round(float(line.strip()), 2)
    with open(running_cost_ele_path,'rU') as f:
        for line in f.readlines():
            gas_cost = round(float(line.strip()), 2)
    with open(running_cost_ele_path,'rU') as f:
        for line in f.readlines():
            mtn_cost = round(float(line.strip()), 2)

    return ele_cost,gas_cost,mtn_cost

def get_costs_running(running_cost_ele_path,running_cost_gas_path,running_cost_mtn_path, output_file):
    ele_cost, gas_cost, mtn_cost = get_running_costs(running_cost_ele_path, running_cost_gas_path,running_cost_mtn_path)
    result = {}
    #["电费","燃气费","维护费"]
    result['x_data'] = ["Electricity","NG","Maintenance"]
    result['optimal_value'] = [ele_cost,gas_cost,mtn_cost]
    result['baseline_value'] = [ele_cost*1.2, gas_cost * 1.4, mtn_cost* 1.5]

    with open(output_file,'w+') as f:
        json.dump(result,f)

def get_costs(annual_cost_path,init_inv_cost_path,running_cost_ele_path,running_cost_gas_path,running_cost_mtn_path, output_file):
    annual_cost = None
    init_inv_cost = None
    running_cost = None
    with open(annual_cost_path,'rU') as f:
        for line in f.readlines(): #Only one line in the file.
            annual_cost = round(float(line.strip()), 2)
    with open(init_inv_cost_path,'rU') as f:
        for line in f.readlines():
            init_inv_cost= round(float(line.strip()), 2)

    ele_cost,gas_cost,mtn_cost = get_running_costs(running_cost_ele_path,running_cost_gas_path,running_cost_mtn_path)
    running_cost = ele_cost + gas_cost + mtn_cost

    result = {}
    #["年化费用", "初投资费用", "运行费用"]
    result['x_data']=["Annual cost","Initial investment","Operation cost"]
    result['optimal_value'] = [annual_cost,init_inv_cost,running_cost]
    result['baseline_value'] = [annual_cost*1.2, init_inv_cost*1.3, running_cost*2]

    with open(output_file, 'w+') as f:
        json.dump(result, f)


###能源消耗
def get_energy_consumption(ele_path, gas_path,output_file):
    with open(ele_path,'rU') as f:
        for line in f.readlines():
            ele_cons=round(float(line.strip()))
    with open(gas_path,'rU') as f:
        for line in f.readlines():
            gas_cons = round(float(line.strip()))
    result={}
    #["电量","燃气量"]
    result['x_data']=["Electricity","NG"]
    result['optimal_value'] = [ele_cons,gas_cons]
    result['baseline_value'] = [ele_cons*0.8, gas_cons*0.7]

    with open(output_file, "w+") as f:
        json.dump(result, f)

###Carbo emission
def get_carto_emission(ele_em_path,gas_em_path,output_file):
    with open(ele_em_path, 'rU') as f:
        for line in f.readlines():
            ele_emission=round(float(line.strip()))
    with open(gas_em_path,'rU') as f:
        for line in f.readlines():
            gas_emission = round(float(line.strip()))
    result={}
    #["电力碳排放","燃气碳排放"]
    result['x_data']=["From electricity","From NG"]
    result['optimal_value'] = [ele_emission,gas_emission]
    result['baseline_value'] = [ele_emission*1.2, gas_emission*1.5]
    with open(output_file, "w+") as f:
        json.dump(result, f)

###Capabilities of the devices.
def get_capabilities(ng_boil_path,ele_chill_path,ele_boil_path,abs_chill_path,ng_chill_path,\
                     hp_path,pv_path,sol_them_path,chp_path,ele_bat_path,heat_tank_path,cool_tank_path,output_file):
    ng_boil=0
    ele_chill = 0
    ele_boil = 0
    abs_chill = 0
    ng_chill = 0
    hp = 0
    pv = 0
    sol_them = 0
    chp = 0
    ele_bat = 0
    heat_tank = 0
    cool_tank = 0
    if os.path.isfile(ng_boil_path):
        with open(ng_boil_path,'rU') as f:
            for line in f.readlines():
                ng_boil = round(float(line.strip()))

    if os.path.isfile(ele_chill_path):
        with open(ele_chill_path, 'rU') as f:
            for line in f.readlines():
                ele_chill = round(float(line.strip()))

    if os.path.isfile(ele_boil_path):
        with open(ele_boil_path,'rU') as f:
            for line in f.readlines():
                ele_boil = round(float(line.strip()))

    if os.path.isfile(abs_chill_path):
        with open(abs_chill_path, 'rU') as f:
            for line in f.readlines():
                abs_chill = round(float(line.strip()))

    if os.path.isfile(ng_chill_path):
        with open(ng_chill_path,'rU') as f:
            for line in f.readlines():
                ng_chill = round(float(line.strip()))

    if os.path.isfile(hp_path):
        with open(hp_path, 'rU') as f:
            for line in f.readlines():
                hp = round(float(line.strip()))

    if os.path.isfile(pv_path):
        with open(pv_path,'rU') as f:
            for line in f.readlines():
                pv = round(float(line.strip()))

    if os.path.isfile(sol_them_path):
        with open(sol_them_path, 'rU') as f:
            for line in f.readlines():
                sol_them = round(float(line.strip()))

    if os.path.isfile(chp_path):
        with open(chp_path,'rU') as f:
            for line in f.readlines():
                chp = round(float(line.strip()))

    if os.path.isfile(ele_bat_path):
        with open(ele_bat_path, 'rU') as f:
            for line in f.readlines():
                ele_bat = round(float(line.strip()))

    if os.path.isfile(heat_tank_path):
        with open(heat_tank_path,'rU') as f:
            for line in f.readlines():
                heat_tank = round(float(line.strip()))

    if os.path.isfile(cool_tank_path):
        with open(cool_tank_path, 'rU') as f:
            for line in f.readlines():
                cool_tank = round(float(line.strip()))

    # ng_boil = 0
    #ele_chill = 0
    #ele_boil = 0
    #abs_chill = 0
    #ng_chill = 0
    #hp = 0
    #pv = 0
    #sol_them = 0
    #chp = 0
    #ele_bat = 0
    #heat_tank = 0
    #cool_tank = 0

    x_data = []
    optimalVal = []

    if ng_boil != 0:
        x_data.append("NG Boiler")
        optimalVal.append(ng_boil)
    if ele_chill != 0:
        x_data.append("Electric chiller")
        optimalVal.append(ele_chill)

    if ele_boil != 0:
        x_data.append("Electric boiler")
        optimalVal.append(ele_boil)

    if abs_chill != 0:
        x_data.append("Absorption chiller")
        optimalVal.append(abs_chill)

    if ng_chill != 0:
        x_data.append("NG chiller")
        optimalVal.append(ng_chill)

    if hp != 0:
        x_data.append("Heat pump")
        optimalVal.append(hp)
    if pv != 0:
        x_data.append("PV")
        optimalVal.append(pv)

    if sol_them != 0:
        x_data.append("Solar thermal")
        optimalVal.append(sol_them)

    if chp != 0:
        x_data.append("Solar thermal")
        optimalVal.append(chp)

    if ele_bat != 0:
        x_data.append("Battery")
        optimalVal.append(ele_bat)

    if heat_tank != 0:
        x_data.append("Heat storage")
        optimalVal.append(heat_tank)

    if cool_tank != 0:
        x_data.append("Cool storage")
        optimalVal.append(cool_tank)

    #["锅炉","电制冷机","电锅炉","吸收式冷机","直燃制冷机","热泵","光伏发电","太阳能热水","天然气冷热电三联供","电池蓄电","蓄热","蓄热"]
    #[ng_boil,ele_chill,ele_boil,abs_chill,ng_chill,hp,pv,sol_them,chp,ele_bat,heat_tank,cool_tank]
    result = {}
    result['x_data'] = x_data
    result['optimal_value'] = optimalVal
    temp_base_value = []
    for i in result['optimal_value']:
        temp_base_value.append(round(i * random.uniform(0,2),2))
    result['baseline_value'] = temp_base_value

    with open(output_file, 'w+') as f:
        json.dump(result, f)

##### test
def test_get_load():
    ele_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/eleLoad.in";
    cool_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/coolLoad.in";
    heat_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Operation/heatLoad.in"
    out_file = './result/result_load.json'
    get_load(ele_path,cool_path,heat_path,out_file)
    # work_data, weekend_data = get_load_data_wkd_wkn(ele_path)
    # print("result:")
    # print(work_data)
    # print(weekend_data)

def test_get_costs():
    annual_cost_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/optimalValue.out"
    init_inv_cost_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/initInv.out"
    running_cost_ele_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/eleTotGrid_cost.out"
    running_cost_gas_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/NGtot.out"
    running_cost_mtn_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/mtn_cost.out"
    costs_output_file = "./result/result_costs.json"
    get_costs(annual_cost_path,init_inv_cost_path,running_cost_ele_path,running_cost_gas_path,running_cost_mtn_path,costs_output_file)

def test_get_costs_running():
    running_cost_ele_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/eleTotGrid_cost.out"
    running_cost_gas_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/NGtot.out"
    running_cost_mtn_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/mtn_cost.out"
    running_costs_output_file = "./result/result_running_costs.json"
    get_costs_running(running_cost_ele_path, running_cost_gas_path, running_cost_mtn_path, running_costs_output_file)

def test_energy_consumption():
    ele_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/eleTotGrid.out"
    gas_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/NGtot.out"
    out_file = "./result/result_consumption.json"
    get_energy_consumption(ele_path,gas_path,out_file)

def test_get_carto_emission():
    ele_em_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/totCarbEmi_ele.out"
    gas_em_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Optimal/totCarbEmi_ng.out"
    out_file = "./result/result_carbo_emission.json"
    get_carto_emission(ele_em_path, gas_em_path, out_file)

def test_get_capabilities():
    ng_boil_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Installation/capNGBoil.out"
    ele_chill_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Installation/capEleChill.out"
    ele_boil_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Installation/capEleBoil.out"
    abs_chill_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Installation/capABSChill.out"
    ng_chill_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Installation/capNGChill.out"
    hp_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Installation/capHP.out"
    pv_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Installation/capPV.out"
    sol_them_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Installation/capSolThem.out"
    chp_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Installation/totCapCHP.out"
    ele_bat_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Installation/capEleBat.out"
    heat_tank_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Installation/capHeatTank.out"
    cool_tank_path = "/home/tianqin/projects/berkeleyLab/resources/deep_ceg_lbnl_linux/demo/Outputs/Installation/capCoolTank.out"
    output_file = "./result/result_capabilities.json"
    get_capabilities(ng_boil_path, ele_chill_path, ele_boil_path, abs_chill_path, ng_chill_path, \
                     hp_path, pv_path, sol_them_path, chp_path, ele_bat_path, heat_tank_path, cool_tank_path,
                     output_file)

if __name__ == "__main__":

    #test_energy_consumption()
    #test_get_carto_emission()
    #test_get_capabilities()
    #test_get_costs()
    #test_get_costs_running()
    test_get_load()
    print("over!!")