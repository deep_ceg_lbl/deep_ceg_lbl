#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Short description of this Python module.
Longer description of this module.
This program is for: .
"""

__authors__ = ["Qin Tian<qtian.whu@gmail.com>", "Ming Jin<jinming@berkeley.edu>", "Wei Feng<weifeng@lbl.gov>"]
__contact__ = "Wei Feng"
__copyright__ = "Copyright 2019, CHINA ENERGY GROUP@LBNL"
__date__ = "19-1-25"
__deprecated__ = False
__email__ = "weifeng@lbl.gov"
__license__ = "GPLv3"
__maintainer__ = "Qin Tian, Ming Jin"
__status__ = "Production"
__version__ = "2.0.0"

import json


def covert(inputFile, outputFile):
    result = []
    with open(inputFile, 'r') as f:
        for line in f:
            result.append(round(float(line), 4))

    with open(outputFile, 'w') as outfile:
        json.dump({"radiance": result}, outfile)

if __name__ == "__main__":
    inputFile = "/home/tianqin/projects/berkeleyLab/workspaces/engine/refactoring/deep_ceg_lbl/data/data/irr_data/beijing_576.txt"
    outputFile = "./beijing_radiance.json"
    covert(inputFile, outputFile)