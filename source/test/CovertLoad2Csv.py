#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Short description of this Python module.
Longer description of this module.
This program is for: .
"""

__authors__ = ["Qin Tian<qtian.whu@gmail.com>", "Ming Jin<jinming@berkeley.edu>", "Wei Feng<weifeng@lbl.gov>"]
__contact__ = "Wei Feng"
__copyright__ = "Copyright 2019, CHINA ENERGY GROUP@LBNL"
__date__ = "19-1-25"
__deprecated__ = False
__email__ = "weifeng@lbl.gov"
__license__ = "GPLv3"
__maintainer__ = "Qin Tian, Ming Jin"
__status__ = "Production"
__version__ = "2.0.0"
import json
import os


def covert(com_cool_input, com_heat_input, com_ele_input, hotel_cool_input, hotel_heat_input, hotel_ele_input,
           off_cool_input, off_heat_input, off_ele_input, res_cool_input, res_heat_input, res_ele_input,
           output_file):
    com_cool_result = []
    com_heat_result = []
    com_ele_result = []

    hotel_cool_result = []
    hotel_heat_result = []
    hotel_ele_result = []

    off_cool_result = []
    off_heat_result = []
    off_ele_result = []

    res_cool_result = []
    res_heat_result = []
    res_ele_result = []

    with open(com_cool_input, 'r') as f:
        for line in f:
            com_cool_result.append(round(float(line), 4))

    with open(com_heat_input, 'r') as f:
        for line in f:
            com_heat_result.append(round(float(line), 4))

    with open(com_ele_input, 'r') as f:
        for line in f:
            com_ele_result.append(round(float(line), 4))

    with open(hotel_cool_input, 'r') as f:
        for line in f:
            hotel_cool_result.append(round(float(line), 4))
    with open(hotel_heat_input, 'r') as f:
        for line in f:
            hotel_heat_result.append(round(float(line), 4))
    with open(hotel_ele_input, 'r') as f:
        for line in f:
            hotel_ele_result.append(round(float(line), 4))

    with open(off_cool_input, 'r') as f:
        for line in f:
            off_cool_result.append(round(float(line), 4))
    with open(off_heat_input, 'r') as f:
        for line in f:
            off_heat_result.append(round(float(line), 4))
    with open(off_ele_input, 'r') as f:
        for line in f:
            off_ele_result.append(round(float(line), 4))

    with open(res_cool_input, 'r') as f:
        for line in f:
            res_cool_result.append(round(float(line), 4))
    with open(res_heat_input, 'r') as f:
        for line in f:
            res_heat_result.append(round(float(line), 4))
    with open(res_ele_input, 'r') as f:
        for line in f:
            res_ele_result.append(round(float(line), 4))

    data = {    "com_ele_load": com_ele_result,
                "com_cool_load": com_cool_result,
                "com_heat_load": com_heat_result,
                "off_ele_load": off_ele_result,
                "off_cool_load": off_cool_result,
                "off_heat_load": off_heat_result,
                "hotel_ele_load": hotel_ele_result,
                "hotel_cool_load": hotel_cool_result,
                "hotel_heat_load": hotel_heat_result,
                "res_ele_load": res_ele_result,
                "res_cool_load": res_cool_result,
                "res_heat_load": res_heat_result}

    print("output_file: ", output_file)
    with open(output_file, 'w+') as outfile:
        json.dump(data, outfile)


if __name__ == "__main__":
    base_directory = "/home/tianqin/projects/berkeleyLab/workspaces/engine/refactoring/deep_ceg_lbl/"
    com_cool_input_file = base_directory + "data/data/building_loads/beijing/com_coolLoad_576.in"
    com_heat_input_file = base_directory + "data/data/building_loads/beijing/com_eleLoad_576.in"
    com_ele_input_file = base_directory + "data/data/building_loads/beijing/com_heatLoad_576.in"

    hotel_cool_input_file = base_directory + "data/data/building_loads/beijing/hotel_coolLoad_576.in"
    hotel_heat_input_file = base_directory + "data/data/building_loads/beijing/hotel_heatLoad_576.in"
    hotel_ele_input_file = base_directory + "data/data/building_loads/beijing/hotel_eleLoad_576.in"

    off_cool_input_file = base_directory + "data/data/building_loads/beijing/off_coolLoad_576.in"
    off_heat_input_file = base_directory + "data/data/building_loads/beijing/off_heatLoad_576.in"
    off_ele_input_file = base_directory + "data/data/building_loads/beijing/off_eleLoad_576.in"

    res_cool_input_file = base_directory + "data/data/building_loads/beijing/res_coolLoad_576.in"
    res_heat_input_file = base_directory + "data/data/building_loads/beijing/res_heatLoad_576.in"
    res_ele_input_file = base_directory + "data/data/building_loads/beijing/res_eleLoad_576.in"
    outputFile = "./default_load.json"
    covert(com_cool_input_file, com_heat_input_file, com_ele_input_file, hotel_cool_input_file, hotel_heat_input_file,
           hotel_ele_input_file, off_cool_input_file, off_heat_input_file, off_ele_input_file, res_cool_input_file,
           res_heat_input_file, res_ele_input_file, outputFile)
