#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Short description of this Python module.
Longer description of this module.
This program is for: .
"""

__authors__ = ["Qin Tian<qtian.whu@gmail.com>", "Ming Jin<jinming@berkeley.edu>", "Wei Feng<weifeng@lbl.gov>"]
__contact__ = "Wei Feng"
__copyright__ = "Copyright 2019, CHINA ENERGY GROUP@LBNL"
__date__ = "19-1-11"
__deprecated__ = False
__email__ = "weifeng@lbl.gov"
__license__ = "GPLv3"
__maintainer__ = "Qin Tian, Ming Jin"
__status__ = "Production"
__version__ = "2.0.0"

import pandas as pd


def compute_monthly(solar_global_file, output_file, perf_ratio=0.75, sol_yield=0.15, area=100):
    """
    Compute how much energy generated from the solar.

    algorithm:
        energy_generated (kWh) = ['solIrradiation'] * ['perfRatio'] * ['solYield'] * ['area']

    :param solar_global_file:  Microsoft Excel table with the extension of .xlsx
    :param output_file:   path to a csv file
    :param perf_ratio: perfRatio denotes the performance ratio / irradiation efficiency,
    :param sol_yield: solYield is the solar panel yield
    :param area: area of the solar panel
    :return:  how much energy (kWh) will be generated.
    """

    original_data = pd.read_excel(solar_global_file)

    month_name = []
    solar_daily = []
    energy_monthly = []
    cols = original_data.columns
    for col in cols:
        month_name.append(col)
        solar_daily.append( original_data[col].sum() * 0.001)  #Wh/m^2/day
        energy_monthly.append(30 * original_data[col].sum() * perf_ratio * sol_yield * area * 0.001 / 16)

    data = pd.DataFrame({
        "Month": month_name,
        "Solar Radiation (kWh/m^2/day)": solar_daily,
        "AC Energy (kWh/100m^2/30day)": energy_monthly
    })

    data = data[["Month", "Solar Radiation (kWh/m^2/day)", "AC Energy (kWh/100m^2/30day)"]]

    data.to_csv(output_file)
    print(data)


if "__main__" == __name__:
    solar_path = "/home/tianqin/projects/berkeleyLab/solarData/beijing_global.xlsx"  # Wh/m^2
    output_path = "/home/tianqin/projects/berkeleyLab/solarData/beijing_energy.csv"  # Wh/m^2
    compute_monthly(solar_path, output_path)

