#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Short description of this Python module.
Longer description of this module.
This program is for: .
"""

__authors__ = ["Qin Tian<qtian.whu@gmail.com>", "Ming Jin<jinming@berkeley.edu>", "Wei Feng<weifeng@lbl.gov>"]
__contact__ = "Wei Feng"
__copyright__ = "Copyright 2018, CHINA ENERGY GROUP@LBNL"
__date__ = "18-12-18"
__deprecated__ = False
__email__ = "weifeng@lbl.gov"
__license__ = "GPLv3"
__maintainer__ = "Qin Tian, Ming Jin"
__status__ = "Production"
__version__ = "2.0.0"

import psycopg2
import logging
import time
import json
from configparser import ConfigParser
from utils import extract_result
from utils import email_util
import numpy as np
from model import deep_model


def config(filename='./database.ini', section='postgresql'):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)

    # get section, default to postgresql
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        logging.error('Section {0} not found in {1} file'.format(section, filename))
        raise Exception('Section {0} not found in {1} file'.format(section, filename))

    conn = psycopg2.connect(**db)

    return conn


def get_email_name_by_user_id(user_id):
    conn = config()
    with conn:
        with conn.cursor() as cur:
            cur.execute("SELECT user_email, user_name  FROM t_user WHERE user_id = '" + user_id + "'")
            if cur.rowcount == 1:
                row = cur.fetchall()[0]
                return row[0], row[1]


def get_email_name_by_project_id(project_id):
    conn = config()
    with conn:
        with conn.cursor() as cur:
            cur.execute("SELECT user_id FROM projects where project_id = " + str(project_id))
            if cur.rowcount == 1:
                user_id = cur.fetchall()[0][0]
                return get_email_name_by_user_id(user_id)


def insert_results(project_id, processing_time, loads, capabilities, costs, oper_costs, energy_cons, carbo_emission,
                   detail_resource_cons, detail_oper_costs, oper_data, oper_data_ele, oper_data_heat, oper_data_cool):

    conn = config()
    insert_sql = "INSERT INTO results (project_id, processing_time, loads,capability, costs, running_costs, consumption," \
                 " carbo_emission, detail_resource_cons, detail_running_costs, detail_running_data, detail_running_ele," \
                 " detail_running_heat, detail_running_cool ) VALUES ( " + \
                 str(project_id) + " , " + str(processing_time) +\
                 " , CAST('" + loads + "' AS jsonb), CAST('" + capabilities + "' AS jsonb), CAST('" + costs + \
                 "' AS jsonb), CAST('" + oper_costs + "'AS jsonb), CAST('" + energy_cons + "' AS jsonb), CAST('" + \
                 carbo_emission + "' AS jsonb), CAST('" + detail_resource_cons + "' AS jsonb), CAST('" + \
                 detail_oper_costs + "' AS jsonb), CAST('" + oper_data + "' AS jsonb), CAST('" + oper_data_ele + \
                 "' AS jsonb), CAST('" + oper_data_heat + "' AS jsonb), CAST('" + oper_data_cool + \
                 "' AS jsonb)" +")"

    # Update table projects, set its status = 2.
    update_sql = "UPDATE projects SET status = 2 WHERE project_id = " + str(project_id)

    with conn:
        with conn.cursor() as cur:
            cur.execute(insert_sql)
            #return cur.rowcount == 1
            if cur.rowcount == 1:
                cur.execute(update_sql)
                return True
            else:
                return False


def str2array(content):
    data = str(content).split(',')
    return np.array([float(d) for d in data])


def get_load(project_id, config_file):
    step_length = 576
    ele_load = np.zeros(step_length)
    cool_load = np.zeros(step_length)
    heat_load = np.zeros(step_length)

    conn = config(filename=config_file)
    with conn:
        with conn.cursor() as cur:
            cur.execute("SELECT com_area, com_ele_load, com_cool_load, com_heat_load, "
                        "off_area, off_ele_load, off_cool_load, off_heat_load,"
                        "hotel_area, hotel_ele_load, hotel_cool_load, hotel_heat_load,"
                        "res_area, res_ele_load, res_cool_load, res_heat_load FROM building WHERE project_id = " + str(project_id))
            rows = cur.fetchall()
            for row in rows:
                ele_load += float(row[0]) * str2array(row[1]) + float(row[4]) * str2array(row[5]) + float(row[8]) * str2array(row[9]) + float(row[12]) * str2array(row[13])
                cool_load += float(row[0]) * str2array(row[2]) + float(row[4]) * str2array(row[6]) + float(row[8]) * str2array(row[10]) + float(row[12]) * str2array(row[14])
                heat_load += float(row[0]) * str2array(row[3]) + float(row[4]) * str2array(row[7]) + float(row[8]) * str2array(row[11]) + float(row[12]) * str2array(row[15])
    return ele_load, cool_load, heat_load


def get_solar_radiance(project_id, config_file):
    conn = config(filename=config_file)
    with conn:
        with conn.cursor() as cur:
            cur.execute("SELECT radiance_576 FROM projects where project_id = " + str(project_id))
            rows = cur.fetchall()
            for row in rows:
                return str2array(str(row[0]).replace("\"", ""))


def get_ele_pur_price(project_id, config_file):
    conn = config(filename=config_file)
    with conn:
        with conn.cursor() as cur:
            cur.execute("SELECT ele_pur_price, ele_pur_price_high, ele_pur_price_med, ele_pur_price_low, ele_sell_price_grid FROM projects where project_id =  " + str(project_id))
            rows = cur.fetchall()
            for row in rows:
                return str2array(row[0]), str2array(row[1]), str2array(row[2]), str2array(row[3]), str2array(row[4])

def run(data, config_file, project_id):

    # loadFileFlag, loadFileName,pipeNetwFlag,parThemPipe,ctrStr,eleLoad,coolLoad,heatLoad,bigM,\
    # smallM,itemParCHP,itemParTime,itemLenLifeYr,itemCandidateTech1,itemCandidateTech2,forceSelect,itemParPV,\
    # itemParSolThem,itemParEleBoil,itemParNGBoil,itemParHeatTank,itemParCoolTank,itemParEleBat,\
    # itemParHP,itemParNGChill,itemParABSChill,itemParEleChill,itemParEleGrid,itemParHeat,rateMonthDemandCharge,\
    # lossCalMode,numbldgCluster,bldgHeatPercent,bldgCoolPercent,mainPipeLink,parMainPipe,parScndPipe,emiFac,\
    # taxCO2,parBasic,idxIniHeatLoad,idxIniCoolLoad,idxIniEleLoad = readPars.readPars()

    ele_load, cool_load, heat_load = get_load(project_id= project_id, config_file=config_file)

    item_par_time=data["itemParTime"]

    # solar radiance information
    solIrradiation = get_solar_radiance(project_id=project_id, config_file = config_file)

    if solIrradiation is None:
        print("Can not find solar irradiation data !!!!")
        return None

    ######### ele grid ####################
    item_par_ele_grid = data["itemParEleGrid"]

    dayPurPriceEleGrid, dayPurPriceEleGrid_high, dayPurPriceEleGrid_med, dayPurPriceEleGrid_low,\
    daySellPriceEleGrid = get_ele_pur_price(project_id=project_id, config_file=config_file)

    item_par_ele_grid['purPriceEleGrid'] = np.tile(dayPurPriceEleGrid, item_par_time['numMonth'] * item_par_time['numDay'])
    item_par_ele_grid['purPriceEleGrid_low'] = np.tile(dayPurPriceEleGrid_low, item_par_time['numMonth'] * item_par_time['numDay'])
    item_par_ele_grid['purPriceEleGrid_med'] = np.tile(dayPurPriceEleGrid_med, item_par_time['numMonth'] * item_par_time['numDay'])
    item_par_ele_grid['purPriceEleGrid_high'] = np.tile(dayPurPriceEleGrid_high, item_par_time['numMonth'] * item_par_time['numDay'])

    if item_par_ele_grid['outType'] == 2:
        item_par_ele_grid['salPriceEleGrid'] = np.tile(daySellPriceEleGrid, item_par_time['numMonth'] * item_par_time['numDay'])
    else:
        item_par_ele_grid['salPriceEleGrid'] = (
            np.zeros((item_par_time['numMonth'], item_par_time['numDay'], item_par_time['numHour']), dtype=np.float32))

    operSummary, instSummary, optimSummary, success_flag = deep_model.run(
          pipeNetwFlag=data["pipeNetwFlag"],
          parThemPipe=data["parThemPipe"],
          ctrStr=data["ctrStr"],
          eleLoad=ele_load,
          coolLoad=cool_load,
          heatLoad=heat_load,
          bigM=data["bigM"],
          smallM=data["smallM"],
          parCHP=data["itemParCHP"],
          parTime=data["itemParTime"],
          LenLifeYr=data["itemLenLifeYr"],
          CandidateTech1=data["itemCandidateTech1"],
          CandidateTech2=data["itemCandidateTech2"],
          forceSelect=data["forceSelect"],
          parPV=data["itemParPV"],
          parSolThem=data["itemParSolThem"],
          parEleBoil=data["itemParEleBoil"],
          parNGBoil=data["itemParNGBoil"],
          parHeatTank=data["itemParHeatTank"],
          parCoolTank=data["itemParCoolTank"],
          parEleBat=data["itemParEleBat"],
          parHP=data["itemParHP"],
          parNGChill=data["itemParNGChill"],
          parABSChill=data["itemParABSChill"],
          parEleChill=data["itemParEleChill"],
          parEleGrid=item_par_ele_grid,
          parHeat=data["itemParHeat"],
          rateMonthDemandCharge=data["rateMonthDemandCharge"],
          lossCalMode=data["lossCalMode"],
          numbldgCluster=data["numbldgCluster"],
          mainPipeLink=data["mainPipeLink"],
          parMainPipe=data["parMainPipe"],
          emiFac=data["emiFac"],
          taxCO2=data["taxCO2"],
          solIrradiation=solIrradiation,
          parBasic=data["parBasic"]
    )

    return operSummary, instSummary, optimSummary


def solve(project_id, project_name, params, configFile):
    """
    :param project_id:
    :param project_name:
    :param params:
    :return:
    """
    start_time = time.time()
    operSummary, instSummary, optimSummary = run(params, configFile, project_id)

    processing_time = time.time() - start_time

    # eleLoad, heatLoad, coolLoad
    loads = extract_result.get_load(operSummary['eleLoad'], operSummary['heatLoad'], operSummary['coolLoad'])
    capabilities = extract_result.get_capabilities(instSummary)
    # costs
    annual_cost = optimSummary['optimalValue']
    init_inv = optimSummary['initInv']
    oper_ele_cost = optimSummary['eleTotGrid_cost']
    oper_gas_cost = optimSummary['NGtot_cost']
    oper_mtn_cost = optimSummary['mtn_cost']
    costs = extract_result.get_costs(annual_cost, init_inv, oper_ele_cost, oper_gas_cost, oper_mtn_cost)

    oper_costs = extract_result.get_oper_costs(oper_ele_cost, oper_gas_cost, oper_mtn_cost)

    energy_cons = extract_result.get_energy_consumption(optimSummary['eleTotGrid'], optimSummary['NGtot'])

    carbo_emission = extract_result.get_carbo_emission(optimSummary['totCarbEmi_ng'], optimSummary['totCarbEmi_ele'])

    # detailed results
    resource_cons = extract_result.get_resouces_cons(optimSummary['ng_monthly'], optimSummary['ele_monthly'],
                                                     optimSummary['ele_monthly_low'], optimSummary['ele_monthly_med'],
                                                     optimSummary['ele_monthly_high'])
    detailed_oper_costs = extract_result.get_running_costs(extract_result.is_exists(optimSummary, 'ng_monthly_cost'),
                                                           extract_result.is_exists(optimSummary, 'ele_monthly_cost_high'),
                                                           extract_result.is_exists(optimSummary, 'ele_monthly_cost_low'),
                                                           extract_result.is_exists(optimSummary, 'ele_monthly_cost_med'),
                                                           extract_result.is_exists(optimSummary, 'valMaxDemand_month'),
                                                           extract_result.is_exists(optimSummary, 'heatInds_cost_month'))

    oper_data = extract_result.get_running_data(extract_result.is_running_data_exists(operSummary, 'eleLoad'),
                                                extract_result.is_running_data_exists(operSummary, 'heatLoad'),
                                                extract_result.is_running_data_exists(operSummary, 'coolLoad'),
                                                extract_result.is_running_data_exists(operSummary, 'eleEleBoil'),
                                                extract_result.is_running_data_exists(operSummary, 'eleEleChill'),
                                                extract_result.is_running_data_exists(operSummary, 'eleHP'),
                                                extract_result.is_running_data_exists(operSummary, 'stoEleBat'),
                                                extract_result.is_running_data_exists(operSummary, 'salEleGrid'),
                                                extract_result.is_running_data_exists(operSummary, 'elePV'),
                                                extract_result.is_running_data_exists(operSummary, 'totEleCHP'),
                                                extract_result.is_running_data_exists(operSummary, 'fromEleBat'),
                                                extract_result.is_running_data_exists(operSummary, 'purEleGrid'),
                                                extract_result.is_running_data_exists(operSummary, 'stoHeatTank'),
                                                extract_result.is_running_data_exists(operSummary, 'heatSal'),
                                                extract_result.is_running_data_exists(operSummary, 'heatNGBoil'),
                                                extract_result.is_running_data_exists(operSummary, 'heatEleBoil'),
                                                extract_result.is_running_data_exists(operSummary, 'totHeatCHP'),
                                                extract_result.is_running_data_exists(operSummary, 'heatSolThem'),
                                                extract_result.is_running_data_exists(operSummary, 'fromHeatTank'),
                                                extract_result.is_running_data_exists(operSummary, 'heatInds'),
                                                extract_result.is_running_data_exists(operSummary, 'stoCoolTank'),
                                                extract_result.is_running_data_exists(operSummary, 'coolHP'),
                                                extract_result.is_running_data_exists(operSummary, 'coolABSChill'),
                                                extract_result.is_running_data_exists(operSummary, 'coolEleChill'),
                                                extract_result.is_running_data_exists(operSummary, 'coolNGChill'),
                                                extract_result.is_running_data_exists(operSummary, 'fromCoolTank'))

    oper_data_ele = extract_result.get_running_data_ele(extract_result.is_exists(operSummary, 'eleLoad'),
                                                        extract_result.is_exists(operSummary, 'eleEleBoil'),
                                                        extract_result.is_exists(operSummary, 'eleEleChill'),
                                                        extract_result.is_exists(operSummary, 'eleHP'),
                                                        extract_result.is_exists(operSummary, 'stoEleBat'),
                                                        extract_result.is_exists(operSummary, 'salEleGrid'),
                                                        extract_result.is_exists(operSummary, 'elePV'),
                                                        extract_result.is_exists(operSummary, 'totEleCHP'),
                                                        extract_result.is_exists(operSummary, 'fromEleBat'),
                                                        extract_result.is_exists(operSummary, 'purEleGrid'))

    oper_data_heat = extract_result.get_running_data_heat(extract_result.is_exists(operSummary, 'heatLoad'),
                                                          extract_result.is_exists(operSummary, 'stoHeatTank'),
                                                          extract_result.is_exists(operSummary, 'heatSal'),
                                                          extract_result.is_exists(operSummary, 'heatABSChill'),
                                                          extract_result.is_exists(operSummary, 'heatNGBoil'),
                                                          extract_result.is_exists(operSummary, 'heatEleBoil'),
                                                          extract_result.is_exists(operSummary, 'totHeatCHP'),
                                                          extract_result.is_exists(operSummary, 'heatSolThem'),
                                                          extract_result.is_exists(operSummary, 'fromHeatTank'),
                                                          extract_result.is_exists(operSummary, 'heatInds'))

    oper_data_cool = extract_result.get_running_data_cool(extract_result.is_exists(operSummary, 'coolLoad'),
                                                          extract_result.is_exists(operSummary, 'stoCoolTank'),
                                                          extract_result.is_exists(operSummary, 'coolHP'),
                                                          extract_result.is_exists(operSummary, 'coolABSChill'),
                                                          extract_result.is_exists(operSummary, 'coolEleChill'),
                                                          extract_result.is_exists(operSummary, 'coolNGChill'),
                                                          extract_result.is_exists(operSummary, 'fromCoolTank'))

    is_inserted = insert_results(project_id, processing_time, json.dumps(loads), json.dumps(capabilities),
                                        json.dumps(costs), json.dumps(oper_costs),
                                        json.dumps(energy_cons), json.dumps(carbo_emission), json.dumps(resource_cons),
                                        json.dumps(detailed_oper_costs), json.dumps(oper_data),
                                        json.dumps(oper_data_ele),
                                        json.dumps(oper_data_heat), json.dumps(oper_data_cool))

    if is_inserted:
        email, user_name = get_email_name_by_project_id(project_id)
        email_util.send_success_email(email, "project processing notice -- success", user_name, project_name)


def connect(fileName):
    conn = config(filename=fileName)
    with conn:
        with conn.cursor() as cur:
            cur.execute("SELECT project_id, project_name, params FROM projects WHERE status  = 1 "
                        "ORDER BY priority DESC, submit_time ASC  limit 1")
            rows = cur.fetchall()
            for row in rows:
                logging.info(("solving---- project id:", row[0], "project name: ", row[1]))
                try:
                    solve(row[0], row[1], row[2], fileName)
                except Exception as e:
                    print(e)
                    cur.execute("UPDATE projects set status = 3 WHERE project_id = " + str(row[0]))
                    conn.commit()
                    email, user_name = get_email_name_by_project_id(row[0])
                    email_util.send_fail_email(email, "project processing notice -- fail", user_name, row[1])



if __name__ == '__main__':
    config_file="../database.ini"
    connect(config_file)

