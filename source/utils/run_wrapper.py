#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Short description of this Python module.
Longer description of this module.
This program is for: .
"""

__authors__ = ["Qin Tian<qtian.whu@gmail.com>", "Ming Jin<jinming@berkeley.edu>", "Wei Feng<weifeng@lbl.gov>"]
__contact__ = "Wei Feng"
__copyright__ = "Copyright 2018, CHINA ENERGY GROUP@LBNL"
__date__ = "18-12-18"
__deprecated__ = False
__email__ = "weifeng@lbl.gov"
__license__ = "GPLv3"
__maintainer__ = "Qin Tian, Ming Jin"
__status__ = "Production"
__version__ = "2.0.0"

import time

from model import deep_model

import logging
import json
import os
import numpy as np
import numpy.matlib as npmtlb

root_dir = os.path.dirname(os.getcwd())
print("root dir:" + root_dir)
root_dir = "/home/tianqin/projects/berkeleyLab/workspaces/engine/refactoring/deep_ceg_lbl"
print("root dir:" + root_dir)
currentPath = root_dir + '/data/parameters/'
currentPathData = root_dir + '/data/data/'

def get_load(parBasic, totHourStep, pipeNetwFlag):
    try:
        location = ['shenzhen', 'beijing', 'shanghai'][int(parBasic['location'])]
        loadPath = currentPathData + 'building_loads/' + location + '/'

        eleLoad_off = np.squeeze(np.loadtxt(loadPath + 'off_eleLoad_576.in', delimiter=','))
        eleLoad_com = np.squeeze(np.loadtxt(loadPath + 'com_eleLoad_576.in', delimiter=','))
        eleLoad_hotel = np.squeeze(np.loadtxt(loadPath + 'hotel_eleLoad_576.in', delimiter=','))
        eleLoad_res = np.squeeze(np.loadtxt(loadPath + 'res_eleLoad_576.in', delimiter=','))

        heatLoad_off = np.squeeze(np.loadtxt(loadPath + 'off_heatLoad_576.in', delimiter=','))
        heatLoad_com = np.squeeze(np.loadtxt(loadPath + 'com_heatLoad_576.in', delimiter=','))
        heatLoad_hotel = np.squeeze(np.loadtxt(loadPath + 'hotel_heatLoad_576.in', delimiter=','))

        heatLoad_res = np.squeeze(np.loadtxt(loadPath + 'res_heatLoad_576.in', delimiter=','))

        coolLoad_off = np.squeeze(np.loadtxt(loadPath + 'off_coolLoad_576.in', delimiter=','))
        coolLoad_com = np.squeeze(np.loadtxt(loadPath + 'com_coolLoad_576.in', delimiter=','))
        coolLoad_hotel = np.squeeze(np.loadtxt(loadPath + 'hotel_coolLoad_576.in', delimiter=','))
        coolLoad_res = np.squeeze(np.loadtxt(loadPath + 'res_coolLoad_576.in', delimiter=','))

        eleLoad = np.zeros(totHourStep)
        coolLoad = np.zeros(totHourStep)
        heatLoad = np.zeros(totHourStep)

        # Determine whether the pipeline network should be taken into account for thermal loss calculate
        # TODO: proble: pipeNetwFlag does not work at all here.
        if pipeNetwFlag == 0:
            com_area, hotel_area, off_area, res_area = np.sum(parBasic['com_area']), np.sum(
                parBasic['hotel_area']), np.sum(parBasic['off_area']), np.sum(parBasic['res_area'])
            eleLoad = (
                    eleLoad_com * com_area + eleLoad_hotel * hotel_area + eleLoad_off * off_area + eleLoad_res * res_area)
            heatLoad = (
                    heatLoad_com * com_area + heatLoad_hotel * hotel_area + heatLoad_off * off_area + heatLoad_res * res_area)
            coolLoad = (
                    coolLoad_com * com_area + coolLoad_hotel * hotel_area + coolLoad_off * off_area + coolLoad_res * res_area)

        else:

            # we store the building loads by orderly iterating over: office, commercial, hotel, residential
            for i in range(len(parBasic['off_area'])):
                eleLoad += eleLoad_off * parBasic['off_area'][i]
                heatLoad += heatLoad_off * parBasic['off_area'][i]
                coolLoad += coolLoad_off * parBasic['off_area'][i]
            ##
            for i in range(len(parBasic['com_area'])):
                eleLoad += eleLoad_com * parBasic['com_area'][i]
                heatLoad += heatLoad_com * parBasic['com_area'][i]
                coolLoad += coolLoad_com * parBasic['com_area'][i]
            ##
            for i in range(len(parBasic['hotel_area'])):
                eleLoad += eleLoad_hotel * parBasic['hotel_area'][i]
                heatLoad += heatLoad_hotel * parBasic['hotel_area'][i]
                coolLoad += coolLoad_hotel * parBasic['hotel_area'][i]
            ##
            for i in range(len(parBasic['res_area'])):
                eleLoad += eleLoad_res * parBasic['res_area'][i]
                heatLoad += heatLoad_res * parBasic['res_area'][i]
                coolLoad += coolLoad_res * parBasic['res_area'][i]

        return eleLoad, heatLoad, coolLoad

    except Exception as e:
        print(e)
        print("cannot handle loads")




def run(data):

    # loadFileFlag, loadFileName,pipeNetwFlag,parThemPipe,ctrStr,eleLoad,coolLoad,heatLoad,bigM,\
    # smallM,itemParCHP,itemParTime,itemLenLifeYr,itemCandidateTech1,itemCandidateTech2,forceSelect,itemParPV,\
    # itemParSolThem,itemParEleBoil,itemParNGBoil,itemParHeatTank,itemParCoolTank,itemParEleBat,\
    # itemParHP,itemParNGChill,itemParABSChill,itemParEleChill,itemParEleGrid,itemParHeat,rateMonthDemandCharge,\
    # lossCalMode,numbldgCluster,bldgHeatPercent,bldgCoolPercent,mainPipeLink,parMainPipe,parScndPipe,emiFac,\
    # taxCO2,parBasic,idxIniHeatLoad,idxIniCoolLoad,idxIniEleLoad = readPars.readPars()


    ele_load, heat_load, cool_load = get_load(data["parBasic"], 576, data["pipeNetwFlag"])


    item_par_time=data["itemParTime"]

    ########################### Solar irradiation #######################################################
    try:
        location = ['shenzhen', 'beijing', 'shanghai'][int(data["parBasic"]['location'])]
        if item_par_time['numDay'] == 2:  # 576 case
            assert (item_par_time['numMonth'] == 12 and item_par_time['numHour'] == 24)
            loadPath = currentPathData + 'irr_data/' + location + '_576.txt'
            daySolIrradiation = np.loadtxt(loadPath)
            solIrradiation = np.loadtxt(loadPath)
        else:
            assert (item_par_time['numDay'] == 30 and item_par_time['numMonth'] == 12 and item_par_time['numHour'] == 24)
            loadPath = currentPathData + 'irr_data/' + location + '.txt'
            daySolIrradiation = np.loadtxt(loadPath)
            solIrradiation = np.loadtxt(loadPath)
    except:
        print('Did not read out solar irradiation data!')


    ######### ele grid ####################
    item_par_ele_grid = data["itemParEleGrid"]

    dayPurPriceEleGrid = np.genfromtxt(currentPath + 'technology/dayPurPriceEleGrid.csv', delimiter=',')
    daySellPriceEleGrid = np.genfromtxt(currentPath + 'technology/daySellPriceEleGrid.csv', delimiter=',')
    dayPurPriceEleGrid_low = np.genfromtxt(currentPath + 'technology/dayPurPriceEleGrid_low.csv')
    dayPurPriceEleGrid_med = np.genfromtxt(currentPath + 'technology/dayPurPriceEleGrid_med.csv')
    dayPurPriceEleGrid_high = np.genfromtxt(currentPath + 'technology/dayPurPriceEleGrid_high.csv')

    item_par_ele_grid['purPriceEleGrid'] = npmtlb.repmat(dayPurPriceEleGrid[0: item_par_time['numHour']], 1,
                                                      item_par_time['numMonth'] * item_par_time['numDay'])
    item_par_ele_grid['purPriceEleGrid_low'] = npmtlb.repmat(dayPurPriceEleGrid_low[0: item_par_time['numHour']], 1,
                                                          item_par_time['numMonth'] * item_par_time['numDay'])
    item_par_ele_grid['purPriceEleGrid_med'] = npmtlb.repmat(dayPurPriceEleGrid_med[0: item_par_time['numHour']], 1,
                                                          item_par_time['numMonth'] * item_par_time['numDay'])
    item_par_ele_grid['purPriceEleGrid_high'] = npmtlb.repmat(dayPurPriceEleGrid_high[0: item_par_time['numHour']],
                                                           1, item_par_time['numMonth'] * item_par_time['numDay'])

    if item_par_ele_grid['outType'] == 2:
        item_par_ele_grid['salPriceEleGrid'] = npmtlb.repmat(daySellPriceEleGrid[0: item_par_time['numHour']], 1,
                                                          item_par_time['numMonth'] * item_par_time['numDay'])
    else:
        item_par_ele_grid['salPriceEleGrid'] = (
            np.zeros((item_par_time['numMonth'], item_par_time['numDay'], item_par_time['numHour']), dtype=np.float32))



    operSummary, instSummary, optimSummary, success_flag = deep_model.run(
          pipeNetwFlag=data["pipeNetwFlag"],
          parThemPipe=data["parThemPipe"],
          ctrStr=data["ctrStr"],
          eleLoad=ele_load,
          coolLoad=cool_load,
          heatLoad=heat_load,
          bigM=data["bigM"],
          smallM=data["smallM"],
          parCHP=data["itemParCHP"],
          parTime=data["itemParTime"],
          LenLifeYr=data["itemLenLifeYr"],
          CandidateTech1=data["itemCandidateTech1"],
          CandidateTech2=data["itemCandidateTech2"],
          forceSelect=data["forceSelect"],
          parPV=data["itemParPV"],
          parSolThem=data["itemParSolThem"],
          parEleBoil=data["itemParEleBoil"],
          parNGBoil=data["itemParNGBoil"],
          parHeatTank=data["itemParHeatTank"],
          parCoolTank=data["itemParCoolTank"],
          parEleBat=data["itemParEleBat"],
          parHP=data["itemParHP"],
          parNGChill=data["itemParNGChill"],
          parABSChill=data["itemParABSChill"],
          parEleChill=data["itemParEleChill"],
          parEleGrid=item_par_ele_grid,
          parHeat=data["itemParHeat"],
          rateMonthDemandCharge=data["rateMonthDemandCharge"],
          lossCalMode=data["lossCalMode"],
          numbldgCluster=data["numbldgCluster"],
          mainPipeLink=data["mainPipeLink"],
          parMainPipe=data["parMainPipe"],
          emiFac=data["emiFac"],
          taxCO2=data["taxCO2"],
          solIrradiation=solIrradiation,
          parBasic=data["parBasic"]
    )

    return operSummary,instSummary,optimSummary



