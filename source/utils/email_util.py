#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" This module is used to send emails.
Longer description of this module.
This program is for: .
"""

__authors__ = ["Qin Tian<qtian.whu@gmail.com>", "Ming Jin<jinming@berkeley.edu>", "Wei Feng<weifeng@lbl.gov>"]
__contact__ = "Wei Feng"
__copyright__ = "Copyright 2018, CHINA ENERGY GROUP@LBNL"
__date__ = "18-12-21"
__deprecated__ = False
__email__ = "weifeng@lbl.gov"
__license__ = "GPLv3"
__maintainer__ = "Qin Tian, Ming Jin"
__status__ = "Production"
__version__ = "2.0.0"

import smtplib
import logging
import traceback
import time

from string import Template
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

logging.basicConfig(level=logging.DEBUG)


def send_success_email(to, subject, user_name, project_name):
    sent_from = 'qtian_gis@163.com'
    mail_pass = 'whu501'

    body = "Dear $PERSON_NAME,\n\nYour project--$PROJECT_NAME--has be optimized successfully, please go back to "\
           "the system to find out more details. \n\nHave a good time! \n\nSincerely, \nDEEP group "

    try:
        server = smtplib.SMTP_SSL(host="smtp.163.com", port=465)
        server.ehlo()
        server.login(sent_from, mail_pass)

        msg = MIMEMultipart()

        message = Template(body).substitute(PERSON_NAME=user_name, PROJECT_NAME=project_name)

        print('msg: ' + message)

        msg['From'] = sent_from
        msg['To'] = to
        msg['Subject'] = subject

        # add in the message body
        msg.attach(MIMEText(message, 'plain'))

        server.send_message(msg)

        del msg

        # Terminate the SMTP session and close the connection
        server.quit()
        logging.info("Sent email to " + to + " successfully at " + time.strftime('%a, %d %b %Y %H:%M:%S GMT',
                                                                                time.localtime()))
    except Exception as e:
        print(e)
        print(traceback.format_exc())
        logging.error('Something went wrong while sending email')


def send_fail_email(to, subject, user_name, project_name):
    sent_from = 'qtian_gis@163.com'
    mail_pass = 'whu501'

    body = "Dear $PERSON_NAME,\n\n I'm sorry to tell you that we have failed to optimize your project--$PROJECT_NAME, please go back to "\
           "the system or contact the administrator to find out more details. \n\nHave a good time! \n\nSincerely, \nDEEP group "

    try:
        server = smtplib.SMTP_SSL(host="smtp.163.com", port=465)
        server.ehlo()
        server.login(sent_from, mail_pass)

        msg = MIMEMultipart()

        message = Template(body).substitute(PERSON_NAME=user_name, PROJECT_NAME=project_name)

        print('msg: ' + message)

        msg['From'] = sent_from
        msg['To'] = to
        msg['Subject'] = subject

        # add in the message body
        msg.attach(MIMEText(message, 'plain'))

        server.send_message(msg)

        del msg

        # Terminate the SMTP session and close the connection
        server.quit()
        logging.info("Sent email to " + to + " successfully at " + time.strftime('%a, %d %b %Y %H:%M:%S GMT',
                                                                                time.localtime()))
    except Exception as e:
        print(e)
        print(traceback.format_exc())
        logging.error('Something went wrong while sending email')








if __name__ == '__main__':
    to = 'tq.email@qq.com'
    subject = "Deep Result--Success"
    user_name = "Qin Tian"
    send_success_email(to, subject, user_name)




