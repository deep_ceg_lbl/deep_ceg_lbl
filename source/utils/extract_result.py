#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Extract important information we want from the result of the solution.
Longer description of this module.
This program is for: .
"""

__authors__ = ["Qin Tian<qtian.whu@gmail.com>", "Ming Jin<jinming@berkeley.edu>", "Wei Feng<weifeng@lbl.gov>"]
__contact__ = "Wei Feng"
__copyright__ = "Copyright 2018, CHINA ENERGY GROUP@LBNL"
__date__ = "18-12-21"
__deprecated__ = False
__email__ = "weifeng@lbl.gov"
__license__ = "GPLv3"
__maintainer__ = "Qin Tian, Ming Jin"
__status__ = "Production"
__version__ = "2.0.0"

import numpy as np


def is_running_data_exists(oper_summary, candi_tech):
    if candi_tech in oper_summary.keys():
        data = oper_summary[candi_tech]
        if isinstance(data, list):
            return data
        else:
            return data.tolist()
    else:
        return [0 for i in range(1, 577)]


def is_exists(optim_summary, cost_name):
    if cost_name in optim_summary.keys():
        data = optim_summary[cost_name]
        if isinstance(data, list):
            return data
        else:
            return data.tolist()
    else:
        return None

def get_load_data_wkd_wkn(load):
    workday_data = []
    weekend_data = []
    for i in range(0, len(load)):
        if (i // 24) % 2 == 0:
            workday_data.append(round(float(load[i]), 2))
        else:
            weekend_data.append(round(float(load[i]), 2))
    return workday_data, weekend_data


def get_load(ele_load, heat_load, cool_load):
    result = {}
    all_data = {}

    all_data['ele_load'] = ele_load.tolist()
    all_data['cool_load'] = cool_load.tolist()
    all_data['heat_load'] = heat_load.tolist()

    mon_num = {
        1: "Jan",
        2: "Feb",
        3: "Mar",
        4: "Apr",
        5: "May",
        6: "Jun",
        7: "Jul",
        8: "Aug",
        9: "Sep",
        10: "Oct",
        11: "Nov",
        12: "Dec"
    }

    x_data_all = []
    for i in range(1, 13):  # month
        for j in range(1, 3):  # workday or weekend
            for k in range(1, 25):  # hours
                if k == 1:
                    if j == 1:
                        x_data_all.append(mon_num.get(i) + "-workday")
                    elif j == 2:
                        x_data_all.append(mon_num.get(i) + "-weekend")
                else:
                    x_data_all.append(k)

    all_data['x_data'] = x_data_all
    result['all_data'] = all_data

    # Get workday data and weekend data.
    x_data_half = []
    for i in range(1, 12):  # month
        for k in range(1, 24):  # hours
            if k == 1:
                x_data_half.append(mon_num.get(i))
            else:
                x_data_half.append(k)

    workday_data = {}
    weekend_data = {}
    workday_data['ele_load'], weekend_data['ele_load'] = get_load_data_wkd_wkn(ele_load)
    workday_data['cool_load'], weekend_data['cool_load'] = get_load_data_wkd_wkn(cool_load)
    workday_data['heat_load'], weekend_data['heat_load'] = get_load_data_wkd_wkn(heat_load)
    workday_data['x_data'] = x_data_half
    weekend_data['x_data'] = x_data_half

    result['workday_data'] = workday_data
    result['weekend_data'] = weekend_data

    return result


def get_capabilities(instSummary):
    x_data = []
    optimalVal = []

    # ["areaPV", "areaSolThem", "capNGBoil", "capNGChill",
    #  "totCapCHP", "capEleChill", "capHP", "capABSChill", "capEleBoil", "capHeatTank", "capEleBat",
    #  "capCoolTank"]

    if 'capNGBoil'in instSummary.keys():
        x_data.append("NG Boiler")
        optimalVal.append(instSummary['capNGBoil'])

    if 'capEleChill'in instSummary.keys():
        x_data.append("Electric chiller")
        optimalVal.append(instSummary['capEleChill'])

    if'capEleBoil'in instSummary.keys():
        x_data.append("Electric boiler")
        optimalVal.append(instSummary['capEleBoil'])

    if 'capABSChill'in instSummary.keys():
        x_data.append("Absorption chiller")
        optimalVal.append(instSummary['capABSChill'])

    if 'capNGChill'in instSummary.keys():
        x_data.append("NG chiller")
        optimalVal.append(instSummary['capNGChill'])

    if 'capHP'in instSummary.keys():
        x_data.append("Heat pump")
        optimalVal.append(instSummary['capHP'])

    if 'areaPV'in instSummary.keys():
        x_data.append("PV")
        optimalVal.append(instSummary['areaPV'])

    if 'areaSolThem'in instSummary.keys():
        x_data.append("Solar thermal")
        optimalVal.append(instSummary['areaSolThem'])

    if 'capEleBat'in instSummary.keys():
        x_data.append("Battery")
        optimalVal.append(instSummary['capEleBat'])

    if 'capHeatTank'in instSummary.keys():
        x_data.append("Heat storage")
        optimalVal.append(instSummary['capHeatTank'])

    if 'capCoolTank'in instSummary.keys():
        x_data.append("Cool storage")
        optimalVal.append(instSummary['capCoolTank'])

    if 'totCapCHP'in instSummary.keys():
        x_data.append("CHP")
        optimalVal.append(instSummary['totCapCHP'])

    # ["锅炉","电制冷机","电锅炉","吸收式冷机","直燃制冷机","热泵","光伏发电","太阳能热水","天然气冷热电三联供","电池蓄电","蓄热","蓄热"]
    # [ng_boil,ele_chill,ele_boil,abs_chill,ng_chill,hp,pv,sol_them,chp,ele_bat,heat_tank,cool_tank]

    return {'x_data': x_data, 'optimal_value': optimalVal}


def get_costs(annual_cost, init_inv, oper_cost_ele, oper_cost_gas, oper_cost_mtn):

    running_cost = oper_cost_ele + oper_cost_gas + oper_cost_mtn

    #result = {}
    #["年化费用", "初投资费用", "运行费用"]
    #result['x_data']=["Annual cost","Initial investment","Operation cost"]
    #result['optimal_value'] = [annual_cost,init_inv,running_cost]

    return {
        'x_data': ["Annual cost","Initial investment","Operation cost"],
        'optimal_value': [annual_cost,init_inv,running_cost]
    }

# Operating costs
def get_oper_costs(oper_cost_ele, oper_cost_gas, oper_cost_mtn):
    # ["电费","燃气费","维护费"]
    return {
        'x_data': ["Electricity", "NG", "Maintenance"],
        'optimal_value': [oper_cost_ele, oper_cost_gas, oper_cost_mtn]
    }


def get_energy_consumption(ele_con, gas_con):
    # ["电量","燃气量"]
    # result['x_data']=["Electricity","NG"]
    # result['optimal_value'] = [ele_con,gas_con]
    return {
        'x_data': ["Electricity","NG"],
        'optimal_value': [ele_con,gas_con]
    }


# carbo_emission
def get_carbo_emission(ele_em, gas_em):

    # result={}
    # ["电力碳排放","燃气碳排放"]
    # result['x_data']=["From electricity","From NG"]
    # result['optimal_value'] = [ele_emission,gas_emission]
    return {
        'x_data': ["From electricity", "From NG"],
        'optimal_value': [ele_em, gas_em]
    }


# ------------ Detailed results  ----------------

# Resource consumptions
def get_resouces_cons(gas_cons,ele_cons, ele_low_cons, ele_med_cons, ele_high_cons):

    return {
        'x_data': ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        'gas_cons': gas_cons.tolist(),
        'ele_cons': ele_cons.tolist(),
        'ele_low_cons': ele_low_cons.tolist(),
        'ele_med_cons': ele_med_cons.tolist(),
        'ele_high_cons': ele_high_cons.tolist()
    }


def get_running_costs(gas_cost, ele_high_cost, ele_low_cost, ele_med_cost, max_demand_cost, heat_inds_cost):

    result = {}
    legend = []
    result['x_data'] = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    y_data = []
    if gas_cost is not None and len(gas_cost) == 12:
        result['gas_cost'] = gas_cost
        legend.append("NG")
        y_data.append(gas_cost)
    if ele_high_cost is not None and len(ele_high_cost) == 12:
        result['ele_high_cost'] = ele_high_cost
        legend.append("Peak electricity")
        y_data.append(ele_high_cost)
    if ele_low_cost is not None and len(ele_low_cost) == 12:
        result['ele_low_cost'] = ele_low_cost
        legend.append("Valley electricity")
        y_data.append(ele_low_cost)
    if ele_med_cost is not None and len(ele_med_cost) == 12:
        result['ele_med_cost'] = ele_med_cost
        legend.append("Other electricity")
        y_data.append(ele_med_cost)
    if max_demand_cost is not None and len(max_demand_cost) == 12:
        result['max_demand_cost'] = max_demand_cost
        legend.append("Demand charge")
        y_data.append(max_demand_cost)
    if heat_inds_cost is not None and  len(heat_inds_cost) == 12:
        result['heat_inds_cost'] = heat_inds_cost
        legend.append("Heat")
        y_data.append(heat_inds_cost)

    result['legend'] = legend
    result['y_data'] = y_data
    return result




def get_running_data(ele_load, heat_load, cool_load,
                     ele_load_boil, ele_load_chill, ele_load_hp, ele_load_sto_bat, ele_load_sal_grid, ele_load_pv,
                     ele_load_chp, ele_load_from_ele_bat,ele_load_pur_ele_grid,\
                     heat_load_sto_heat_tank, heat_load_heat_sal, heat_load_heat_ngboil, heat_load_heat_eleboil,\
                     heat_load_to_heat_chp, heat_load_heat_solthem, heat_load_from_heat_tank, heat_load_heat_inds,\
                     cool_load_sto_cool_tank, cool_load_cool_hp, cool_load_abschill, cool_load_elechill,
                     cool_load_ngchill, cool_load_from_cool_tank):
    # ele_load=[] #电负荷
    # heat_load = [] #热负荷
    # cool_load = []  #冷负荷

    # ele_load_boil = [] #电力需求 锅炉耗电
    # ele_load_chill = [] #电力需求 冷机耗电
    # ele_load_hp = [] # 电力需求 热泵耗电
    # ele_load_sto_bat = [] #电力需求 电池储电
    # ele_load_sal_grid = [] #电力需求 向电网卖电
    # ele_load_pv = [] #电力供给 光伏发电
    # ele_load_chp = [] #电力供给 CHP发电
    # ele_load_from_ele_bat = [] #电力供给 电池放电
    # ele_load_pur_ele_grid = [] #电力供给 电网供电

    # heat_load_sto_heat_tank = [] #热力需求 蓄热罐储热
    # heat_load_heat_sal = [] #热力需求 卖给热网
    # heat_load_heat_ngboil = [] #热力供给 燃气锅炉
    # heat_load_heat_eleboil = [] #热力供给 电锅炉
    # heat_load_to_heat_chp = []#热力供给 CHP
    # heat_load_heat_solthem = [] #热力供给 太阳能热水
    # heat_load_from_heat_tank = [] #热力供给 蓄热罐释放
    # heat_load_heat_inds = [] #热网供热

    # cool_load_sto_cool_tank = [] #冷量需求 蓄冷罐储冷
    # cool_load_cool_hp = [] #冷量供给 热泵
    # cool_load_abschill = [] #冷量供给 吸收式冷机
    # cool_load_elechill = [] #冷量供给 电制冷机
    # cool_load_ngchill = [] #冷量供给 天然气冷机
    # cool_load_from_cool_tank = [] #冷量供给 冷罐释放

    x_data = []
    for i in range(1, 577):
        x_data.append(i)

    result = {}
    result['x_data'] = x_data
    result['ele_load_total'] = ele_load
    result['heat_load_total'] = heat_load
    result['cool_load_total'] = cool_load


    ele_load_value_supply={}
    ele_load_value_demand={}
    ele_load_value_demand['ele_ele_boil'] = ele_load_boil
    ele_load_value_demand['ele_ele_chill'] = ele_load_chill
    ele_load_value_demand['ele_hp'] = ele_load_hp
    ele_load_value_demand['sto_ele_bat'] = ele_load_sto_bat
    ele_load_value_demand['sal_ele_grid'] = ele_load_sal_grid
    ele_demand_sub_total_arr = np.array(ele_load_boil) + np.array(ele_load_chill) + np.array(ele_load_hp) + np.array(ele_load_sto_bat) + \
        np.array(ele_load_sal_grid)
    ele_load_value_demand['sub_total'] = [round(i, 2) for i in ele_demand_sub_total_arr.tolist()]

    ele_load_value_supply['ele_pv'] = (np.array(ele_load_pv)*(-1)).tolist()
    ele_load_value_supply['tot_ele_chp'] = (np.array(ele_load_chp)*(-1)).tolist()
    ele_load_value_supply['from_ele_bat'] = (np.array(ele_load_from_ele_bat)*(-1)).tolist()
    ele_load_value_supply['pur_ele_grid'] = (np.array(ele_load_pur_ele_grid)*(-1)).tolist()
    ele_suppy_sub_total_arr = np.array(ele_load_pv) + np.array(ele_load_chp) + np.array(ele_load_from_ele_bat) + np.array(ele_load_pur_ele_grid)
    ele_load_value_supply['sub_total'] = [round(i, 2) for i in ele_suppy_sub_total_arr.tolist()]

    result['ele_load_demand'] = ele_load_value_demand
    result['ele_load_supply'] = ele_load_value_supply

    heat_load_value_demand = {}
    heat_load_value_supply = {}
    heat_load_value_demand['sto_heat_tank'] = heat_load_sto_heat_tank
    heat_load_value_demand['heat_sal'] = heat_load_heat_sal
    heat_demand_sub_total_arr = np.array(heat_load_sto_heat_tank) + np.array(heat_load_heat_sal)
    heat_load_value_demand['sub_total'] = [round(i, 2) for i in heat_demand_sub_total_arr.tolist()]

    heat_load_value_supply['heat_ng_boil'] = heat_load_heat_ngboil
    heat_load_value_supply['heat_ele_boil'] = heat_load_heat_eleboil
    heat_load_value_supply['tot_heat_chp'] = heat_load_to_heat_chp
    heat_load_value_supply['heat_sol_them'] = heat_load_heat_solthem
    heat_load_value_supply['from_heat_tank'] = heat_load_from_heat_tank
    heat_load_value_supply['heat_load_heat_inds'] = heat_load_heat_inds
    heat_supply_sub_total_arr = np.array(heat_load_heat_ngboil) + np.array(heat_load_heat_eleboil) + np.array(heat_load_to_heat_chp)+\
        np.array(heat_load_heat_solthem) + np.array(heat_load_from_heat_tank)+np.array(heat_load_heat_inds)
    heat_load_value_supply['sub_total'] = [round(i, 2) for i in heat_supply_sub_total_arr.tolist()]
    result['heat_load_demand'] = heat_load_value_demand
    result['heat_load_supply'] = heat_load_value_supply


    cool_load_value_demand = {}
    cool_load_value_supply = {}
    cool_load_value_demand['sto_cool_tank'] = cool_load_sto_cool_tank
    cool_load_value_demand['sub_total'] = [round(i, 2) for i in cool_load_sto_cool_tank]

    cool_load_value_supply['cool_hp'] = cool_load_cool_hp
    cool_load_value_supply['cool_abs_chill'] = cool_load_abschill
    cool_load_value_supply['cool_ele_chill'] = cool_load_elechill
    cool_load_value_supply['cool_ng_chill'] = cool_load_ngchill
    cool_load_value_supply['from_cool_tank'] = cool_load_from_cool_tank
    cool_supply_sub_total_arr = np.array(cool_load_cool_hp) + np.array(cool_load_abschill) + np.array(cool_load_elechill) \
                                + np.array(cool_load_ngchill) + np.array(cool_load_from_cool_tank)
    cool_load_value_supply['sub_total'] = [round(i, 2) for i in cool_supply_sub_total_arr.tolist()]

    result['cool_load_demand'] = cool_load_value_demand
    result['cool_load_supply'] = cool_load_value_supply

    return result




def get_running_data_ele(ele_load_ele, ele_load_boil, ele_load_chill, ele_load_hp,\
                        ele_load_sto_bat, ele_load_sal_grid, ele_load_pv, ele_load_chp, ele_load_from_ele_bat,\
                        ele_load_pur_ele_grid):

    """
    Extract the operation information about electricity load.
    """
    # ele_load_ele = [] #电力需求 电负荷
    # ele_load_boil = [] #电力需求 锅炉耗电
    # ele_load_chill = [] #电力需求 冷机耗电
    # ele_load_hp = [] # 电力需求 热泵耗电
    # ele_load_sto_bat = [] #电力需求 电池储电
    # ele_load_sal_grid = [] #电力需求 向电网卖电
    #
    # ele_load_pv = [] #电力供给 光伏发电
    # ele_load_chp = [] #电力供给 CHP发电
    # ele_load_from_ele_bat = [] #电力供给 电池放电
    # ele_load_pur_ele_grid = [] #电力供给 电网供电

    legend = []
    ele_load = []
    category = []

    if ele_load_ele is not None:
        ele_load_ele = [round(float(i), 2) for i in ele_load_ele]
        legend.append("Electric load")
        ele_load.append((np.array(ele_load_ele)*(-1)).tolist())
        category.append("demand")

    if ele_load_boil is not None:
        ele_load_boil = [round(float(i), 2) for i in ele_load_boil]
        legend.append("Electric boiler")
        ele_load.append((np.array(ele_load_boil)*(-1)).tolist())
        category.append("demand")

    if ele_load_chill is not None:
        ele_load_chill = [round(float(i), 2) for i in ele_load_chill]
        legend.append("Electric chiller")
        ele_load.append((np.array(ele_load_chill) * (-1)).tolist())
        category.append("demand")

    if ele_load_hp is not None:
        ele_load_hp = [round(float(i), 2) for i in ele_load_hp]
        legend.append("Heat pump")
        ele_load.append((np.array(ele_load_hp) * (-1)).tolist())
        category.append("demand")

    if ele_load_sto_bat is not None:
        ele_load_sto_bat = [round(float(i), 2) for i in ele_load_sto_bat]
        legend.append("Battery charge")
        ele_load.append((np.array(ele_load_sto_bat) * (-1)).tolist())
        category.append("demand")

    if ele_load_sal_grid is not None:
        ele_load_sal_grid = [round(float(i), 2) for i in ele_load_sal_grid]
        legend.append("Grid export")
        ele_load.append((np.array(ele_load_sal_grid) * (-1)).tolist())
        category.append("demand")

    if ele_load_pv is not None:
        ele_load_pv = [round(float(i), 2) for i in ele_load_pv]
        legend.append("PV")
        ele_load.append(ele_load_pv)
        category.append("supply")

    if ele_load_chp is not None:
        ele_load_chp = [round(float(i), 2) for i in ele_load_chp]
        legend.append("CHP")
        ele_load.append(ele_load_chp)
        category.append("supply")

    if ele_load_from_ele_bat is not None:
        ele_load_from_ele_bat = [round(float(i), 2) for i in ele_load_from_ele_bat]
        legend.append("Battery discharge")
        ele_load.append(ele_load_from_ele_bat)
        category.append("supply")

    if ele_load_pur_ele_grid is not None:
        ele_load_pur_ele_grid = [round(float(i), 2) for i in ele_load_pur_ele_grid]
        legend.append("Grid import")
        ele_load.append(ele_load_pur_ele_grid)
        category.append("supply")

    x_data = []
    mon_num = {
        1: "Jan",
        2: "Feb",
        3: "Mar",
        4: "Apr",
        5: "May",
        6: "Jun",
        7: "Jul",
        8: "Aug",
        9: "Sep",
        10: "Oct",
        11: "Nov",
        12: "Dec"
    }

    for i in range(1, 13):  # month
        for j in range(1, 3):  # workday or weekend
            for k in range(1, 25):  # hours
                if k == 1:
                    if j == 1:
                        x_data.append(mon_num.get(i) + "-workday")
                    elif j == 2:
                        x_data.append(mon_num.get(i) + "-weekend")
                else:
                    x_data.append(k)

    return {
        'x_data': x_data,
        'legend': legend,
        'ele_load': ele_load,
        'category': category
    }

def get_running_data_heat(heat_load_heat, heat_load_sto_heat_tank, heat_load_heat_sal, heat_load_abs_chiller,
                          heat_load_heat_ngboil, heat_load_heat_eleboil, heat_load_to_heat_chp,
                          heat_load_heat_solthem, heat_load_from_heat_tank, heat_load_heat_inds):

    # heat_load_heat = []  #热力需求 热负荷
    # heat_load_sto_heat_tank = [] #热力需求 蓄热罐储热
    # heat_load_heat_sal = [] #热力需求 卖给热网
    # heat_load_abs_chiller=[]#热力需求  abs chiller
    #
    # heat_load_heat_ngboil = [] #热力供给 燃气锅炉
    # heat_load_heat_eleboil = [] #热力供给 电锅炉
    # heat_load_to_heat_chp = []#热力供给 CHP
    # heat_load_heat_solthem = [] #热力供给 太阳能热水
    # heat_load_from_heat_tank = [] #热力供给 蓄热罐释放
    # heat_load_heat_inds = [] #热网供热

    legend = []
    category = []
    y_data = []

    if heat_load_heat is not None:
        heat_load_heat = [round(float(i), 2) for i in heat_load_heat]
        legend.append("Heat load")
        category.append("demand")
        y_data.append((np.array(heat_load_heat) * (-1)).tolist())

    if heat_load_sto_heat_tank is not None:
        heat_load_sto_heat_tank = [round(float(i), 2) for i in heat_load_sto_heat_tank]
        legend.append("Storage charge")
        category.append("demand")
        y_data.append((np.array(heat_load_sto_heat_tank)*(-1)).tolist())

    if heat_load_heat_sal is not None:
        heat_load_heat_sal = [round(float(i), 2) for i in heat_load_heat_sal]
        legend.append("Heat export")
        category.append("demand")
        y_data.append((np.array(heat_load_heat_sal)*(-1)).tolist())

    if heat_load_abs_chiller is not None:
        heat_load_abs_chiller = [round(float(i), 2) for i in heat_load_abs_chiller]
        legend.append("Absorption Chiller")
        category.append("demand")
        y_data.append((np.array(heat_load_abs_chiller)*(-1)).tolist())

    if heat_load_heat_ngboil is not None:
        heat_load_heat_ngboil = [round(float(i), 2) for i in heat_load_heat_ngboil]
        legend.append("NG boiler")
        category.append("supply")
        y_data.append(heat_load_heat_ngboil)

    if heat_load_heat_eleboil is not None:
        heat_load_heat_eleboil = [round(float(i), 2) for i in heat_load_heat_eleboil]
        legend.append("Electric boiler")
        category.append("supply")
        y_data.append(heat_load_heat_eleboil)

    if heat_load_to_heat_chp is not None:
        heat_load_to_heat_chp = [round(float(i), 2) for i in heat_load_to_heat_chp]
        legend.append("CHP")
        category.append("supply")
        y_data.append(heat_load_to_heat_chp)

    if heat_load_heat_solthem is not None:
        heat_load_heat_solthem = [round(float(i), 2) for i in heat_load_heat_solthem]
        legend.append("Solar thermal")
        category.append("supply")
        y_data.append(heat_load_heat_solthem)

    if heat_load_from_heat_tank is not None:
        heat_load_from_heat_tank = [round(float(i), 2) for i in heat_load_from_heat_tank]
        legend.append("Storage discharge")
        category.append("supply")
        y_data.append(heat_load_from_heat_tank)

    if heat_load_heat_inds is not None:
        heat_load_heat_inds = [round(float(i), 2) for i in heat_load_heat_inds]
        legend.append("Heat import")
        category.append("supply")
        y_data.append(heat_load_heat_inds)

    x_data = []
    mon_num = {
        1: "Jan",
        2: "Feb",
        3: "Mar",
        4: "Apr",
        5: "May",
        6: "Jun",
        7: "Jul",
        8: "Aug",
        9: "Sep",
        10: "Oct",
        11: "Nov",
        12: "Dec"
    }
    for i in range(1, 13):  # month
        for j in range(1, 3):  # workday or weekend
            for k in range(1, 25):  # hours
                if k == 1:
                    if j == 1:
                        x_data.append(mon_num.get(i) + "-workday")
                    elif j == 2:
                        x_data.append(mon_num.get(i) + "-weekend")
                else:
                    x_data.append(k)

    return {
        'x_data': x_data,
        'y_data': y_data,
        'category': category,
        'legend': legend
    }

def get_running_data_cool(cool_load_cool, cool_load_sto_cool_tank, cool_load_cool_hp, cool_load_abschill,
                          cool_load_elechill, cool_load_ngchill, cool_load_from_cool_tank):


    # cool_load_cool = [] #冷量需求 冷负荷
    # cool_load_sto_cool_tank = [] #冷量需求 蓄冷罐储冷
    # cool_load_cool_hp = [] #冷量供给 热泵
    # cool_load_abschill = [] #冷量供给 吸收式冷机
    # cool_load_elechill = [] #冷量供给 电制冷机
    # cool_load_ngchill = [] #冷量供给 天然气冷机
    # cool_load_from_cool_tank = [] #冷量供给 冷罐释放

    legend = []
    y_data = []
    category = []

    if cool_load_cool is not None:
        cool_load_cool = [round(float(i), 2) for i in cool_load_cool]
        legend.append("Cooling load")
        category.append("demand")
        y_data.append((np.array(cool_load_cool)*(-1)).tolist())

    if cool_load_sto_cool_tank is not None:
        cool_load_sto_cool_tank = [round(float(i), 2) for i in cool_load_sto_cool_tank]
        legend.append("Storage charge")
        category.append("demand")
        y_data.append(np.array((cool_load_sto_cool_tank)*(-1)).tolist())

    if cool_load_cool_hp is not None:
        cool_load_cool_hp = [round(float(i), 2) for i in cool_load_cool_hp]
        legend.append("Heat pump")
        category.append("supply")
        y_data.append(cool_load_cool_hp)

    if cool_load_abschill is not None:
        cool_load_abschill = [round(float(i), 2) for i in cool_load_abschill]
        legend.append("Absorption chiller")
        category.append("supply")
        y_data.append(cool_load_abschill)

    if cool_load_elechill is not None:
        cool_load_elechill = [round(float(i), 2) for i in cool_load_elechill]
        legend.append("Electric chiller")
        category.append("supply")
        y_data.append(cool_load_elechill)

    if cool_load_ngchill is not None:
        cool_load_ngchill = [round(float(i), 2) for i in cool_load_ngchill]
        legend.append("NG chiller")
        category.append("supply")
        y_data.append(cool_load_ngchill)

    if cool_load_from_cool_tank is not None:
        cool_load_from_cool_tank = [round(float(i), 2) for i in cool_load_from_cool_tank]
        legend.append("Storage discharge")
        category.append("supply")
        y_data.append(cool_load_from_cool_tank)

    mon_num = {
        1: "Jan",
        2: "Feb",
        3: "Mar",
        4: "Apr",
        5: "May",
        6: "Jun",
        7: "Jul",
        8: "Aug",
        9: "Sep",
        10: "Oct",
        11: "Nov",
        12: "Dec"
    }

    x_data = []
    for i in range(1, 13):  # month
        for j in range(1, 3):  # workday or weekend
            for k in range(1, 25):  # hours
                if k == 1:
                    if j == 1:
                        x_data.append(mon_num.get(i) + "-workday")
                    elif j == 2:
                        x_data.append(mon_num.get(i) + "-weekend")
                else:
                    x_data.append(k)

    return {
        'x_data': x_data,
        "y_data": y_data,
        "legend": legend,
        "category": category
    }
