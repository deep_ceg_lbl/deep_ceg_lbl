# -*- coding: utf-8 -*-
#!/usr/bin/env python

""" Short description of this Python module.
Longer description of this module.
This program is for: .
"""

__authors__ = ["Qin Tian<qtian.whu@gmail.com>", "Ming Jin<jinming@berkeley.edu>", "Wei Feng<weifeng@lbl.gov>"]
__contact__ = "Wei Feng"
__copyright__ = "Copyright 2018, CHINA ENERGY GROUP@LBNL"
__date__ = "18-1-22"
__deprecated__ = False
__email__ = "weifeng@lbl.gov"
__license__ = "GPLv3"
__maintainer__ = "Qin Tian, Ming Jing"
__status__ = "Production"
__version__ = "2.0.0"

import time

from model import deep_model

import logging
import json
import os
from utils import run_wrapper
from db import run4db
from utils import extract_result
from utils import email_util

root_dir = os.path.dirname(os.getcwd())
logging.basicConfig(level=logging.INFO)


def is_running_data_exists(oper_summary, candi_tech):
    if candi_tech in oper_summary.keys():
        data = oper_summary[candi_tech]
        if isinstance(data, list):
            return data
        else:
            return data.tolist()
    else:
        return [0 for i in range(1, 577)]


def is_exists(optim_summary, cost_name):
    if cost_name in optim_summary.keys():
        data = optim_summary[cost_name]
        if isinstance(data, list):
            return data
        else:
            return data.tolist()
    else:
        return None


def run():

    data_path = "/home/tianqin/projects/berkeleyLab/resources/test/data1.json"
    default_dat = root_dir + "/data/data.json"
    with open(default_dat) as f:
        data = json.load(f)

    #start_time = time.time()
    operSummary, instSummary, optimSummary =run_wrapper.run(data)

    #processing_time = time.time() -  start_time

    # # eleLoad, heatLoad, coolLoad
    # loads = extract_result.get_load(operSummary['eleLoad'], operSummary['heatLoad'], operSummary['coolLoad'])
    # capabilities = extract_result.get_capabilities(instSummary)
    # #costs
    # annual_cost = optimSummary['optimalValue']
    # init_inv = optimSummary['initInv']
    # oper_ele_cost = optimSummary['eleTotGrid_cost']
    # oper_gas_cost = optimSummary['NGtot_cost']
    # oper_mtn_cost = optimSummary['mtn_cost']
    # costs = extract_result.get_costs(annual_cost, init_inv, oper_ele_cost, oper_gas_cost, oper_mtn_cost)
    #
    # oper_costs = extract_result.get_oper_costs(oper_ele_cost, oper_gas_cost, oper_mtn_cost)
    #
    # energy_cons = extract_result.get_energy_consumption(optimSummary['eleTotGrid'], optimSummary['NGtot'])
    #
    # carbo_emission = extract_result.get_carbo_emission(optimSummary['totCarbEmi_ng'], optimSummary['totCarbEmi_ele'])
    #
    # # detailed results
    # resource_cons = extract_result.get_resouces_cons(optimSummary['ng_monthly'], optimSummary['ele_monthly'],
    #                                   optimSummary['ele_monthly_low'],  optimSummary['ele_monthly_med'],
    #                                   optimSummary['ele_monthly_high'])
    # detailed_oper_costs = extract_result.get_running_costs(is_exists(optimSummary, 'ng_monthly_cost'),
    #                                                        is_exists(optimSummary, 'ele_monthly_cost_high'),
    #                                                        is_exists(optimSummary, 'ele_monthly_cost_low'),
    #                                                        is_exists(optimSummary, 'ele_monthly_cost_med'),
    #                                                        is_exists(optimSummary, 'valMaxDemand_month'),
    #                                                        is_exists(optimSummary, 'heatInds_cost_month'))
    #
    # oper_data = extract_result.get_running_data(is_running_data_exists(operSummary,'eleLoad'), is_running_data_exists(operSummary, 'heatLoad'),
    #                              is_running_data_exists(operSummary, 'coolLoad'), is_running_data_exists(operSummary, 'eleEleBoil'),
    #                              is_running_data_exists(operSummary, 'eleEleChill'),
    #                              is_running_data_exists(operSummary, 'eleHP'),
    #                              is_running_data_exists(operSummary, 'stoEleBat'), is_running_data_exists(operSummary, 'salEleGrid'),
    #                              is_running_data_exists(operSummary, 'elePV'),
    #                              is_running_data_exists(operSummary, 'totEleCHP'), is_running_data_exists(operSummary, 'fromEleBat'),
    #                              is_running_data_exists(operSummary, 'purEleGrid'),
    #                              is_running_data_exists(operSummary, 'stoHeatTank'), is_running_data_exists(operSummary, 'heatSal'),
    #                              is_running_data_exists(operSummary, 'heatNGBoil'),
    #                              is_running_data_exists(operSummary, 'heatEleBoil'),
    #                              is_running_data_exists(operSummary,'totHeatCHP'), is_running_data_exists(operSummary, 'heatSolThem'),
    #                              is_running_data_exists(operSummary, 'fromHeatTank'), is_running_data_exists(operSummary, 'heatInds'),
    #                              is_running_data_exists(operSummary, 'stoCoolTank'),
    #                              is_running_data_exists(operSummary, 'coolHP'),
    #                              is_running_data_exists(operSummary, 'coolABSChill'), is_running_data_exists(operSummary, 'coolEleChill'),
    #                              is_running_data_exists(operSummary, 'coolNGChill'), is_running_data_exists(operSummary,'fromCoolTank'))
    #
    # oper_data_ele = extract_result.get_running_data_ele(is_exists(operSummary, 'eleLoad'), is_exists(operSummary, 'eleEleBoil'),
    #                                      is_exists(operSummary, 'eleEleChill'),
    #                                      is_exists(operSummary, 'eleHP'), is_exists(operSummary, 'stoEleBat'),
    #                                      is_exists(operSummary, 'salEleGrid'),
    #                                      is_exists(operSummary, 'elePV'), is_exists(operSummary, 'totEleCHP'),
    #                                      is_exists(operSummary, 'fromEleBat'), is_exists(operSummary, 'purEleGrid'))
    #
    # oper_data_heat = extract_result.get_running_data_heat(is_exists(operSummary, 'heatLoad'), is_exists(operSummary, 'stoHeatTank'),
    #                                        is_exists(operSummary, 'heatSal'),is_exists(operSummary, 'heatABSChill'),
    #                                        is_exists(operSummary, 'heatNGBoil'), is_exists(operSummary, 'heatEleBoil'),
    #                                        is_exists(operSummary, 'totHeatCHP'), is_exists(operSummary, 'heatSolThem'),
    #                                        is_exists(operSummary, 'fromHeatTank'), is_exists(operSummary, 'heatInds'))
    #
    # oper_data_cool = extract_result.get_running_data_cool(is_exists(operSummary, 'coolLoad'), is_exists(operSummary, 'stoCoolTank'),
    #                                        is_exists(operSummary, 'coolHP'), is_exists(operSummary, 'coolABSChill'),
    #                                        is_exists(operSummary, 'coolEleChill'), is_exists(operSummary, 'coolNGChill'),
    #                                        is_exists(operSummary, 'fromCoolTank'))
    #
    # is_inserted = run4db.insert_results(86, processing_time, json.dumps(loads), json.dumps(capabilities), json.dumps(costs), json.dumps(oper_costs),
    #                       json.dumps(energy_cons), json.dumps(carbo_emission), json.dumps(resource_cons),
    #                       json.dumps(detailed_oper_costs), json.dumps(oper_data), json.dumps(oper_data_ele),
    #                       json.dumps(oper_data_heat), json.dumps(oper_data_cool))
    #
    # if is_inserted:
    #     email, user_name = run4db.get_email_name_by_project_id(86)
    #     email_util.send_success_email(email, "project result notice -- success", user_name, "Project_Name")


    #logging.debug("all_data", result["all_data"])

    #logging.debug("operation summary: {}".format(operSummary))
    #logging.debug("installation summary: {}".format(instSummary))
    #logging.debug("optimization summary:{}".format(optimSummary))

    # Organize the result.

    # ###################################################################### #
    #                                                                        #
    #    Plot figure                                                         #
    #                                                                        #
    #    Here we verify that the optimal solution computed by CPLEX (and     #
    #    the qpi[] values computed above) satisfy the KKT conditions.        #
    #                                                                        #
    # ###################################################################### #

    #plotFlag = data["ctrStr"]['plotFlag']

    # if plotFlag is None:
    #     plotFlag = 0
    #
    # if plotFlag == 1:
    #     print("Being drawing figures for the optimization results......................")
    #     plotOptDES.draw(pipeNetwFlag=pipeNetwFlag, currentFilePath=None, candidateTech=candidateTech, parTime=parTime,
    #                     parCHP=parCHP, parHP=parHP, parEleGrid=parEleGrid,
    #                     valBinInst=valBinInst, pipeHeatLoss=pipeHeatLoss, pipeCoolLoss=pipeCoolLoss,
    #                     nonzeroIndEle=nonzeroIndEle, nonzeroIndHeat=nonzeroIndHeat,
    #                     nonzeroIndCool=nonzeroIndCool, oriNonzeroIndEle=oriNonzeroIndEle,
    #                     oriNonzeroIndHeat=oriNonzeroIndHeat, oriNonzeroIndCool=oriNonzeroIndCool,
    #                     eleLoad=eleLoad, coolLoad=coolLoad, heatLoad=heatLoad, valBinInstTypeCHP=valBinInstTypeCHP,
    #                     valBinInstCHP=valBinInstCHP, mainPipeHeatLoss=mainPipeHeatLoss,
    #                     mainPipeCoolLoss=mainPipeCoolLoss, bldgHeat=bldgHeat, bldgCool=bldgCool,
    #                     parThemPipe=parThemPipe)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    start_time = time.time()
    run()
    logging.info("Running time after refactoring: {} seconds".format(time.time() - start_time))
    #print(run4db.get_email_by_project_id(86))

