# README #

**Optimal Planning and Operation of District Energy System (DEEP):**

> This software implements a Mixed integer linear programming (MILP) model to minimize the investment, operational, maintenance and environmental costs by determining the structure of the distributed energy system, i.e., selecting appropriate equipment, capacity of the adopted equipment, and the operating strategies. Supported technologies include Combined heat and power plants (CHP), photovoltaic systems (PV), small-scale wind turbines, back-up boiler, energy storage for heatting, cooling, and electricity, in additional to the grid.





## Getting Started

* These instructions will get you a copy of the project up and running on your local machine for production, development and testing purposes.

* Current version: 2.0.0

* Contact: Weifeng@lbl.gov


### File folders

There are two important folders residing in the project for now, all the code in *source*
folder and all the data in *data* folder, respectively.

- source: folder contains python scripts that implement the MILP. 
    
    There are three packages (please omit other packages for present) and one Python script
    * model: contains all the models, we have implemented the default model in module *deep_model.py*
    * solver: all the solvers will be here, but now we only support Gurobi, which is implemented in module *gurobi_solver.py*
    * test: some test modules in this package.
    * run.py: entrance of engine
    
- data: contains data.json and other folders; for the most users, they only take attention to the former one, 
namely, data.json, they need to configure the parameters according to their interests, while the other
folders contain some auxiliary data which will be used when running, please don not 
change these files unless you are familiar with them.

 

### How to run
1. To get started, you need to have Python 3.x running on your computer (Python 3.6 has been fully tested),
The easiest way is to use Anaconda Python distribution, which comes with a 
graphical installer (https://www.continuum.io/downloads). 

2. Install the latest dependent packages.
    * gurobipy
    * numpy
    * scipy
    * copy
    * logging
    
3. Install Gurobi 

    the latest Gurobi is recommended, Please head to Gurobi website(http://www.gurobi.com/) for more details.
Commerical license, free commercial evaluation license, or free university license is needed to 
run the model, however, the free online course license will not work because of its complexity of the model.  

4. Configure the model by specifying the parameters in *data.json*

5. Solve the model by executing the module run.py.

## Other guidelines

To use DEEP for technology planning, there are several design considerations:


   - Time horizon: how many distinct days and hours to be included. Examples are: 24 hours a day, 2 different days per month (accounting for weekday and weekend), and 12 months a year. This can be set in the parameter:

        ```
        itemParTime : {
                'numHour': 24,
                'numDay': 2,
                'numMonth': 12,
                'stepHr': 1,
                'longIntr': 0.15} 

        ``` 
        


   - Load types: there are three types of loads considered, namely, heating, cooling, and electricity loads. Note that the load file you provide should be able to match the specification of time horizon accordingly.  For the example data, in folder *data/data/building_loads*, they follow the specification above, and include loads of electricity, heating, cooling, hot water, and natural gas. 
	
	    * To include one type of load, e.g., HeatLoad, simply put:
`idxIniHeatLoad = 0` and for the argument of `deep_model.run()`, specify: 
`heatLoad=None`

        * To exclude one type of load, e.g., HeatLoad, specify:
`idxIniHeatLoad = 0 heatLoad = np.zeros((1,itemParTime['numMonth'] * itemParTime['numDay']*itemParTime['numHour']))`
and for the argument of `deep_model.run()` , specify: 
`heatLoad=heatLoad`

        * Note that the unit of the load is kW, and it is aggregated for the whole district. The percentage of each building in the district can be set using the parameter:  
`bldgHeatPercent = [0.2, 0.25, 0.1,0.15, 0.25, 0.05]` 
for heat load. You are free to specify the loads based on the profile of your building cluster. 

   - MIP Solver: you can specify the solver for the MIP problem. Currently only supported solvers Gurobi.	
Note that you can specify the solver with `mySolverInterface` in `ctrStr` parameter, and one of the items in the corresponding parenthesis as the `mySolver` argument (don not change it because only gurobi supported for present):

   ```
        ctrStr: {	…
                   mySolverInterface = 'gurobi'
                   mySolver = 'mps', 
                   mpsWriteFlag=1,
                   mpsGenMed='gurobi',
                    …
                   }
   ```
-	Technology to consider: there are a range of technologies that are implemented in DEEP, including centralized heat and cooling supplier, such as Combined Heat and Power (CHP), absorption chiller (ABSChill), PV generators, etc., and energy storage technologies, such as electricity battery (eleBat), heat tank (heatTank) etc., as well as the grid (eleGrid). You can specify their inclusion in the optimization by modifying the parameter:

```
    itemCandidateTech1 : {
                   'PV': 0,
                   'solThem': 0,
                   'NGBoil': 0,
                   'NGChill': 0,
                   'heatTank': 0,
                   'eleBat': 0,
                   'eleChill': 0,
                   'HP': 0,
                   'eleBoil': 0,
                   'ABSChill': 0,
                   'coolTank': 0,
                   'wind':1,}
 
    itemCandidateTech2 : {
                  'CHP': 0,
                   'eleGrid': 1,
                   'heatInds': 0,
                   'heatSal': 0,
                   'demandCharge': 0}
```

   where 1 means inclusion and 0 otherwise. Note that the inclusion indicates availability, and DEEP will optimize the objective of total cost to decide if this technology should be included or not in it’s optimal output.

-	Technology parameters: each technology, such as PV, comes with a set of parameters to specify its functionality and installation requirements. For instance,
```
    itemParPV = dict(fixIns=0, #  $/kW
                  varIns=3200, #  $/kW
                  unitOut=0.15, # kW/m^2, this needs to be confirmed
                  maxInstArea=2400000, #  m^2
                  minInstArea=smallM, #  m^2
                  perfRatio=0.75,
                  solYield=0.15)
```
where `varIns` denotes the capital cost of solar PV panels, $/kW, unitOut is the rated electricity generation per unit area of PV panel system, kW/m^2, perfRatio denotes the performance ratio / irradiation efficiency, solYield is the solar panel yield. You can feel free to specify the technology parameters. 

Note that some reasons for the problem being infeasible are related to technology parameters. For more details, please contact Dr. Wei Feng (weifeng@lbl.gov)
