Author: Ming Jin
# Parameter csv files instructions

- Technology parameters
- Method parameters
- Objectives
- Solver parameters

## Technology parameters:

### Technology to be considered: 1 / 0 means include / not include
- itemCandidateTech1
	- PV: 0,
	- solThem: 0, #solar thermal
	- NGBoil: 0, #natural gas boiler
	- NGChill: 0, #natural gas chiller
	- heatTank: 0, 
	- eleBat: 0, #electric battery
	- eleChill: 1, #electric chiller
	- HP: 1, $heat pump
	- eleBoil: 1,
	- ABSChill: 1, #absorption chiller
	- coolTank: 0,
	- wind:0
- itemLenLifeYr: associated life year for itemCandidateTech1

- itemCandidateTech2 
	- CHP: 1, #combined heat and power
	- eleGrid: 1,
	- heatInds: 0, #heat from industrial waster
	- heatSal: 1, #heat sale
	- demandCharge: 0

### Combined Heat and Power:
- itemParCHP: define the number & capacity of discrete CHP technologies. For CHP efficiency data see http://www.epa.gov/chp/basic/methods.html
	- varInst: [2500, 4000, 5000, 1500], # $/kW, variable installation cost
	- fixInst: [0.001,0.001,0.001,0.001], # $, 
	- capGen:  [20000, 10000, 15000, 23000], # rated capacity of CHP, kW
	- eleEff:  [0.4, 0.45, 0.5, 0.25], #electrical efficiency of CHP
	- heatEff: [1.2, 1.45, 1.5, 1], #factor relating electricity generation to heat supply from CHP
	- parLoad: [0.4, 0.45, 0.5, 0.3],
	- lifeYear:[20, 15, 10, 25],
	- numInst: [1,1,1,1],# maximum number of installed CHP for each type
	- numInv:   4,
	- heatSta: 0,# 'heatSta', 'coolSta', used to detect when CHP should be considered. When CHP is considered for heating purpose, heatSta =1
 	- coolSta: 0, # when CHP is used for cooling purpose, providing heating for ABSChiller, coolSta = 1
	- eleSta: 1,# when CHP is considered for electricity purpose, eleSta =1
	- unitGasPriceCHP: 0.0007  # gasPriceCHP: $/kWh
	- unitMtnPriceCHP: 0.036 #unit maintenance price, $35/kWe - $55 /kWe, average $40/kWe

### Heat Pump:
- itemParHP 
	- fixIns=smallM,
 	- varIns=500,
   	- heatCOP=3.24,
	- coolCOP=3.82,
	- eleEff=0.9,
 	- maxCap=435000, #  kW
	- minCap=smallM,
 	- outType=1, # could be '0,1,2', '1' means either 'coolSta' or 'heatSta' is 'ON'; '2' means both 'coolSta' and 'heatSta' are 'ON'
	- coolSta=1, # determine the cooling status of HP
  	- heatSta=1,# determine the heating status of HP
	- maxHeat=bigM, #  maximum heating generation from heat pump
	- minHeat=smallM, # minimum heating generation from heat pump
 	- maxCool=bigM, # maximum cooling generation from heat pump
 	- minCool=smallM, # minimum cooling generation from heat pump
	- maxEle=bigM, # maximum electricity consumption for heat pump, used for linearization of heat pump model,
     	- minEle=smallM, #  minimum electricity consumption for heat pump
	- unitMtnPriceHP: 0.01
                 
### Electric Battery:
- itemParEleBat:
	- fixIns: 295, #  $
	- varIns: 193, #  $/kWh
        - chgEff: 0.9,
        - disEff: 0.91,
	- decayFac: 0.001,
	- iniTot: 5000, #  kWh
	- iniSoc: 0.9,
	- maxFracChg: 0.37,
	- minFracChg: 0.02,
	- maxFracDis: 0.35,
	- minFracDis: 0.05,
	- maxSoc:1,
	- minSoc: 0.2,
	- unitOut:1,
	- maxCap: 18000.0, #  kWh
	- minCap: smallM, #  kWh
	- mode:'cycleCharge',#'priceFollow',
        - unitMtnPriceEleBat: 0.02

### Electric Boiler:
- itemParEleBoil
	- fixIns=1000,
	- varIns=200,
	- themEff=0.9,
	- maxInstCap=38000,
	- minInstCap=smallM,
	- maxTotGen=bigM

### Cool tank:
- itemParCoolTank:
	- fixIns=1000.0,
	- varIns=100,
	- chgEff=0.85,
	- disEff=0.75,
	- decayFac=0.01,
	- iniTot=18000,
	- iniSoc=0.75,
	- maxCap=155390 * 40, # kWh
	- minCap=smallM, # kWh
	- maxFracChg=0.166,
	- minFracChg=0,
	- maxFracDis=0.96,
	- minFracDis=0,
	- maxSoc=1,
	- minSoc=0,
	- unitOut=1, # this parameter is used to convert capacity from non-kwh to kwh
	- mode='cycleCharge'
	- unitMtnPriceCoolTank: 0.032

### Heat tank:
- itemParHeatTank
	- fixIns=100,
	- varIns=100,
	- chgEff=0.9,
	- disEff=0.8,
	- decayFac=0.01,
	- iniTot=10000,
	- iniSoc=0.85,
	- maxCap=16500 * 400, # m^3
	- minCap=smallM, # m^3
 	- maxFracChg=0.5,
 	- minFracChg=0,
	- maxFracDis=0.5,
  	- minFracDis=0,
  	- maxSoc=1,
 	- minSoc=0,
  	- unitOut=1, #kWh/m^3
  	- mode='cycleCharge',
        - unitMtnPriceHeatTank: 0.03

### Heat sale:
- itemParHeat
	- maxPur=bigM,
	- maxSal=bigM,
	- minPur=smallM,
	- minSal=smallM,
	- outType=1
	- unitPurPriceInds: 0.01 
	- unitSalPriceHeat: 0.01

### Natural gas boiler:
- itemParNGBoil
	- fixIns=1000,
       	- varIns=200,
 	- themEff=0.82,
   	- maxInstCap=80000,
    	- minInstCap=10,
  	- maxTotGen=10**6
	- unitGasPriceNGBoil = 0.0007	
	- unitMtnPriceNGBoil = 0.03

### Natural gas chiller:
- itemParNGChill
	- fixIns=1000,
      	- varIns=100,
   	- themEff=0.52,
    	- coolCOP=1.2,
       	- maxCap=80000,
    	- minCap=smallM
	- unitGasPriceNGChill: 0.0007
	- unitMtnPriceNGChill: 0.023

### Absorption chiller:
- itemParABSChill
	- fixIns: 2000,
	- varIns: 127,
	- coolCOP: 1.0,
	- maxCap: 450000, #  max cooling capacity, kW
	- minCap=smallM # min cooling capacity, kW
	- unitMtnPriceABSChill: 1.88 #unit maintain price

### Electric Chiller:
- itemParEleChill
	- fixIns=1000,
	- varIns=100,
	- coolCOP=5.47,
	- maxCap=150000,#  cooling capacity, kW
	- minCap=smallM

### Electric Grid:
- itemParEleGrid 
	- maxPur=bigM,#bigM
	- maxSal=bigM,
	- minPur=smallM,
	- minSal=smallM,
	- outType=1
	- sellBack: 1, control if the district can sell back to the grid	
	- sellPrice: 0.035, sell back price
- dayPurPriceEleGrid: 24 hours electricity price in $/kWh

### PV panels:
- daySolIrradiation: 24 hours irradiation in kW/m2
- itemParPV: #E_PV <= A_PV * perfRatio * solYield * unitOut
	- fixIns=0, #  $/kW
        - varIns=3200, #  capital cost of solar PV panels, $/kW
   	- unitOut=0.15, # rated electricity generation per unit area of PV panel system, kW/m^2
   	- maxInstArea=2400000, #  m^2
   	- minInstArea=smallM, #  m^2
     	- perfRatio=0.75, #performance ratio / irradiation efficiency
   	- solYield=0.15 #solar panel yield
	- unitMtnPrice = 0.025


### Solar Thermal
- itemParSolThem: #H_SolThem <= A_SolThem * irrEff * unitOut
	- fixIns=1000, #  $/kW
 	- varIns=500, #  capital cost of solar thermal, $/kW
    	- unitOut=0.10, # rated electricity generation per unit area of PV panel system, kW/m^2
 	- maxInstArea=1300, #  m^2
     	- minInstArea=500, #  m^2
	- irrEff=0.69
	- unitMtnPrice = 0.05

## Method parameters:
- ctrStr:
	- plotFlag: 1 or 0, control whether to plot results into figures
	- mpsWriteFlag: 1 or 0, write model to MyMPS.mps and read it. Always set to 1.
	- heatMode: sell
	- heatStoMode: cycleCharge
	- sampleTimeFlag: control whether the time horizon should be sampled based on the typical week and weekend days
	- CHPMode: heating
	- eleStoMode: cycleCharge
	- sparseProbFlag: 1 (default), control whether to use sparse matrix
	- mpsGenMed: gurobi or cplex. Methods to generate MPS for models.
	- HPMode: 2
	- mySolverInterface: gurobi or cplex. Solver selected for the problem.
	- gridMode: both
	- probReduceFlag: 
	- CHPCOP: linear, nonlinear (default)
	- CO2Mode: Y
	- coolStoMode: cycleCharge
	- taxCO2Flag: 0 (default) control whether to tax CO2
	- mySolver: mps (default)
- default_vals:
	- bigM: 1E6
	- smallM: 1E-6
- loadMethod:
	- loadFileFlag: 1 (read from data/ for total consumption), 2 (read from excel), 3 (for building clusters)
	- loadFileName: None (for loadFileFlag = 1 or 3) or typicalLoad.xlsx (for loadFileFlag = 2)	
	- idxInitHeatLoad: choose from which row to select the heating load in the excel when loadFile=1	
	- idxIniCoolLoad: choose from which row to select the cooling load in the excel when loadFile=1
	- idxIniEleLoad: choose from which row to select the electricity load in the excel when loadFile=1
	- incHeatLoad: 1 or 0 to include or not include heating load	
	- incCoolLoad: 1 or 0 to include or not include cooling load	
	- incEleLoad: 1 or 0 to include or not include electricity load

## Objectives:

### Time horizons:
- itemParTime
	- numHour: 24,
	- numDay: 30, # can be 2
	- numMonth: 12,
   	- stepHr: 1,
	- longIntr: 0.15

### CO2 emission (kgCO2/kWh)
- emiFac:
	- ele: 0.088,  #  carbon intensity of electricity from Grid
	- solThem: 0, # 0.022, carbon intensity of heat from solar thermal
        - PV: 0, #101 , carbon intensity of electricity from PV, kgCO2/m^2
        - natGas: 0.231, #  carbon intensity of heating and cooling from natural gas
        - heatTank: 0, #821.3, kgCO2/m^3
        - coolTank: 0, #821.3, kgCO2/m^3
- taxCO2:
	- ele: 1.023,  #  carbon intensity of electricity from Grid
        - solThem: 0.022, #  carbon intensity of heat from solar thermal
        - PV: 101, #  carbon intensity of electricity from PV, kgCO2/m^2
        - natGas: 0.13, #  carbon intensity of heating and cooling from natural gas
        - heatTank: 821.3, # kgCO2/m^3
        - coolTank: 821.3, # kgCO2/m^3

### Demand charge:
- rateMonthDemandCharge: 8.23

### Building networks:
- pipePar:
	- lossCalMode: loadFollow, #lengthFollow, calculate pipeline loss according to the delivered load or the lengh of the pipeline
	- pipeNetwFlag: 0 or 1, determine whether the pipeline network should be taken into account for thermal loss calculate	
	- numbldgCluster: 6 #number of buildings connected to the pipeline network
- parMainPipe:
	- lenMainSec: [44,39,15,61,40],# m : size is 1 * parMainPipe['numMainSec']
	- lenLossCoef:[0.1, 0.2,0.12,0.23,0.13], # size is 1 * parMainPipe['numMainSec']
	- heatLossCoef: [0.01, 0.02,0.012,0.023,0.013], # size is 1 * parMainPipe['numMainSec']
	- coolLossCoef:[0.015,0.014,0.013,0.016,0.021], # size is 1 * parMainPipe['numMainSec']
- parScndPipe
	- lenScndSec: [45, 10, 5, 100, 35, 11], #size is 1 * parScndPipe['numScndSec']
     	- lenLossCoef:[0.1, 0.2,0.12,0.23,0.13, 0.14], #size is 1 * parScndPipe['numScndSec']
     	- heatLossCoef: [0.015, 0.025,0.015,0.025,0.015, 0.014], #size is 1 * parScndPipe['numScndSec']
    	- coolLossCoef: [0.025,0.024,0.023,0.026,0.031,0.017] #size is 1 * parScndPipe['numScndSec']
- parThemPipe: this parameter is only needed when the pipeline network is not considered (pipeNetwFlag=0)
	- coolDistEff: 0.85 , # distribution efficiency of pipeline for cooling energy
	- heatDistEff': 0.9           
- bldgCoolPercent: ditribution of cool load in buildings, size is 1 * numbldgCluster
- bldgHeatPercent: distribution of heat load in buildings, size is 1 * numbldgCluster
- mainPipeLink: connection of buildings with primary pipeline section, size is parMainPipe['numMainSec']* numbldgCluster

## Solver parameters:
### Gurobi:
- gurCntr:
	- timeLimit: maximum time allowed for solving (seconds)
	- MIPGap: Duality gap tolerance (default 1e-4)
	- num_threads: number of threads employed (default 6)

### Cplex:
- cplexCntr:
	- strongit: MIP strong branching iterations limit. http://www.ibm.com/support/knowledgecenter/SSSA5P_12.6.3/ilog.odms.cplex.help/CPLEX/Parameters/topics/StrongItLim.html
	- relobjdiff: relative objective difference cutoff: 0 - 1. Default: 0.0. See: http://www.ibm.com/support/knowledgecenter/SSSA5P_12.6.3/ilog.odms.cplex.help/CPLEX/Parameters/topics/RelObjDif.html
	- itCnt: number of iterations limits
	- timelimit: control the running time of CPlex for solving the MIP problem (seconds)
	- threads: Specifies how CPLEX binds threads to cores
	- mipgap: Sets a relative tolerance on the gap between the best integer objective and the objective of the best node remaining.
	- objdiff: absolute objective difference cutoff. see: http://www.ibm.com/support/knowledgecenter/SSSA5P_12.6.3/ilog.odms.cplex.help/CPLEX/Parameters/topics/ObjDif.html
	- nodes: Sets the maximum number of nodes solved before the algorithm terminates without reaching optimality. See: http://www.ibm.com/support/knowledgecenter/SSSA5P_12.6.3/ilog.odms.cplex.help/CPLEX/Parameters/topics/NodeLim.html
	- parallel: 0 (default), Sets the parallel optimization mode. Possible modes are automatic, deterministic, and opportunistic.
	- display: Decides what CPLEX reports to the screen during mixed integer optimization (MIP). default: 2. See: http://www.ibm.com/support/knowledgecenter/SS9UKU_12.5.0/com.ibm.cplex.zos.help/Parameters/topics/MIPDisplay.html 
